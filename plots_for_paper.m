MY_VISCERAL_TMP = 'D:/MY_VISCERAL_TMP'; 

CBIR_INIT 
global dry_run CP      
dry_run =0                    ;    
MOTI = 0;
global CBIR_params_file CBIR_ip_params_file

fid = fopen(CBIR_ip_params_file);
buffer = fread(fid, [1 inf], '*uint8');
fclose(fid); 
CP = pb_read_CBIR_images_prms(buffer);
%% LUNGS SEPRATOR
% = = = = = = = = 
sbj_id = '10000112_1_CTce_ThAb';
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Nii') 
c_im =load_untouch2_nii(['D:\MY_VISCERAL_TMP\' sbj_id '.nii']);
c_im_res1= (c_im);

ix = PB_ip_shortcuts.get_index_by_image_name(sbj_id)

Kidney_Axial_upperBound_Slice_  = CP.ip(ix).Kidney_Axial_upperBound_Slice+15;
Kidney_Coronal_upperBound_Slice = CP.ip(ix).Kidney_Coronal_upperBound_Slice ;
 
%%
Trachea_Axial_upperBound_Slice_ = CP.ip(ix).Trachea_Axial_upperBound_Slice  
Lung_lowerBound_Slice_  = min ([ CP.ip(ix).Lung_Left_Axial_lowerBound_Slice ...
                                CP.ip(ix).Lung_Right_Axial_lowerBound_Slice])
Lung_Axial_Biggest_Slice_ = CP.ip(ix).Lung_Axial_Biggest_Slice
Bones_Axial_smalles_Slice_ = CP.ip(ix).Bones_Axial_smalles_Slice
Sagittal_mid_Slice_ = CP.ip(ix).Sagittal_mid_Slice
 
%% ALL SEPRTORS
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
img__ = zeros(size(c_im_res1));
%          
img__(:,:, ... 
     [Trachea_Axial_upperBound_Slice_-10:Trachea_Axial_upperBound_Slice_-8])=1;
img__(:,:,  ...  
     [Lung_lowerBound_Slice_-1:Lung_lowerBound_Slice_+1] )=3;
img__(:,:,  ...  
     [Lung_Axial_Biggest_Slice_-1:Lung_Axial_Biggest_Slice_+1] )=5;
img__(:,:,  ...    
     [Bones_Axial_smalles_Slice_-20:Bones_Axial_smalles_Slice_-18] )=7;
img__( Sagittal_mid_Slice_-1:Sagittal_mid_Slice_+1,  ...    
    :, ...
       : )=9; 
 
c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...
                  ['D:/MY_VISCERAL_TMP/SEPRTORS_ALL_BOOK_' sbj_id '.nii']);

              
%% LUNGS SEPRTORS
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
img__ = zeros(size(c_im_res1));
%          
img__(:,:, ... 
     [Trachea_Axial_upperBound_Slice_-10:Trachea_Axial_upperBound_Slice_-8])=1;
img__(:,:,  ...  
     [Lung_lowerBound_Slice_-1:Lung_lowerBound_Slice_+1] )=3;

c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...
                  ['D:/MY_VISCERAL_TMP/SEPRTORS_LUNGS_BOOK_' sbj_id '.nii']);

              
%% LUNGS BOUNDING BOX
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
img__ = zeros(size(c_im_res1));
%          
img__(:,:, ... 
     [Trachea_Axial_upperBound_Slice_-10:Trachea_Axial_upperBound_Slice_-8])=1;
img__(:,:,  ...  
     [Lung_lowerBound_Slice_-1:Lung_lowerBound_Slice_+1] )=3;
img__(1,:,  ...   
     [Lung_lowerBound_Slice_-1:Trachea_Axial_upperBound_Slice_-10] )=3;
img__(end,:,  ...  
     [Lung_lowerBound_Slice_-1:Trachea_Axial_upperBound_Slice_-10] )=3;

c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...
                  ['D:/MY_VISCERAL_TMP/SEPRTORS_LUNGS_BOOK_' sbj_id '.nii']);


%% RIGHT Kidney --  BOUNDING BOX
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
img__ = zeros(size(c_im_res1));
%          

img__(Sagittal_mid_Slice_:end,:,  ...  
     [Lung_Axial_Biggest_Slice_-1-15:Lung_Axial_Biggest_Slice_+1-15] )=3;
 
img__(Sagittal_mid_Slice_:end,:,  ...  
     [Bones_Axial_smalles_Slice_-1-15:Bones_Axial_smalles_Slice_+1-15] )=5;

img__( end,  ...    
    :, ...
       Bones_Axial_smalles_Slice_-15:Lung_Axial_Biggest_Slice_-15)=9; 
 
img__( Sagittal_mid_Slice_-1:Sagittal_mid_Slice_+1,  ...    
    :, ...
       Bones_Axial_smalles_Slice_-15:Lung_Axial_Biggest_Slice_-15)=9; 
 
c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...  
                  ['D:/MY_VISCERAL_TMP/Kidney_RIGHT_SEPRTORS_' sbj_id '.nii']);

%% LEFT LIVER --  BOUNDING BOX
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

img__ = zeros(size(c_im_res1));

% - - - - - - - - - - - - - - - - - 
%   TRIU 
% - - - - - - - - - - - - - - - - - 

load('D:\MY_VISCERAL_TMP\triu_mask_10000113_1_CTce_ThAb.nii.gz.mat')

dim3_ = size(mat2save,3);
trui_ = zeros(size(mat2save));
for ii=1:dim3_
    tt = squeeze(   mat2save(:,:,ii) );
    if (sum(tt(:))==0)
        continue
    end
    [Gmag, Gdir] = imgradient(tt,'prewitt');

     img__(:,:,ii) = Gdir;
end

%          

img__(:,:,  ...  
     [Lung_Axial_Biggest_Slice_-1-15:Lung_Axial_Biggest_Slice_+1-15] )=3;
 
img__(:,:,  ...  
     [Bones_Axial_smalles_Slice_-1-15:Bones_Axial_smalles_Slice_+1-15] )=5;
 
img__(1,:,  ...    
     [Bones_Axial_smalles_Slice_-1-15:Lung_Axial_Biggest_Slice_+1-15] )=3;
img__(end,:,  ...  
     [Bones_Axial_smalles_Slice_-1-15:Lung_Axial_Biggest_Slice_+1-15] )=3;

 
 



 
 
c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...  
                  ['D:/MY_VISCERAL_TMP/LIVER_SEPRTORS_' sbj_id '.nii']);
              
%% LEFT Kidney / Spleen --  SEPRTORS
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
img__ = zeros(size(c_im_res1));
%          

img__(1:Sagittal_mid_Slice_,:,  ...  
     [Lung_Axial_Biggest_Slice_-1-15:Lung_Axial_Biggest_Slice_+1-15] )=3;

img__(1:Sagittal_mid_Slice_,:,  ...  
     [Bones_Axial_smalles_Slice_-1-15:Bones_Axial_smalles_Slice_+1-15] )=5;
 
img__( Sagittal_mid_Slice_-1:Sagittal_mid_Slice_+1,  ...    
    :, ...
       Bones_Axial_smalles_Slice_-15:Lung_Axial_Biggest_Slice_-15)=9; 
img__( 1,  ...    
    :, ...
       Bones_Axial_smalles_Slice_-15:Lung_Axial_Biggest_Slice_-15)=9; 
  
c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...  
                  ['D:/MY_VISCERAL_TMP/Kidney_Spleen_SEPRTORS_' sbj_id '.nii']);

                   
              
%% BV_MEAN_
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c_im =load_untouch2_nii(['D:\MY_VISCERAL_TMP\' sbj_id '.nii']);
   ct_= (c_im);
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph');   
c_im =load_untouch2_nii(['D:\MY_VISCERAL_TMP\LUNGS_n_TRACHEA_' sbj_id '.nii']);
 %  LUNGS_n_TRACHEA= (c_im); 
LUNGS_n_TRACHEA      = simple_3D_SEmorph.run(c_im,'imclose',3,'disk',20,0);
  
ct_(LUNGS_n_TRACHEA==0)=0; 


save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (ct_), ...
                   ['D:/MY_VISCERAL_TMP/BV_MEAN_' sbj_id '.nii']);    
ct_(ct_<-50)=0;
ct_(ct_~=0)=1 ;              
LUNGS_n_TRACHEA(LUNGS_n_TRACHEA==1)= 25;
LUNGS_n_TRACHEA(ct_==1) = 50;

save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (ct_), ...
                  ['D:/MY_VISCERAL_TMP/BV_MEAN_1_' sbj_id '.nii']);    
save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (LUNGS_n_TRACHEA), ...
                  ['D:/MY_VISCERAL_TMP/BV_MEAN_2_' sbj_id '.nii']);                
%% 
img__ = zeros(size(c_im_res1));

img__(:,:, ... 
     [Trachea_Axial_upperBound_Slice_-10:Trachea_Axial_upperBound_Slice_-8])=1;

img__(:,:,  ...  
     [Lung_lowerBound_Slice_-1:Lung_lowerBound_Slice_+1] )=3;

img__(:,:,  ...  
     [Lung_Axial_Biggest_Slice_-1:Lung_Axial_Biggest_Slice_+1] )=5;
 
img__(:,:,  ...    
     [Bones_Axial_smalles_Slice_-20:Bones_Axial_smalles_Slice_-18] )=7;
 
img__( Sagittal_mid_Slice_-1:Sagittal_mid_Slice_+1,  ...    
    :, ...
       : )=9; 
 
c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...
                  ['D:/MY_VISCERAL_TMP/SEPRTORS_ALL_BOOK_' sbj_id '.nii']);

              
     
%%

%%
img__ = zeros(size(c_im_res1));
%          

img__(:,:, ... 
     [Trachea_Axial_upperBound_Slice_-10:Trachea_Axial_upperBound_Slice_-8])=1;

img__(:,:,  ...  
     [Lung_lowerBound_Slice_-1:Lung_lowerBound_Slice_+1] )=3;


 
c_im_res8=save_untouch2_nii(['D:/MY_VISCERAL_TMP/' sbj_id '.nii'], ...
                  (img__), ...
                  ['D:/MY_VISCERAL_TMP/SEPRTORS_ALL_BOOK_' sbj_id '.nii']);

              
              
% - - - - - - -  - - - - - - -   - - - - - - -   - - - - - - -   - - - - - -  
% - - - - - - -  - - - - - - -   - - - - - - -   - - - - - - -   - - - - - -                
% - - - - - - -  - - - - - - -   - - - - - - -   - - - - - - -   - - - - - -                
% - - - - - - -  - - - - - - -   - - - - - - -   - - - - - - -   - - - - - -                
% - - - - - - -  - - - - - - -   - - - - - - -   - - - - - - -   - - - - - -                
% - - - - - - -  - - - - - - -   - - - - - - -   - - - - - - -   - - - - - -                

%% RIGHT KIDNEY ROI
% = = = = = = = = = = = =

c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im = (c_im_res1);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res2=CBIR_SPLEEN_aux_func.Kidney_Coarse_ROI(c_im,0);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\BONE_SKELETON_10000100_1_CTce_ThAb.nii');
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res3= (c_im);
   c_im_res3=imfill(c_im_res3,18,'holes');
   c_im_res3=simple_3D_SEmorph.run(c_im_res3,'imdilate',3,'disk',3,0);
c_im = (c_im_res3);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res4=LogicalAssignment(c_im_res2,logical(c_im_res3),0);
c_im = (c_im_res4);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res5=threshold_image_BW(c_im,88,370);
   c_im_res5=LogicalAssignment(c_im_res1,~logical(c_im_res5),0);
c_im = (c_im_res5);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
c_im111 =load_untouch2_nii('D:\MY_VISCERAL_TMP\SPINAL_STRIP_10000100_1_CTce_ThAb.nii');

   c_im_res6=CBIR_LIVER_aux_func.segment_LIVER_triu(c_im111);
   c_im_res6_=LogicalAssignment(c_im,logical(c_im_res6),0);
   c_im_res7=largest_conncomp(c_im_res6_);
sing_sl179 = c_im_res7(:,:,179);
sing_sl180 = c_im_res7(:,:,180);
sing_sl179(sing_sl179>0)=2; 
sing_sl180(sing_sl180>0)=2; 
c_im_res7(:,:,179) = sing_sl179;
c_im_res7(:,:,180) = sing_sl180;
    c_im_res8=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',(c_im_res7),'D:/MY_VISCERAL_TMP/KIDNEY_R_FOR_PAPER_10000100_1_CTce_ThAb.nii');

    
%% LIVER ROI 
% = = = = = = = = = = = =

c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im = (c_im_res1); 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res2=CBIR_SPLEEN_aux_func.Kidney_Coarse_ROI(c_im,0);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\BONE_SKELETON_10000100_1_CTce_ThAb.nii');
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res3= (c_im);
   c_im_res3=imfill(c_im_res3,18,'holes');
   c_im_res3=simple_3D_SEmorph.run(c_im_res3,'imdilate',3,'disk',3,0);
c_im = (c_im_res3);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res4=LogicalAssignment(c_im_res2,logical(c_im_res3),0);
c_im = (c_im_res4);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res5=threshold_image_BW(c_im,80,140);
   c_im_res5=LogicalAssignment(c_im_res1,logical(c_im_res5),0);
c_im = (c_im_res5);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
c_im111 =load_untouch2_nii('D:\MY_VISCERAL_TMP\SPINAL_STRIP_10000100_1_CTce_ThAb.nii');

   c_im_res6=CBIR_LIVER_aux_func.segment_LIVER_triu(c_im111);
   c_im_res6_=LogicalAssignment(c_im,logical(c_im_res6),0);
   c_im_res7=largest_conncomp(c_im_res6_);
sing_sl179 = c_im_res7(:,:,179);
sing_sl180 = c_im_res7(:,:,180);
sing_sl179(sing_sl179>0)=2;
sing_sl180(sing_sl180>0)=2; 
c_im_res7(:,:,179) = sing_sl179;
c_im_res7(:,:,180) = sing_sl180;
    c_im_res8=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',(c_im_res7),'D:/MY_VISCERAL_TMP/LIVER_FOR_PAPER_10000100_1_CTce_ThAb.nii');


%% SPLEEN 
% = = = = = = = = = = = =
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im = (c_im_res1);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res2=CBIR_SPLEEN_aux_func.Kidney_Coarse_ROI(c_im,0);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\BONE_SKELETON_10000100_1_CTce_ThAb.nii');
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res3= (c_im);
   c_im_res3=imfill(c_im_res3,18,'holes');
   c_im_res3=simple_3D_SEmorph.run(c_im_res3,'imdilate',3,'disk',3,0);
c_im = (c_im_res3);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res4=LogicalAssignment(c_im_res2,logical(c_im_res3),0);
c_im = (c_im_res4); 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res5=threshold_image_BW(c_im,80,140); 
   c_im_res5=LogicalAssignment(c_im_res1,~logical(c_im_res5),0);
c_im = (c_im_res5);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
c_im111 =load_untouch2_nii('D:\MY_VISCERAL_TMP\SPINAL_STRIP_10000100_1_CTce_ThAb.nii');

   c_im_res6=CBIR_LIVER_aux_func.segment_LIVER_triu(c_im111);
   c_im_res6_=LogicalAssignment(c_im,~logical(c_im_res6),0);
   c_im_res7=largest_conncomp(c_im_res6_);
sing_sl233 = c_im_res7(:,:,233);
sing_sl234 = c_im_res7(:,:,234);
sing_sl233(sing_sl233>0)=2;
sing_sl234(sing_sl234>0)=2; 
c_im_res7(:,:,233) = sing_sl233;
c_im_res7(:,:,234) = sing_sl234;
    c_im_res8=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',(c_im_res7),'D:/MY_VISCERAL_TMP/SPLEEN_L_FOR_PAPER_10000100_1_CTce_ThAb.nii');


 

%% TRACHEA CAOURSE ROI
% = = = = = = = = = = = =

c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\LUNGS_n_TRACHEA_10000100_1_CTce_ThAb.nii');
   c_im_res2= (c_im);
c_im = (c_im_res2);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
global CP
Trachea_Axial_upperBound_Slice_ = 402; % CP.ip(1).Lung_Right_Axial_upperBound_Slice+1;
sing_sl = c_im(:,:,Trachea_Axial_upperBound_Slice_);
sing_sl(sing_sl>0)=2;
c_im(:,:,Trachea_Axial_upperBound_Slice_) = sing_sl;
c_im(:,:,Trachea_Axial_upperBound_Slice_-1) = sing_sl;

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
c_im_res6=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',c_im,'D:/MY_VISCERAL_TMP/TRACHEA_V2_FOR_PAPER_10000100_1_CTce_ThAb.nii');

%% TRACHEA CAOURSE ROI
% = = = = = = = = = = = =

c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\LUNGS_n_TRACHEA_10000100_1_CTce_ThAb.nii');
   c_im_res2= (c_im);
c_im = (c_im_res2);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
global CP

Trachea_Axial_upperBound_Slice_ = 295; % CP.ip(1).Lung_Right_Axial_upperBound_Slice+1;
sing_sl = c_im(:,:,Trachea_Axial_upperBound_Slice_);
sing_sl(sing_sl>0)=2; 
c_im(:,:,Trachea_Axial_upperBound_Slice_) = sing_sl;
c_im(:,:,Trachea_Axial_upperBound_Slice_-1) = sing_sl;

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
c_im_res6=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',c_im,'D:/MY_VISCERAL_TMP/LUNGS_FOR_PAPER_10000100_1_CTce_ThAb.nii');



%% TRIU   
% =======================================================
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\SPINAL_STRIP_10000100_1_CTce_ThAb.nii');
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res2= (c_im);
c_im = (c_im_res2); 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res3=CBIR_LIVER_aux_func.segment_LIVER_triu(c_im);
   c_im_res3=LogicalAssignment(c_im_res1,logical(c_im_res3),0);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\BODY_10000100_1_CTce_ThAb.nii');
   c_im_res4= (c_im);
c_im = (c_im_res4);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res5=LogicalOperators(c_im,'>',-10000,0);
   c_im_res5=LogicalAssignment(c_im_res5,logical(c_im_res3),1);
   c_im_res5=LogicalAssignment(c_im_res5,~logical(c_im_res4),0);
c_im = (c_im_res5);
   c_im_res6=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',c_im,'D:/MY_VISCERAL_TMP/PLOTS_FOR_PAPER10000100_1_CTce_ThAb.nii');
  
%% LUNGS -- The whitest and narrowest skeleton diameters 
% =======================================================
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\BODY_10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res2= (c_im);
c_im = (c_im_res2);   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res3=CBIR_SPLEEN_aux_func.Kidney_Coarse_ROI(c_im,1);
c_im = (c_im_res3);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res4=LogicalOperators(c_im,'>',-10000,0);
   c_im_res4=LogicalAssignment(c_im_res4,logical(c_im_res3),1); 
   c_im_res4=LogicalAssignment(c_im_res4,~logical(c_im_res1),0);
c_im = (c_im_res4);
   c_im_res5=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',c_im,'D:/MY_VISCERAL_TMP/lungs_PLOTS_FOR_PAPER10000100_1_CTce_ThAb.nii');

   
   
%% The whitest and narrowest skeleton diameters 
% =======================================================
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\BODY_10000100_1_CTce_ThAb.nii');
   c_im_res1= (c_im);
c_im =load_untouch2_nii('D:\MY_VISCERAL_TMP\10000100_1_CTce_ThAb.nii');
   c_im_res2= (c_im);
c_im = (c_im_res2);   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res3=CBIR_SPLEEN_aux_func.Kidney_Coarse_ROI(c_im,1);
c_im = (c_im_res3);
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   c_im_res4=LogicalOperators(c_im,'>',-10000,0);
   c_im_res4=LogicalAssignment(c_im_res4,logical(c_im_res3),1); 
   c_im_res4=LogicalAssignment(c_im_res4,~logical(c_im_res1),0);
c_im = (c_im_res4);
   c_im_res5=save_untouch2_nii('D:/MY_VISCERAL_TMP/10000100_1_CTce_ThAb.nii',c_im,'D:/MY_VISCERAL_TMP/PLOTS_FOR_PAPER10000100_1_CTce_ThAb.nii');
%        function [Kidney_Coarse_ROI2] = ...
%                     Kidney_Coarse_ROI(Coarse_ROI, is_left)
%             for_paper = 1;
% 
%             global CP  
%             Kidney_Coarse_ROI2 = Coarse_ROI;
%             ix = PB_ip_shortcuts.get_image_idx( );
% 
%             if (is_left)
%                 Lung_Axial_lowerBound_Slice_ = CP.ip(ix).Lung_Left_Axial_lowerBound_Slice; 
%             else
%                 Lung_Axial_lowerBound_Slice_ = CP.ip(ix).Lung_Right_Axial_lowerBound_Slice; 
%             end  
%             Bones_Axial_smalles_Slice = CP.ip(ix).Bones_Axial_smalles_Slice;
% 
%             Cornal_mid_Slice_ = CP.ip(ix).Cornal_mid_Slice; 
%             Sagittal_mid_Slice_= CP.ip(ix).Sagittal_mid_Slice; 
% 
%             % Set to zero all slices to the above
%             %  'Lung_Axial_lowerBound_Slice_'+20         
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~
%             if (is_left) 
%                 if (for_paper)
%                    % for paper
%                    % ---------  
%                    Kidney_Coarse_ROI2(:,:,[Lung_Axial_lowerBound_Slice_+40:end])=0;  
%                 else
%                    Kidney_Coarse_ROI2(:,:,[Lung_Axial_lowerBound_Slice_+20:end])=0;  
%                 end
%     %%%%             
% 
% 
%             else
%                 % just below the liver, so it's higher then the right kidney  
%                 Kidney_Coarse_ROI2(:,:,[Lung_Axial_lowerBound_Slice_:end])=0; 
%             end
%             Bones_Axial_smalles_Slice_ = Bones_Axial_smalles_Slice-40;
%             if (Bones_Axial_smalles_Slice_<1)
%                 Bones_Axial_smalles_Slice_ = 1; 
%             end 
% 
%             if (~for_paper)
%                    Kidney_Coarse_ROI2(:,:,[1:Bones_Axial_smalles_Slice_-40])=0;
%             else 
%                    % for paper
%                    % ---------
%                    Kidney_Coarse_ROI2(:,:,[1:Bones_Axial_smalles_Slice_+15])=0;
%             end
% 
% 
%             Kidney_Coarse_ROI2(:,1:Cornal_mid_Slice_-40,:)=0;
% 
%             if (~for_paper)
%                 if (is_left)     
%                   Kidney_Coarse_ROI2(Sagittal_mid_Slice_-20:end,:,:)=0;
%                 else   
%                   Kidney_Coarse_ROI2(1:Sagittal_mid_Slice_+20,:,:)=0;
%                 end
%             end
% 
% 
%        end   