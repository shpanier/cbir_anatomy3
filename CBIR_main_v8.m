% proto location - 
if (0)
%% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
    clc     
    copyfile('\\\\cifs\\spanier\\protoMatlab\\protobuf-2.4.1\\src\\*.m', ...
     'C:/Dropbox/MY_CODE/PHD/CBIR_anatomy3/protobuf') 
    copyfile('\\\\cifs\\spanier\\protoMatlab\\protobuf-2.4.1\\src\\*.proto', ...
     'C:/Dropbox/MY_CODE/PHD/CBIR_anatomy3/protobuf')  
end
      
if (0) 
%% = = = = = = = = = = = = = = = = = = = �= = = = = = = = = = = = = = = = = 
    path1 = getenv('PATH')
    path1 = [path1 ';C:/Dropbox/MY_CODE/PHD/CBIR_anatomy3/protobuf/']
    setenv('PATH', path1) 
    %% 
    clc
    cd('C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\protobuf') ; 
    status = system('copy  ..\VISCERAL_CBIR_params_Psubmit.pb . /Y'); 
    status = system('piqi.exe of-proto CBIRImagesPrms.proto');
    status = system('piqi.exe convert -f pb  -t xml --add-defaults --type  CBIRImagesPrms/CBIRImagesPrms VISCERAL_CBIR_images_prms3_Psubmit.pb AA.xml'); 
    cd('C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');
    %% 
    clc
    cd('C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\protobuf') ; 
    status = system('protoc  --python_out=. CBIRImagesPrms.proto');
 
end

    
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% * * At this point the 'CBIR_params_file' supposed to be fully with 
% * *     operations to perform... At this point we (1) save everything to file
% * *     (2) clean the workspace and (3) read the protocol buffers again  
% * *     (4) execute everything 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
global CBIR_params_file C 
    buffer = pblib_generic_serialize_to_string(C);
    fid = fopen(CBIR_params_file , 'w+');  
    fwrite(fid, buffer, 'uint8');   
    fclose(fid);      
    

     
CLEAR_EXCEPT   
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

%% init
% clc  
run(['C:\Dropbox\MY_CODE\lib\MATLAB\MY_LOCAL_MATLAB_SETTING.m']);
base_code_lib_dir = getenv( 'base_code_lib_dir');
base_code_dir  = getenv( 'base_code_dir');

% protobuf lib. 
addpath([base_code_dir '\lib\Misc\protobuf-matlab\protobuflib']);
% protobuf  
addpath([base_code_dir '\PHD\CBIR_anatomy3\protobuf']);  
 
addpath( [base_code_lib_dir '\plots_n_figs\imagine']);
addpath( [base_code_lib_dir '\plots_n_figs\cprintf']); 
CLEAR_EXCEPT  
  
close all
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');

%%  protocol buffer - CBIRImagesPrms
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =



% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% * * load the CBIR_params_file 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
global TMP_DATA_DIR  forceRecalculation
    fid = fopen(CBIR_params_file);
    buffer = fread(fid, [1 inf], '*uint8');
    fclose(fid);
    C = pb_read_CBIR(buffer); 
    clear buffer fid
    

    

    

%% more inits....
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% im_idx =1;    
imagine_str = [ 'imagine_v2( '];
              
clear_str = '';

 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%   VISCERAL_MODE is always forced
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
 
load_str_log = '';

% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% --- if new process... clear tamporary image files ---
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
if (~isempty(clear_str))
   % clear tamporary images
   % ~~~~~~~~~~  
   disp('% --- clear tamporary images --- ');
   disp(clear_str);
   if (~dry_run), eval(['clear ' clear_str]); ,  end
   clear_str = '';
end

sting_signature=''; 
hash_signature='';
fl_mat='';
fl_txt='';   
fl_txt_last='';

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% * * 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
all_skiped = 1; 



%% MAIN LOOP
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % MAIN LOOP
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
for im_idx = [1:length(C.im)]   
   if (~C.im(im_idx).Enabled), continue, end
    
   img_res = ['c_im_res' num2str(im_idx)];
     
   % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   % --- NEW IMAGE START .... ---
   %   load the first nii...
   % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   imageName =  C.im(im_idx).imageName;
   processDescription  = C.im(im_idx).processDescription;
   
   imageLoadFunc = C.im(im_idx).imageLoadFunc;
   imageLoadFunc_ = strtrim(imageLoadFunc); 
   imageLoadFuncPath =  C.im(im_idx).imageLoadFuncPath;
   DisplayResult = C.im(im_idx).DisplayResult;
   isSegmentation = C.im(im_idx).isSegmentation;
   change_curr_image_name = C.im(im_idx).change_curr_image_name;
   documantaion = C.im(im_idx).documantaion     ;
    
   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
   % once forced is turn on, all consecutive also forced....
   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
   if (forceRecalculation == 0) 
        forceRecalculation  = C.im(im_idx).forceRecalculation;
   end   
   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   
   
   % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   % * *  IMAGINE STRING  
   % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   if (DisplayResult==1) 
                 %  STOP_HERE();    
 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
        %  IMAGINE - WITH -  segmentaion... (as a second parameter) 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
        segmentaion_holder = '[]'; 

        if (isSegmentation)
            k = strfind(imagine_str, segmentaion_holder);
            try  
                k = k(end);
                if (length(k) >1)
                      error(['ambiguse more then one segmentaion_holder ' segmentaion_holder]) ;
                end
                imagine_str = [imagine_str(1:k-1) ' '  img_res ' ' imagine_str(k+2:end)];
            catch
             error('imagine2_str ') 
            end 
       % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
       %  IMAGINE  - WITHOUT -   segmentaion... (as a second parameter) 
       % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
       else
            imagine_str = [ imagine_str , img_res ', ' segmentaion_holder ' ,''' img_res '''  ,'   ];
       end
       clearvars_except_str = [clearvars_except_str ' ' img_res];
   else  
       
       % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
       % keep even after "imagine"
       % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
       if (DisplayResult>2)   
             clearvars_except_str = [clearvars_except_str ' ' img_res]; 
       end
   end
    
   
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *
    % * *          Detect if a procedure * already exists  * 
    % * *              i.e. hased-mat file-name exists
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     if (~dry_run),   
        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
        %   hash_signature -  
        %   newest_dependent_time -  ==>  SEE fucntio dis
        %   sting_signature - 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
        [ hash_signature , newest_dependent_time , sting_signature] = ...
                            PB.get_hashCode_by_node_idx2(im_idx);
        
         
        
        disp('= = = = = = = = = new process, hash_signature  = = = = = = ');
        if ~isempty(documantaion)  
            disp( ['% ' documantaion ] );
            % disp( ['% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~' ] ); 
        end   
        disp(hash_signature);
        global VISCERAL_OUT_DIR

        fl_mat = [ TMP_DATA_DIR '/' hash_signature '.mat'];
        fl_txt = [  TMP_DATA_DIR '/' hash_signature '.txt'];
        
        %  type(fl_txt);   type(fl_txt1);     
        %  STOP_HERE();   
        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
        % once  one of the file is loaded no more checks is needed we calculate
        % from now on....
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
        if (all_skiped==1) 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
        if (exist(fl_mat, 'file') && exist(fl_txt, 'file'))
 
            sting_signature_last = sting_signature; 
             hash_signature_last = hash_signature;
             fl_mat_last = fl_mat;
             fl_txt_last = fl_txt;     
   
    
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
             %  when the 'forceRecalculation' flag is turn on. 
             %    recalucate (i.e. not loading pre-existing calculation..)
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
 
             if (forceRecalculation)
                disp('Process found, FORCE-Recalculation...');
                
                
     
             else 
                 eval(['curr_f_time = GetFileTime(''' fl_mat ''' , [],''Write'');'])                 %   break
                 new_file_exists_in_the_pipeline = 0;
                 % one of the files in Process is newer/modified. Because of thisThe regal
                 % collation of the all Processes need
                 if (any(curr_f_time <  newest_dependent_time))
                         new_file_exists_in_the_pipeline = 1;
                        STOP_HERE(); 
                 end 
                   
                 if (new_file_exists_in_the_pipeline) 
                   disp('Process found, BUT depending on an newer existing file: RE-calculating...'); 
                 else  
                     cprintf([0,0,1] , '# # #~ Process found, SKIP... # # # #'); disp(' ');
                     try
                         type(fl_txt);
                     end 
                     continue 
%                      try 
% % %                         XX= load(fl_mat);
% % %                         % eval( [img_res '=XX.saved_im;']);
%                         continue, 
%                      catch 
%                         warning('failed loading...');  
%                      end
                 end
             end 
         else  
             disp('Process NOT found, calculating...'); 
%               STOP_HERE();
%               eee=1;   
         end 
          
         if (~isempty(fl_txt_last)) 
             disp('loading LAST process before continuing...'); 
             disp('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');  
             all_skiped = 0; 
             disp(fl_txt_last);
             load(fl_mat_last);
         end   
  
         end  % END OF (all_skiped==1) 
    end   
     
     
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *           Load the fist image
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    addpath(imageLoadFuncPath);
    load_str =['c_im =' imageLoadFunc '(' imageName ');'];

    disp(load_str);
    % STOP_HERE(); 
    load_str_log =  strvcat(load_str_log ,  load_str); 
  
    if (~dry_run), 
         eval(load_str);   
    end
    

    %% Processing
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *
    % * *           Processing
    % * *
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    p_idx = 1; % The process index... 
     
        
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     % ~~~ process documantion... 
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     if ~isempty(documantaion)  
        doc_str =  ['% '  ];
        load_str_log =  strvcat(load_str_log , doc_str);
        doc_str =  ['% ' documantaion ];
        load_str_log =  strvcat(load_str_log , doc_str);
        doc_str =  ['% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~' ];
        load_str_log =  strvcat(load_str_log , doc_str);
  
        disp( doc_str ) 
     end  
    for p_idx=[1:length( C.im(im_idx).P )]
    
        if (~C.im(im_idx).P(p_idx).Enabled), continue, end
         
        MethodFilesLocation = C.im(im_idx).P(p_idx).MethodFilesLocation;
        twoDMethod = C.im(im_idx).P(p_idx).twoDMethod;
        addpath(MethodFilesLocation);

        MethodName = C.im(im_idx).P(p_idx).MethodName;

        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % ~~~ Set the function name & the return value.
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        eval_str = [ img_res '=' ...
             MethodName '(' ] ; 
        
       
        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % ~~~ build/extrct the list of function's paramters
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
       
        for prm_idx=[1:length( C.im(im_idx).P(p_idx).prms )]
            pv = C.im(im_idx).P(p_idx).prms(prm_idx).prm_value;
            eval_str = [eval_str pv ];

            if (prm_idx < length( C.im(im_idx).P(p_idx).prms))
                 eval_str = [eval_str ','];
            end

        end
         
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % ~~~ terminate function string.
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        eval_str = [eval_str ');'];
        disp(['   ' eval_str]);
           
 
        % ~~~~~~~~~~~ command log.. 
        load_str_log =  strvcat(load_str_log , ['  ' eval_str]);
 
        % load_str_log  
        if (~dry_run)  
             eval(eval_str);    
          
            %fprintf(fileID, '\t%s\r\n', eval_str);
        end
 
    end 

    %% save the result. (into hash filename)
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *
    % * *           save the result. (into hash filename)
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    if (~dry_run),       
        cprintf([1,0,0] , '~ ~ ~ start, saving into Hash ~ ~ ~ ~'); disp(' ');
        cprintf([0,1,0] , [' save the result. (into hash filename):' ]);
        disp(' '); 
        disp( load_str_log );   
        cprintf([1,0,0] , '~ ~ ~ end, saving into Hash~ ~ ~ ~'); disp(' ');
        save( fl_mat, '-regexp', 'c_im_res\d\d?');
      
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % txt-file contain the function called of the saved proces (Maily for debuging)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        fileID = fopen(fl_txt,'w'); 
        for line_i = 1:size(load_str_log,1)
            fprintf(fileID, '%s\r\n', load_str_log(line_i,:));
        end
        fclose(fileID);  

        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % txt-file contain the hash signature (Maily for debuging)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        fileID1 = fopen([fl_txt '_ss.txt'],'w');  
        fprintf(fileID1, 'SIG: ''%s''\r\n', sting_signature) ;  
        Opt.Method = 'SHA-512';      
        fprintf(fileID1, 'HASH: ''%s''\r\n', DataHash_20120627(sting_signature,Opt));
            
        fclose(fileID1);
    end 
    
end


if (all_skiped) 
    cprintf([0,0,1] , ' ALL SKIPEd LOAD THE LAST ONE !!'); disp(' ');
    cprintf([0,0,1] , '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'); disp(' ');
    disp(fl_txt);
    type(fl_txt);
   
%     STOP_HERE();
%     eee=1;        
  if (~dry_run), 
    load(fl_mat);
  end     
      
else         
    %% <> SAVE PB only if some process was done....
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %             addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
    %          STOP_HERE();
    if (~dry_run),
         
        % global CP 
        try
            buffer = pblib_generic_serialize_to_string(CP);
            fid1 = fopen(CBIR_ip_params_file , 'w+');
            fwrite(fid1, buffer, 'uint8');
            fclose(fid1); 
        end 

    end
end 

%% images_for_global_imagine
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% * *
% * *         create the images_for_global_imagine !!
% * * 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   
% trminate imagine_str
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
if (imagine_str(end) == ','), imagine_str(end) = ' '; , end
imagine_str = [imagine_str ');'];
disp(' ');
disp(imagine_str);   
disp(clearvars_except_str);
  
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2');
  
global curr_image_name file2save_imagine
if (~dry_run), 
    
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  * *   START images_for_global_imagine
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  save images_for_global_imagine
    %    search all the image name in  'imagine_str' and for every image
    %    saved it into Dedicated mat file (file2save_imagine)
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   % 
    imagine_str2 = imagine_str;
    mats_2_save = []  ; 
    
    % Regular expression pattern that match the image name
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    [tokens,matches] = regexp(imagine_str,'([^'']c_im_res\d\d?)');
    for im2save_idx=1:length(tokens)
        
        % save the images into the dedicated mat file. Also give a
        % unique global name ( to avoid duplicated  image name between
        % runs)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        
        % Convert the local 'imagine_str' string into a global 'imagine_str' 
        % By renaming the string unique global name ( this is the same image name 
        % as been saved into the .mat file)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

        mat_image_2_save = imagine_str(tokens(im2save_idx)+1:matches(im2save_idx));
        new_mat_image_2_save =  ['im_' curr_image_name '_' mat_image_2_save];
         
        eval([new_mat_image_2_save '=' mat_image_2_save ';']);  
        %STOP_HERE()    
         
        % unique global name for the image  
        imagine_str2 = regexprep(imagine_str2,mat_image_2_save,new_mat_image_2_save,'once');   
        % unique global image string title
        imagine_str2 = regexprep(imagine_str2,['''' mat_image_2_save ''''], ...
            ['''' new_mat_image_2_save ''''],'once');   

        %  imagine_str2
        % after the first, only append.
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        try   
            eval(['save(''' file2save_imagine ''', '''  ...
                                    new_mat_image_2_save ''', ''-append'' )' ]);
            
        % first time 'create the file' 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        catch      
          %  STOP_HERE();     
             imagine_global_str = [];
            eval(['save(''' file2save_imagine ''', ''' ...
                                    new_mat_image_2_save ''', ''imagine_global_str'')' ]) ;
           %    save( file2save_imagine , new_mat_image_2_save , 'imagine_global_str') ;
        end 
    end 
   
    
    %
    % If 'file2save_imagine' does not exist hat in the current process, no
    % image is then asked display... is no need to save a global 'imagine_str'
    %
    %  file2save_imagine - is being set in 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
    if (exist(file2save_imagine, 'file') )
         
        % conctnate into the global image string.
        
        eval(['load(''' file2save_imagine ''', ''imagine_global_str'');']);
        imagine_str2 = imagine_str2(12:end)  ;   % remove leading  'imagine_v2('
        imagine_str2 = imagine_str2(1:end-2) ; % remove trailing ');'
        if (isempty(imagine_global_str)  )
            imagine_global_str = imagine_str2; 
        else 
            if (length(imagine_str2)>1) 
                imagine_global_str = [imagine_global_str ', ' imagine_str2 ]
               
 
            end
        end  
            eval(['save(''' file2save_imagine ''', ''imagine_global_str'', ''-append'' );' ]);
    end 
     
    %  END images_for_global_imagine
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    eval(clearvars_except_str); 
   
end  
% % 
% % % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % * * After everything was run....  
% % % * *     Save the CBIR_ip_params_file 
% % % * *    (Hold information about mean, gray level etc' ) 
% % % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

% STOP_HERE() 
%%
buffer = pblib_generic_serialize_to_string(CP);
fid1 = fopen(CBIR_ip_params_file , 'w+');
fwrite(fid1, buffer, 'uint8');
fclose(fid1);  
% %       
%% 
% % % 
% % % fl = 'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\owner-PC_pipline_paramters.pb'
% % % buffer = pblib_generic_serialize_to_string(CP);
% % % fid1 = fopen(fl , 'w+');
% % % fwrite(fid1, buffer, 'uint8');
% % % fclose(fid1); 
% % % %%
% % % global CP
% % % for ix=1:33
% % %     ix
% % %     pblib_set(CP.ip(ix), 'prm109', num2str(0) ); 
% % %     CP.ip(ix) = pblib_set(CP.ip(ix), 'Liver_Axial_in_Slice', ix );   
% % % end  
%%    
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% * *  dry_run --> stop for debuging...    
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
if (dry_run)
   addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
    STOP_HERE(); 
end 



