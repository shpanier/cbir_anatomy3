%% init
% update 03 2013

addpath('C:\Users\as\Documents\CODE\lib\MATLAB\plots_n_figs\tightfig');

%% Load Pre-Liver se
global subj_id
subj_id
load(['subject_' num2str(subj_id) '.mat']);
 
 
% liver threshold
% ~~~~
i_m33_adjust1 =i_m33_adjust;

% Remove kidny & Heart
% ~~~~
i_m33_adjust1(liver_mask_w_kidny)=0;

% remove the pixel which has low gray-level then the majorty
% ~~~~
TBL = tabulate(i_m33_adjust1(i_m33_adjust1>0));
[cc,II ] = sort(TBL(:,2), 'descend');
liver_tr = mean(TBL(II(2:4),1))- 1*(std(TBL(II(2:4),1)));

liver_img = i_m33_adjust1;
liver_img(liver_img<liver_tr)  = 0 ;
%liver_img(liver_img>=liver_tr) = 1;


% Use opening, to remove some noise
% ~~~~
liver_img1 =liver_img;
SE00 = strel('square', 2)

for ii=1:size(liver_img1,3)
        t_im = liver_img1(:,:,ii);
        t_im = imopen(t_im,SE00);
        liver_img1(:,:,ii)  = t_im;
end

liver_img1(liver_img1>=0.98)=0;


%% find CC with more then 90%
liver_mask_ez1 = zeros(size(liver_img1));
slices_w_90percent = (zeros(1,size(liver_img1,3)));

for ii=1:size(liver_img1,3)
        dt = liver_img1(:,:,ii); 
        CC = bwconncomp(dt);
        cc2imshow = zeros(size(dt));

        numPixels = cellfun(@numel,CC.PixelIdxList);
        [B,IX] = sort(numPixels,'descend');
        c1 = 1; c2 = 2; c3 = 3; c4 = 4;  c5 = 5;  c6 = 6;
        try cc2imshow(CC.PixelIdxList{IX(1)}) = c1; end
        try cc2imshow(CC.PixelIdxList{IX(2)}) = c2; end
        try cc2imshow(CC.PixelIdxList{IX(3)}) = c3; end
        try cc2imshow(CC.PixelIdxList{IX(4)}) = c4; end

        TB = tabulate(cc2imshow((cc2imshow(:)>0)));
        title(['CC, c1: ' num2str(TB(1,3)) ]);
        slices_percents(ii) =  str2num(num2str(TB(1,3),'%2.3e'));
        if (TB(1,3) > 90)
            
            try cc2imshow(CC.PixelIdxList{IX(2)}) = 0; end
            try cc2imshow(CC.PixelIdxList{IX(3)}) = 0; end
            try cc2imshow(CC.PixelIdxList{IX(4)}) = 0; end
       else
            cc2imshow = bwlabeln(dt);
        end
        
       liver_mask_ez1(:,:,ii) =   cc2imshow;
    
end
%% find slice in liver
slices_w_90percent = (slices_percents>90);
CC1 = bwconncomp(slices_w_90percent);
[a,b] = max(cellfun(@numel,CC1.PixelIdxList));
[nm,mx] = max(cellfun(@numel,CC1.PixelIdxList));
slice_in_liver = round(mean(cell2mat(CC1.PixelIdxList(mx))))


%%
% the upper part!
liver_mask_ez2 = zeros(size(liver_img1));
if ~any(slices_w_90percent)
   warning('No 90% Slices....');
   return 
end
for ii=slice_in_liver:size(liver_img1,3)
    if (slices_w_90percent(ii))
            liver_mask_ez2(:,:,ii) = liver_mask_ez1(:,:,ii);
    else
        if(slices_w_90percent(ii-1))
            liver_mask_ez2(:,:,ii) = ...
                (liver_mask_ez1(:,:,ii)).*(logical(liver_mask_ez2(:,:,ii-1)));
            
            slices_w_90percent(ii) = 1;
        
        end
    end
end 
%%
for ii=slice_in_liver:-1:1
    if (slices_w_90percent(ii))
            liver_mask_ez2(:,:,ii) = liver_mask_ez1(:,:,ii);
    else
        if(slices_w_90percent(ii+1))
            liver_mask_ez2(:,:,ii) = ...
                (liver_mask_ez1(:,:,ii)).*(logical(liver_mask_ez2(:,:,ii+1)));
            
            slices_w_90percent(ii) = 1;
        
        end
    end
end 

%% 
% for kk=1:size(liver_img1,3)
%     liver_mask_ez2(:,:,kk) = rot90(liver_mask_ez2(:,:,kk));
% end
% %% debug
% imshow((liver_mask_ez1(:,:,ii)))
% %%
% imshow((liver_mask_ez2(:,:,ii-1)))
% %% 
% imshow(liver_mask_ez1(:,:,ii).*logical(liver_mask_ez2(:,:,ii-1)))

%%
addpath('C:\Users\as\Documents\CODE\lib\MATLAB\imp\Nii');

my_niii = load_untouch_nii( ['liver_mask_' subj_id '.nii'] );

my_niii.img = liver_mask_ez2; 
save_untouch_nii(my_niii, ['new_liver_mask__' subj_id '.nii']);

%% debug
kk = 30; 
imshow((liver_mask_ez2(:,:,kk)))

%%
return 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% G-G et al
clc
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\clustering\FuzzyClusteringToolbox_m\FUZZCLUST');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ii = sl;
[row,col] = find(curr_sl >0); [inds] = find(curr_sl >0);
%DATA = [row,col]; 
DATA = [row,col,curr_sl(inds)];
DATA = double(DATA);
%
% ~~~~~~~~~~ run GG ~~~~~~~~~~~~~~~~~~~~~~~~~~~

data.X = DATA; 
num_of_cluster = 3;
%parameters
param.c=num_of_cluster; param.m=2; param.e=1e-4; param.vis=0;  param.val=1; 
%normalization
data=clust_normalize(data,'range');  
fuzzy_failed = 0;
%try  
  %  result = FCMclust(data,param);
   %  param.c=result.data.f; %initializing with the results of C-means
    result = GGclust(data,param); 
    result = validity(result,data,param);
    U = result.data.f';
    maxU = max(U);
%catch    fuzzy_failed = 1; end
fuzzy_failed
%
cc2imshow33 = zeros(size(curr_sl));
imshow_2DGG = zeros(size(curr_sl));

if (~fuzzy_failed)
    for ii=1:3
        idx = find(U(ii,:) == maxU);
        ix = sub2ind((size(curr_sl)), DATA(idx,1) , DATA(idx,2)  );
        cc2imshow33(ix) = ii*0.3;
        imshow_2DGG(ix) = ii;
    end
end
imshow(cc2imshow33); 
%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
fuzzy_failed1 = 1;
% data.X = DATA1; 
% num_of_cluster = 3 ;
% %parameters
% param.c=num_of_cluster; param.m=2; param.e=1e-4; param.vis=0;  param.val=1; 
% %normalization
% data=clust_normalize(data,'range');  
% fuzzy_failed1 = 0;
% try
%     result = FCMclust(data,param);
%     param.c=result.data.f; %initializing with the results of C-means
%     result = GGclust(data,param); 
%     result = validity(result,data,param);
%     U1 = result.data.f';
%     maxU1 = max(U1);
% catch    fuzzy_failed1 = 1; end
end

%figure
clf
    % plot orig
    % ~~~~~~~~~
     subplot(4,2,1); 
            hold on
            title([' --> '  subj_id ' orig, slice: ' num2str(ii)]);
            imshow(org_im);
            
     % input for GG
     % ~~~~~~~~~
     subplot(4,2,2); 
            hold on
            title('Input, for GG');
            dt = rot90(dt);
            imshow(dt);
            
    %  GG output 1 (2D)
    % ~~~~~~~~~  
   %
    subplot(4,2,3);
            title(['2D --- OUFC ' num2str(num_of_cluster)]);
            hold on; 
            cc2imshow33 = zeros(512,512);
            imshow_2DGG = zeros(512,512);

            if (~fuzzy_failed)
                for ii=1:3
                    idx = find(U(ii,:) == maxU);
                    ix = sub2ind((size(dt)), DATA(idx,1) , DATA(idx,2)  );
                    cc2imshow33(ix) = ii*0.3;
                    imshow_2DGG(ix) = ii;
                end
            end
            imshow_2DGG = rot90(imshow_2DGG);
            cc2imshow33 = rot90(cc2imshow33);
            imshow(cc2imshow33); 
            clear cc2imshow33
            
            
           

   %  GG output 2 (3D + color)
    % ~~~~~~~~~        
    subplot(4,2,4);
        hold on;
        title(['2D --- OUFC, cluster1+2 ']);
        try
            [B1,IX1] = Extrema_3regionprops(imshow_2DGG);

             imshow_2DGG(imshow_2DGG == IX1(3))=0;
             imshow_2DGG(imshow_2DGG>0)=1;
             imshow(imshow_2DGG);   
        end
    % 
    %  conv hull
    subplot(4,2,5); 
    % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        

            CH_objects = bwconvhull(dt,'objects');
            hold on
%             title('Objects Convex Hull');
%             imshow(CH_objects);
            CC = bwconncomp(CH_objects)
            cc2imshow = zeros(size(dt));
            numPixels = cellfun(@numel,CC.PixelIdxList);
            [B,IX] = sort(numPixels,'descend');
            try cc2imshow(CC.PixelIdxList{IX(1)}) = 0.9; end
            try cc2imshow(CC.PixelIdxList{IX(2)}) = 0.8; end
            try cc2imshow(CC.PixelIdxList{IX(3)}) = 0.7; end
            try cc2imshow(CC.PixelIdxList{IX(4)}) = 0.6; end
            try cc2imshow(CC.PixelIdxList{IX(5)}) = 0.5; end
            try cc2imshow(CC.PixelIdxList{IX(6)}) = 0.4; end
            try cc2imshow(CC.PixelIdxList{IX(7)}) = 0.3; end
            try cc2imshow(CC.PixelIdxList{IX(8)}) = 0.2; end
            try cc2imshow(CC.PixelIdxList{IX(9)}) = 0.1; end
            hold on 
            
            title('Convex Hull');
            imshow(cc2imshow);
    
    %  (CH-1) conjunction with (GG-input)'
   % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        
    subplot(4,2,6); 
            CC = bwconncomp(CH_objects)
            cc2imshow = zeros(size(dt));
            numPixels = cellfun(@numel,CC.PixelIdxList);
            [B,IX] = sort(numPixels,'descend');
            try cc2imshow(CC.PixelIdxList{IX(1)}) = 0.9; end
            hold on;
            title('(CH-1) conjunction with (GG-input)' );
            cc2imshow = cc2imshow.*dt;
        
            imshow(cc2imshow);
    
    %  CC
    % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~         
    subplot(4,2,7); 
             
            CC = bwconncomp(dt)
            cc2imshow = zeros(size(dt));
            cc2imshow_dis = zeros(size(dt));
            numPixels = cellfun(@numel,CC.PixelIdxList);
            [B,IX] = sort(numPixels,'descend');
            c1 = 0.9; c2 = 0.7; c3 = 0.5; c4 = 0.3;
            try cc2imshow_dis(CC.PixelIdxList{IX(1)}) = c1; end
            try cc2imshow_dis(CC.PixelIdxList{IX(2)}) = c2; end
            try cc2imshow_dis(CC.PixelIdxList{IX(3)}) = c3; end
            try cc2imshow_dis(CC.PixelIdxList{IX(4)}) = c4; end
            c1 = 1; c2 = 2; c3 = 3; c4 = 4;
            try cc2imshow(CC.PixelIdxList{IX(1)}) = c1; end
            try cc2imshow(CC.PixelIdxList{IX(2)}) = c2; end
            try cc2imshow(CC.PixelIdxList{IX(3)}) = c3; end
            try cc2imshow(CC.PixelIdxList{IX(4)}) = c4; end
            hold on
            title('CC');
            
            
           TB = tabulate(cc2imshow((cc2imshow(:)>0)));
           title(['CC, c1: ' num2str(TB(1,3)) ]);
           if (TB(1,3) > 90)
            try cc2imshow_dis(CC.PixelIdxList{IX(2)}) = 0; end
            try cc2imshow_dis(CC.PixelIdxList{IX(3)}) = 0; end
            try cc2imshow_dis(CC.PixelIdxList{IX(4)}) = 0; end
           end
           imshow(cc2imshow_dis);
           clear cc2imshow_dis
           % legend(['1','2','3','4']);
           
    %  (2D-GG) conjunction with ( CC-1+2 )
    subplot(4,2,8); 
    % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         [B2,IX2] = Extrema_3regionprops(cc2imshow);

         % Extract 2 left-bottom cluster from 2D-GG  
         
          cc2imshow(cc2imshow == IX2(3))=0;
          cc2imshow(cc2imshow == IX2(4))=0;

%          imshow_2DGG(imshow_2DGG>0)=1;

%          
         if (~fuzzy_failed)
            % first & second Connected Componets

%             cc2imshow(cc2imshow == IX2(1))=0;
%             cc2imshow(cc2imshow == IX2(4))=0;

            cc2imshow(cc2imshow >0)=1;
%             cc2imshow(cc2imshow >= c2) = 1;
%             cc2imshow(cc2imshow < c2) = 0;
            
            %cc2imshow2 = rot90(cc2imshow2,-1);


         
                %imshow_2DGG = rot90(imshow_2DGG);
                cc2imshow2 = cc2imshow; % cc2imshow.*imshow_2DGG;
                cc2imshow2 = cc2imshow.*imshow_2DGG;
          
            hold on
            title('(CC-1 + CC-2) conjunction with (OUFC-2D)');
          
         end
         imshow(cc2imshow2);
       
if (do_tight)
    tightfig
end
% fcm_lbl = fcm();
% OUFC_lbl = OUFC();
%

%%
  
 %%
subplot(121)
%  BWnobord = imclearborder(BW, 4);
% imshow(BWnobord)

CH = bwconvhull(BW);
imshow(~CH);

subplot(122)

c_hull = BW | ~CH;

imshow( c_hull)



%%
BW1 = edge(BW,'sobel');
BW2 = edge(BW,'canny');
imshow(BW1)
figure, imshow(BW2)
%%
%BW_filled = imfill(BW,'holes');
boundaries = bwboundaries(BW);
for k=1:10
   b = boundaries{k};
   plot(b(:,2),b(:,1),'g','LineWidth',3);
end
%%
return  

%% dt = liver_img1
ii = 198;
dt = liver_img1(:,:,ii);
imshow(dt)
% %% debug 2
% % figure
% % i_m33_adjust
% % %%
%   ii = 21;
%  % imshow(liver_img1(:,:,ii));
% BW = liver_mask(:,:,ii);
% subplot(221)
%  imshow(BW);
% subplot(222)
%  imshow(liver_img1(:,:,ii));
% subplot(223)
%  imshow(liver_img(:,:,ii));
% subplot(224)
%  imshow(i_m33_adjust1(:,:,ii));