classdef CBIR_LUNG_aux_func
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
     
     methods(Static)
   
    %% lung_smooth_region_growing
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * lung_smooth_region_growing 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [lung_img] = lung_smooth_region_growing( lungs_BI_ROI , lung_LACSS ) 
        
        global CP  
        ix = PB_ip_shortcuts.get_image_idx( );
        
        base_code_lib_dir = getenv( 'base_code_lib_dir');
        addpath(genpath([ base_code_lib_dir '\clustering\SpectralClustering']));
   
        LACSS_number = round( CP.ip(ix).Lung_Axial_Biggest_Slice );
       %  lung_LACSS = squeeze( lungs_BI_ROI(:,:, LACSS_number));
   
        lung_img = zeros(size(lungs_BI_ROI),'int16');                                           
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
		% From the LACSS slice downward (toward the leges).   
		% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        curr_sl = lung_LACSS;
 
        lung_img(:,:,LACSS_number) =  curr_sl;  % start from the LACSS, 
                                                 % placed it in the restult
                                                % mat 
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% 		% From the LACSS slice upward (toward the head).     
% 		% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%         for sl_idx = LACSS_number:-1:2      
% 	            next_sl = lungs_BI_ROI(:,:,sl_idx-1);  
%                 [any_inter_curr_n_next] = CBIR_LEAK_REMOVAL_v1.object_with_any_intersect( curr_sl , next_sl);
%                 lung_img(:,:,sl_idx-1) = any_inter_curr_n_next; 
%                  curr_sl = any_inter_curr_n_next; 
%                  
%               %   figure;imagesc(curr_sl);
%               %          title(['curr_sl:' num2str(sl_idx)]);
%               %     figure;imagesc(next_sl);   
%               %           title(['next_sl:' num2str(sl_idx)])
%                %    figure;imagesc(any_inter_curr_n_next);  
%                %         title(['any_inter_curr_n_next:' num2str(sl_idx)])
%       
%                
%         end  
%           STOP_HERE();  
%                 eee=1; 

        for up_down = [-1,1]
        if (up_down ==-1)
            
            sl_idx_range = LACSS_number:-1:2 ;
        else    
            sl_idx_range = LACSS_number:size(lungs_BI_ROI,3)-1 ;
        end 
        two_lung_connected_slices = 0; 
        curr_sl = lung_LACSS;   
        for sl_idx = sl_idx_range
            sl_idx
                next_sl = (lungs_BI_ROI(:,:,sl_idx+up_down)); 

                % remove small untarget object that might that might 
                % ruin the division of the spectral clustring into 
                % two lobes
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                if (two_lung_connected_slices)  
                   next_sl = largest_conncomp(next_sl);
                end 
                % next_sl = lungs_BI_ROI(:,:,sl_idx+1);   
                [any_inter_curr_n_next] = CBIR_LEAK_REMOVAL_v1.object_with_any_intersect( curr_sl , next_sl); 
                  
                % After the two_lung_connected_slices region the two lungs
                % are seprate again...so is take the all slice... 
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                inter_ = any_inter_curr_n_next & curr_sl; 
                if (two_lung_connected_slices && (sum(inter_(:))<100)) % (~any(any_inter_curr_n_next(:))) 
                    next_sl = (lungs_BI_ROI(:,:,sl_idx+up_down)); 
                    [any_inter_curr_n_next] = CBIR_LEAK_REMOVAL_v1.object_with_any_intersect( curr_sl , next_sl); 
                end
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
		        % Lungs connected slice! ( Where the two lungs   
                %                            "touch" each other ) 
                % 
                %  If 1.5 * (the number of pixels in the current slice) is
                %  less the the number of pixels in the next slice, it
                %  means the the next slice, is a slice that contains the
                %  two lungs.  
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                number_of_cluster=2;
               % sl_idx 
                if (sum(curr_sl(:))*1.3 < sum(any_inter_curr_n_next(:)))
                     two_lung_connected_slices = 1;
                     disp(['lungs spectral clustering -  ' num2str(sl_idx)]);
                     
                     % remove small untarget object that might that might 
                     % ruin the division of the spectral clustring into 
                     % two lobes
                     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                     next_sl = largest_conncomp(next_sl); 
                     
                     % largest_conncomp(next_sl = largest_conncomp(next_sl););
%                      STOP_HERE(); 
%                      eee=1;   
  
                    % for finding the 'separated location' for the lunges
                    % we can take only the first 60 corronal rows. 
                    % (this is much more quick and accurate..) 
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    offset_for_connected_check = 60;
                    lung_starts_ = find(sum(any_inter_curr_n_next,1),1,'first'); 
                    lung_connected_area = any_inter_curr_n_next;
                    lung_separated_area = any_inter_curr_n_next;
                    lung_connected_area(:,lung_starts_+offset_for_connected_check:end)=0;
                    lung_separated_area(:,1:lung_starts_+offset_for_connected_check-1)=0;

                    % label image if the different clusters
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    %tic
                    cluster_any_inter_curr_n_next = ...
                        binary_spectral_clustring_segmantion( ...
                                   lung_connected_area ,  ...
                                   number_of_cluster);
                    %toc           
                                
                    
                    
                   %  first cluster
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                   cluster1_img =  cluster_any_inter_curr_n_next;
                   cluster1_img(cluster1_img==2)=0;
                   cluster1_img = cluster1_img | lung_separated_area;
                   cluster1_img = largest_conncomp(cluster1_img);
                   
                   %  second cluster
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                   cluster2_img =  cluster_any_inter_curr_n_next;
                   cluster2_img(cluster2_img==1)=0; 
                   cluster2_img = cluster2_img | lung_separated_area; 
                   cluster2_img = largest_conncomp(cluster2_img);
                    
                   overlap1 = curr_sl & cluster1_img;
                   overlap2 = curr_sl & cluster2_img; 
     
                   if (sum(overlap1(:))>sum(overlap2(:)))
                      any_inter_curr_n_next =  cluster1_img;
                   else 
                      any_inter_curr_n_next =  cluster2_img; 
                   end 
%                    figure;imagesc(overlap1);  
%                    figure;imagesc(overlap2);   
%                    figure;imagesc(any_inter_curr_n_next);   
%     
  
%                    STOP_HERE();  
%                     eee=1;      
% 
%                        [any_inter_curr_n_next] =  ...
%                            CBIR_LEAK_REMOVAL_v1.object_with_any_intersect( ..
%                                     curr_sl , curr_cluster_img); 
%                   
%                         
%                     end
                   
                else 
%                     STOP_HERE();  
%                     eee=1;   
                    if (two_lung_connected_slices==1)
%                                     
                        two_lung_connected_slices =0; 
                    end
                end 
                 
	            % new_next_sl = find_overlap_cc(curr_sl ,next_sl);
                curr_sl = logical(any_inter_curr_n_next); 
                lung_img(:,:,sl_idx+up_down) = curr_sl;  
                
               
                
                
        end 
        end
%         STOP_HERE();   
%         eee=1;    
%        lung_img(:,:,LACSS_number) =1;
%          STOP_HERE(); 
%          eee=1;    
%   
           
    end
      
    %% segment_AORTA_stage1
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_AORTA_stage1
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [lung_img] = extract_lung_from_air_n_trachea( trachea_img, air_img )
        if ~exist('verbose_','var'), verbose_= 1; end 
  
        lung_img =air_img & (~trachea_img);

   
        global CP  
        Lung_Axial_Biggest_Slice = ...
         CP.ip(PB_ip_shortcuts.get_image_idx( )).Lung_Axial_Biggest_Slice; 
      
     
        for sl = 1:size(lung_img,3)  
			if (verbose_)
			   fprintf('.');
			   if (mod(sl,10)==0)
					fprintf(num2str(sl));
			   end

			   if (mod(sl,80)==0)
				   disp('.')
			   end
			end

			CC = bwconncomp(squeeze(lung_img(:,:,sl)));
			numel_ans = cellfun(@numel,CC.PixelIdxList);
			mn  = length(numel_ans);
			if (mn>0)
				 num_of_cc(sl) = mn ;
				 num_of_pix(sl) =  sum(numel_ans(:));
			end
         
		end
	    Bronchi_Axial_End_Slice = ...
			 CP.ip(PB_ip_shortcuts.get_image_idx( )).Bronchi_Axial_End_Slice ;
	    num_of_cc(Bronchi_Axial_End_Slice:end)=0;
	    num_of_cc;
	   
		addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
		two_cc = find(num_of_cc==2);
		two_cc_diff = (diff(two_cc));
		two_cc_diff(two_cc_diff>1)=0;
 
 
		CC = bwconncomp(two_cc_diff);
		numPixels = cellfun(@numel,CC.PixelIdxList);
		for ii=1:length(numPixels)
			ar =CC.PixelIdxList(ii);
			two_cc_diff(ar{:}) = numPixels(ii);
			num_of_pix_ = num_of_pix(two_cc);
		end
    
    
     
		diff_TIMES_pix = two_cc_diff;
		 
		x_sum =0;  
		Trachea_from =0;
		Trachea_to=0; 
		diff_TIMES_pix = [0 diff_TIMES_pix 0];
		zeros_ind = find(diff_TIMES_pix==0);
    
     
    
    	ix = PB_ip_shortcuts.get_image_idx( );
		for ii=[1:length(zeros_ind)-1]
	%         
			tx_sum =sum(diff_TIMES_pix(zeros_ind(ii):zeros_ind(ii+1)))
			if (tx_sum >  x_sum)
				x_sum = tx_sum;
				lung_from = two_cc(zeros_ind(ii+1)-1);
				lung_from = two_cc(zeros_ind(ii)); 
                lung_from = round(lung_from);
				% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
				%   save the (aproximated) bifurcation end location
				% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
		 	
				CP.ip(ix) = ...  
						   pblib_set(CP.ip(ix), 'Bronchi_Axial_Start_Slice', lung_from);
						% lung_img(:,:,Bronchi_Axial_Start_Slice:Bronchi_Axial_End_Slice) =1;
						
				%Trachea_from = two_cc(zeros_ind(ii+1)-1); 
			end
		end 
       
		% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
		% ~~~~~~~~~~~~~ Bronchi_Coronal_End_Slice  
		find_as = find((squeeze(trachea_img(:,:,Bronchi_Axial_End_Slice+1))));
		[~, f_as_subs] = ...
			ind2sub([size(trachea_img,1),size(trachea_img,2)],find_as);
		find_as_sort = sort(f_as_subs);
					
		CP.ip(ix) = pblib_set(CP.ip(ix), 'Bronchi_Coronal_End_Slice', ...
						round(median(find_as_sort(1:10))));
					
		clear find_as f_as_subs find_as_sort
		% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  

     
		Bronchi_Axial_Start_Slice = ...
         CP.ip(PB_ip_shortcuts.get_image_idx( )).Bronchi_Axial_Start_Slice; 
     
		% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
		% ~~~~~~~~~~~~~  
		for ii = Bronchi_Axial_Start_Slice:Bronchi_Axial_End_Slice
			curr_sl = (squeeze(lung_img(:,:,ii))); 
			CC_curr_sl = bwconncomp(curr_sl);
			numPixels = cellfun(@numel,CC_curr_sl.PixelIdxList);
			[C, I] =  sort(numPixels,'descend');
			if (length(numPixels) ==2)
				
				if (numPixels(I(2)) < 1000)
					curr_sl(CC_curr_sl.PixelIdxList{I(2)})=0;
					lung_img(:,:,ii)=curr_sl;
				end 
				continue
			end
			
			%  STOP_HERE();
			
			I(3:length(numPixels));
			for jj=I(3:length(numPixels))
				curr_sl(CC_curr_sl.PixelIdxList{jj})=0;
	%             one_cc_diff(ar{:}) = numPixels(ii);
	%             num_of_pix_ = num_of_pix(one_cc);
			end
			lung_img(:,:,ii)=curr_sl;
		   
		 end
  
 
    end

     %% left_lung_seed_slice  
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % * * left_lung_seed_slice
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     function [seed_slice] = left_lung_seed_slice(  lung_n_trachea_seg )
             global CP  
            ix = PB_ip_shortcuts.get_image_idx( )
                  
    
            Sagittal_mid_Slice = round( CP.ip(ix).Sagittal_mid_Slice );
            Lung_Axial_Biggest_Slice = round( CP.ip(ix).Lung_Axial_Biggest_Slice );

            seed_slice = squeeze( lung_n_trachea_seg(:,:, Lung_Axial_Biggest_Slice));
            seed_slice(1:Sagittal_mid_Slice,:)=0; 
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');
            seed_slice = largest_conncomp(seed_slice);   
     end 
     
     
     %% right_lung_seed_slice_debug  
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % * * right_lung_seed_slice_debug
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     function [lung_n_trachea_seg] = right_lung_seed_slice_debug(  lung_n_trachea_seg )
            global CP  
            ix = PB_ip_shortcuts.get_image_idx( )
                
 
            Lung_Axial_Biggest_Slice = round( CP.ip(ix).Lung_Axial_Biggest_Slice );
            seed_slice = squeeze( lung_n_trachea_seg(:,:, Lung_Axial_Biggest_Slice));
            seed_slice(seed_slice==0)=2; 
             lung_n_trachea_seg(:,:, Lung_Axial_Biggest_Slice) =seed_slice;
             % return lung_n_trachea_seg;
            
     end 
     
      
     
     %% right_lung_seed_slice  
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % * * right_lung_seed_slice
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     function [seed_slice] = right_lung_seed_slice(  lung_n_trachea_seg )
            global CP  
            ix = PB_ip_shortcuts.get_image_idx( )
                

            Sagittal_mid_Slice = round( CP.ip(ix).Sagittal_mid_Slice );
            Lung_Axial_Biggest_Slice = round( CP.ip(ix).Lung_Axial_Biggest_Slice );

            seed_slice = squeeze( lung_n_trachea_seg(:,:, Lung_Axial_Biggest_Slice));
            seed_slice(Sagittal_mid_Slice:end,:)=0; 
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');
            seed_slice = largest_conncomp(seed_slice);  
     end 
     %% lung_refine_thresholding
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % * * lung_refine_thresholding
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     function thresholded_image = lung_refine_thresholding(original_image, original_image2, min_threshold, max_threshold)

        original_image(~original_image2)=0;
        if min_threshold > max_threshold
            error('threshold_grayscale_image : min_threshold is greater then max_threshold');
        end

        thresholded_image = false(size(original_image));
        selected = (min_threshold <= original_image) & (original_image <= max_threshold);
        thresholded_image(selected) = 1;
     end
 
    
    end
end

