classdef CBIR_SPLEEN_aux_func
    methods(Static)
 
     %% Kidney_Right_Coarse_ROI_basedonLeft 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Kidney_Right_Coarse_ROI_basedonLeft  
    % * *     CBIR_SPLEEN_aux_func.SPLEEN_POST_Segmentaion_stat 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    
    function [origCT] =  SPLEEN_POST_Segmentaion_stat( spleen_final_seg ,origCT  ) 
%             imagine_v2(origCT, spleen_final_seg, 'asdf')                                                 
             
  
% % %    
%            STOP_HERE(); 
%            eeee=1;   
%                       
 
          global CP
          ix = PB_ip_shortcuts.get_image_idx( );
          num_of_slices_ = size(spleen_final_seg,3);
          spleen_final_seg_slices_ = sum( ...
                                        reshape(  ...
                                            permute(spleen_final_seg,[3 1 2]) ...
                                         ,num_of_slices_,[]) ...
                                         ,2); 
          % Search for the first (lower bound) and the last (upperBound) 
          % axial slices which does not contain any pixels.     
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
           
          spleen_Left_Axial_upperBound_Slice_ = ...
                                find(spleen_final_seg_slices_,1, 'last')+ 1 ;
          CP.ip(ix) = pblib_set(CP.ip(ix), 'Spleen_open_oper1', ... 
                                round( double(spleen_Left_Axial_upperBound_Slice_)) ); 
                              
              origCT(~logical(spleen_final_seg)) = 0;   
            Spleen_GrayLevel_thr1_ = round(mean( double(origCT(origCT>0))));
            Spleen_GrayLevel_thr2_ = round(std( double(origCT(origCT>0))));  
            CP.ip(ix) = pblib_set(CP.ip(ix), 'Spleen_GrayLevel_thr1', ...
                                Spleen_GrayLevel_thr1_ ); 
                             
            CP.ip(ix) = pblib_set(CP.ip(ix), 'Spleen_GrayLevel_thr2', ...
                                Spleen_GrayLevel_thr2_ );  
%               Kidney_Left_GrayLevel_thr1: -1
%              Kidney_Left_GrayLevel_thr2: -1                 
          
    end

    
    
    %% Sagittal_mid_Slice_Coarse_ROI
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  * *   Set to zero all pixels to the rigth of the
    %  * *   'Sagittal_mid_Slice'
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [Refine2_Coarse_ROI2] = Sagittal_mid_Slice_Coarse_ROI(Coarse_ROI)
            global CP  
            

            Refine2_Coarse_ROI2 = Coarse_ROI;
            ix = PB_ip_shortcuts.get_image_idx( );
            Sagittal_mid_Slice = CP.ip(ix).Sagittal_mid_Slice; 
              
            % set to zero all pixels to the rigth of the
            % 'Sagittal_mid_Slice'
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            Refine2_Coarse_ROI2([Sagittal_mid_Slice-20:end],:,:)=0; 
    end
   
    
    %% Seprate_kidney_from_spleen_ROI
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  * *   Set to zero all slices to the above 
    %  * *          'Trachea_Axial_lowerBound_Slice'+20  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [Kidney_Coarse_ROI2] = ...
                Seprate_kidney_from_spleen_ROI(coarse_ROI)
            
            Kidney_Coarse_ROI2 = Coarse_ROI;
            
            CC = bwconncomp(Kidney_Coarse_ROI2);
            BW1 = zeros(size(BW),'int16'); 
            BW2 = zeros(size(BW),'int16');
            numPixels = cellfun(@numel,CC.PixelIdxList);
            [biggest,idx] = sort(numPixels);
            
            % The spleen is assumed to be:
            %     1. One of the two biggest this Connected-component
            %     2. the Connected-component with a 'lower-Centroid' among
            %        those two Connected-component
            %     
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            BW1(CC.PixelIdxList{idx(1)}) = 1;    
            BW2(CC.PixelIdxList{idx(2)}) = 1;    
            
            STATS1 = regionprops(BW1, 'Centroid');
            STATS2 = regionprops(BW2, 'Centroid');
            STATS1 = STATS1.Centroid;
            STATS2 = STATS2.Centroid;  
    end
    %% Kidney_Coarse_ROI
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  * *   Set to zero all slices to the above 
    %  * *          'Trachea_Axial_lowerBound_Slice'+20  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [Kidney_Coarse_ROI2] = ...
                Kidney_Coarse_ROI(Coarse_ROI, is_left)
             
     %    STOP_HERE()
       %%  
        global CP  
        Kidney_Coarse_ROI2 = Coarse_ROI;
        ix = PB_ip_shortcuts.get_image_idx( );
  
        if (is_left)
            Lung_Axial_lowerBound_Slice_ = CP.ip(ix).Lung_Left_Axial_lowerBound_Slice; 
        else
            Lung_Axial_lowerBound_Slice_ = CP.ip(ix).Lung_Right_Axial_lowerBound_Slice; 
        end  
        
        Bones_Axial_smalles_Slice_ = CP.ip(ix).Bones_Axial_smalles_Slice;
        if (Lung_Axial_lowerBound_Slice_ < Bones_Axial_smalles_Slice_ )
            Lung_Axial_lowerBound_Slice_ = CP.ip(ix).Lung_Axial_Biggest_Slice;
        end  
        Cornal_mid_Slice_ = CP.ip(ix).Cornal_mid_Slice; 
        Sagittal_mid_Slice_= CP.ip(ix).Sagittal_mid_Slice; 
           
        Kidney_Coarse_ROI2(:,:,[Lung_Axial_lowerBound_Slice_+20:end])=0;  
        Kidney_Coarse_ROI2(:,:,[1:Bones_Axial_smalles_Slice_-40])=0;
        
        % Set to zero all slices to the above 
        %  'Lung_Axial_lowerBound_Slice_'+20         
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~
%         if (is_left) 
%             
%                Kidney_Coarse_ROI2(:,:,[Lung_Axial_lowerBound_Slice_+20:end])=0;  
%                    
%  
%         else   
%             % just below the liver, so it's higher then the right kidney  
%                 Kidney_Coarse_ROI2(:,:,[Lung_Axial_lowerBound_Slice_+20:end])=0; 
%         end 
         %%
% %         Bones_Axial_smalles_Slice_ = Bones_Axial_smalles_Slice-40;
% %         if (Bones_Axial_smalles_Slice_<1)
% %             Bones_Axial_smalles_Slice_ = 1; 
% %         end 
% %         
% %             %
% %               
           
           
            Kidney_Coarse_ROI2(:,1:Cornal_mid_Slice_-50,:)=0;
         %%      
            if (is_left)     
              Kidney_Coarse_ROI2(Sagittal_mid_Slice_-30:end,:,:)=0;  % was -20
            else   
              Kidney_Coarse_ROI2(1:Sagittal_mid_Slice_+30,:,:)=0;    % was -20
            end
  
        
    end
    
     %% gross_SPLEEN_segment 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * gross_SPLEEN_segment
%     % * *  
%     % * * 
%     % * * Given:
%     % * *     o gross_SPLEEN_segment
%     % * *   
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [img__] = gross_SPLEEN_segment(liver_heart_leftKidny_seg)
 
        global CP   
        ix = PB_ip_shortcuts.get_image_idx( )
        Kidney_Axial_upperBound_Slice  = CP.ip(ix).Kidney_Axial_upperBound_Slice;
        Kidney_Coronal_upperBound_Slice = CP.ip(ix).Kidney_Coronal_upperBound_Slice ;
    
        Lung_Axial_Biggest_Slice = CP.ip(ix).Lung_Axial_Biggest_Slice  ;
        
             
        
        img__ = liver_heart_leftKidny_seg; 
%         w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
%         SPINAL_w_raito1 = 120; % round(SPINAL_w_raito_top*w_);
%         SPINAL_w_raito2 = 120; %round(SPINAL_w_raito_bot*w_);
%         SPINAL_w_raito3 = 70;
%   
%          img__([1:(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1) (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito2):end  ]   ,   ...
%              : , ...
%              : )=0; 
         img__(:   ,   ... 
              : , ...  
             [Lung_Axial_Biggest_Slice:end] )=0;
             
%          img__(:  ,   ...
%               [1:Kidney_Coronal_upperBound_Slice]  , ...
%              :)=0;
         
      
         
    end 
%     
    
    %% locate_the_spleen_in_the_spleen_Axial_in_Slice  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * locate_the_spleen_in_the_spleen_Axial_in_Slice
    % * * 
    % * * 
    % * * Given:  
    % * *     o locate_the_spleen_in_the_spleen_Axial_in_Slice
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
         
     function [res] = locate_the_spleen_in_the_spleen_Axial_in_Slice( ...
                                         gross_seg  , orig_ct   ) 
          addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\'); 
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');  
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\');                   
        global CP  curr_image_name  TMP_DIR 
       % res = zeros(size(orig_ct)); 
       % addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_orig')
       % imagine(gross_seg)
           
           
%        %    save it!    
%  
%     out_file_prefix_ = 'SPLEEN_BIROI1_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
%     file2load = evalin('caller','file2load')
%     based_on_file = evalin('caller','based_on_file')
%     file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
%      STOP_HERE();   
%      
%          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     % 
%     % save_untouch2_nii( based_on_file, gross_seg, file2save_t  )
%     %
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  
%      
%     PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   
  
     
     
               img__ = gross_seg;   
        ix = PB_ip_shortcuts.get_image_idx( ) ; 
        Heart_GrayLevel_mean = CP.ip(ix).Heart_GrayLevel_mean;
        Heart_GrayLevel_std = CP.ip(ix).Heart_GrayLevel_std; 
        prm101 = str2num(CP.ip(ix).prm101);          
        Spleen_Axial_in_Slice = CP.ip(ix).Kidney_Axial_upperBound_Slice 
        Spleen_Axial_in_Slice = 228     
        Spleen_Axial_Slice =  squeeze(  img__(:,:, Spleen_Axial_in_Slice ) );
        orig_ct_spleen_Axial_in_Sl =  squeeze(  orig_ct(:,:, Spleen_Axial_in_Slice) );
        orig_ct_spleen_Axial_in_Sl2 = orig_ct_spleen_Axial_in_Sl; 
        orig_ct_spleen_Axial_in_Sl2(~Spleen_Axial_Slice) =0; 
           
           tt1_thr             = threshold_image_BW(orig_ct_spleen_Axial_in_Sl2, ... 
                                  Heart_GrayLevel_mean - prm101*Heart_GrayLevel_std   , ...  
                                  Heart_GrayLevel_mean + 0.2*Heart_GrayLevel_std );               
                             
            tt1_thr_close           = simple_3D_SEmorph.run(tt1_thr,'imclose',3,'disk',1,0);
                 
            tt1_thr_close_fill      = simple_2D_to_3D.run(tt1_thr_close,'imfill',3);        
            tt1_thr_close_fill_open = simple_3D_SEmorph.run(tt1_thr_close_fill ...
                                    ,'imopen',3,'disk',7,0); 
            tt1_thr_close_fill_open_leg = largest_conncomp(   tt1_thr_close_fill_open );                             
           %  save('c:\tt1_thr_close_fill_open_leg', 'tt1_thr_close_fill_open_leg')
            
          res__ =  tt1_thr_close_fill_open_leg;
          
           
        %   STOP_HERE();   
           
          res = res__; 
          return
           CH = bwconvhull(tt1_thr_close_fill_open)          ;           
           STATS = regionprops(CH, 'Perimeter')
        bwc = (bwconvhull(orig_ct_spleen_Axial_in_Sl2,'objects')); 
        img__(:,:, CP.ip(ix).Kidney_Axial_upperBound_Slice) = bwc; 
        gl = double(orig_ct_spleen_Axial_in_Sl2(find(orig_ct_spleen_Axial_in_Sl2)));
%            
%          figure 
%             subplot(1,5,1) 
%                 imagesc(tt1_thr);
%                 title(num2str(curr_image_name)); 
%             subplot(1,5,2)
%                 imagesc(tt1_thr_close);
%  
%             subplot(1,5,3)
%                 imagesc(tt1_thr_close_fill);
%             subplot(1,5,4)
%                 imagesc(tt1_thr_close_fill_open);
%            subplot(1,5,5)
%                 imagesc(tt1_thr_close_fill_open_leg);                
%                 
%          colormap(gray)
%           STOP_HERE();   
%         eee=1;          
    return   
% %%   
% addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Segmentaion\ActiveContour\');
% alpha_p = .2;
% dsp_p = true;      
% for jj = Spleen_Axial_in_Slice:-1:1
%     jj
%     msk = tt1_thr_close_fill_open_leg;
%     iimg = squeeze(orig_ct(:,:,Kidney_Axial_upperBound_Slice-1));
%         iimg             = threshold_image_BW(iimg, ... 
%                                   Heart_GrayLevel_mean - prm101*Heart_GrayLevel_std   , ...  
%                                   Heart_GrayLevel_mean + 0.2*Heart_GrayLevel_std );      
%                               
%     % skip empty slices ot empty mask...
%     if (any(iimg(:)) && any(msk(:)))
%         try 
%     %    active_counture__ = ... 
%          figure        
% 
%                 region_seg( iimg  , msk , 250 , alpha_p, dsp_p ); %-- Run segmentation
%         
%         active_counture(:,:,jj) = imfill( active_counture(:,:,jj) ,'holes');
% 
%         end
%     end
% end    
 

     end
     
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
    % * * locate_the_spleen_in_the_spleen_Axial_in_Slice2
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
        function [res] = locate_the_spleen_in_the_spleen_Axial_in_Slice2( ...
                                         gross_seg  , orig_ct   ) 
          addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\'); 
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');  
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\');                   
        global CP  curr_image_name
        res = zeros(size(orig_ct)); 
        
        img__ = gross_seg;  
        ix = PB_ip_shortcuts.get_image_idx( ) ; 
        Heart_GrayLevel_mean = CP.ip(ix).Heart_GrayLevel_mean;
        Heart_GrayLevel_std = CP.ip(ix).Heart_GrayLevel_std; 
        prm101 = str2num(CP.ip(ix).prm101);          
        Spleen_Axial_in_Slice = CP.ip(ix).Kidney_Axial_upperBound_Slice 
            
        Spleen_Axial_Slice =  squeeze(  img__(:,:, Spleen_Axial_in_Slice ) );
        orig_ct_spleen_Axial_in_Sl =  squeeze(  orig_ct(:,:, CP.ip(ix).Kidney_Axial_upperBound_Slice) );
        orig_ct_spleen_Axial_in_Sl2 = orig_ct_spleen_Axial_in_Sl; 
        orig_ct_spleen_Axial_in_Sl2(~Spleen_Axial_Slice) =0; 
           
           tt1_thr             = threshold_image_BW(orig_ct_spleen_Axial_in_Sl2, ... 
                                  Heart_GrayLevel_mean - prm101*Heart_GrayLevel_std   , ...  
                                  Heart_GrayLevel_mean + 0.2*Heart_GrayLevel_std );               
                             
            tt1_thr_close           = simple_3D_SEmorph.run(tt1_thr,'imclose',3,'disk',1,0);
                 
            tt1_thr_close_fill      = simple_2D_to_3D.run(tt1_thr_close,'imfill',3);        
            tt1_thr_close_fill_open = simple_3D_SEmorph.run(tt1_thr_close_fill ...
                                    ,'imopen',3,'disk',7,0); 
            tt1_thr_close_fill_open_leg = largest_conncomp(   tt1_thr_close_fill_open );                             
            
           
          res__ =  tt1_thr_close_fill_open_leg;
     
          res(:,:, Spleen_Axial_in_Slice) = res__; 
          return
           CH = bwconvhull(tt1_thr_close_fill_open)          ;           
           STATS = regionprops(CH, 'Perimeter')
        bwc = (bwconvhull(orig_ct_spleen_Axial_in_Sl2,'objects')); 
        img__(:,:, CP.ip(ix).Kidney_Axial_upperBound_Slice) = bwc; 
        gl = double(orig_ct_spleen_Axial_in_Sl2(find(orig_ct_spleen_Axial_in_Sl2)));
%            
%          figure 
%             subplot(1,5,1) 
%                 imagesc(tt1_thr);
%                 title(num2str(curr_image_name)); 
%             subplot(1,5,2)
%                 imagesc(tt1_thr_close);
%  
%             subplot(1,5,3)
%                 imagesc(tt1_thr_close_fill);
%             subplot(1,5,4)
%                 imagesc(tt1_thr_close_fill_open);
%            subplot(1,5,5)
%                 imagesc(tt1_thr_close_fill_open_leg);                
%                 
%          colormap(gray)
%           STOP_HERE();   
%         eee=1;          
    return   
% %%   
% addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Segmentaion\ActiveContour\');
% alpha_p = .2;
% dsp_p = true;      
% for jj = Spleen_Axial_in_Slice:-1:1
%     jj
%     msk = tt1_thr_close_fill_open_leg;
%     iimg = squeeze(orig_ct(:,:,Kidney_Axial_upperBound_Slice-1));
%         iimg             = threshold_image_BW(iimg, ... 
%                                   Heart_GrayLevel_mean - prm101*Heart_GrayLevel_std   , ...  
%                                   Heart_GrayLevel_mean + 0.2*Heart_GrayLevel_std );      
%                               
%     % skip empty slices ot empty mask...
%     if (any(iimg(:)) && any(msk(:)))
%         try 
%     %    active_counture__ = ... 
%          figure        
% 
%                 region_seg( iimg  , msk , 250 , alpha_p, dsp_p ); %-- Run segmentation
%         
%         active_counture(:,:,jj) = imfill( active_counture(:,:,jj) ,'holes');
% 
%         end
%     end
% end    
 

        end      
  
    
        
    
    %% Tune_Liver_Thresholding 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Tune_Liver_Thresholding 
    % * *   
    % * *  
    % * * Given:
    % * *     o Tune_Liver_Thresholding
    % * *    
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [Spleen_seg] = Tune_Spleen_Thresholding( Spleen_seg )
         
            global CP         
            ix = PB_ip_shortcuts.get_image_idx( )
            Kidney_Axial_upperBound_Slice = CP.ip(ix).Kidney_Axial_upperBound_Slice;
            Heart_GrayLevel_mean = CP.ip(ix).Heart_GrayLevel_mean;
            Heart_GrayLevel_std = CP.ip(ix).Heart_GrayLevel_std; 
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\'); 
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');  
            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
         %    STOP_HERE(); 
            eee=1;     
            STATS.Perimeter = 0; 
            th_delta = 0.1;
            th_from = 1.2;
            th_to = 1.6;
            low_th = th_from;
            for low_th = [th_from:th_delta:th_to]

                STATS_lst =  STATS; 
                tt1 = Spleen_seg(:,:,Kidney_Axial_upperBound_Slice);
  
                lo_ = round(Heart_GrayLevel_mean - low_th*Heart_GrayLevel_std);
                up_ = round(Heart_GrayLevel_mean + 0.2*Heart_GrayLevel_std); 
 
                tt1_thr             = threshold_image_BW(tt1, ... 
                                      lo_ , ...
                                      up_ );               
 
                tt1_thr_close           = simple_3D_SEmorph.run(tt1_thr,'imclose',3,'disk',1,0);

                tt1_thr_close_fill      = simple_2D_to_3D.run(tt1_thr_close,'imfill',3);        
                tt1_thr_close_fill_open = simple_3D_SEmorph.run(tt1_thr_close_fill ...
                                    ,'imopen',3,'disk',7,0); 
                CH = bwconvhull(tt1_thr_close_fill_open)          ;          
                STATS = regionprops(CH, 'Perimeter');  



               dif_ = STATS.Perimeter - STATS_lst.Perimeter ; 
               if (dif_ < 30 )
                   prm101 = (low_th-th_delta);
                   CP.ip(ix) = pblib_set(CP.ip(ix), 'prm101', num2str(prm101));    
                   prm101 
                   break  
               end  
                 
          %      STOP_HERE();  
               eee=1; 
            end 
%               STOP_HERE();   
%                eee=1;  
    end
           
        
        
        
        
        
        
        
        
    end
    
   
end

