  
CBIR_INIT 
global dry_run  
dry_run =0  ;   
    
 
%% subjects 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
if (1)         
    % 10000105   
    subjects_ids =[ 20000103 ,20000105, 20000106,  20000107, 20000108,20000109];
    subjects_ids =[ 20000110 ,20000112, 20000113,  20000115, 20000117,20000118]; 
       
     
    subjects_ids = [ 20000103 ,20000105, 20000106,  20000107, 20000108,20000109 ...
        20000110 ,20000112, 20000113,  20000115, 20000117,20000118]; 
     
 %    subjects_ids =[ 20000117  ]; 
    image_sufix = '_1_Moti_CTce_ThAb';
%    subjects_ids =[ 10000110 ]; 
end    
  
%% 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % The main loop  
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
for sbj_id=subjects_ids       
    if (~VISCERAL_SUBMIT_MODE) 
        file2load = [  num2str(sbj_id) image_sufix '.nii'];
        based_on_file =  [ '''' TMP_DIR file2load '''']; 
    end     
         
   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
   % ~~~~~~~~~~~~ -> load_nii_image
    im_str = [' ''C:/Users/owner/DATA/VISCERAL/visceral-dataset/trainingset/volumes/' ...
              num2str(sbj_id) image_sufix '.nii'' '];
    PB_shortcuts.load_nii_image( im_str ); 
      
        PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
        PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT(); 
        
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ~~~~~~~~~~~~~ Axial_Biggest_Slice 
    PB.add_im_Process(    'CBIR_Moti_aux_func.Axial_Biggest_Slice', '');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );  
    
     
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ~~~~~~~~~~~~~ load orig CT 
    PB_shortcuts.load_nii_image( ...
            [' ''C:/Users/owner/DATA/VISCERAL/visceral-dataset/trainingset/volumes/' ...
              num2str(sbj_id) image_sufix '.nii'' ']); 
    enabled_=1;   
             PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
   
    PB_shortcuts.extract_body(enabled_);
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
    %   save_untouch2_nii          
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
    out_file_prefix_ = ['BODY_'] ; 
    out_file_suffix = [image_sufix ];
    PB_shortcuts.save_untouch2_nii(out_file_prefix_ , sbj_id , image_sufix ,out_file_suffix);
 
	  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ~~~~~~~~~~~~~ extract_air_organs_from_body
    PB_shortcuts.set_new_image_index();
    CBIR_LUNG_n_TRACHEA_aux_func.extract_air_organs_from_body( enabled_ );
                 PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 

        PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT(); 
   
          
          
   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
   %   save_untouch2_nii      
   out_file_prefix_ = 'LUNGS_n_TRACHEA_'; 
   out_file_suffix = [image_sufix ];
   PB_shortcuts.save_untouch2_nii(out_file_prefix_ , sbj_id , image_sufix ,out_file_suffix);
             
 
end
%% <> SAVE & EXIT
%  
%  buffer = pblib_generic_serialize_to_string(C);
%   fid = fopen(CBIR_params_file , 'w+');
%   fwrite(fid, buffer, 'uint8');
%   fclose(fid); 
% clearvars -except CBIR_params_file 

%% call the MAIN
 
CBIR_main_v6 
return

% % 
% % if (1)    
% %     % 10000105 
% %     subjects_ids =[ 10000104 ,10000106, 10000108,  10000109, 10000110,10000111];
% %   %      subjects_ids =[ 10000109, 10000110,10000111   ]; 
% %   
% %   image_sufix = '_1_CTce_ThAb';
% %  %  lung_sufix = '_1326' 
% %   
% %     subjects_ids =[ 10000104 ];  
% % end 
    

