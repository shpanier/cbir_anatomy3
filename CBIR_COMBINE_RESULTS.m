MY_VISCERAL_TMP = 'D:/MY_VISCERAL_TMP'; 

CBIR_INIT 
global dry_run CP      
dry_run =0                    ;    
MOTI = 0;  
%% subjects      
% ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
subjects_ids = [ 113 100 104 105 106 108 109 110 111 112 113 127 128 129 130 131 132 133 134 135 136  ] 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % The main loop     
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
for sbj_id=subjects_ids 
    CBIR_INIT
    %if (~VISCERAL_SUBMIT_MODE)   
        image_sufix = '_1_CTce_ThAb';
        file2load = [   '10000' num2str(sbj_id) image_sufix '.nii']
        based_on_file =  [ '''' TMP_DIR '/' file2load '''']; 
    % end  
        
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ===> load the CT volume
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = [];       change_curr_image_name =1;      nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load , change_curr_image_name ) 
    PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
        
   
    % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ===> load SPLEEN_V3
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['SPLEEN_V5_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
   
     
    % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ===> load LIVER_phase3_V1_AA
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['LIVER_V5_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
 
  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ===> load   KIDNEY_RIGHT_V1_
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['KIDNEY_RIGHT_V5_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
      
    % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ===> load   LEFT_KIDNEY_V1_ 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['KIDNEY_LEFT_V5_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
 
    
    if (~MOTI) 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        % ===> load   TRACHEA_V2_
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        file_prefix = ['TRACHEA_V2_'];  change_curr_image_name =0;  nii_prefix =  [];    
        PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
        PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 

 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        % ===> load   LUNG_RIGHT2_V1_
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        file_prefix = ['LUNG_RIGHT2_V1_'];  change_curr_image_name =0;  nii_prefix =  [];    
        PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
        PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 


        % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        % ===> load   LUNG_LEFT2_V1_ 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        file_prefix = ['LUNG_LEFT2_V1_'];  change_curr_image_name =0;  nii_prefix =  [];    
        PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
        PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
        
         
        % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        % ===> load   LUNG_LEFT2_V1_ 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        file_prefix = ['BONE_SKELETON_'];  change_curr_image_name =0;  nii_prefix =  [];    
        PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
        PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
      
        
    end 
        
% % 
% %      % ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% %     % ===> load   BONE_SKELETON_
% %     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% %     file_prefix = ['BONE_SKELETON_'];  change_curr_image_name =0;  nii_prefix =  [];    
% %     PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
% %       PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
% %     
    
    % zeros matrix (Ad-hoc)  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    PB_shortcuts.set_new_image_index();  
    PB.add_im_Process(    'LogicalOperators', ...
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   %  A(A 'oper_' B) = C
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
         PB.add_im_Process_parm('oper_' ,  '''>''')  
         PB.add_im_Process_parm('B' ,     '-10000' );
         PB.add_im_Process_parm('C' ,  '0')   

      
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index();  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...      
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );  
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-2) ,')']);
         PB.add_im_Process_parm('C' ,  '1') 
               
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...           
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-3) ,')']);
         PB.add_im_Process_parm('C' ,  '2')    
  
          
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...         
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-4) ,')']);
         PB.add_im_Process_parm('C' ,  '3')  
         
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...         
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-5) ,')']);
         PB.add_im_Process_parm('C' ,  '4')            
         
   if (~MOTI)       
  %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...         
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-6) ,')']);
         PB.add_im_Process_parm('C' ,  '5')           
         
         
         
           %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...         
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-7) ,')']);
         PB.add_im_Process_parm('C' ,  '6')  
               
         %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...         
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-8) ,')']);
         PB.add_im_Process_parm('C' ,  '7')  
         
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
    PB.add_im_Process(    'LogicalAssignment', ...         
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-9) ,')']);
         PB.add_im_Process_parm('C' ,  '8')           
   end             
%     %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
%     PB.add_im_Process(    'LogicalAssignment', ...         
%          'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');        
%          PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );   
%          PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-8) ,')']);
%          PB.add_im_Process_parm('C' ,  '8')  
%               
         
    PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    %    save it!  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    out_file_prefix_ = 'CMB_RESULT_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
    file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
    PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   
               file2load = [   '10000' num2str(sbj_id) image_sufix '.nii']

    CBIR_main_v8
end
%% <> SAVE & EXIT
% 
%  buffer = pblib_generic_serialize_to_string(C);
%   fid = fopen(CBIR_params_file , 'w+');
%   fwrite(fid, buffer, 'uint8');
%   fclose(fid);  
% clearvars -except CBIR_params_file 

%% call the MAIN

  
return
 
