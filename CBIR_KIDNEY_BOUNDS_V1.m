 
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load LUNGS_n_TRACHEA_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['CHEST_POSTERIOR_P_V2_'];    nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
%PB_shortcuts.KEEP_RESULT_AT_THIS_POINT();
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load SPINAL_STRIP_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['SPINAL_STRIP_'];  nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
%PB_shortcuts.KEEP_RESULT_AT_THIS_POINT();
     
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];          nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
%PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
     
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%  Find the kidney-upper-bound and kidney-in
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
PB_shortcuts.set_new_image_index();     
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE()
PB.add_im_Process(    'CBIR_KIDNEY_aux_func.Kidney_Axial_upperBound_Slice', '');
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );      % orig_ct 
    PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ...
                           ['c_im_res' num2str(im_idx-2)] );   % Aorta_seg
      PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ...
                           ['c_im_res' num2str(im_idx-3)] );   % CHEST_POSTERIOR


%PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();
