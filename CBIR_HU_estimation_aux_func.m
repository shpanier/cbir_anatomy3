classdef CBIR_HU_estimation_aux_func

    methods(Static)
 
    %% gross_CT_landmarks 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * gross_CT_landmarks
    %   Get:
    %      o orig_ct_im  - orig ct (as its name implies :) 
    %      o lung_seg    - lungS segmentation.  
    %  
    %   Set:   
    %       o Bones_GrayLevel_mean
    %       o Bones_GrayLevel_std
    %
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res] = run( orig_ct_im , lung_seg )
        res = orig_ct_im; 
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2c');
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\freezeColors');
    %  imagine_v2(orig_ct_im,lung_seg , 'asdf')
     %     STOP_HERE();   
       %%      
         global CP  
         ix = PB_ip_shortcuts.get_image_idx( )
         Lung_Axial_Biggest_Slice =  CP.ip(ix).Lung_Axial_Biggest_Slice

          orig_ct_im_sl = orig_ct_im(:,:,Lung_Axial_Biggest_Slice);
          %orig_ct_im_sl1 =orig_ct_im_sl;
          orig_ct_im_sl_liver =orig_ct_im_sl;
          %orig_ct_im_sl1(orig_ct_im_sl<1)=0;
          orig_ct_im_sl_liver(orig_ct_im_sl_liver<1)=0;

          %orig_ct_im_sl1 = imclearborder(orig_ct_im_sl1);
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          %   Lung convex hull at the Biggest_Slice
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
          lung_seg_im_sl = lung_seg(:,:,Lung_Axial_Biggest_Slice);
          CH1 = bwconvhull(lung_seg_im_sl);
          SE = strel('disk',5,0);
            
        
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          CH_dilate = imdilate(CH1,SE); 
          CH_reg = regionprops(CH_dilate, 'Extrema');
          
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          % sagittal line
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
              top_sagittal = CH_reg.Extrema(2,2);
              bottom_sagittal =  CH_reg.Extrema(6,2);
              mid_sagittal = round(mean([bottom_sagittal top_sagittal])) ;
              CP.ip(ix) = pblib_set(CP.ip(ix), 'Sagittal_mid_Slice', mid_sagittal);  

          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          % coronal line
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
              top_coronal  = CH_reg.Extrema(7,1) ;
              bottom_coronal  =  CH_reg.Extrema(4,1) ;
              mid_coronal  = round(mean([bottom_coronal top_coronal])) ;
              CP.ip(ix) = pblib_set(CP.ip(ix), 'Cornal_mid_Slice', mid_coronal);  
  
         
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          %  the liver ROI 
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
          SE_disk8 = strel('disk',8,0); 
          CH_erode = imerode(CH1,SE_disk8);  
          orig_ct_im_sl_liver(~CH_erode)=0; 
          orig_ct_im_sl_liver(:,mid_coronal:end) = 0;

        
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          %  the liver mean & std   
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
             tm = double(orig_ct_im_sl_liver(orig_ct_im_sl_liver>0));
             tm_s = sort(tm);  
             p95 = tm_s(round(length(tm_s)*0.95)) ; 
             Heart_GrayLevel_mean = round(mean(tm));
             Heart_GrayLevel_std   = round(std(tm));  
             CP.ip(ix) = pblib_set(CP.ip(ix), 'Heart_GrayLevel_mean', Heart_GrayLevel_mean);  
             CP.ip(ix) = pblib_set(CP.ip(ix), 'Heart_GrayLevel_std', Heart_GrayLevel_std);      
             CP.ip(ix) = pblib_set(CP.ip(ix), 'Heart_Axial_in_Slice', Lung_Axial_Biggest_Slice); 
             Liver_Axial_upperBound_Slice=round(min([Lung_Axial_Biggest_Slice+10, size(orig_ct_im,3)]))   ;
             CP.ip(ix) = pblib_set(CP.ip(ix), 'Liver_Axial_upperBound_Slice', Liver_Axial_upperBound_Slice);  
 
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          %   left lung lower bound 
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
          coarse_left_lung = lung_seg( [1:mid_sagittal] , : ,:);
          num_of_slices_ = size(coarse_left_lung,3);
          coarse_left_lung_slices_ = sum( ...
                                        reshape(  ...
                                            permute(coarse_left_lung,[3 1 2]) ...
                                         ,num_of_slices_,[]) ...
                                         ,2); 
          % Search for the first (lower bound) and the last (upperBound) 
          % axial slices which does not contain any pixels.     
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
          Lung_Left_Axial_lowerBound_Slice = ...
                                find(coarse_left_lung_slices_,1, 'first');
          Lung_Left_Axial_upperBound_Slice = ...
                                find(coarse_left_lung_slices_,1, 'last')  ;
          CP.ip(ix) = pblib_set(CP.ip(ix), 'Lung_Left_Axial_lowerBound_Slice', ...
                                Lung_Left_Axial_lowerBound_Slice);  
          CP.ip(ix) = pblib_set(CP.ip(ix), 'Lung_Left_Axial_upperBound_Slice', ...
                                Lung_Left_Axial_upperBound_Slice);      
           
        
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          %   right lung lower bound 
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
          coarse_right_lung = lung_seg( [mid_sagittal:end] , : ,:);
          num_of_slices_ = size(coarse_right_lung,3);
          coarse_left_lung_slices_ = sum( ...
                                        reshape(  ...
                                            permute(coarse_right_lung,[3 1 2]) ...
                                         ,num_of_slices_,[]) ...
                                         ,2);
          % Search for the first (lower bound) and the last (upperBound) 
          % axial slices which does not contain any pixels.     
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
          Lung_Right_Axial_lowerBound_Slice = ...
                                find(coarse_left_lung_slices_,1, 'first');
          Lung_Right_Axial_upperBound_Slice = ...
                                find(coarse_left_lung_slices_,1, 'last')  ;
          CP.ip(ix) = pblib_set(CP.ip(ix), 'Lung_Right_Axial_lowerBound_Slice', ...
                                Lung_Right_Axial_lowerBound_Slice);  
          CP.ip(ix) = pblib_set(CP.ip(ix), 'Lung_Right_Axial_upperBound_Slice', ...
                                Lung_Right_Axial_upperBound_Slice);      
       
        
        
 
  
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          %  the liver HU as function of rigion.  
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%          liver_gl =  double(orig_ct_im_sl_liver(orig_ct_im_sl_liver>0)) ; 
%           delt_ = 10;
%           from_ = min(liver_gl);
%           region_as_func_of_thresh = [];
%           examine_th = [from_+delt_:delt_:max(liver_gl)];
%           for to_= examine_th
%               orig_ct_im_sl_liver_t = orig_ct_im_sl_liver;
%               orig_ct_im_sl_liver_t(orig_ct_im_sl_liver_t >to_)=0;
%               bw_cv_ = bwconvhull(orig_ct_im_sl_liver_t);
%               region_as_func_of_thresh = [ region_as_func_of_thresh, sum(bw_cv_(:))];
%               
%           end   
%            
%           orig_ct_im_sl1(~CH_dilate)=0; 
%           CH_in_lung = bwconvhull(orig_ct_im_sl1,'objects');
          

%         plot_it = 0;
%         if (plot_it)
%             global curr_image_name 
%             figure;
%                 subplot(2,3,1);
%                     hist(double(orig_ct_im_sl1(orig_ct_im_sl1>0)),50);
%                     title([ 'Subject name: '  num2str(curr_image_name)]);  
%                     grid on;
%                     colormap(jet) 
%                     freezeColors
%                 subplot(2,3,2); 
%                     imagesc(CH_in_lung);
%                     imagesc(orig_ct_im_sl_liver);  
%                     title(['Slice: ' num2str(Lung_Axial_Biggest_Slice) ]);
%                      colormap(jet) 
%                      freezeColors
%                 subplot(2,3,3);
%                     imagesc(orig_ct_im_sl1);   
%                     colormap(gray) 
%                     freezeColors  
%                 subplot(2,3,4);                
%                    level = graythresh(orig_ct_im_sl1);
%                    [level EM] =  graythresh(orig_ct_im_sl1);
%                    BW = im2bw(orig_ct_im_sl1,level);
%                    BW(mid_sagittal,:)=1; 
%                    BW(:, mid_coronal)=1;
%                    imagesc(BW); 
%                 subplot(2,3,5);   
%                      plot(examine_th, region_as_func_of_thresh); 
%                      title( num2str(max(liver_gl)));  
%                 subplot(2,3,6);
%                    two_std = Heart_GrayLevel_mean + 2*Heart_GrayLevel_std; 
%                    hold on;
%                    grid on; 
%  
%                    hist(tm ,50);   
%                    title({['Mean: ' num2str(Heart_GrayLevel_mean) ', STD: ' num2str(Heart_GrayLevel_std)] ... 
%                         ,['68% : ' num2str(Heart_GrayLevel_mean - Heart_GrayLevel_std) ' -- ' num2str(Heart_GrayLevel_mean + Heart_GrayLevel_std) ]...
%                         ,['95% : ' num2str(Heart_GrayLevel_mean - 2*Heart_GrayLevel_std) ' -- ' num2str(Heart_GrayLevel_mean + 2*Heart_GrayLevel_std) ]}  ) ;
% 
% 
%     %                   
%     %            STOP_HERE();   
%     %            eee=1;    
%         end
%   
           
         
    end
        
% %%        
%     % pre axial-plane-sum processing....
%     % --- 
%     s1 = size(img_,1); 
%     s2 = size(img_ ,2);
%     img__ = reshape(img_, s1*s2, []);
% 
%     % sum over the axial plane
%     % ---  
%     SM = sum(img__,1);
%     SM = smooth(SM); 
%      
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     SM_t = SM;  
%     SM_t(1:round(length(SM)/2))=0;
%     [max_p ,I_max] = max( SM_t );  
%     SM(I_max(1):end) = max_p;
%     SM(1:100) = max_p;
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     
%     
%     [min_p ,I_min] = min(SM);
% 
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     % ~~~~~~~~~~~~~ save markers.
%     c_im = img_(:,:,I_min);
%     orig_ct_im_sl = orig_ct_im(:,:,I_min);
%     orig_ct_im_sl(~c_im) = 0;
%    
% 
%     prp = regionprops(c_im, 'Extrema' )
%     global CP  
%     ix = PB_ip_shortcuts.get_image_idx( )
%     CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_Axial_smalles_Slice', ...
%             I_min);  
%     mean_ =  mean(orig_ct_im_sl(orig_ct_im_sl>150));
%     CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_GrayLevel_mean', ...
%           mean_ );  
%     st_ = std(double(orig_ct_im_sl(orig_ct_im_sl>150)));
%     CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_GrayLevel_std', ...
%           st_  ); 
%           
%     try     
%         CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_Axial_smalles_Slice_Ytop',...
%              prp.Extrema(1,2));
%         CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_Axial_smalles_Slice_Ybottom', ...
%             prp.Extrema(6,2));
%            
%         if (0) 
%            sss = orig_ct_im_sl(orig_ct_im_sl>0);
%            hist(double(sss),50)  
%            STOP_HERE();
%            eee=1;   
%         end
%     catch
%          warning('@@@@@@ Bones_Axial_smalles_Slice_Ytop    can''t be set ');
%          warning('@@@@@@ Bones_Axial_smalles_Slice_Ybottom can''t be set ');
% %          [ymax,imax,ymin,imin] = extrema(SM);
% %          global mn mx
% %          mx = sort(imax); mn = sort(imin);
% %          addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
% %          STOP_HERE(); 
%     end
% %     
% %     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
% %     STOP_HERE() ; 
%        
%     f1 = figure('visible','off'); 
%         hold on; 
%         plot(SM,'k');
%         plot(I_min(1), min_p ,'k*','MarkerSize',10);
%         text(I_min(1), round((7)*min_p) ,num2str(I_min(1)));
%         %xlim([100 300])
% 
%         grid on;
%         
%    res = hardcopy(f1, '-dzbuffer', '-r0');        
% end
    
    
    end
end

