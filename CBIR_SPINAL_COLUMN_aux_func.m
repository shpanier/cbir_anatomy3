classdef CBIR_SPINAL_COLUMN_aux_func
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
     
     methods(Static)
 
        
    
    %% segment_SPINAL_COLUMN_sage1
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_SPINAL_COLUMN_sage1
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [] = segment_SPINAL_COLUMN_sage1(enabled_) 
        if (~enabled_), return , end
        global im_idx  
            
              %%
        PB_shortcuts.set_new_image_index();enabled_=1;
        % PB.add_im_p( 'forceRecalculation', 1) ; 
        % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        PB.add_im_P_p( 'NEW', 'NEW') 
            PB.add_im_P_p( 'id', 1);
            PB.add_im_P_p( 'Enabled', 1);
            PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp'); 
                % paramter indexes  
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im']);
 
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                PB.add_im_P_Prm_p( 'prm_value', '''bwconvhull_objects'''  );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                PB.add_im_P_Prm_p( 'prm_value', '3' );
            
        % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        PB.add_im_P_p( 'NEW', 'NEW')
        PB.add_im_P_p( 'id', 1);
        PB.add_im_P_p( 'Enabled', 1);
        PB.add_im_P_p( 'MethodName', 'largest_conncomp');
        PB.add_im_P_p( 'MethodFilesLocation', ...
                       'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');
  
                % paramter indexes 
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
                
                
        % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 
%         PB.add_im_P_p( 'NEW', 'NEW') 
%         PB.add_im_P_p( 'id', 1); 
%         PB.add_im_P_p( 'Enabled', 1);
%         PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
%         PB.add_im_P_p( 'MethodFilesLocation', ...
%                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp'); 
%             % paramter indexes  
%             % ~~~~~~~~~~~~~~~~
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%             PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%             PB.add_im_P_Prm_p( 'prm_value', '''bwconvhull'''  );
% 
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%             PB.add_im_P_Prm_p( 'prm_value', '3' );
                
    end
    
    %% segment_SPINAL_COLUMN_sage2
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_SPINAL_COLUMN_sage2
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [] = segment_SPINAL_COLUMN_sage2(enabled_)
         if (~enabled_), return , end
          global im_idx  
           PB_shortcuts.set_new_image_index(); enabled_=1;

           PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 2); 
            PB.add_im_P_p( 'Enabled', 1);
            PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
                % paramter indexes 
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im'] );
 
                PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                PB.add_im_P_Prm_p( 'prm_value', '''imclose'''  );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                PB.add_im_P_Prm_p( 'prm_value', '1' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
                PB.add_im_P_Prm_p( 'prm_value', '''disk''' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );  
                PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                PB.add_im_P_Prm_p( 'prm_value', '18' );
                
                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                PB.add_im_P_Prm_p( 'prm_value', '0' );
    end
    
    %% Aorta_n_Spinal_BoundingBox
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Aorta_n_Spinal_BoundingBox
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [img__] = Aorta_n_Spinal_BoundingBox(img_)
       
          img__ = ones(size(img_)); 
          [img__ ] = CBIR_SPINAL_COLUMN_aux_func.extract_spinal_from_BonesSeg_and_Limits( img__  ); 
       
    end
  
    %% extract_spinal_from_BonesSeg_and_Limits   
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * extract_spinal_from_BonesSeg_and_Limits
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [BonesSeg ] = extract_spinal_from_BonesSeg_and_Limits( BonesSeg  )
   %       STOP_HERE(); 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        % ===> spinal lower and upper bounds (Axial)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        global CP       curr_image_name ;  
 
        ix = PB_ip_shortcuts.get_image_idx( )
        Kidney_Left_Axial_lowerBound_Slice =  CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice
        spinal_Axial_upper_bound =  CP.ip(ix).Lung_Axial_Biggest_Slice 
 
              
        Bones_A_smalles_Slice_Ytop    = CP.ip(ix).Bones_Axial_smalles_Slice_Ytop
        Bones_A_smalles_Slice_Ybottom = CP.ip(ix).Bones_Axial_smalles_Slice_Ybottom
          
        Bronchi_Coronal_End_Slice         = CP.ip(ix).Bronchi_Coronal_End_Slice
         if ((Bronchi_Coronal_End_Slice==-1) && (~isempty(strfind(curr_image_name, 'Moti')))) 
            Bronchi_Coronal_End_Slice = 200;
        end
        SPINAL_w_raito_top = 0.6;
        SPINAL_w_raito_bot = 0.3;
    
       %   img__ = ones(size(img_)); 
        w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
        SPINAL_w_raito1 = round(SPINAL_w_raito_top*w_);
        SPINAL_w_raito2 = round(SPINAL_w_raito_bot*w_);

        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        %   AD HOC -- to fix with hilbert transform
        %      http://blogs.mathworks.com/pick/2014/05/23/automatic-activity-detection-using-hilbert-transform/
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    
%         spinal_Axial_lowwer_bound = spinal_Axial_lowwer_bound -50;
%         if (spinal_Axial_lowwer_bound<0)
%             spinal_Axial_lowwer_bound=1;
%         end
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        
        BonesSeg([1:(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1) ...
                    (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito2):end  ]   ,   ...
             : , ...
             : )=0; 
         BonesSeg(:   ,   ...
              : , ...       
             [1:Kidney_Left_Axial_lowerBound_Slice spinal_Axial_upper_bound:end] )=0;
         BonesSeg(:  ,   ...
              [1:round(0.85*Bronchi_Coronal_End_Slice)]  , ...
             :)=0; 
    
    end
    
   function [BonesSeg ] = extract_spinal_from_BonesSeg_and_Limits1( BonesSeg  )
   %       STOP_HERE(); 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        % ===> spinal lower and upper bounds (Axial)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        global CP       curr_image_name ;  
 
        ix = PB_ip_shortcuts.get_image_idx( )
        Kidney_Left_Axial_lowerBound_Slice =  CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice
        spinal_Axial_upper_bound =  CP.ip(ix).Lung_Axial_Biggest_Slice 
 
              
        Bones_A_smalles_Slice_Ytop    = CP.ip(ix).Bones_Axial_smalles_Slice_Ytop
        Bones_A_smalles_Slice_Ybottom = CP.ip(ix).Bones_Axial_smalles_Slice_Ybottom
          
        Bronchi_Coronal_End_Slice         = CP.ip(ix).Bronchi_Coronal_End_Slice
         if ((Bronchi_Coronal_End_Slice==-1) && (~isempty(strfind(curr_image_name, 'Moti')))) 
            Bronchi_Coronal_End_Slice = 200;
        end
        SPINAL_w_raito_top = 0.6;
        SPINAL_w_raito_bot = 0.3;
    
       %   img__ = ones(size(img_)); 
        w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
        SPINAL_w_raito1 = round(SPINAL_w_raito_top*w_);
        SPINAL_w_raito2 = round(SPINAL_w_raito_bot*w_);

        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        %   AD HOC -- to fix with hilbert transform
        %      http://blogs.mathworks.com/pick/2014/05/23/automatic-activity-detection-using-hilbert-transform/
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    
%         spinal_Axial_lowwer_bound = spinal_Axial_lowwer_bound -50;
%         if (spinal_Axial_lowwer_bound<0)
%             spinal_Axial_lowwer_bound=1;
%         end
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        
        BonesSeg([1:(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1) ...
                    (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito2):end  ]   ,   ...
             : , ...
             : )=0; 
         BonesSeg(:   ,   ...
              : , ...       
             [1:Kidney_Left_Axial_lowerBound_Slice ] )=0;
         BonesSeg(:  ,   ...
              [1:round(0.85*Bronchi_Coronal_End_Slice)]  , ...
             :)=0; 
    
    end
    
   
     end
    
     
     
end





%         function [img_] = segment_SPINAL_COLUMN_sage2_old(img_)
%          global CP SPINAL_w_raito 
%          ix = PB_ip_shortcuts.get_image_idx( )
%           Bones_A_smalles_Slice_Ytop    = CP.ip(ix).Bones_Axial_smalles_Slice_Ytop;
%           Bones_A_smalles_Slice_Ybottom = CP.ip(ix).Bones_Axial_smalles_Slice_Ybottom;
%           Bones_Axial_smalles_Slice         = CP.ip(ix).Bones_Axial_smalles_Slice;
%           Bronchi_Axial_End_Slice         = CP.ip(ix).Bronchi_Axial_End_Slice;
% 
% %                    addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
% %          STOP_HERE();    
%          SPINAL_w_raito = 0.5; 
%          w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
%          SPINAL_w_raito1 = w_*SPINAL_w_raito;
%         
%           jj_sl = [(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1): ...
%                     (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito1)] ;
%                 
%                 
%          spainal_range_from = (Bones_Axial_smalles_Slice-3);
%          spainal_range_to = (Bronchi_Axial_End_Slice+3);
%          spainal_range = [spainal_range_from:spainal_range_to];
%          win_size = round(length(spainal_range)/5);
%          
%          f_stp = spainal_range_from:3:spainal_range_to;
%          %linspace( spainal_range_from , spainal_range_to, 7);
%          
%          for ii = [1:(length(f_stp))]
%          %    ii
%              aa = [(f_stp(ii)):(f_stp(ii)+win_size)];
%              fprintf('*.');   
%              for jj=[jj_sl]
%                   
%                 img_(jj, : , aa) = ...
%                      bwconvhull(squeeze(img_(jj, : , aa)));
%                 
%              end
%                
%          end
%     end

% % % 
% % % 
% % %     function [] = segment_SPINAL_COLUMN_sage1(enabled_) 
% % %         if (~enabled_), return , end
% % %         global im_idx  
% % %             
% % %               %%
% % %         PB_shortcuts.set_new_image_index();enabled_=1;
% % %         % PB.add_im_p( 'forceRecalculation', 1) ; 
% % %         PB.add_im_P_p( 'NEW', 'NEW') 
% % %             PB.add_im_P_p( 'id', 1);
% % %             PB.add_im_P_p( 'Enabled', 1);
% % %             PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
% % %             PB.add_im_P_p( 'MethodFilesLocation', ...
% % %                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp'); 
% % %                 % paramter indexes  
% % %                 % ~~~~~~~~~~~~~~~~
% % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % %                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
% % %                 PB.add_im_P_Prm_p( 'prm_value', ['c_im']);
% % %  
% % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % %                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
% % %                 PB.add_im_P_Prm_p( 'prm_value', '''bwconvhull_objects'''  );
% % % 
% % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % %                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
% % %                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% % %             
% % % %         % Processing - simple_2D_to_3D
% % % %         % ~~~~~~~~~~ 
% % % %         PB.add_im_P_p( 'NEW', 'NEW')
% % % %         PB.add_im_P_p( 'id', 3);
% % % %         PB.add_im_P_p( 'Enabled', 1);
% % % %         PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
% % % %         PB.add_im_P_p( 'MethodFilesLocation', ...
% % % %                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
% % % %                 % paramter indexes  
% % % %                 % ~~~~~~~~~~~~~~~~
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% % % % 
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '''imfill'''  );
% % % %                  
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '1' );
% % % %                 
% % % %                 
% % % %         % Processing - simple_3D_SEmorph
% % % %         % ~~~~~~~~~~ 
% % % %         PB.add_im_P_p( 'NEW', 'NEW')
% % % %         PB.add_im_P_p( 'id', 2);
% % % %         PB.add_im_P_p( 'Enabled', 1);
% % % %         PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
% % % %         PB.add_im_P_p( 'MethodFilesLocation', ...
% % % %                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
% % % %                 % paramter indexes 
% % % %                 % ~~~~~~~~~~~~~~~~
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% % % % 
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '''imclose'''  );
% % % % 
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% % % % 
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '''disk''' );
% % % % 
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '2' );
% % % %                 
% % % %                  PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '0' );    
% % % %         
% % % %         % Processing - simple_2D_to_3D
% % % %         % ~~~~~~~~~~ 
% % % %         PB.add_im_P_p( 'NEW', 'NEW')
% % % %         PB.add_im_P_p( 'id', 3);
% % % %         PB.add_im_P_p( 'Enabled', 1);
% % % %         PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
% % % %         PB.add_im_P_p( 'MethodFilesLocation', ...
% % % %                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
% % % %                 % paramter indexes  
% % % %                 % ~~~~~~~~~~~~~~~~
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% % % % 
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '''imfill'''  );
% % % %                  
% % % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % % %                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
% % % %                 PB.add_im_P_Prm_p( 'prm_value', '2' );
% % % %                  
% % %         PB.add_im_P_p( 'NEW', 'NEW')
% % %         PB.add_im_P_p( 'id', 1);
% % %         PB.add_im_P_p( 'Enabled', 1);
% % %         PB.add_im_P_p( 'MethodName', 'largest_conncomp');
% % %         PB.add_im_P_p( 'MethodFilesLocation', ...
% % %                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');
% % %   
% % %                 % paramter indexes 
% % %                 % ~~~~~~~~~~~~~~~~
% % %                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % %                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
% % %                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% % %                 
% % %                 
% % % 
% % %         PB.add_im_P_p( 'NEW', 'NEW') 
% % %         PB.add_im_P_p( 'id', 1); 
% % %         PB.add_im_P_p( 'Enabled', 1);
% % %         PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
% % %         PB.add_im_P_p( 'MethodFilesLocation', ...
% % %                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp'); 
% % %             % paramter indexes  
% % %             % ~~~~~~~~~~~~~~~~
% % %             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % %             PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
% % %             PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% % % 
% % %             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % %             PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
% % %             PB.add_im_P_Prm_p( 'prm_value', '''bwconvhull'''  );
% % % 
% % %             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% % %             PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
% % %             PB.add_im_P_Prm_p( 'prm_value', '3' );
% % %                 
% % %     end
% % % 
