clc  
close all   
clear all   
                        
% -      - -  -  -  -  -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -     
% lung 
% - done --> 

% curr_image_name = 'Moti'  --> chnage to configuration       
 % *112   *12 7      ***129  **131   
          
%  subj = [ 100 104 105 106 108 109 110 111 112 113 127 128 129 130 131 132 133 134 135 136  ] 
   
% 135 105    127             
%  100 104 105 106 108 109 110 111 112 113 127  128 129 130 131 132 133 134 135 136
hostname = char( getHostName( java.net.InetAddress.getLocalHost ) )
silver_subjects = [ 101 , 107, 114, 115, 116, 117, 118, 119, 120, 121, 122, 124, 125, 126, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162,163, 164, 165, 166, 167, 168,169, 170, 171, 171, 172, 173,174,175, 177, 178,179,180,181,183,184, 185,186, 187,188,189,190,191,192,193,194,196,198,199,200,201,203,204,205] 

DATA_PATH = ''    
output_PATH = ''  
global VISCERAL_OUT_DIR VISCERAL_TMP CODE_DIR 
% PY_EXEC
if (strcmp( hostname , char('LAPTOP-JCS65RGV')))
    data_type_ =  'sliver_dataset';
     %PY_EXEC = 'C:/Users/User/Anaconda2/python.exe';
       
    % When running on silver data-set.
    % - - - - - - - - - - - - - - - -
    if (strcmp(data_type_, 'sliver_dataset'))
        DATA_PATH = 'E:/visceral-dataset/SilverCorpus/Volumes/';
        VISCERAL_OUT_DIR = 'E:/VISERAL_OUT';
        CODE_DIR = 'C:/BITBUCKET/cbir_anatomy3'    
        
    else 
        DATA_PATH = 'C:/DATA/VISCERAL/volumes/';
        VISCERAL_OUT_DIR = 'C:/VISERAL_OUT';
        
    end
       
    output_PATH = [ VISCERAL_OUT_DIR '/seg_out' ] ;  
    VISCERAL_TMP_PREFIX = [ VISCERAL_OUT_DIR  '/INTERMEDIATE_RES'];

elseif (isequal( hostname , 'AAA')) 
    DATA_PATH = 'D:/DATA/VISCERAL/volumes/';
    VISCERAL_OUT_DIR = 'D:/VISERAL_OUT';
    output_PATH = 'd:/seg_out' ;
end

%assert( exist(PY_EXEC,'file')==2  , 'Can not find python.exe');
    
% 'file2save_imagine' is a file that will hold all intermediate images 
%  the eventually (after the loop below :) will be display by imagine.
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -     
global file2save_imagine         
file2save_imagine = [ VISCERAL_OUT_DIR '/images_for_global_imagine.mat' ]; 
delete(file2save_imagine);     % init an empty file.
   
addpath( [CODE_DIR '/protoBuf'] );
%                  
  
% LUNG
% - - - - - 101 -  
% not tested:  107
% PROBLEM: 107, 168, 180 , 157 , 158,- lung problem...  
% DONE: 115, 116, 117, 118, 119, 120  181,183,184, 185,186, 187,188,189

% Right kidney
% --------------
% FAILED:    157, 178, 180, 181,  192 , 193,  198

% OK: 114, 115, 116, 117, 118, 119, 120, 121,  122, 124 , 125,126,   150,
%      151, 152,  154, 155, 156, 158, 159, 160, 161, 162,163, 164, 165, 166,
%     167, 168,169, 170, 171, 171, 172, 173,  174,175, 177, 179,
%     183,184, 185,186, 187,188,189,190,191,194,196, 199,200,201,203,204,205

% left Lung  
% ------ --------
% OK: 115, 116, 117, 118, 119, 120  181,183,184, 185,186, 187,188,189
%      114, 122, 124 , 125,126,   150,  151, 152,  154, 155, 156, 158, 159, 160, 161, 162,163, 164, 165, 166
%     167, 168,169, 170, 171, 171, 172, 173,  174,175, 177, 179,  183,184, 185,186, 187,188,189,190,191,
%      196, 199,200,201,203,204,205
%
% NOT-OK: 194,157, 178, 180, 181,  192 , 193,  198
% 
% not tested:   
%          
%     
%     
%  

% liver
% ------ --------
% OK: 115, 116, 117, 118, 119, 120  181,183,184, 185,186, 187,188,189
%      114, 122, 124 , 125,126,   150,  151, 152,  154, 155, 156, 158, 159, 160, 161, 162,163, 164, 165, 166
%     167, 168,169, 170, 171, 171, 172, 173,  174,175, 177, 179,  183,184, 185,186, 187,188,189,190,191,
%      196, 199,200,201,203,204,205
%
% NOT-OK: 194,157, 178, 180, 181,  192 , 193,  198
% 

  

for subj = [  100 ]       %%%%    
                                                                                                                                                                                                                                                        
    curr_image_name = ['10000' num2str(subj) '_1_CTce_ThAb'];    
    VISCERAL_TMP = [ VISCERAL_TMP_PREFIX '/' curr_image_name]
    
    file_as_URL  = [DATA_PATH curr_image_name '.nii.gz']
        
    ConfigerID  =  '1'    
   % RadID = '1247'   %  Trachea                
   %    RadID = '1326'  % left Lung     
   %   RadID = '1302'  % right  Lung  
                                           
   RadID = '58'  % liver                      
 %    RadID = '86'     % Spleen                    
   %  RadID = '29662' % right kidney   
   %   RadID =   '29663'    % left  kidney    
    to_exit = 0                                                                                                      
    dry_run = 0                    
                         
    VISCERAL_SUBMIT_MODE = 1; % TODO: remove this flag.
        
     
    VISCERAL_RUNER( file_as_URL , output_PATH , ConfigerID, RadID ,  ...
        to_exit  , dry_run  )
  
                                                                            
end                                         
                                  
%% Display Resutls
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% * *            
% * *   Display Resutls 
% * *                  
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                              
display_results = 1; 
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2');              
if (display_results==1)
    clearvars -except file2save_imagine
    close all    
       
    load(file2save_imagine) 
    imagine_str = [ 'imagine_v2( ' imagine_global_str(2:end) ' ); ' ];
    eval(imagine_str)
end 
        