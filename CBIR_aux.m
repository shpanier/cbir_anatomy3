classdef CBIR_aux
    % Holds general auxiliary functions
     
     methods(Static) 
 
     %% hist_of_every_slice
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % 
    %  This function get a 3-D metrics not plot a histogram of every slice    
    %
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    function [hist_3d_mat] = empty_check(in_3d_mat, lo_th_ , hi_th_) 
        
        
        
    end
    %% hist_of_every_slice
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % 
    %  This function get a 3-D metrics not plot a histogram of every slice    
    %
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    function [hist_3d_mat] = hist_of_every_slice(in_3d_mat, lo_th_ , hi_th_) 
          addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\export_fig') ;
          [numrows numcols] = deal(512,512); 
          num_of_slices_= size(in_3d_mat,3);
          hist_3d_mat = zeros(numrows, numcols, ...
                num_of_slices_,'uint8');
          % STOP_HERE();  
          for ii=1:num_of_slices_
              ii ;  
             curr_sl = in_3d_mat(:,:,ii); 
             % kidney_sl_o = kidney_sl_;  
              curr_sl(curr_sl<lo_th_ | curr_sl>hi_th_)=0;
             
              if (max(curr_sl(:))==0)
                  continue
              end  
             %  kidney_sl_(kidney_sl_>300)=0;
             f = figure('Visible','off'); 
             hist(double(curr_sl(curr_sl>0)),500);   
             A = rgb2gray(print2array(f));
             B = imresize(A, [numrows numcols]);
             hist_3d_mat(:,:,ii) = B;
             close(f); 
          end 
         %  STOP_HERE(); 
    end
    
    
     %% hist_of_every_slice
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % 
    %  This function get a 3-D metrics not plot a histogram of every slice    
    %
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    function [kmeans_3d_mat] = kmeans_of_every_slice(in_3d_mat, lo_th_ , hi_th_) 
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\clustering\kmeans_fast_Color')
        num_of_slices_= size(in_3d_mat,3);
   
        kmeans_3d_mat = zeros(size(in_3d_mat),'uint8');
          % STOP_HERE();  
        for ii=1:num_of_slices_
             ii ;  
             curr_sl = in_3d_mat(:,:,ii); 
             % kidney_sl_o = kidney_sl_;  
             curr_sl(curr_sl<lo_th_ | curr_sl>hi_th_)=0;
             
             if (max(curr_sl(:))==0)
                  continue
             end  
             im_o = kmeans_fast_Color(curr_sl,3);
  
             kmeans_3d_mat(:,:,ii) = im_o;
%             close(f); 
        end   
        %   STOP_HERE(); 
          
    
    
     end
     end
end

