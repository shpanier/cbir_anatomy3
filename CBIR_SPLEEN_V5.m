  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];           nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
% PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT(); 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load SPINAL_STRIP_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['SPINAL_STRIP_'];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , 'load SPINAL_STRIP_' ) 
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> The Aorta's triu mask  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Aorta''s triu mask');  
    PB.add_im_Process(    'CBIR_LIVER_aux_func.segment_LIVER_triu', '');
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );
 
    PB.add_im_Process(    'LogicalAssignment', ...    
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');      
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );
         PB.add_im_Process_parm('B' ,              ['~logical(c_im_res'  num2str(im_idx) ')']);
         PB.add_im_Process_parm('C' ,  '0')    
           
    % Remove the right area to the Sagittal_mid_Slic     
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~     
    PB.add_im_Process(    'CBIR_SPLEEN_aux_func.Sagittal_mid_Slice_Coarse_ROI', ...
                                      'C:\Dropbox\MY_CODE\PHD\CBIR'); 
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );     

    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % Remove non-body region'
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    
    file_prefix = ['BODY_'];   nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
    %PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 

    %    continue   
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index('Remove non-body region'); 
        PB.add_im_Process(    'LogicalAssignment', ...    
        'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');    
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] ); 
        PB.add_im_Process_parm('B' ,              ['~logical(c_im_res' num2str(im_idx-1) ')']);
        PB.add_im_Process_parm('C' ,  '0') 
     
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> remove KIDNEY_LEFT_V4_
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['KIDNEY_LEFT_V5_'];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , 'load KIDNEY_LEFT_V4_'  )

    PB.add_im_Process(  'simple_3D_SEmorph.run', ...
                          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
            PB.add_im_Process_parm('oper_type' , '''imdilate''' ); 
            PB.add_im_Process_parm('dim_' , '3' );  
            PB.add_im_Process_parm('SE1' , '''disk''' ); 
            PB.add_im_Process_parm('SE2' , '3' );    
            PB.add_im_Process_parm('SE3' , '0' ); 
            
    %PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 

    PB_shortcuts.set_new_image_index(' KIDNEY_LEFT_V4_  '); 
        PB.add_im_Process(    'LogicalAssignment', ...  
             'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');    
             PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );
             PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-1) ')']);
             PB.add_im_Process_parm('C' ,  '0')   
     
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load BONE_SKELETON_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['BONE_SKELETON_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
    % close the chest 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
    %  PB_shortcuts.set_new_image_index(); 
        CBIR_LIVER_aux_func.segment_LIVER_close_chest(); % -3

    % Remove bones... 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index('Remove bones...'); 

    PB.add_im_Process(    'LogicalAssignment', ...   
             'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');    
             PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );
             PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-1) ')']);
             PB.add_im_Process_parm('C' ,  '0')   
             
              
% major change at 30/08/2015 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Spleen/kidny Thresholding 1'); 
% PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ;  
    global CP  
    ix = PB_ip_shortcuts.get_image_idx( );
% % % % % % % % % % % % % % %         up_  = round((CP.ip(ix).Kidney_Left_GrayLevel_thr1)) - ...
% % % % % % % % % % % % % % %                    round((CP.ip(ix).Kidney_Left_GrayLevel_thr2)) ; 
        up_ =  round(round(str2num(CP.ip(ix).prm104))) 
           
        lo_ =  round(((CP.ip(ix).GrayLevel_thr1))) - 0.3*round((CP.ip(ix).GrayLevel_thr2));
    if (lo_ < 0)    
       lo_ = 40;
    end
      
    PB.add_im_Process(  'threshold_image_BW', ...   
                     'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
        PB.add_im_Process_parm('prm_value', ...      
              num2str(lo_) ); 
        PB.add_im_Process_parm('prm_value',  ...          
              num2str(up_) );  
 
 PB.add_im_Process(    'largest_conncomp', ...  
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
          PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res'  num2str(im_idx)] ); 
 
 PB.add_im_Process(  'simple_2D_to_3D.run', ... 
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
        PB.add_im_Process_parm('oper_type' , '''imfill''' );
        PB.add_im_Process_parm('dim_' , '3' ); 
         
  PB.add_im_Process(  'simple_2D_to_3D.run', ...  
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
        PB.add_im_Process_parm('oper_type' , '''bwareaopen''' );
        PB.add_im_Process_parm('dim_' , '3' );         
 
PB_shortcuts.set_new_image_index(' gross_SPLEEN_segment ...'); 
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ;     
    PB.add_im_Process('CBIR_SPLEEN_aux_func.gross_SPLEEN_segment', ...
                         ' ');      
   PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im' ] ); %  ['c_im_res' num2str(im_idx)] );
   
% % 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Spleen -- Smooth_gross_Seg_WOct_WsliceNumber_as_seed'); 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  

ix = PB_ip_shortcuts.get_image_idx( );
sliceNumber_as_seed_  = CP.ip(ix).Kidney_Left_Axial_upperBound_Slice+1;
upperBound_Slice_ =  CP.ip(ix).Lung_Axial_Biggest_Slice;  
lowerBound_Slice_ =  CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice;

object_w_max_intersect_only =  1;  % <-- this is exatctly what we search for!
epsilon_f_segmentation_size = -1;  % not relevant in this case...
max_allow_thres =             -1;  % not relevant in this case...
dbg_ =0;     
SCS_type =                    4 ;  % <-- Consecutive slices with the largerest
                                     %     Intersect objects  
% dbg_gt_slice = 437  ;      
% dbg_lt_slice  =  437;  
% dbg_gt_slice = 1437  ;      
% dbg_lt_slice  =  0 ; % 415   
 dbg_lt_slice = -1; dbg_gt_slice = -1;
% % % % 
% % % % %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % % % smooth it...    
% % % % %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB.add_im_Process(    'CBIR_LEAK_REMOVAL_v1.Smooth_gross_Seg_WOct_Wslice_as_seed', ...
             'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');      

         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ...    
                           ['c_im_res' num2str(im_idx-2)] );    % gross segment
         PB.add_im_Process_parm('slice_seed' , ...       
                          ['squeeze(c_im_res' num2str(im_idx-1) '(:,:,' num2str(sliceNumber_as_seed_) ' ))'] );                % slice_seed 
         PB.add_im_Process_parm('sliceNumber_as_seed' , ...
                          num2str(sliceNumber_as_seed_) );    % sliceNumber_as_seed  

         PB.add_im_Process_parm('to_upper' , ...       
                          num2str(upperBound_Slice_) );   % to_upper   
         PB.add_im_Process_parm('to_lower' , ...   
                          num2str(lowerBound_Slice_));    % to_lower 
         PB.add_im_Process_parm('max_allow_thres' , ...     
                          num2str(max_allow_thres));   % max_allow_thres             
         PB.add_im_Process_parm('object_w_max_intersect_only' , ...      
                            num2str(object_w_max_intersect_only));            % max_allow_thres             
         PB.add_im_Process_parm('SCS_type' , ...       
                                num2str(SCS_type));                            % SCS_type    
         PB.add_im_Process_parm('epsilon_f_segmentation_size' , ...       
                             num2str(epsilon_f_segmentation_size));     % stop cratria                
         PB.add_im_Process_parm('dbg_gt_slice' , ...      
                            num2str(dbg_gt_slice));                             % dbg_gt_slice             
         PB.add_im_Process_parm('dbg_lt_slice' , ...      
                            num2str(dbg_lt_slice));                             % dbg_lt_slice    

                        
PB.add_im_Process(    'largest_conncomp', ...  
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
          PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res'  num2str(im_idx)] ); 
          
PB.add_im_Process(  'simple_3D_SEmorph.run', ...
            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
        PB.add_im_Process_parm('oper_type' , '''imclose''' );
        PB.add_im_Process_parm('dim_' , '3' );
        PB.add_im_Process_parm('SE1' , '''disk''' );
        PB.add_im_Process_parm('SE2' , '3' ); 
        PB.add_im_Process_parm('SE3' , '0' );  

        % strel('square',2) 
%  PB.add_im_Process(  'simple_3D_SEmorph.run', ...
%                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%             PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
%             PB.add_im_Process_parm('oper_type' , '''imdilate''' ); 
%             PB.add_im_Process_parm('dim_' , '3' );  
%             PB.add_im_Process_parm('SE1' , '''disk''' ); 
%             PB.add_im_Process_parm('SE2' , '1' );    
%             PB.add_im_Process_parm('SE3' , '0' ); 
%             
             
%PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();




% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it!! --  SPLEEN_V3
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    out_file_prefix_ = 'SPLEEN_V2';   out_file_suffix = [ ];  
out_file_prefix_ = ['SPLEEN_V5_'] ;     out_file_suffix = [];   out_dir = TMP_DIR;    
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   ;

% % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% Left_Kidney_POST_Segmentaion_stat
% % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();    
       
PB.add_im_Process(  'CBIR_SPLEEN_aux_func.SPLEEN_POST_Segmentaion_stat', ...
                      ' ');  
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );   % final segmentation
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-11)] );  % orig CT
   

% % % % 
% % % %                         
% % % %  
% % % % % 
% % % % % %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% % % % PB_shortcuts.set_new_image_index(' im-fill ');  
% % % % PB.add_im_Process(  'simple_2D_to_3D.run', ...
% % % %                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
% % % %             PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im'] );   
% % % %             PB.add_im_Process_parm('oper_type' , '''imfill''' ); 
% % % %             PB.add_im_Process_parm('dim_' , '3' ); 
% % % %              
% % % %   PB.add_im_Process(    'largest_conncomp', ...  
% % % %          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
% % % %           PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res' num2str(im_idx)] ); 
% % % % %       
% % % % %
% % % % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% % % % % ===> remove KIDNEY_LEFT_V5_
% % % % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% % % % file_prefix = ['KIDNEY_LEFT_V5_'];    
% % % % PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , 'load KIDNEY_LEFT_V4_'  )
% % % % 
% % % %   
% % % % %PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 
% % % %  
% % % % PB_shortcuts.set_new_image_index(' KIDNEY_LEFT_V4_  '); 
% % % %     PB.add_im_Process(    'LogicalAssignment', ...  
% % % %          'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   
% % % %          PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );
% % % %          PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-1) ')']);
% % % %          PB.add_im_Process_parm('C' ,  '0')   
% % % %          
% % % %           
% % % %          
% % % % %PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();  
% % % %   
% % % %   
% % % %   
% % % % 
% % % % %%      
% % % % 
% % % % % PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
% % % % 
% % % %     
% % % %     
% % % %     
     
    
    
    
    
    
    
    
    
    
                          
                        
    
% % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% PB_shortcuts.set_new_image_index(' kmean3 - select 3  '); 
%           
    
%   PB.add_im_Process(    'LogicalOperators', ...
%          'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   %  A(A 'oper_' B) = C
%          PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
%          PB.add_im_Process_parm('oper_' ,  '''==''')       
%          PB.add_im_Process_parm('B' ,     '3' );
%          PB.add_im_Process_parm('C' ,  '1') 
          
% % %            
% % %  
% % % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  PB_shortcuts.set_new_image_index('Coarse_ROI refiments 2');
% % % PB.add_im_Process('imfill', ' ');       
% % %     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
% % %     PB.add_im_Process_parm('conn' , '18' ); 
% % %     PB.add_im_Process_parm('holes' , '''holes''' );
% PB.add_im_Process(    'largest_conncomp', ... 
%          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');   
%           PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );  
  

%PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();   
     