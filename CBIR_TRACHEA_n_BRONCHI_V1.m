
	
 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];          nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load ) 
%  PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 
%PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load LUNGS_n_TRACHEA_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['LUNGS_n_TRACHEA_'];   nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 



%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  TRACHEA   threshold  
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index(); 
 
PB.add_im_Process(    'CBIR_TRACHEA_aux_func.TRACHEA_threshold', ...
				      'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');  
		PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );    % orig_ct 
		
		
   
		
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%   Extract the gross segmentation... 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index();  
PB.add_im_Process(    'LogicalAssignment', ...    
	 'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');      
	 PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-1)] );  
	 PB.add_im_Process_parm('B' ,              ['~logical(c_im_res'  num2str(im_idx-2) ,')']);
	 PB.add_im_Process_parm('C' ,  '0') 

	 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index();          
 PB.add_im_Process(  'simple_3D_SEmorph.run', ...
					  'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
		PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] ); 
		PB.add_im_Process_parm('oper_type' , '''imopen''' ); 
		PB.add_im_Process_parm('dim_' , '3' );  
		PB.add_im_Process_parm('SE1' , '''disk''' ); 
		PB.add_im_Process_parm('SE2' , '4' );       
		PB.add_im_Process_parm('SE3' , '0' ); 
		

		
			
		  
% ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index();    
PB.add_im_Process(  'CBIR_TRACHEA_n_BRONCHI_aux_func.extract_TRACHEA_n_BRONCHI', ... 
			  'C:\Dropbox\MY_CODE\PHD\CBIR');     
   PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im' ] );      
		
% PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
 		 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it!      
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = 'TRACHEA_n_BRONCHI_V1_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   
  

