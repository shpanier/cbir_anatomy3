classdef PB_ip_shortcuts
    methods(Static)
 
    %% get_index_by_image_name
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * get_index_by_image_name
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [idx] = get_index_by_image_name( image_name_ )
        
        global CP
        
        % -1, indicate missing image
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        idx = -1;
        
        for ii = [1:length(CP.ip)]  
            % disp(['strcmp(' CP.ip(ii).ImageName ' , ' image_name_ ')']);
             
            % search of the location of the given image name
            % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
            if (strcmp(CP.ip(ii).ImageName, image_name_))
                idx = ii;
                return
            end
        end 
    end
    
    %% get_image_idx 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * get_image_idx (of the current image)
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [idx] = get_image_idx( )
       global CP 
       global curr_image_name 
       
%         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
%         STOP_HERE();
       [idx] = PB_ip_shortcuts.get_index_by_image_name( curr_image_name );
       if (idx < 0)
           idx = length(CP.ip)+1;
           if (idx==1)
                 CP = pblib_set(CP, 'ip', pb_read_ImagePrms());
           else
                 CP.ip(idx) = pb_read_ImagePrms(); 
           end
           CP.ip(idx) = pblib_set(CP.ip(idx), 'ImageName', curr_image_name); % .ImageName = curr_image_name;
       end
    end
    
    end
    
end

