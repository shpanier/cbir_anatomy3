
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];           nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
%PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
     
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load SPINAL_STRIP_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['SPINAL_STRIP_'];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , 'load SPINAL_STRIP_' ) 
          
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> The Aorta's triu mask  1
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Aorta''s triu mask');  
    PB.add_im_Process(    'CBIR_LIVER_aux_func.segment_LIVER_triu', '');
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );
          
 out_file_prefix_ = ['triu_mask_'] ;     out_file_suffix = [];   out_dir = TMP_DIR;    
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   ; 
  
    PB.add_im_Process(    'LogicalAssignment', ...     
         'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');       
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-3)] );
         PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx) ')']);
         PB.add_im_Process_parm('C' ,  '0')   

     
 

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load BONE_SKELETON_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['BONE_SKELETON_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
    % close the chest 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
  %  PB_shortcuts.set_new_image_index(); 
    CBIR_LIVER_aux_func.segment_LIVER_close_chest(); % -3
            
  % Remove bones...  2
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index('Remove bones...'); 

    PB.add_im_Process(    'LogicalAssignment', ...   
             'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');    
             PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );
             PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-1) ')']);
             PB.add_im_Process_parm('C' ,  '0')   
                   
         
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> remove KIDNEY_RIGHT_V5_
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['KIDNEY_RIGHT_V5_'];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , 'load KIDNEY_LEFT_V4_'  )

PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 
 
    PB.add_im_Process(  'simple_3D_SEmorph.run', ...
                          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
            PB.add_im_Process_parm('oper_type' , '''imdilate''' ); 
            PB.add_im_Process_parm('dim_' , '3' );  
            PB.add_im_Process_parm('SE1' , '''disk''' ); 
            PB.add_im_Process_parm('SE2' , '3' );    
            PB.add_im_Process_parm('SE3' , '0' ); 
            
    %  3
    PB_shortcuts.set_new_image_index(' KIDNEY_RIGHT_V5_  '); 
        PB.add_im_Process(    'LogicalAssignment', ...  
             'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');    
             PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );
             PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-1) ')']);
             PB.add_im_Process_parm('C' ,  '0')   
     
      
%%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===>   CBIR_LIVER_aux_func.LIVER_ROI 
%                Axial lower and upper bounderis.      4
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
PB_shortcuts.set_new_image_index(' LIVER_ROI define BY Aorta Stip');  
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
     PB.add_im_Process(    'CBIR_LIVER_aux_func.LIVER_ROI', ... 
        'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');   
         PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ...     
                                     ['c_im_res'  num2str(im_idx-1)] );   % liver seg (pre ROI define BY Aorta Stip) 
         PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ...  
                                  ['c_im_res'  num2str(im_idx-4)] );   % Aorta Stip

% major change at 30/08/2015  5
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Spleen/kidny Thresholding 1'); 
% 
    global CP 
    ix = PB_ip_shortcuts.get_image_idx( );
        up_  = round((CP.ip(ix).Kidney_Left_GrayLevel_thr1)) - ...
                   1.2*round((CP.ip(ix).Kidney_Left_GrayLevel_thr2)) ;
               
        lo_ =  round(((CP.ip(ix).GrayLevel_thr1))) - 0.5*round((CP.ip(ix).GrayLevel_thr2));
    if (lo_ < 0)     
       lo_ = 40; 
    end
      
    PB.add_im_Process(  'threshold_image_BW', ...   
                     'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
        PB.add_im_Process_parm('prm_value', ...      
              num2str(lo_) ); 
        PB.add_im_Process_parm('prm_value',  ...          
              num2str(up_) );  
  PB.add_im_Process(    'largest_conncomp', ...  
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
          PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res'  num2str(im_idx)] ); 

  PB.add_im_Process(  'simple_2D_to_3D.run', ...  
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
        PB.add_im_Process_parm('oper_type' , '''bwareaopen''' );
        PB.add_im_Process_parm('dim_' , '3' );  
        
   PB.add_im_Process(    'largest_conncomp', ...  
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
          PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res'  num2str(im_idx)] ); 
 
 %  PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();  
        
    %%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ===> load volume: LUNGS_LEFT_
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
    file_prefix = ['LUNG_LEFT2_V1_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
   %     
   %   PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ;      
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    %   HEART_SEPRATOR based on LUNGS_LEFT_   6
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    %  
    PB_shortcuts.set_new_image_index();    
        PB.add_im_Process(    'CBIR_LIVER_aux_func.HEART_SEPRATOR', ... 
            'C:\Dropbox\MY_CODE\PHD\CBIR\');   
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ...  
                                     ['c_im' ] );   % liver_heart_leftKidny_seg
    
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    %  remove the HEART_SEPRATOR..  7
    %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index(); 
         
    PB.add_im_Process(    'LogicalAssignment', ...
             'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');     
             PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-3)] );
             PB.add_im_Process_parm('B' ,              ['logical(c_im)']);
             PB.add_im_Process_parm('C' ,  '0') 
   
   PB.add_im_Process(    'largest_conncomp', ...  
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
          PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res'  num2str(im_idx)] ); 
   
   
   PB.add_im_Process(  'simple_2D_to_3D.run', ... 
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
        PB.add_im_Process_parm('oper_type' , '''imfill''' );
        PB.add_im_Process_parm('dim_' , '3' ); 
  %  
  

                                  
   
  
    % %  8
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index('Spleen -- Smooth_gross_Seg_WOct_WsliceNumber_as_seed'); 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 
    PB.add_im_Process(    'CBIR_LIVER_aux_func.FIND_LIVER_SLICE_AS_SEED', ... 
            'C:\Dropbox\MY_CODE\PHD\CBIR\');   
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ...    
                                     ['c_im_res' num2str(im_idx-1)]  );   % liver_heart_leftKidny_seg

                                 
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it!! --  LIVER_V5_
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    out_file_prefix_ = 'SPLEEN_V2';   out_file_suffix = [ ];  
out_file_prefix_ = ['LIVER_PRE_SMOOTH_V5_'] ;     out_file_suffix = [];   out_dir = TMP_DIR;    
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   ;
 
                              
                                 
%                                    
%     PB.add_im_Process(    'CBIR_LIVER_aux_func.SMOOTHE_LIVER_SURFACE', ... 
%             'C:\Dropbox\MY_CODE\PHD\CBIR\');   
%         PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ...    
%                                      ['c_im_res' num2str(im_idx-1)]  );   % liver_heart_leftKidny_seg
% 
%          [liver_gross_seg ] = SMOOTHE_LIVER_SURFACE( liver_gross_seg  )
%          
% % % %     ix = PB_ip_shortcuts.get_image_idx( );
% % % %     % largest CC of liver (from CBIR_LIVER_aux_func.FIND_LIVER_SLICE_AS_SEED(
% % % %     % - - - -  - - - - - - - - - - - - - - - - 
% % % %     sliceNumber_as_seed_  = CP.ip(ix).Liver_Axial_in_Slice;
% % % %     % - - - -  - - - - - - - - - - - - - - - -
% % % % 
% % % % 
% % % %     upperBound_Slice_ =  CP.ip(ix).Liver_Axial_upperBound_Slice;  
% % % %     lowerBound_Slice_ =  CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice;
% % % % 
% % % %     object_w_max_intersect_only =  1;  % <-- this is exatctly what we search for!
% % % %     epsilon_f_segmentation_size = -1;  % not relevant in this case...
% % % %     max_allow_thres =             -1;  % not relevant in this case...
% % % %     dbg_ =0;     
% % % %     SCS_type =                    4 ;  % <-- Consecutive slices with the largerest
% % % %                                          %     Intersect objects  
% % % % 
% % % %      dbg_lt_slice = -1; dbg_gt_slice = -1;
% % % %     % % % %  
% % % %     % % % % %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %     % % % % % smooth it...    
% % % %     % % % % %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %     PB.add_im_Process(    'CBIR_LEAK_REMOVAL_v1.Smooth_gross_Seg_WOct_Wslice_as_seed', ...
% % % %                  'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');      
% % % %  
% % % %              PB.add_im_Process_parm('IMAGE_TAMPLATE' , ...    
% % % %                                ['c_im_res' num2str(im_idx-1)] );    % gross segment
% % % %              PB.add_im_Process_parm('slice_seed' , ...       
% % % %                               ['largest_conncomp(squeeze(c_im_res' num2str(im_idx-1) '(:,:,' num2str(sliceNumber_as_seed_) ' )))'] );                % slice_seed 
% % % %              PB.add_im_Process_parm('sliceNumber_as_seed' , ...
% % % %                               num2str(sliceNumber_as_seed_) );    % sliceNumber_as_seed  
% % % % 
% % % %              PB.add_im_Process_parm('to_upper' , ...       
% % % %                               num2str(upperBound_Slice_) );   % to_upper   
% % % %              PB.add_im_Process_parm('to_lower' , ...   
% % % %                               num2str(lowerBound_Slice_));    % to_lower 
% % % %              PB.add_im_Process_parm('max_allow_thres' , ...     
% % % %                               num2str(max_allow_thres));   % max_allow_thres             
% % % %              PB.add_im_Process_parm('object_w_max_intersect_only' , ...      
% % % %                                 num2str(object_w_max_intersect_only));            % max_allow_thres             
% % % %              PB.add_im_Process_parm('SCS_type' , ...       
% % % %                                     num2str(SCS_type));                            % SCS_type    
% % % %              PB.add_im_Process_parm('epsilon_f_segmentation_size' , ...       
% % % %                                  num2str(epsilon_f_segmentation_size));     % stop cratria                
% % % %              PB.add_im_Process_parm('dbg_gt_slice' , ...      
% % % %                                 num2str(dbg_gt_slice));                             % dbg_gt_slice             
% % % %              PB.add_im_Process_parm('dbg_lt_slice' , ...      
% % % %                                 num2str(dbg_lt_slice));                             % dbg_lt_slice    

        
     
%   PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();  
  
 