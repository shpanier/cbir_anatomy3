clc
close all
clear all
%%
volumePath = 'D:\10000111\10000111_1_CTce_ThAb.nii';
segPath = 'D:\10000111\KIDNEY_RIGHT_V5_10000111_1_CTce_ThAb.nii'
% add pahts of NIFTI tookbox and the viewer
addpath('./NIFTI_analyze_toolbox_gzip_extension')
addpath('C:\Dropbox\MY_DOC\Phd\MIP\2015\ex1\NIFTI_analyze_toolbox_gzip_extension')
addpath('C:\Dropbox\MY_DOC\Phd\MIP\2015\ex1\NIFTI_analyze_toolbox')
addpath(genpath('./lib')) 

%% load nifit file.


volumeNii = load_untouch_nii_gzip(volumePath);
vl = volumeNii.img;
segNii = load_untouch_nii(segPath);
sg = segNii.img;
%%

vl2=double((vl-min(vl(:)))/(max(vl(:))-min(vl(:))));
sg2=double(sg);
[X,Y]=meshgrid(1:512,1:512);

[X2,Y2]=meshgrid(-5:5,-5:5);
C=exp(-0.06*(((X2.^2)+(Y2.^2))));
imshow(C,[]);

[X2,Y2]=meshgrid(-15:15,-15:15);
C2=exp(-0.05*(((X2.^2)+(Y2.^2))));
imshow(C2,[]);
for p=1:40
for q=150:250
Z(:,:)=sg2(:,:,q);

Z=double(Z);
Z1=double(conv2(Z,C,'same')>0.5);

Z3=Z1.*Z7;
Z7=double(conv2(Z,C2,'same')>0.5);

CX=sum(sum(Z1.*X))/sum(Z(:));
CY=sum(sum(Z1.*Y))/sum(Z(:));


Z2(:,:)=vl2(:,:,q);
Q(:,:,1)=255*Z2+200*Z7;
Q(:,:,2)=255*Z2+200*Z1;
Q(:,:,3)=255*Z2+200*Z3;

imshow(uint8(Q),'InitialMagnification','fit');
line(CX,CY,'Marker','.','MarkerSize',35,'color',[0 0 1]);
text(10,10,num2str(q),'FontSize',15,'color',[0 1 0]);
pause(0.1)
end
end

%%
for p=1:2
for q=150:250
Z(:,:)=sg2(:,:,q);

Z=double(Z);
Z1=double(conv2(Z,C,'same')>0.5);

Z7=double(conv2(Z,C2,'same')>0.5);
Z3=Z1.*Z7;

CX=sum(sum(Z1.*X))/sum(Z(:));
CY=sum(sum(Z1.*Y))/sum(Z(:));


Z2(:,:)=vl2(:,:,q);
Q(:,:,1)=255*Z2+200*Z7;
Q(:,:,2)=255*Z2+200*Z1;
Q(:,:,3)=255*Z2+200*Z3;

imshow(uint8(Q),'InitialMagnification','fit');
line(CX,CY,'Marker','.','MarkerSize',35,'color',[0 0 1]);
text(10,10,num2str(q),'FontSize',15,'color',[0 1 0]);
pause(0.1)
end
end