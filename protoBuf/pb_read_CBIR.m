function [c_b_i_r] = pb_read_CBIR(buffer, buffer_start, buffer_end)
%pb_read_CBIR Reads the protobuf message CBIR.
%   function [c_b_i_r] = pb_read_CBIR(buffer, buffer_start, buffer_end)
%
%   INPUTS:
%     buffer       : a buffer of uint8's to parse
%     buffer_start : optional starting index to consider of the buffer
%                    defaults to 1
%     buffer_end   : optional ending index to consider of the buffer
%                    defaults to length(buffer)
%
%   MEMBERS:
%     im             : repeated <a href="matlab:help pb_read_image_processing">image_processing</a>, defaults to struct([]).
%
%   See also pb_read_image_processing, pb_read_Processing, pb_read_volumeinfo.
  
  if (nargin < 1)
    buffer = uint8([]);
  end
  if (nargin < 2)
    buffer_start = 1;
  end
  if (nargin < 3)
    buffer_end = length(buffer);
  end
  
  descriptor = pb_descriptor_CBIR();
  c_b_i_r = pblib_generic_parse_from_string(buffer, descriptor, buffer_start, buffer_end);
  c_b_i_r.descriptor_function = @pb_descriptor_CBIR;
