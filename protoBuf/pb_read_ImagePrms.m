function [image_prms] = pb_read_ImagePrms(buffer, buffer_start, buffer_end)
%pb_read_ImagePrms Reads the protobuf message ImagePrms.
%   function [image_prms] = pb_read_ImagePrms(buffer, buffer_start, buffer_end)
%
%   INPUTS:
%     buffer       : a buffer of uint8's to parse
%     buffer_start : optional starting index to consider of the buffer
%                    defaults to 1
%     buffer_end   : optional ending index to consider of the buffer
%                    defaults to length(buffer)
%
%   MEMBERS:
%     ImageName      : required string, defaults to ''.
%     modality       : required string, defaults to 'ct'.
%     isContrasted   : required uint32, defaults to uint32(1).
%     Cornal_mid_Slice: optional int32, defaults to int32(-1).
%     Sagittal_mid_Slice: optional int32, defaults to int32(-1).
%     Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Axial_lowerBound_Slice: optional int32, defaults to int32(1).
%     GrayLevel_thr1 : optional int32, defaults to int32(-1).
%     GrayLevel_thr2 : optional int32, defaults to int32(-1).
%     GrayLevel_thr3 : optional int32, defaults to int32(-1).
%     Trachea_Axial_Start_Slice: optional int32, defaults to int32(-1).
%     Trachea_Axial_Mid_Slice: optional int32, defaults to int32(-1).
%     Trachea_GrayLevel_threshold: optional int32, defaults to int32(-1).
%     Trachea_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Trachea_GrayLevel_std: optional int32, defaults to int32(-1).
%     Trachea_Voxel_num: optional int32, defaults to int32(-1).
%     Trachea_Axial_in_Slice: optional int32, defaults to int32(-1).
%     Trachea_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Trachea_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Trachea_open_oper1: optional int32, defaults to int32(-1).
%     Trachea_open_oper2: optional int32, defaults to int32(-1).
%     Trachea_close_oper1: optional int32, defaults to int32(-1).
%     Trachea_close_oper2: optional int32, defaults to int32(-1).
%     Trachea_oper1  : optional int32, defaults to int32(-1).
%     Trachea_oper2  : optional int32, defaults to int32(-1).
%     Bronchi_Axial_Start_Slice: optional int32, defaults to int32(-1).
%     Bronchi_Axial_End_Slice: optional int32, defaults to int32(-1).
%     Bronchi_Coronal_End_Slice: optional int32, defaults to int32(-1).
%     Bronchi_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Bronchi_GrayLevel_std: optional int32, defaults to int32(-1).
%     Bronchi_Voxel_num: optional int32, defaults to int32(-1).
%     Bronchi_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Bronchi_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Lung_Axial_Biggest_Slice: optional int32, defaults to int32(-1).
%     Lung_Coronal_Anterior_Slice: optional int32, defaults to int32(-1).
%     Lung_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Lung_GrayLevel_std: optional int32, defaults to int32(-1).
%     Lung_Voxel_num : optional int32, defaults to int32(-1).
%     Lung_Left_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Lung_Left_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Lung_Right_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Lung_Right_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Lungs_open_oper1: optional int32, defaults to int32(-1).
%     Lungs_open_oper2: optional int32, defaults to int32(-1).
%     Lungs_close_oper1: optional int32, defaults to int32(-1).
%     Lungs_close_oper2: optional int32, defaults to int32(-1).
%     Lungs_oper1    : optional int32, defaults to int32(-1).
%     Lungs_oper2    : optional int32, defaults to int32(-1).
%     Lungs_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Lungs_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     Kidney_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Kidney_Axial_in_Slice: optional int32, defaults to int32(-1).
%     Kidney_Coronal_upperBound_Slice: optional int32, defaults to int32(-1).
%     Kidney_Coronal_Mid_Slice: optional int32, defaults to int32(-1).
%     Kidney_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Kidney_GrayLevel_std: optional int32, defaults to int32(-1).
%     Kidney_Voxel_num: optional int32, defaults to int32(-1).
%     Kidney_Left_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Kidney_Left_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Kidney_Right_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Kidney_Right_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Kidneys_open_oper1: optional int32, defaults to int32(-1).
%     Kidneys_open_oper2: optional int32, defaults to int32(-1).
%     Kidneys_close_oper1: optional int32, defaults to int32(-1).
%     Kidneys_close_oper2: optional int32, defaults to int32(-1).
%     Kidneys_oper1  : optional int32, defaults to int32(-1).
%     Kidneys_oper2  : optional int32, defaults to int32(-1).
%     Kidney_Left_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Kidney_Left_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     Kidney_Right_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Kidney_Right_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     Kidnes_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Kidnes_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     Bones_Axial_smalles_Slice: optional int32, defaults to int32(-1).
%     Bones_Axial_smalles_Slice_Ytop: optional int32, defaults to int32(-1).
%     Bones_Axial_smalles_Slice_Ybottom: optional int32, defaults to int32(-1).
%     Bones_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Bones_GrayLevel_std: optional int32, defaults to int32(-1).
%     Bones_Voxel_num: optional int32, defaults to int32(-1).
%     Bones_open_oper1: optional int32, defaults to int32(-1).
%     Bones_open_oper2: optional int32, defaults to int32(-1).
%     Bones_close_oper1: optional int32, defaults to int32(-1).
%     Bones_close_oper2: optional int32, defaults to int32(-1).
%     Bones_oper1    : optional int32, defaults to int32(-1).
%     Bones_oper2    : optional int32, defaults to int32(-1).
%     Bones_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Bones_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     Liver_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Liver_Axial_in_Slice: optional int32, defaults to int32(-1).
%     Liver_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Liver_GrayLevel_std: optional int32, defaults to int32(-1).
%     Liver_Voxel_num: optional int32, defaults to int32(-1).
%     Liver_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Liver_open_oper1: optional int32, defaults to int32(-1).
%     Liver_open_oper2: optional int32, defaults to int32(-1).
%     Liver_close_oper1: optional int32, defaults to int32(-1).
%     Liver_close_oper2: optional int32, defaults to int32(-1).
%     Liver_oper1    : optional int32, defaults to int32(-1).
%     Liver_oper2    : optional int32, defaults to int32(-1).
%     Liver_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Liver_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     Spleen_Axial_in_Slice: optional int32, defaults to int32(-1).
%     Spleen_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Spleen_GrayLevel_std: optional int32, defaults to int32(-1).
%     Spleen_open_oper1: optional int32, defaults to int32(-1).
%     Spleen_open_oper2: optional int32, defaults to int32(-1).
%     Spleen_close_oper1: optional int32, defaults to int32(-1).
%     Spleen_close_oper2: optional int32, defaults to int32(-1).
%     Spleen_oper1   : optional int32, defaults to int32(-1).
%     Spleen_oper2   : optional int32, defaults to int32(-1).
%     Spleen_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Spleen_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     Heart_Axial_in_Slice: optional int32, defaults to int32(-1).
%     Heart_GrayLevel_mean: optional int32, defaults to int32(-1).
%     Heart_GrayLevel_std: optional int32, defaults to int32(-1).
%     Heart_GrayLevel_thr: optional int32, defaults to int32(-1).
%     Heart_Axial_upperBound_Slice: optional int32, defaults to int32(-1).
%     Heart_Axial_lowerBound_Slice: optional int32, defaults to int32(-1).
%     Heart_open_oper1: optional int32, defaults to int32(-1).
%     Heart_open_oper2: optional int32, defaults to int32(-1).
%     Heart_close_oper1: optional int32, defaults to int32(-1).
%     Heart_close_oper2: optional int32, defaults to int32(-1).
%     Heart_oper1    : optional int32, defaults to int32(-1).
%     Heart_oper2    : optional int32, defaults to int32(-1).
%     Heart_GrayLevel_thr1: optional int32, defaults to int32(-1).
%     Heart_GrayLevel_thr2: optional int32, defaults to int32(-1).
%     prm101         : optional string, defaults to '0'.
%     prm102         : optional string, defaults to '0'.
%     prm103         : optional string, defaults to '0'.
%     prm104         : optional string, defaults to '0'.
%     prm105         : optional string, defaults to '0'.
%     prm106         : optional string, defaults to '0'.
%     prm107         : optional string, defaults to '0'.
%     prm108         : optional string, defaults to '0'.
%     prm109         : optional string, defaults to '0'.
%
%   See also pb_read_CBIRImagesPrms.
  
  if (nargin < 1)
    buffer = uint8([]);
  end
  if (nargin < 2)
    buffer_start = 1;
  end
  if (nargin < 3)
    buffer_end = length(buffer);
  end
  
  descriptor = pb_descriptor_ImagePrms();
  image_prms = pblib_generic_parse_from_string(buffer, descriptor, buffer_start, buffer_end);
  image_prms.descriptor_function = @pb_descriptor_ImagePrms;
