function [parameters] = pb_read_Processing__Parameters(buffer, buffer_start, buffer_end)
%pb_read_Processing__Parameters Reads the protobuf message Parameters.
%   function [parameters] = pb_read_Processing__Parameters(buffer, buffer_start, buffer_end)
%
%   INPUTS:
%     buffer       : a buffer of uint8's to parse
%     buffer_start : optional starting index to consider of the buffer
%                    defaults to 1
%     buffer_end   : optional ending index to consider of the buffer
%                    defaults to length(buffer)
%
%   MEMBERS:
%     prm_name       : required string, defaults to ''.
%     prm_value      : required string, defaults to ''.
%     prm_type       : optional string, defaults to ''.
%
%   See also pb_read_Processing, pb_read_volumeinfo, pb_read_image_processing, pb_read_CBIR.
  
  if (nargin < 1)
    buffer = uint8([]);
  end
  if (nargin < 2)
    buffer_start = 1;
  end
  if (nargin < 3)
    buffer_end = length(buffer);
  end
  
  descriptor = pb_descriptor_Processing__Parameters();
  parameters = pblib_generic_parse_from_string(buffer, descriptor, buffer_start, buffer_end);
  parameters.descriptor_function = @pb_descriptor_Processing__Parameters;
