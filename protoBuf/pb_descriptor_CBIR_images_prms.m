function [descriptor] = pb_descriptor_CBIR_images_prms()
%pb_descriptor_CBIR_images_prms Returns the descriptor for message CBIR_images_prms.
%   function [descriptor] = pb_descriptor_CBIR_images_prms()
%
%   See also pb_read_CBIR_images_prms
  
  descriptor = struct( ...
    'name', 'CBIR_images_prms', ...
    'full_name', 'CBIR_images_prms', ...
    'filename', 'CBIR_images_prms.proto', ...
    'containing_type', '', ...
    'fields', [ ...
      struct( ...
        'name', 'ip', ...
        'full_name', 'CBIR_images_prms.ip', ...
        'index', 1, ...
        'number', uint32(1), ...
        'type', uint32(11), ...
        'matlab_type', uint32(9), ...
        'wire_type', uint32(2), ...
        'label', uint32(3), ...
        'default_value', struct([]), ...
        'read_function', @(x) pb_read_ImagePrms(x{1}, x{2}, x{3}), ...
        'write_function', @pblib_generic_serialize_to_string, ...
        'options', struct('packed', false) ...
      ) ...
    ], ...
    'extensions', [ ... % Not Implemented
    ], ...
    'nested_types', [ ... % Not implemented
    ], ...
    'enum_types', [ ... % Not Implemented
    ], ...
    'options', [ ... % Not Implemented
    ] ...
  );
  
  descriptor.field_indeces_by_number = java.util.HashMap;
  put(descriptor.field_indeces_by_number, uint32(1), 1);
  
