function [processing] = pb_read_Processing(buffer, buffer_start, buffer_end)
%pb_read_Processing Reads the protobuf message Processing.
%   function [processing] = pb_read_Processing(buffer, buffer_start, buffer_end)
%
%   INPUTS:
%     buffer       : a buffer of uint8's to parse
%     buffer_start : optional starting index to consider of the buffer
%                    defaults to 1
%     buffer_end   : optional ending index to consider of the buffer
%                    defaults to length(buffer)
%
%   MEMBERS:
%     id             : required int32, defaults to int32(0).
%     Enabled        : required uint32, defaults to uint32(0).
%     twoDMethod     : optional uint32, defaults to uint32(0).
%     MethodName     : required string, defaults to ''.
%     MethodFilesLocation: optional string, defaults to ''.
%     plane          : optional enum, defaults to int32(0).
%     prms           : repeated <a href="matlab:help pb_read_Processing__Parameters">Processing.Parameters</a>, defaults to struct([]).
%
%   See also pb_read_Processing__Parameters, pb_read_volumeinfo, pb_read_image_processing, pb_read_CBIR.
  
  if (nargin < 1)
    buffer = uint8([]);
  end
  if (nargin < 2)
    buffer_start = 1;
  end
  if (nargin < 3)
    buffer_end = length(buffer);
  end
  
  descriptor = pb_descriptor_Processing();
  processing = pblib_generic_parse_from_string(buffer, descriptor, buffer_start, buffer_end);
  processing.descriptor_function = @pb_descriptor_Processing;
