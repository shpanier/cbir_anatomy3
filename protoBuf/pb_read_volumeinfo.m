function [volumeinfo] = pb_read_volumeinfo(buffer, buffer_start, buffer_end)
%pb_read_volumeinfo Reads the protobuf message volumeinfo.
%   function [volumeinfo] = pb_read_volumeinfo(buffer, buffer_start, buffer_end)
%
%   INPUTS:
%     buffer       : a buffer of uint8's to parse
%     buffer_start : optional starting index to consider of the buffer
%                    defaults to 1
%     buffer_end   : optional ending index to consider of the buffer
%                    defaults to length(buffer)
%
%   MEMBERS:
%     imageName      : required string, defaults to ''.
%     patientid      : optional string, defaults to ''.
%     modalitycounter: optional string, defaults to ''.
%     modality       : optional string, defaults to ''.
%     region         : optional string, defaults to ''.
%
%   See also pb_read_Processing, pb_read_image_processing, pb_read_CBIR.
  
  if (nargin < 1)
    buffer = uint8([]);
  end
  if (nargin < 2)
    buffer_start = 1;
  end
  if (nargin < 3)
    buffer_end = length(buffer);
  end
  
  descriptor = pb_descriptor_volumeinfo();
  volumeinfo = pblib_generic_parse_from_string(buffer, descriptor, buffer_start, buffer_end);
  volumeinfo.descriptor_function = @pb_descriptor_volumeinfo;
