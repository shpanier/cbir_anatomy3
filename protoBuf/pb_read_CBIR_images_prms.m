function [c_b_i_r_images_prms] = pb_read_CBIR_images_prms(buffer, buffer_start, buffer_end)
%pb_read_CBIR_images_prms Reads the protobuf message CBIR_images_prms.
%   function [c_b_i_r_images_prms] = pb_read_CBIR_images_prms(buffer, buffer_start, buffer_end)
%
%   INPUTS:
%     buffer       : a buffer of uint8's to parse
%     buffer_start : optional starting index to consider of the buffer
%                    defaults to 1
%     buffer_end   : optional ending index to consider of the buffer
%                    defaults to length(buffer)
%
%   MEMBERS:
%     ip             : repeated <a href="matlab:help pb_read_ImagePrms">ImagePrms</a>, defaults to struct([]).
%
%   See also pb_read_ImagePrms.
  
  if (nargin < 1)
    buffer = uint8([]);
  end
  if (nargin < 2)
    buffer_start = 1;
  end
  if (nargin < 3)
    buffer_end = length(buffer);
  end
  
  descriptor = pb_descriptor_CBIR_images_prms();
  c_b_i_r_images_prms = pblib_generic_parse_from_string(buffer, descriptor, buffer_start, buffer_end);
  c_b_i_r_images_prms.descriptor_function = @pb_descriptor_CBIR_images_prms;
