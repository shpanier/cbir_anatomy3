function [image_processing] = pb_read_image_processing(buffer, buffer_start, buffer_end)
%pb_read_image_processing Reads the protobuf message image_processing.
%   function [image_processing] = pb_read_image_processing(buffer, buffer_start, buffer_end)
%
%   INPUTS:
%     buffer       : a buffer of uint8's to parse
%     buffer_start : optional starting index to consider of the buffer
%                    defaults to 1
%     buffer_end   : optional ending index to consider of the buffer
%                    defaults to length(buffer)
%
%   MEMBERS:
%     imageName      : required string, defaults to ''.
%     imageLoadFunc  : required string, defaults to ''.
%     imageLoadFuncPath: optional string, defaults to ''.
%     processDescription: optional string, defaults to ''.
%     change_curr_image_name: optional uint32, defaults to uint32(1).
%     timestamp_for_image_load: optional string, defaults to ''.
%     documantaion   : optional string, defaults to ''.
%     P              : repeated <a href="matlab:help pb_read_Processing">Processing</a>, defaults to struct([]).
%     Enabled        : optional uint32, defaults to uint32(1).
%     DisplayResult  : optional uint32, defaults to uint32(1).
%     isSegmentation : optional uint32, defaults to uint32(0).
%     TV             : optional <a href="matlab:help pb_read_volumeinfo">volumeinfo</a>, defaults to struct([]).
%     SV             : repeated <a href="matlab:help pb_read_volumeinfo">volumeinfo</a>, defaults to struct([]).
%     forceRecalculation: optional uint32, defaults to uint32(0).
%
%   See also pb_read_Processing, pb_read_volumeinfo, pb_read_CBIR.
  
  if (nargin < 1)
    buffer = uint8([]);
  end
  if (nargin < 2)
    buffer_start = 1;
  end
  if (nargin < 3)
    buffer_end = length(buffer);
  end
  
  descriptor = pb_descriptor_image_processing();
  image_processing = pblib_generic_parse_from_string(buffer, descriptor, buffer_start, buffer_end);
  image_processing.descriptor_function = @pb_descriptor_image_processing;
