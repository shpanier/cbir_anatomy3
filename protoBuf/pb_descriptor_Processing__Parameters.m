function [descriptor] = pb_descriptor_Processing__Parameters()
%pb_descriptor_Processing__Parameters Returns the descriptor for message Parameters.
%   function [descriptor] = pb_descriptor_Processing__Parameters()
%
%   See also pb_read_Processing__Parameters
  
  descriptor = struct( ...
    'name', 'Parameters', ...
    'full_name', 'Processing.Parameters', ...
    'filename', 'CBIR_Process_flow.proto', ...
    'containing_type', 'Processing', ...
    'fields', [ ...
      struct( ...
        'name', 'prm_name', ...
        'full_name', 'Processing.Parameters.prm_name', ...
        'index', 1, ...
        'number', uint32(1), ...
        'type', uint32(9), ...
        'matlab_type', uint32(7), ...
        'wire_type', uint32(2), ...
        'label', uint32(2), ...
        'default_value', '', ...
        'read_function', @(x) char(x{1}(x{2} : x{3})), ...
        'write_function', @uint8, ...
        'options', struct('packed', false) ...
      ), ...
      struct( ...
        'name', 'prm_value', ...
        'full_name', 'Processing.Parameters.prm_value', ...
        'index', 2, ...
        'number', uint32(2), ...
        'type', uint32(9), ...
        'matlab_type', uint32(7), ...
        'wire_type', uint32(2), ...
        'label', uint32(2), ...
        'default_value', '', ...
        'read_function', @(x) char(x{1}(x{2} : x{3})), ...
        'write_function', @uint8, ...
        'options', struct('packed', false) ...
      ), ...
      struct( ...
        'name', 'prm_type', ...
        'full_name', 'Processing.Parameters.prm_type', ...
        'index', 3, ...
        'number', uint32(3), ...
        'type', uint32(9), ...
        'matlab_type', uint32(7), ...
        'wire_type', uint32(2), ...
        'label', uint32(1), ...
        'default_value', '', ...
        'read_function', @(x) char(x{1}(x{2} : x{3})), ...
        'write_function', @uint8, ...
        'options', struct('packed', false) ...
      ) ...
    ], ...
    'extensions', [ ... % Not Implemented
    ], ...
    'nested_types', [ ... % Not implemented
    ], ...
    'enum_types', [ ... % Not Implemented
    ], ...
    'options', [ ... % Not Implemented
    ] ...
  );
  
  descriptor.field_indeces_by_number = java.util.HashMap;
  put(descriptor.field_indeces_by_number, uint32(1), 1);
  put(descriptor.field_indeces_by_number, uint32(2), 2);
  put(descriptor.field_indeces_by_number, uint32(3), 3);
  
