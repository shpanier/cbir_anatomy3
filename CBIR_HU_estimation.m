
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load LUNGS_n_TRACHEA_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['LUNGS_n_TRACHEA_'];    nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load ) 

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];          nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
% PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ~~~~~~~~~~~~ -> CBIR_HU_estimation_aux_func.run
PB_shortcuts.set_new_image_index();     % -1
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
        
PB.add_im_Process(    'CBIR_HU_estimation_aux_func.run', ...
        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] ); % orig CT
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-2)] ); % lungs

% PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT(); 







