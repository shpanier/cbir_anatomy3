classdef CBIR_KIDNEY_aux_func
    methods(Static)
    
         
    %% Kidney_Right_Coarse_ROI_basedonLeft 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Kidney_Right_Coarse_ROI_basedonLeft
    % * *     CBIR_KIDNEY_aux_func.Left_Kidney_POST_Segmentaion_stat 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    
    function [origCT] =  Left_Kidney_POST_Segmentaion_stat( Kidney_final_seg ,origCT  ) 
%             imagine_v2(Roi_right, Kidney_Left, 'asdf')                                                 
             
 

%            STOP_HERE(); 
%            eeee=1; 
             
          global CP
          ix = PB_ip_shortcuts.get_image_idx( );
          num_of_slices_ = size(Kidney_final_seg,3);
          Kidney_final_seg_slices_ = sum( ...
                                        reshape(  ...
                                            permute(Kidney_final_seg,[3 1 2]) ...
                                         ,num_of_slices_,[]) ...
                                         ,2); 
          % Search for the first (lower bound) and the last (upperBound) 
          % axial slices which does not contain any pixels.     
          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
          Kidney_Left_Axial_upperBound_Slice_ = ...
                                find(Kidney_final_seg_slices_,1, 'last')+ 1 ;
          CP.ip(ix) = pblib_set(CP.ip(ix), 'Kidney_Left_Axial_upperBound_Slice', ...
                                Kidney_Left_Axial_upperBound_Slice_ ); 
                             
              origCT(~logical(Kidney_final_seg)) = 0;   
            Kidney_Left_GrayLevel_thr1_ = round(mean( double(origCT(origCT>0))));
            Kidney_Left_GrayLevel_thr2_ = round(std( double(origCT(origCT>0))));  
            CP.ip(ix) = pblib_set(CP.ip(ix), 'Kidney_Left_GrayLevel_thr1', ...
                                Kidney_Left_GrayLevel_thr1_ ); 
                            
            CP.ip(ix) = pblib_set(CP.ip(ix), 'Kidney_Left_GrayLevel_thr2', ...
                                Kidney_Left_GrayLevel_thr2_ );  
%               Kidney_Left_GrayLevel_thr1: -1
%              Kidney_Left_GrayLevel_thr2: -1                 
          
    end
    %% Kidney_Right_Coarse_ROI_basedonLeft 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Kidney_Right_Coarse_ROI_basedonLeft
    % * *        
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% %     function [img__] =  Kidney_Right_Coarse_ROI_basedonLeft( Roi_right, ...
% %                                                              Kidney_Left ) 
% % %             imagine_v2(Roi_right, Kidney_Left, 'asdf')                                                 
% %                
% %            
% %           global CP;
% %           ix = PB_ip_shortcuts.get_image_idx( );
% %              
% % % %       Kidney_Left_GrayLevel_thr1: -1
% % % %       Kidney_Left_GrayLevel_thr2: -1  
% %    
% %           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% %           %   l
% %           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% %           % coarse_left_lung = lung_seg( [1:mid_sagittal] , : ,:);
% %           dim2_ = size(Kidney_Left,2);
% %           coarse_left_kidney_slices_ = sum( ...
% %                                         reshape(  ... 
% %                                             permute(Kidney_Left,[2 1 3]) ...
% %                                          ,dim2_,[]) ...
% %                                          ,2); 
% %           % Search for the first (lower bound) and the last (upperBound) 
% %           % axial slices which does not contain any pixels.     
% %           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% %           Kidney_Left_cornal_lowerBound_Slice = ...
% %                                 find(coarse_left_kidney_slices_,1, 'first');
% %           Kidney_Left_cornal_upperBound_Slice = ...
% %                                 find(coarse_left_kidney_slices_,1, 'last')  ;
% %           CP.ip(ix) = pblib_set(CP.ip(ix), 'prm103', ... 
% %                                 Kidney_Left_cornal_lowerBound_Slice);  
% %           CP.ip(ix) = pblib_set(CP.ip(ix), 'prm104', ...
% %                                 Kidney_Left_cornal_upperBound_Slice);    
% %             
% %           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% %           %   2
% %           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% %       
% %           dim1_ = size(Kidney_Left,1);
% %           coarse_left_kidney_slices_ = sum( ...
% %                                         reshape(  ... 
% %                                             Kidney_Left ...
% %                                          ,dim1_,[]) ...
% %                                          ,2); 
% %           % Search for the first (lower bound) and the last (upperBound) 
% %           % axial slices which does not contain any pixels.     
% %           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% %           Kidney_Left_Sagital_lowerBound_Slice = ...
% %                                 find(coarse_left_kidney_slices_,1, 'first');
% %            CP.ip(ix) = pblib_set(CP.ip(ix), 'prm105', ... 
% %                                 Kidney_Left_cornal_lowerBound_Slice); 
% %          %  CP.ip(ix)
% %           img__ =    Roi_right;               
% %       try                     
% %         img__( end-Kidney_Left_Sagital_lowerBound_Slice+30:end,:,:)=0; 
% %       end
% %       
% %       try 
% %          img__( :,[1:Kidney_Left_cornal_lowerBound_Slice-10 , ...
% %              Kidney_Left_cornal_upperBound_Slice+10:dim2_] ,:)=0; 
% %       end
% %          
% % if (0)         
% %     img__( end-Kidney_Left_Sagital_lowerBound_Slice+20:end,:,:)=0; 
% %          img__( :,[1:Kidney_Left_cornal_lowerBound_Slice-10 , ...
% %              Kidney_Left_cornal_upperBound_Slice+10:dim2_] ,:)=0; 
% % end    
% %          
% % %          imagine_v2(img__, Kidney_Left, 'asdf')    
% % %          STOP_HERE();
% % %          eee=1;    
% %     end
% %      %% gross_KIDNEY_RIGHT_segment 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * gross_KIDNEY_RIGHT_segment
    % * *  
    % * * 
    % * * Given:
    % * *     o gross_KIDNEY_segment
    % * *         
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [img__] = gross_KIDNEY_RIGHT_segment(liver_heart_leftKidny_seg)
 
        global CP   
        ix = PB_ip_shortcuts.get_image_idx( )      
        Kidney_Axial_upperBound_Slice_  = CP.ip(ix).Kidney_Axial_upperBound_Slice+15;
        Kidney_Coronal_upperBound_Slice = CP.ip(ix).Kidney_Coronal_upperBound_Slice ;
    
        Kidney_Axial_in_Slice = CP.ip(ix).Kidney_Axial_in_Slice  ;
        Sagittal_mid_Slice = CP.ip(ix).Sagittal_mid_Slice; 
         
        
        img__ = liver_heart_leftKidny_seg; 
%         w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
%         SPINAL_w_raito1 = 120; % round(SPINAL_w_raito_top*w_);
%         SPINAL_w_raito2 = 120; %round(SPINAL_w_raito_bot*w_);
%         SPINAL_w_raito3 = 70;
%          
     
         img__([(Sagittal_mid_Slice-20):end  ]   ,   ...
             : , ...
             : )=0; 
         img__(:   ,   ... 
              : , ...  
             [Kidney_Axial_upperBound_Slice_:end] )=0;
           
         img__(:  ,   ...
              [1:Kidney_Coronal_upperBound_Slice]  , ...
             :)=0;
         
      
         
    end
     
    %% gross_KIDNEY_LEFT_segment 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * gross_KIDNEY_LEFT_segment
    % * *  
    % * * 
    % * * Given: 
    % * *     o gross_KIDNEY_segment
    % * *  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [img__] = gross_KIDNEY_LEFT_segment(liver_heart_leftKidny_seg)
 
        global CP   
        ix = PB_ip_shortcuts.get_image_idx( )      
        Kidney_Axial_upperBound_Slice_  = CP.ip(ix).Kidney_Axial_upperBound_Slice+15;
        Kidney_Coronal_upperBound_Slice = CP.ip(ix).Kidney_Coronal_upperBound_Slice ;
    
        Kidney_Axial_in_Slice = CP.ip(ix).Kidney_Axial_in_Slice  ;
        Sagittal_mid_Slice = CP.ip(ix).Sagittal_mid_Slice; 
  
         
        
        img__ = liver_heart_leftKidny_seg; 
%         w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
%         SPINAL_w_raito1 = 120; % round(SPINAL_w_raito_top*w_);
%         SPINAL_w_raito2 = 120; %round(SPINAL_w_raito_bot*w_);
%         SPINAL_w_raito3 = 70;
%    
%          img__([1:(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1) (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito2):end  ]   ,   ...
%              : , ...
%              : )=0; 
    
            img__([1:(Sagittal_mid_Slice+30) ]   ,   ...
             : , ...
             : )=0; 
         img__(:   ,   ... 
              : , ...  
             [Kidney_Axial_upperBound_Slice_:end] )=0;
           
         img__(:  ,   ...
              [1:Kidney_Coronal_upperBound_Slice]  , ...
             :)=0;
         
      
         
    end
    
    %% gross_KIDNEY_segment 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * gross_KIDNEY_segment
    % * *  
    % * * 
    % * * Given:
    % * *     o gross_KIDNEY_segment
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [img__] = gross_KIDNEY_segment(liver_heart_leftKidny_seg)
 
        global CP   
        ix = PB_ip_shortcuts.get_image_idx( )      
        Kidney_Axial_upperBound_Slice_  = CP.ip(ix).Kidney_Axial_upperBound_Slice+15;
        Kidney_Coronal_upperBound_Slice = CP.ip(ix).Kidney_Coronal_upperBound_Slice ;
    
        Kidney_Axial_in_Slice = CP.ip(ix).Kidney_Axial_in_Slice  ;
        
         
        
        img__ = liver_heart_leftKidny_seg; 
%         w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
%         SPINAL_w_raito1 = 120; % round(SPINAL_w_raito_top*w_);
%         SPINAL_w_raito2 = 120; %round(SPINAL_w_raito_bot*w_);
%         SPINAL_w_raito3 = 70;
%   
%          img__([1:(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1) (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito2):end  ]   ,   ...
%              : , ...
%              : )=0; 
         img__(:   ,   ... 
              : , ...    
             [Kidney_Axial_upperBound_Slice_:end] )=0;
           
         img__(:  ,   ...
              [1:Kidney_Coronal_upperBound_Slice]  , ...
             :)=0;
          
    end
  
      
    %% Kidney_Axial_upperBound_Slice 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Kidney_Axial_upperBound_Slice
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_img] = Kidney_Axial_upperBound_Slice( orig_ct    , ...
                                                        Aorta_seg  , ... 
                                                        Chest_posterior_seg ) 
        verbose_=1;  
         
minmin = min(Aorta_seg(:));  
maxmax = max(Aorta_seg(:)) ; 
        if ((minmin==0) && (maxmax==0))
            error('Corapt orta segmentaion'); 
        end
        %addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE\');  
      %  STOP_HERE();    
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        dim_= 3;                     
     
        res_img= simple_2D_to_3D.run(Aorta_seg , 'bwulterode' , dim_);
       % clear Aorta_seg 
        res_img = CTpermute(res_img, 'forward', dim_);
        orig_ct = CTpermute(orig_ct, 'forward', dim_);
        Chest_posterior_seg = CTpermute(Chest_posterior_seg, 'forward', dim_);

        global CP 
         ix = PB_ip_shortcuts.get_image_idx( )
        Lung_Axial_Biggest_Slice =  CP.ip(ix).Lung_Axial_Biggest_Slice;
                                       
  
        % ones_ = ones(size(res_img,2),size(res_img,3),'int16'); 
        sl = Lung_Axial_Biggest_Slice
        Kidney_Coronal_upperBound_Slice = 0;
        
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
        % * *   
        % * *     Kidney_Axial_upperBound_Slice
        % * *     
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        Kidney_Axial_upperBound_Slice_FOUND = false;
        for sl=[Lung_Axial_Biggest_Slice:-1:1] 
             
            if (verbose_) 
                fprintf('.');
                if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
                if (mod(sl,80)==0),  disp('.'),    end
            end    
             
            curr_sl = squeeze(  res_img(sl,:,:) );
            [a,b]=ind2sub(size(curr_sl),find(curr_sl)); 
            mm = min(curr_sl(:)); , MM =max(curr_sl(:)); , a ;
 
            if (~isempty(a))  
                 
                 % = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
                 % * * The Lung is behind (posterior) the Aorta
                 % * *     this is the (tight) upper bound limit 
                 % * *     for the -- kidney...
                 % = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
                 if (~Kidney_Axial_upperBound_Slice_FOUND)
                    orig_ct_sl = squeeze(  orig_ct(sl,:,:) );
                    indicate_rode = orig_ct_sl([a(1)-100:a(1)+100],b(1));

                    if (isempty(find(indicate_rode<-500)))  
                       % STOP_HERE(); 
                        CP.ip(ix) = ...   
                            pblib_set(CP.ip(ix), 'Kidney_Axial_upperBound_Slice',...
                                                   sl);
                        CP.ip(ix) = ...                                                 
                            pblib_set(CP.ip(ix), 'Liver_Axial_in_Slice',...
                                                   sl+10);  
                        CP.ip(ix) = ...                                                 
                             pblib_set(CP.ip(ix), 'Spleen_Axial_in_Slice',...
                                                   sl);
                            Spleen_Axial_in_Slice_ = sl;                     
                            Kidney_Coronal_upperBound_Slice = b(1); 
                            Kidney_Axial_upperBound_Slice_FOUND = true;
                             
                            % res_img(sl,:,:)=1; 

                            continue      
 
                    end 
                    
                 % = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
                 % * * 
                 % * *    The Chest_posterior_seg is behind (posterior) the Aorta
                 % * *     
                 % = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
         
                 else  
                    Chest_posterior_seg_sl = squeeze(  Chest_posterior_seg(sl,:,:) );
                    indicate_rode = Chest_posterior_seg_sl([a(1)-100:a(1)+100],b(1));
 
                    if (~isempty(find(indicate_rode, 1)))  
                                            
                       % = = = = = = = = = = = = = = = = = = = = = = = = =
                       % 
                       %  ERROR in case 
                       %     The 'kidney-upper-bound' and The 'kidney-in'
                       %     are consecutive most probably error in the 
                       %     bone thresholding / segmention   
                       %  = = = = = = = = = = = = = = = = = = = = = = = = = 
  
                        if (CP.ip(ix).Kidney_Axial_upperBound_Slice-1 == sl)
                            disp ('The ''kidney-upper-bound'' and The ''kidney-in'' are consecutive');
%                             STOP_HERE();  
%                             eee=1;  
                        end
                          
                         CP.ip(ix) = ...   
                            pblib_set(CP.ip(ix), 'Kidney_Axial_in_Slice',...
                                                    sl);
                           Kidney_Axial_in_Slice_ = sl;                       
                        %     res_img(sl,:,:)=1; 

                            break          
                    end 
                 end 
             %    b(1)-a(1)  
              %   res_img(sl,:,:) = triu(ones_,b(1)-a(1)); 
 
            end 
        end  
          
        if (~Kidney_Axial_upperBound_Slice_FOUND)
            error(['eror Kidney_Axial_upperBound_Slice_FOUND not found !!!']);
        end
         
%         STOP_HERE()
%         eee=1;   
        
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
        % * *   
        % * *     Kidney_Coronal_upperBound_Slice
        % * *     
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
        for sl1=[sl:-1:1]  
            if (verbose_) 
                fprintf(':');
                if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
                if (mod(sl,80)==0),  disp(':'),    end
            end  
             
            curr_sl = squeeze(  res_img(sl1,:,:) );
            [a,b]=ind2sub(size(curr_sl),find(curr_sl));
  
            
            
        end
        
        
        for sl=[Lung_Axial_Biggest_Slice:-1:1]
 
            curr_sl = squeeze(  res_img(sl,:,:) );
            [a,b]=ind2sub(size(curr_sl),find(curr_sl));
            if ((~isempty(b)) && (b(1) < Kidney_Coronal_upperBound_Slice)) 
                Kidney_Coronal_upperBound_Slice = b(1);  
            end
 
            
       end
       Kidney_Coronal_upperBound_Slice =Kidney_Coronal_upperBound_Slice -30;
       if (Kidney_Coronal_upperBound_Slice <100 )
           Kidney_Coronal_upperBound_Slice = 100 ;
       end 
       
       CP.ip(ix) = ...     
         pblib_set(CP.ip(ix), 'Kidney_Coronal_upperBound_Slice',...
                          Kidney_Coronal_upperBound_Slice);                                          
 
                           
       res_img(:,Kidney_Coronal_upperBound_Slice,:)=1; 
       res_img(Spleen_Axial_in_Slice_,1:50,:)=1; res_img(Spleen_Axial_in_Slice_,51:end,:)=2;
       res_img(Kidney_Axial_in_Slice_,:,1:50)=1; res_img(Kidney_Axial_in_Slice_,:,51:end)=2;
 
  
%        Spleen_Axial_in_Slice
       
       res_img = CTpermute(res_img, 'backward', dim_);
     
        
    end 
         
    %% locate_the_kidney_in_the_Kidney_Axial_in_Slice2 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * locate_the_kidney_in_the_Kidney_Axial_in_Slice2
    % * * 
    % * * 
    % * * Given:   
    % * *     
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
       
     function [img__] = locate_the_kidney_in_the_Kidney_Axial_in_Slice2( ...
                                        gross_seg       ) 
                          
        global CP    
        img__ = gross_seg;  
        ix = PB_ip_shortcuts.get_image_idx( )
  
        Kidney_Axial_in_Sl =  squeeze(  img__(:,:, CP.ip(ix).Kidney_Axial_in_Slice) );
        orig_ct_Kidney_Axial_in_Sl =  squeeze(  orig_ct(:,:, CP.ip(ix).Kidney_Axial_in_Slice) );
        orig_ct_Kidney_Axial_in_Sl2 = orig_ct_Kidney_Axial_in_Sl; 
        orig_ct_Kidney_Axial_in_Sl2(~Kidney_Axial_in_Sl) =0; 
        bwc = (bwconvhull(orig_ct_Kidney_Axial_in_Sl2,'objects'));
        img__(:,:, CP.ip(ix).Kidney_Axial_in_Slice) = bwc;
        gl = orig_ct_Kidney_Axial_in_Sl2(find(orig_ct_Kidney_Axial_in_Sl2));
%         
%         STOP_HERE();   
%         figure;                            
                                            
     end
 
      
     
     
      %% locate_the_kidney_in_the_Kidney_Axial_in_Slice 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * locate_the_kidney_in_the_Kidney_Axial_in_Slice
    % * * 
    % * * 
    % * * Given:  
    % * *     o locate_the_kidney_in_the_Kidney_Axial_in_Slice
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        
     function [img__] = locate_the_kidney_in_the_Kidney_Axial_in_Slice( ...
                                        gross_seg  , orig_ct   ) 
                          
        global CP    
        img__ = gross_seg;  
        ix = PB_ip_shortcuts.get_image_idx( )
  
        Kidney_Axial_in_Sl =  squeeze(  img__(:,:, CP.ip(ix).Kidney_Axial_in_Slice) );
        orig_ct_Kidney_Axial_in_Sl =  squeeze(  orig_ct(:,:, CP.ip(ix).Kidney_Axial_in_Slice) );
        orig_ct_Kidney_Axial_in_Sl2 = orig_ct_Kidney_Axial_in_Sl; 
        orig_ct_Kidney_Axial_in_Sl2(~Kidney_Axial_in_Sl) =0; 
        bwc = (bwconvhull(orig_ct_Kidney_Axial_in_Sl2,'objects'));
        img__(:,:, CP.ip(ix).Kidney_Axial_in_Slice) = bwc;
        gl = orig_ct_Kidney_Axial_in_Sl2(find(orig_ct_Kidney_Axial_in_Sl2));
%         
%         STOP_HERE();   
%         figure;                            
                                            
     end
    
% % % %     %% gross_KIDNEY_marker2 
% % % %     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % %     % * * gross_KIDNEY_marker2
% % % %     % * * 
% % % %     % * * 
% % % %     % * * Given:  
% % % %     % * *     o liver_heart_leftKidny_seg
% % % %     % * * 
% % % %     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % %       
% % % %      function [img__] = gross_KIDNEY_marker2(liver_heart_leftKidny_seg , ...
% % % %                                                orig_ct                    )
% % % %                                            
% % % %             global CP   
% % % %             img__ = liver_heart_leftKidny_seg; 
% % % %             ix = PB_ip_shortcuts.get_image_idx( )
% % % %      
% % % %             Kidney_Axial_in_Sl =  squeeze(  img__(:,:, CP.ip(ix).Kidney_Axial_in_Slice) );
% % % %             orig_ct_Kidney_Axial_in_Sl =  squeeze(  orig_ct(:,:, CP.ip(ix).Kidney_Axial_in_Slice) );
% % % %             orig_ct_Kidney_Axial_in_Sl2 = orig_ct_Kidney_Axial_in_Sl; 
% % % %             orig_ct_Kidney_Axial_in_Sl2(~Kidney_Axial_in_Sl) =0; 
% % % %             gl = orig_ct_Kidney_Axial_in_Sl2(find(orig_ct_Kidney_Axial_in_Sl2));
% % % %                 
% % % %             figure;
% % % %               
% % % %               
% % % %             %  STOP_HERE();  
% % % %              
% % % %             CH_CC = bwconncomp(orig_ct_Kidney_Axial_in_Sl2);
% % % % %             bwc = (bwconvhull(orig_ct_Kidney_Axial_in_Sl2,'objects'));
% % % % %              
% % % % %             CH_CC = bwconncomp(bwc);
% % % %           %   CH_CC 
% % % %             num_of_obj =0;
% % % %   
% % % %             %%  
% % % %             max_inter_CC = 0;
% % % %             % figure; hold on;
% % % %             subplot(1,CH_CC.NumObjects+2,1);
% % % %                     imagesc(orig_ct_Kidney_Axial_in_Sl2);
% % % %             %  max_inter_curr_n_next = 0;
% % % %             %  if  (CC_next_sl.NumObjects>1)
% % % %             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %             % Search for the object with the maximum intersect.... 
% % % %             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %             ii=1; 
% % % %             my_colors = {'r','b','g','y','k'};
% % % %             for ii=[1:CH_CC.NumObjects] 
% % % %                 if (ii > length(my_colors))
% % % %                     break 
% % % %                 end
% % % %                 tmp_sl = zeros(size(orig_ct_Kidney_Axial_in_Sl2));
% % % %                 tmp_sl(CH_CC.PixelIdxList{ii}) = 1;
% % % %                 orig_ct_Kidney_Axial_in_Sl3 =orig_ct_Kidney_Axial_in_Sl2;
% % % %                 orig_ct_Kidney_Axial_in_Sl3(~tmp_sl)=1; 
% % % %                 dt_ = double(orig_ct_Kidney_Axial_in_Sl3(orig_ct_Kidney_Axial_in_Sl3>1));
% % % %  
% % % %                 subplot(1,CH_CC.NumObjects+2,ii+1);
% % % %                     hold on;
% % % %                     imagesc(tmp_sl); 
% % % %                     title(['Mean: ' num2str(mean(dt_(:))) ... 
% % % %                                             ', Std: '   num2str(std(dt_(:))) ] );   
% % % %                     set(gca,'YDir','reverse'); 
% % % %                    % set(gca,'XDir','reverse');
% % % % 
% % % %                 subplot(1,CH_CC.NumObjects+2,CH_CC.NumObjects+2);
% % % %                      hold on;
% % % %                     [n, xout] = hist(dt_,100);  
% % % %                     
% % % %                 disp(['Group-' num2str(ii) ': Mean: ' num2str(mean(dt_(:))) ...
% % % %                                             ', Std: '   num2str(std(dt_(:))) ] );   
% % % %                %  [n, xout] = hist(once_slice(:),512);
% % % %                     n = n/sum(n); 
% % % %                   bar(xout, n,my_colors{ii} ); %, 'barwidth', 1, 'basevalue', 1);
% % % %                 
% % % % %                 inter_curr_next = curr_sl & next_tmp_bw ;
% % % % %                 if (sum( inter_curr_next(:)) > max_inter_CC )  
% % % % %                      max_inter_curr_n_next = next_tmp_bw;
% % % % %                      max_inter_CC = sum( inter_curr_next(:))  ;
% % % % %                 end 
% % % %             end
% % % %         %% 
% % % %         
% % % %             STOP_HERE();   
% % % %              
% % % %             ix = PB_ip_shortcuts.get_image_idx( )
% % % %             img__(:,:, CP.ip(ix).Kidney_Axial_in_Slice) = 1; 
% % % %             img__(:,:, CP.ip(ix).Lung_Axial_Biggest_Slice) = 1;  
% % % %      
% % % %          
% % % %      end
% % % % 
% % % %        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % %  
% % % % %      % CBIR_KIDNEY_aux_func.gross_KIDNEY_marker
% % % % %      function [img__] = gross_KIDNEY_marker_old(img_)
% % % % %       
% % % % %     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% % % % %     % ~~~~~~~~~~~~~ save markers.
% % % % %        
% % % % %         global CP  
% % % % %         
% % % % %         ix = PB_ip_shortcuts.get_image_idx( )
% % % % %               
% % % % %         Bones_A_smalles_Slice_Ytop    = CP.ip(ix).Bones_Axial_smalles_Slice_Ytop;
% % % % %         Bones_A_smalles_Slice_Ybottom = CP.ip(ix).Bones_Axial_smalles_Slice_Ybottom;
% % % % %         Bones_Axial_smalles_Slice         = CP.ip(ix).Bones_Axial_smalles_Slice;
% % % % %         Bronchi_Axial_End_Slice         = CP.ip(ix).Bronchi_Axial_End_Slice;
% % % % %        
% % % % %         Bronchi_Coronal_End_Slice         = CP.ip(ix).Bronchi_Coronal_End_Slice;
% % % % %         SPINAL_w_raito_top = 0.6;
% % % % %         SPINAL_w_raito_bot = 0.3;
% % % % % 
% % % % %         img__ = ones(size(img_)); 
% % % % %         w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
% % % % %         SPINAL_w_raito1 = 120; % round(SPINAL_w_raito_top*w_);
% % % % %         SPINAL_w_raito2 = 120; %round(SPINAL_w_raito_bot*w_);
% % % % %         SPINAL_w_raito3 = 90;  
% % % % %  
% % % % %         img__([1:(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1) (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito2):end  ]   ,   ...
% % % % %              : , ...
% % % % %              : )=0; 
% % % % %          img__(:   ,   ... 
% % % % %               : , ... 
% % % % %              [1:(Bones_Axial_smalles_Slice-SPINAL_w_raito3) (Bones_Axial_smalles_Slice+SPINAL_w_raito3):end] )=0;
% % % % %          img__(:  ,   ...
% % % % %               [1:round(0.85*Bronchi_Coronal_End_Slice)]  , ...
% % % % %              :)=0;
% % % % %          
% % % % %     %      Bronchi_Coronal_End_Slice
% % % % %     %     Bronchi_Axial_End_Slice
% % % % %     %     Bones_Axial_smalles_Slice
% % % % %     
% % % % %      end
% % % % %      
% % % %      
% % % %      
% % % %      %% segment_KIDNEY_stage1
% % % %     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % %     % * * segment_KIDNEY_stage1
% % % %     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % %    
% % % %     
% % % %     function [Aorta_BoundingBox] = segment_KIDNEY_stage1(Aorta_n_Spinal_BoundingBox , Spinal_seg )
% % % %      
% % % %              
% % % %     end 
% % % %         %%
% % % %         
         
    end
    
   
end

 %% segment_LEFT_KIDNEY_triu  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_LEFT_KIDNEY_triu    - triu
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function [res_img] = segment_LEFT_KIDNEY_triu( Aorta_seg ) 
%           
%         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\'); 
%          
%         
%           angle = 90;
%          dim = 3;  
%              
%         
%         
%          
%         verbose_=1;
%         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
%         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');
%         
%         trius_ = [];
%         
%         if ~exist('dim_','var'), dim_= 3; end                           
%      
%         res_img= simple_2D_to_3D.run(Aorta_seg , 'bwulterode' , dim_);
% 
%         clear Aorta_seg
%         res_img = rotate_3D_img(res_img,angle,dim );    
%         res_img = CTpermute(res_img, 'forward', dim_);
%       %    STOP_HERE() 
%  
%         ones_ = ones(size(res_img,2),size(res_img,3),'int16'); 
%         for sl=[size(res_img,1):-1:1]
%              
%             if (verbose_) 
%                 fprintf('.');
%                 if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
%                 if (mod(sl,80)==0),  disp('.'),    end
%             end           
%             curr_sl = squeeze(  res_img(sl,:,:) );
%             [a,b]=ind2sub(size(curr_sl),find(curr_sl));
%             if (~isempty(a))  
%                  triu_ =     b(1)-a(1)+15;     
%                  trius_ = [trius_ triu_];  
%                  triu_ = max([round(trius_) triu_])  ;  
%                  last_triu_idx = sl; 
%                  res_img(sl,:,:) = triu(ones_ , triu_ ); 
%               %  STOP_HERE(); 
%   
%             end  
%         end
%             %   STOP_HERE() 
%      
%          
%         
%         % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%         % * * make sure the lower-bound kidney is in 
%         % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%         global CP 
%         ix = PB_ip_shortcuts.get_image_idx( );   
%         Bones_Axial_smalles_Slice         = CP.ip(ix).Bones_Axial_smalles_Slice; 
%         if (last_triu_idx > Bones_Axial_smalles_Slice)    
%             for sl=[last_triu_idx:-1:Bones_Axial_smalles_Slice-30]
% 
%                 if (verbose_) 
%                     fprintf('.');
%                     if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
%                     if (mod(sl,80)==0),  disp('.'),    end
%                 end  
%   
%               %  if (~isempty(a))   
%                        res_img(sl,:,:) = triu(ones_ , triu_ ); 
%               %   end 
%             end
%         end
%          
%              
%         res_img = CTpermute(res_img, 'backward', dim_);
%         res_img = rotate_3D_img(res_img,-angle,dim );    
% 
%       
%         
%        
%     %      return   
%         
%     end 
%           
    