


% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];         nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
% PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT(); 
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%   extract_body
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index( ' extract_body from the CT  ' );     enabled_=1;  
PB_shortcuts.extract_body(enabled_);
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%  * *  save it!! BODY_  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
out_file_prefix_ = ['BODY_'] ;     out_file_suffix = [];   out_dir = TMP_DIR;    
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file , file2save_t  )    ;
   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  extract_air_organs_from_body
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index( ' extract_air_organs_from_body ' );    enabled_=1;  
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ;
CBIR_LUNG_n_TRACHEA_aux_func.extract_air_organs_from_body( enabled_ );
   
   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  * *  save it!! LUNGS_n_TRACHEA_
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = 'LUNGS_n_TRACHEA_';   out_file_suffix = [ ];          
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   ;
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% * *  lung_markers  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index(' calculate the lung_markers ');
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 
PB.new_Process(  'FUNC_PATH'          , 'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\'  , ...
                 'FUNC_NAME'         , 'CBIR_LUNG_n_TRACHEA_aux_func.lung_markers', ...
                 'Enabled'           , 1 , ... % adsf
                 'INPUT_IMAGE_STEP'  , -1 , ... % ['c_im'] ,  from where to take the input image... ( -1 == c_im)
                 'PRAM_dim_'         , '3'  ); 
             
% PB.add_im_Process(    'CBIR_LUNG_n_TRACHEA_aux_func.lung_markers', ...
%                       'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');
%         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );
%         PB.add_im_Process_parm('dim_' , '3'  ); 
  

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% * *  lung_markers  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index( 'calculate the trachea_markers2 (_mean, _std, _upperBound, _lowerBound ');
%     PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ;
PB.new_Process(  'FUNC_PATH'          , 'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\'  , ...
                 'FUNC_NAME'         , 'CBIR_LUNG_n_TRACHEA_aux_func.trachea_markers2', ...
                 'Enabled'           , 1 , ...        % adsf 
                 'INPUT_IMAGE_STEP' , -6 , ...           % ['c_im_res'  num2str(im_idx-6)] ,... % ['c_im'] ,  from where to take the input image... ( -1 == c_im)
                 'INPUT_IMAGE_STEP' , -2   );            % ['c_im_res'  num2str(im_idx-2)]  ); 
             
             
PB.add_im_Process(    'CBIR_LUNG_n_TRACHEA_aux_func.trachea_markers2', ...
                      'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');   
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-6)] );    %  orig_ct   
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );    %  lungs_n_trachea 


