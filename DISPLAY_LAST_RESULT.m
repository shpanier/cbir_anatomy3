clear all
close all 
global file2save_imagine  
file2save_imagine = 'D:/MY_VISCERAL_TMP/images_for_global_imagine_SPLEEN_ROI_ALL.mat';

addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2');

%% Display Resutls
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% * *    
% * *   Display Resutls
% * *   
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    clearvars -except file2save_imagine
    close all  
    load(file2save_imagine)
    imagine_str = [ 'imagine_v2( ' imagine_global_str(2:end) ' ); ' ];
    eval(imagine_str)
