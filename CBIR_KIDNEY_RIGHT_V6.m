   
%   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];           nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
%PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();    
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%   Extract Kidney Coarse segmentation... 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Extract Coarse segmentation...'); 
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ;    
    PB.add_im_Process('CBIR_SPLEEN_aux_func.Kidney_Coarse_ROI', ...
                         ' ');   
       PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im' ] ); %  ['c_im_res' num2str(im_idx)] );
             PB.add_im_Process_parm('is_left' ,  '0') ;
    
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load BONE_SKELETON_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['BONE_SKELETON_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 

    PB.add_im_Process('imfill', ' ');       
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
        PB.add_im_Process_parm('conn' , '18' ); 
        PB.add_im_Process_parm('holes' , '''holes''' );

    PB.add_im_Process(  'simple_3D_SEmorph.run', ...
                          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
            PB.add_im_Process_parm('oper_type' , '''imdilate''' ); 
            PB.add_im_Process_parm('dim_' , '3' );  
            PB.add_im_Process_parm('SE1' , '''disk''' ); 
            PB.add_im_Process_parm('SE2' , '3' );    
            PB.add_im_Process_parm('SE3' , '0' ); 

    % Remove bones... 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index('Remove bones...'); 

    PB.add_im_Process(    'LogicalAssignment', ...   
             'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');    
             PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );
             PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-1) ')']);
             PB.add_im_Process_parm('C' ,  '0')          

% major change at 21/06/2015
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Spleen/kidny Thresholding 1'); 
    global CP  
    ix = PB_ip_shortcuts.get_image_idx( ); 
  up_  = round((CP.ip(ix).Kidney_Left_GrayLevel_thr1)) + 2*round((CP.ip(ix).Kidney_Left_GrayLevel_thr2)) ;
       
      
  % CP.ip(ix) = pblib_set(CP.ip(ix), 'prm102', up_);                   
     
  %   
    try
        lo_ =  round(round(str2num(CP.ip(ix).prm104))); % - 0.05*round(str2num(CP.ip(ix).prm104)));
    catch 
        lo_ =  round(round((CP.ip(ix).prm104))); %  - 0*round((CP.ip(ix).prm104)));
    end 
   %  CP.ip(ix) = pblib_set(CP.ip(ix), 'prm104', lo_);                   
    
    if (lo_ < 0)    
       lo_ = 40;
    end     

    PB.add_im_Process(  'threshold_image_BW', ...   
                     'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
        PB.add_im_Process_parm('prm_value', ...      
              num2str(lo_) ); 
        PB.add_im_Process_parm('prm_value',  ...          
              num2str(up_) );  

  PB.add_im_Process(    'largest_conncomp', ...  
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
          PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res'  num2str(im_idx)] ); 
  
  PB.add_im_Process(  'simple_2D_to_3D.run', ... 
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
        PB.add_im_Process_parm('oper_type' , '''imfill''' );
        PB.add_im_Process_parm('dim_' , '3' ); 
        
  PB.add_im_Process(  'simple_2D_to_3D.run', ...  
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
        PB.add_im_Process_parm('oper_type' , '''bwareaopen''' );
        PB.add_im_Process_parm('dim_' , '3' );    
         
 PB.add_im_Process(    'largest_conncomp', ...  
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
          PB.add_im_Process_parm('IMAGE_TAMPLATE'  , ['c_im_res'  num2str(im_idx)] );
  
 PB.add_im_Process(  'simple_2D_to_3D.run', ...    
                         'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
            PB.add_im_Process_parm('oper_type' , '''largest_conncomp''' );
            PB.add_im_Process_parm('dim_' , '3' ); 
             
 PB.add_im_Process(  'simple_3D_SEmorph.run', ...
            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
        PB.add_im_Process_parm('oper_type' , '''imclose''' );
        PB.add_im_Process_parm('dim_' , '3' );
        PB.add_im_Process_parm('SE1' , '''disk''' );
        PB.add_im_Process_parm('SE2' , '4' ); 
        PB.add_im_Process_parm('SE3' , '0' );  

            
%PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();         

out_file_prefix_ = ['KIDNEY_RIGHT_V5_'];   
 
out_file_suffix = [];   out_dir = TMP_DIR;    
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   ;
