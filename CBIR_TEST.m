  
CBIR_INIT 
global dry_run CP      
dry_run =0            ;    
                              
%% subjects      
% ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
VISCERAL_SUBMIT_MODE = 0
if (~VISCERAL_SUBMIT_MODE) 
if (1)                                                
    % 10000108                                 
    subjects_ids =[ 10000104 , 10000106, 10000108,  10000109, 10000110,10000111];
     subjects_ids =[ 10000110  ];      
   image_sufix = '_1_CTce_ThAb';     
end              
  
      
if (0)                      
    % 10000105      
    subjects_ids =[ 20000103 ,20000105, 20000106,  20000107, 20000108,20000109];
      subjects_ids =[ 20000110 ,20000112, 20000113,  20000115, 20000117,20000118]; 
    
   subjects_ids = [ 20000103 ,20000105, 20000106,  20000107, 20000108,20000109 ...
        20000110 ,20000112, 20000113,  20000115, 20000117,20000118]; 
          
   %     subjects_ids =[ 20000103  ]; 
    image_sufix = '_1_Moti_CTce_ThAb';
%    subjects_ids =[ 10000110 ]; 
end 
end                        
%%  
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % The main loop    
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
for sbj_id=subjects_ids         
    if (~VISCERAL_SUBMIT_MODE)  
        file2load = [  num2str(sbj_id) image_sufix '.nii'];
        based_on_file =  [ '''' TMP_DIR file2load '''']; 
    end  
       
    
      
            
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    file_prefix = ['LIVER_phase1_V2_AA_'];  change_curr_image_name =0;  nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
    PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();   
   
    
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     % ===> load the CT volume
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     file_prefix = [];       change_curr_image_name =1;      nii_prefix =  [];    
%     PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load , change_curr_image_name ) 
%     PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
%     
%     
%      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     % ===> load the CT volume
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     file_prefix = [];       change_curr_image_name =1;      nii_prefix =  [];    
%     PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load , change_curr_image_name ) 
%     PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();  
   

end
%% <> SAVE & EXIT
% 
%  buffer = pblib_generic_serialize_to_string(C);
%   fid = fopen(CBIR_params_file , 'w+');
%   fwrite(fid, buffer, 'uint8');
%   fclose(fid); 
% clearvars -except CBIR_params_file 

%% call the MAIN

CBIR_main_v6 
return
 

 
%                              
%       %%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     % ===> load volume: orig CT 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
%     im_str = [' ''C:/Users/owner/DATA/VISCERAL/visceral-dataset/trainingset/volumes/' ...
%               num2str(sbj_id) image_sufix '.nii'' '];
%     PB_shortcuts.load_nii_image( im_str );  
%     PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();      
%      
%     %%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     % ===> load volume: LIVE_n_LEFT_KIDNEY_GROSS_SEG_
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
%     im_str2 = [ ' ''C:/Users/owner/DATA/VISCERAL/visceral-dataset/trainingset/volumes/' ...
%         'LIVE_n_LEFT_KIDNEY_GROSS_SEG_'  num2str(sbj_id) '_1_CTce_ThAb.nii'' '];
%     change_curr_image_name = 0; 
%     PB_shortcuts.load_nii_image( im_str2 , change_curr_image_name ); 
%     PB_shortcuts.KEEP3_RESULT_AT_THIS_POINT();
%     
%     %%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%     % ===> load volume: LIVER 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
%     im_str2 = [ ' ''C:/Users/owner/DATA/VISCERAL/visceral-dataset/trainingset/volumes/' ...
%         'LIVER_phase3_V1'  num2str(sbj_id) '_1_CTce_ThAb.nii'' '];
%     change_curr_image_name = 0; 
%     PB_shortcuts.load_nii_image( im_str2 , change_curr_image_name ); 
%     PB_shortcuts.KEEP3_RESULT_AT_THIS_POINT();
%        