classdef CBIR_LIVER_aux_func
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here 
     
     methods(Static)
 
    %% FIND_LIVER_SLICE_AS_SEED 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * FIND_LIVER_SLICE_AS_SEED  - posterior
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [liver_gross_seg ] = FIND_LIVER_SLICE_AS_SEED( liver_gross_seg  ) 
         
                 
        global CP
        ix = PB_ip_shortcuts.get_image_idx( ); 
              
            
        to_ = size(liver_gross_seg,3); 
        sum_ll  = 0;
        sum_ii = 0;
 
        for ii=[1:to_]   
           curr_ = largest_conncomp(liver_gross_seg(:,:,ii));
            
           n_sum_ll = sum(curr_(:));
           
           if (n_sum_ll > sum_ll)
               sum_ll = n_sum_ll;
               sum_ii = ii;

           end
          
        end  
          
        disp(['FIND_LIVER_SLICE_AS_SEED -- Liver_Axial_in_Slice : ' num2str(sum_ii)  ]);  
        CP.ip(ix) = pblib_set(CP.ip(ix), 'Liver_Axial_in_Slice', sum_ii );  
      %  STOP_HERE()
       %   CP.ip(ix) = pblib_set(CP.ip(ix), 'prm109', num2str(0) ); 
    end
      
    
                            
         
    %% segment_LIVER_posterior 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_LIVER_posterior  - posterior
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_img] = HEART_SEPRATOR( Left_lung ) 
         %    STOP_HERE(); 
               
        res_img = zeros(size(Left_lung));
        global CP
        ix = PB_ip_shortcuts.get_image_idx( ); 
           
           
        to_ = size(Left_lung,3); 
        from_   = CP.ip(ix).Lung_Axial_Biggest_Slice -25  ;  
        res_img(:,:,from_:to_) =  Left_lung(:,:,from_:to_);
          
        for ii=[from_:to_]    
           res_ez =  bwconvhull(res_img(:,:,ii)); 
           res_ez =  imcomplement(res_ez); 
           res_img(:,:,ii) =  res_ez;   
        end
         
    end 
    
    
    %% segment_LIVER_posterior 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_LIVER_posterior  - posterior
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_img] = segment_AORTA_posterior( Aorta_seg ) 
        verbose_=1;
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');
        if ~exist('dim_','var'), dim_= 3; end                           
     
        res_img= simple_2D_to_3D.run(Aorta_seg , 'bwulterode' , dim_);
        % clear Aorta_seg
        res_img = CTpermute(res_img, 'forward', dim_);

        ones_ = ones(size(res_img,2),size(res_img,3),'int16'); 
        for sl=[1:size(res_img,1)]
            
            if (verbose_) 
                fprintf('.');
                if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
                if (mod(sl,80)==0),  disp('.'),    end
            end   
            curr_sl = squeeze(  res_img(sl,:,:) );
            [a,b]=ind2sub(size(curr_sl),find(curr_sl));
            if (~isempty(a)) 
             %    b(1)-a(1)  
             % %  STOP_HERE();     
                res_img(sl,:,:) =  cat(2, zeros(size(res_img,2), a(1) ) , ones(size(res_img,2) , size(res_img,3)-a(1) )); 

            end  
        end
        
        res_img = CTpermute(res_img, 'backward', dim_);
     
        
    end 
    
    
    %% segment_LIVER_triu  85ee5601247b67dc03cc9da0b5122cd01bdd6a2ff39ec5adff86734645c11039c69be5dc51a937d318e798ea50e4cb15920773e3d774564695348e10ca84ba38
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_LIVER_triu  - triu
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_img] = segment_LIVER_triu( Aorta_seg ) 
        verbose_=1;
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');
          
        trius_ = [];
        
        if ~exist('dim_','var'), dim_= 3; end                           
     
        res_img= simple_2D_to_3D.run(Aorta_seg , 'bwulterode' , dim_);
        clear Aorta_seg
        res_img = CTpermute(res_img, 'forward', dim_);
      %    STOP_HERE() 
 
        ones_ = ones(size(res_img,2),size(res_img,3),'int16'); 
        for sl=[size(res_img,1):-1:1]
             
            if (verbose_) 
                fprintf('.');
                if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
                if (mod(sl,80)==0),  disp('.'),    end
            end           
            curr_sl = squeeze(  res_img(sl,:,:) );
            [a,b]=ind2sub(size(curr_sl),find(curr_sl));
            if (~isempty(a)) 
                 triu_ =     b(1)-a(1) ; 
                 trius_ = [trius_ triu_]; 
                 triu_ = max([round(trius_) triu_])   ; 
                 last_triu_idx = sl; 
                 res_img(sl,:,:) = triu(ones_ , triu_ ); 
              %  STOP_HERE(); 
            else
                 last_triu_idx = 20;
            end  
        end
            %   STOP_HERE() 
     
        
        
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * make sure the lower-bound kidney is in 
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        global CP  
        ix = PB_ip_shortcuts.get_image_idx( );   
        Bones_Axial_smalles_Slice         = CP.ip(ix).Bones_Axial_smalles_Slice; 
        if (last_triu_idx > Bones_Axial_smalles_Slice)
            to1_ = max([1 , Bones_Axial_smalles_Slice-30])
            for sl=[last_triu_idx:-1:to1_]  

                if (verbose_) 
                    fprintf('.');
                    if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
                    if (mod(sl,80)==0),  disp('.'),    end
                end  
  
              %  if (~isempty(a))   
                       res_img(sl,:,:) = triu(ones_ , triu_ ); 
              %   end 
            end
        end
        
            
        res_img = CTpermute(res_img, 'backward', dim_);
     
        
    end 
    
    %% segment_LIVER_close_chest 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_LIVER_close_chest 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [input_img] = segment_LIVER_close_chest(input_img ) 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %
        PB.add_im_Process(    'CBIR_LIVER_aux_func.segment_LIVER_stage2', ...
            ' ');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );
        global im_idx    
        % ~ ~ ~ ~ ~ ~
        PB.add_im_Process(  'simple_3D_SEmorph.run', ...
            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
        PB.add_im_Process_parm('oper_type' , '''imclose''' );
        PB.add_im_Process_parm('dim_' , '3' );
        PB.add_im_Process_parm('SE1' , '''disk''' );
        PB.add_im_Process_parm('SE2' , '7' );
        PB.add_im_Process_parm('SE3' , '0' );
        
        % ~ ~ ~ ~ ~ ~
        PB.add_im_Process(  'simple_3D_SEmorph.run', ...
            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
        PB.add_im_Process_parm('oper_type' , '''imclose''' );
        PB.add_im_Process_parm('dim_' , '2' );
        PB.add_im_Process_parm('SE1' , '''disk''' );
        PB.add_im_Process_parm('SE2' , '15' );
        PB.add_im_Process_parm('SE3' , '0' );
        
        % ~ ~ ~ ~ ~ ~
        PB.add_im_Process(  'simple_3D_SEmorph.run', ... 
            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
        PB.add_im_Process_parm('oper_type' , '''imclose''' );
        PB.add_im_Process_parm('dim_' , '1' );
        PB.add_im_Process_parm('SE1' , '''disk''' );
        PB.add_im_Process_parm('SE2' , '15' );
        PB.add_im_Process_parm('SE3' , '0' );
        
%          % ~ ~ ~ ~ ~ ~
%         PB.add_im_Process(  'simple_3D_SEmorph.run', ...
%             'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
%         PB.add_im_Process_parm('oper_type' , '''imclose''' );
%         PB.add_im_Process_parm('dim_' , '1' );
%         PB.add_im_Process_parm('SE1' , '''arbitrary''' );
%         PB.add_im_Process_parm('SE2' , '[1 0 ; 0 1]' );
    end
         
         
    %% segment_LIVER_stage2  -close chast 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * segment_LIVER_stage2 -close chast (stage 1 out of 2) 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [input_img] = segment_LIVER_stage2(input_img )
        stp = 10;
        win_ = [1:stp:size(input_img,3)];
        slices_ = zeros(size(input_img,1) , size(input_img,2) , stp);
        sl=1; 
        for sl=[1:length(win_)-1]
            %    sl
            from_idx = win_(sl);
            to_idx = win_(sl+1);
            slices_ = input_img(:,:,from_idx:to_idx-1);
            max_slices_ = logical( max(slices_,[],3) );
            %    STOP_HERE();
            input_img(:,:,from_idx:to_idx-1) = repmat(max_slices_,[1 1 stp]);
        end
    end
    %% LIVER_ROI 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * LIVER_ROI 
    % * *   
    % * * 
    % * * Given:
    % * *     o LIVER_ROI
    % * *  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [img__] = LIVER_ROI(liver_heart_leftKidny_seg, Spinal_strinp )
        % STOP_HERE(); 
%           num_of_slices_ = size(Spinal_strinp,3);
%           Spinal_strinp_slices_ = sum( ...
%                                         reshape(  ...
%                                             permute(Spinal_strinp,[3 1 2]) ...
%                                          ,num_of_slices_,[]) ...
%                                          ,2); 
%                                       
%                                        
%           % Search for the first (lower bound) and the last (upperBound) 
%           % axial slices which does not contain any pixels.     
%           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%           Spinal_strinp_slices_lowerBound_Slice = ...
%                 find(Spinal_strinp_slices_,1, 'first');
%           Spinal_strinp_slices_upperBound_Slice = ... 
%                 find(Spinal_strinp_slices_,1, 'last')  ;
            global CP      
            ix = PB_ip_shortcuts.get_image_idx( )                  
          img__ = liver_heart_leftKidny_seg; 
            Kidney_Left_Axial_lowerBound_Slice =  CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice;
            Lung_Axial_upperBound_Slice_ = CP.ip(ix).Lung_Right_Axial_upperBound_Slice; 
            Liver_Axial_upperBound_Slice = CP.ip(ix).Liver_Axial_upperBound_Slice  ;
        %   STOP_HERE(); 
       
          img__(:   ,   ... 
               :   ,   ...    
             [1:Kidney_Left_Axial_lowerBound_Slice  , ...
                Liver_Axial_upperBound_Slice:end] )=0; 
               
            

% % %         Kidney_Axial_upperBound_Slice  = CP.ip(ix).Kidney_Axial_upperBound_Slice;
% % %         Kidney_Coronal_upperBound_Slice = CP.ip(ix).Kidney_Coronal_upperBound_Slice ;
% % %     
% % %         Lung_Axial_Biggest_Slice = CP.ip(ix).Lung_Axial_Biggest_Slice  ;
% % %          
% % %       
% %      
% %            
% %         
% %         img__ = liver_heart_leftKidny_seg; 
% % %         w_ = abs(Bones_A_smalles_Slice_Ytop-Bones_A_smalles_Slice_Ybottom);
% % %         SPINAL_w_raito1 = 120; % round(SPINAL_w_raito_top*w_);
% % %         SPINAL_w_raito2 = 120; %round(SPINAL_w_raito_bot*w_);
% % %         SPINAL_w_raito3 = 70;
% % %   
% % %          img__([1:(Bones_A_smalles_Slice_Ytop-SPINAL_w_raito1) (Bones_A_smalles_Slice_Ybottom+SPINAL_w_raito2):end  ]   ,   ...
% % %              : , ...
% % %              : )=0;   
% %  
% %          img__(:   ,   ... 
% %                :   ,   ...  
% %              [1:Spinal_strinp_slices_upperBound_Slice , ...
% %                   Spinal_strinp_slices_lowerBound_Slice:end] )=0;
% %              
% % %          img__(:  ,   ...
% % %               [1:Kidney_Coronal_upperBound_Slice]  , ...
% % %              :)=0;
% %          
      
         
    end 
     
    %% Tune_Liver_Thresholding 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Tune_Liver_Thresholding 
    % * *   
    % * * 
    % * * Given:
    % * *     o Tune_Liver_Thresholding
    % * *  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function [liver_seg] = Tune_Liver_Thresholding( liver_seg )
%             global CP    
%             ix = PB_ip_shortcuts.get_image_idx( )
%             Kidney_Axial_upperBound_Slice = CP.ip(ix).Kidney_Axial_upperBound_Slice;
%             Heart_GrayLevel_mean = CP.ip(ix).Heart_GrayLevel_mean;
%             Heart_GrayLevel_std = CP.ip(ix).Heart_GrayLevel_std; 
%             addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\'); 
%             addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');  
%             addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
% %             STOP_HERE(); 
% %             eee=1;    
%             prm101 = -1;
%             STATS.Perimeter = 0; 
%             th_delta = 0.1;
%             th_from = 1.2;
%             th_to = 1.6;
%             low_th = th_from;
%             for low_th = [th_from:th_delta:th_to]
% 
%                 STATS_lst =  STATS; 
%                 tt1 = liver_seg(:,:,Kidney_Axial_upperBound_Slice);
%                 tt1_thr             = threshold_image_BW(tt1, ... 
%                                       Heart_GrayLevel_mean - low_th*Heart_GrayLevel_std  , ...
%                                       Heart_GrayLevel_mean + 0.2*Heart_GrayLevel_std );               
%  
%                 tt1_thr_close           = simple_3D_SEmorph.run(tt1_thr,'imclose',3,'disk',1,0);
% 
%                 tt1_thr_close_fill      = simple_2D_to_3D.run(tt1_thr_close,'imfill',3);        
%                 tt1_thr_close_fill_open = simple_3D_SEmorph.run(tt1_thr_close_fill ...
%                                     ,'imopen',3,'disk',7,0); 
%                 CH = bwconvhull(tt1_thr_close_fill_open)          ;          
%                 STATS = regionprops(CH, 'Perimeter');  
% 
% 
% 
%                dif_ = STATS.Perimeter - STATS_lst.Perimeter ; 
%                if (dif_ < 30 ) 
%                    prm101 = ((low_th-th_delta)); 
%                    CP.ip(ix) = pblib_set(CP.ip(ix), 'prm101', num2str(prm101));    
%                   % prm101 
%                    break  
%                end  
%                 
% %                STOP_HERE();  
% %                eee=1; 
%             end 
% %               STOP_HERE();  
% %                eee=1;
%  
%             if (prm101 == -1) 
%                 prm101 = 1.2;
%                 CP.ip(ix) = pblib_set(CP.ip(ix), 'prm101', num2str(prm101));  
%             end
%     end
%     
    
    
    
     end
end

