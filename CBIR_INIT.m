%% inits
% clc
run(['C:/Dropbox/MY_CODE/lib/MATLAB/MY_LOCAL_MATLAB_SETTING.m']);
base_code_lib_dir = getenv( 'base_code_lib_dir');
base_code_dir  = getenv( 'base_code_dir');

addpath([base_code_dir '/lib/Misc/protobuf-matlab/protobuflib']);
addpath([base_code_dir '/PHD/CBIR_anatomy3/protobuf']);   % protobuf my proto files.
addpath([base_code_dir '/lib/MATLAB/imp/NIFTI_analyze_toolbox']);
addpath([base_code_dir '/lib/MATLAB/imp/NIFTI_analyze_toolbox_gzip_extension']);
   
addpath([base_code_lib_dir '/plots_n_figs/cprintf']);
 
%  STOP_HERE();     
%% Protocol buffer initiation
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
global TMP_DIR  TMP_DATA_DIR

if (exist('VISCERAL_TMP','var')==0) 
        input('VISCERAL_TMP is emplty, are you sure you want to proceed.') ;
        VISCERAL_TMP =  ''
end  
    
TMP_DIR = VISCERAL_TMP;  
TMP_DATA_DIR = VISCERAL_TMP; 
  
global CBIR_params_file CBIR_ip_params_file

global C im_idx p_idx prm_idx CP;
im_idx = 0; 
p_idx = 0;  
prm_idx = 0;  
% % if (VISCERAL_SUBMIT_MODE)  
hostname = char( getHostName( java.net.InetAddress.getLocalHost ) );
  
CBIR_params_file = ['./' hostname '_pipline_paramters.pb']; 
CBIR_ip_params_file = ['./' hostname '_images_paramters.pb']; % 'VISCERAL_CBIR_images_prms3_Psubmit.pb';

C = pb_read_CBIR(); 
C = pblib_set(C, 'im', pb_read_image_processing());

cprintf([1,0.5,0.5], 'pb_read_CBIR: init a new file \n'); 


% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% For testing the CP values.
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%{

fid = fopen(CBIR_ip_params_file);
buffer = fread(fid, [1 inf], '*uint8');
fclose(fid); 
CP = pb_read_CBIR_images_prms(buffer);

%}


