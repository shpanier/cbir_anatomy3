  

%% process it... % major change at 28.5.2014@17:30
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
          
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];         nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
%  PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 
% PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  

% ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load LUNGS_n_TRACHEA_ segmentation  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['LUNGS_n_TRACHEA_'];    nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load ) 
  

%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  TRACHEA   threshold   
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index( 'find the TRACHEA_threshold' ); 

%  PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();   
PB.add_im_Process(    'CBIR_TRACHEA_aux_func.TRACHEA_threshold', ...
                      'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');  
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );    % orig_ct 

        
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%   TRACHEA BI-ROI
%                Extract the gross segmentation... 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index(' Extract Breathing system BI-ROI..' );  
%  PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
PB.add_im_Process(    'LogicalAssignment', ...   
     'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');      
     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );  
     PB.add_im_Process_parm('B' ,              ['~logical(c_im_res'  num2str(im_idx-1) ,')']);
     PB.add_im_Process_parm('C' ,  '0')    
   
%%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~              
%  Segment_with_given_slice_as_seed 
%     Smooth_gross_Seg_WOct_Wslice_as_seed(        ...
%                                         gross_seg                     , ...
%                                         the_seed_slice                , ... 
%                                         sliceNumber_as_seed           , ...
%                                         to_upper                      , ... 
%                                         to_lower                      , ...
%                                         max_allow_thres               , ...
%                                         object_w_max_intersect_only   , ...
%                                         SCS_type                      , ...  %  the type of smoothness for consecutive slice
%                                         dbg_gt_slice                  , ...
%                                         dbg_lt_slice                       )  ; 

%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~      
PB_shortcuts.set_new_image_index( ' Smooth_region growing to  adjacent slices ');     
%  PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();  


ix = PB_ip_shortcuts.get_image_idx( );  
min_segmentation_size = 100; % 100 pixels  

 
Trachea_Axial_upperBound_Slice = CP.ip(ix).Trachea_Axial_upperBound_Slice-1;
Trachea_Axial_lowerBound_Slice = CP.ip(ix).Lung_Axial_Biggest_Slice   ; 
Trachea_Axial_in_Slice = CP.ip(ix).Trachea_Axial_in_Slice    ;   
   
% STOP_HERE() ; 
if (Trachea_Axial_upperBound_Slice == -1)
    error('Trachea_Axial_upperBound_Slice == -1')
end 

allow_growing = 1;  
object_w_max_intersect_only = 1;
epsilon_f_segmentation_size = 10;  
max_allow_thres = 8;    
dbg_ =0;     
SCS_type = 3    ;
dbg_gt_slice = 437  ;      
dbg_lt_slice  =  437; 
dbg_gt_slice = 1437  ;      
dbg_lt_slice  =  0 ; % 415 
 

%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% smooth it...    
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB.add_im_Process(    'CBIR_LEAK_REMOVAL_v1.Smooth_gross_Seg_WOct_WsliceNumber_as_seed', ...
             'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');      
 
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ...    
                           ['c_im_res' num2str(im_idx-1)] );    % gross segment
         PB.add_im_Process_parm('sliceNumber_as_seed' , ...
                          num2str(Trachea_Axial_in_Slice) );      % sliceNumber_as_seed  

         PB.add_im_Process_parm('to_upper' , ...     
                          num2str(Trachea_Axial_upperBound_Slice) );   % to_upper   
         PB.add_im_Process_parm('to_lower' , ...   
                          num2str(Trachea_Axial_lowerBound_Slice));    % to_lower 
         PB.add_im_Process_parm('max_allow_thres' , ...     
                          num2str(max_allow_thres));   % max_allow_thres             
         PB.add_im_Process_parm('object_w_max_intersect_only' , ...      
                            num2str(object_w_max_intersect_only));   % max_allow_thres             
         PB.add_im_Process_parm('SCS_type' , ...       
                                num2str(SCS_type));                           % SCS_type    
         PB.add_im_Process_parm('epsilon_f_segmentation_size' , ...       
                             num2str(epsilon_f_segmentation_size));     % stop cratria                
         PB.add_im_Process_parm('dbg_gt_slice' , ...      
                            num2str(dbg_gt_slice));                             % dbg_gt_slice             
         PB.add_im_Process_parm('dbg_lt_slice' , ...      
                            num2str(dbg_lt_slice));                             % dbg_lt_slice             

%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% smooth it...    run_w_CT(   ct_img, ...
%                                        seg_img, ...
%                                        method_, dim_,  ...
%                                          verbose_)  
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB_shortcuts.set_new_image_index();     % -1
PB.add_im_Process(    'simple_2D_to_3D.run_w_CT', ... 
          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');   
    PB.add_im_Process_parm('ct_img' , ['c_im_res' num2str(im_idx-5)] );
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
    PB.add_im_Process_parm('oper_' ,'''FastRegionGrowing2'''  );
    PB.add_im_Process_parm('dim_' , '3' );                       
                        
                        % PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it!    
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = 'TRACHEA_V2_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   

