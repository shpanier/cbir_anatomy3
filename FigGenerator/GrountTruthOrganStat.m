%% init
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\NIFTI_analyze_toolbox')
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\NIFTI_analyze_toolbox_gzip_extension')
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2c')

subj = [ 100 104 105 106 108 109 110 111 112 113 127 128 129 130 131 132 133 134 135 136  ] 
DATA_PATH = 'D:/DATA/VISCERAL/volumes/';

GT_PATH = 'D:/DATA/visceral3-dataset/Anatomy3-trainingset/Segmentations/';
%% 
LiverRadID = '58';  % liver                      
SpleenRadID = '86'  ;   % Spleen                    
rKidneyRadID = '29662' ;% right kidney   
lKidneyRadID =   '29663' ;   % left  kidney   
leftLungRadID = '1326';  % left Lung     
righLungtRadID = '1302'  ;% right  Lung


allStat = {};
allStat(1).subjName = sb

sb = 111;
% 10000129_1_CTce_ThAb_58_5.nii.gz
ii =0;
for sb = subj
    ii = ii +1;
    allStat(ii).subjName = sb;
    
    %
    % Load CT
    %
    curr_image_name = ['10000' num2str(sb) '_1_CTce_ThAb'];    
    subjectFile  = [DATA_PATH curr_image_name '.nii.gz'];
    ctIm = load_untouch_nii_gzip(subjectFile);
    ctIm = ctIm.img; 
    disp([' Subject Name: ' subjectFile '  '])
    disp([' ***************** '])
    
    %
    % load liver segmentaion
    % 
    LiverSegFile = [GT_PATH '10000' num2str(sb) '_1_CTce_ThAb_' LiverRadID '*.nii.gz'];
    r= dir(LiverSegFile);
    LiverSegFile = [GT_PATH r.name];
    segLiverIm = load_untouch_nii_gzip(LiverSegFile);
    segLiverIm = segLiverIm.img;
    
    %
    % load right Kidney segmentaion
    % 
    rKidneySegFile = [GT_PATH '10000' num2str(sb) '_1_CTce_ThAb_' rKidneyRadID '*.nii.gz'];
    r= dir(rKidneySegFile);
    rKidneySegFile = [GT_PATH r.name];
    segrKidneyIm = load_untouch_nii_gzip(rKidneySegFile);
    segrKidneyIm = segrKidneyIm.img;
    
    %
    % load left Kidney segmentaion
    %     
    lKidneySegFile = [GT_PATH '10000' num2str(sb) '_1_CTce_ThAb_' lKidneyRadID '*.nii.gz'];
    r= dir(lKidneySegFile);
    lKidneySegFile = [GT_PATH r.name];
    seglKidneyIm = load_untouch_nii_gzip(lKidneySegFile);
    seglKidneyIm = seglKidneyIm.img;

    
    %
    % load Spleen segmentaion
    % 
    SpleenSegFile = [GT_PATH '10000' num2str(sb) '_1_CTce_ThAb_' SpleenRadID '*.nii.gz'];
    r= dir(SpleenSegFile);
    SpleenSegFile = [GT_PATH r.name];
    segSpleenIm = load_untouch_nii_gzip(SpleenSegFile);
    segSpleenIm = segSpleenIm.img;
 
    %
    % load left lung segmentaion
    % 
    leftLungSegFile = [GT_PATH '10000' num2str(sb) '_1_CTce_ThAb_' leftLungRadID '*.nii.gz'];
    r= dir(leftLungSegFile);
    leftLungSegFile = [GT_PATH r.name];
    leftLungIm = load_untouch_nii_gzip(leftLungSegFile);
    leftLungIm = leftLungIm.img;
    
    %
    % load right lung segmentaion
    %  
    rightLungSegFile = [GT_PATH '10000' num2str(sb) '_1_CTce_ThAb_' righLungtRadID '*.nii.gz'];
    r= dir(rightLungSegFile);
    rightLungSegFile = [GT_PATH r.name];
    rightLungIm = load_untouch_nii_gzip(rightLungSegFile);
    rightLungIm = rightLungIm.img;
 
    
    
    
    LiverVoxVal   =  ctIm(segLiverIm~=0);
    rKidneyVoxVal = ctIm(segrKidneyIm~=0);
    lKidneyVoxVal = ctIm(seglKidneyIm~=0);
    spleenVoxVal = ctIm(segSpleenIm~=0); 
    leftLungVoxVal = ctIm(leftLungIm~=0 | rightLungIm~=0 );  
    lungBV =  leftLungVoxVal(leftLungVoxVal>50);
    
    allStat(ii).LiverVoxVal = LiverVoxVal;
    allStat(ii).rKidneyVoxVal = rKidneyVoxVal;
    allStat(ii).lKidneyVoxVal = lKidneyVoxVal;
    allStat(ii).spleenVoxVal = spleenVoxVal;
    allStat(ii).lungBV = lungBV;

    fig = figure;
        grid on;
        hold on;     
%         [n xout] =hist(LiverVoxVal,50);     n = n/sum(n);
%         bar(xout,n,'b');
%         [n xout] = hist(rKidneyVoxVal,50); n = n/sum(n);
%         bar(xout,n,'r');
%         [n xout] = hist(lKidneyVoxVal,50); n = n/sum(n);
%         bar(xout,n,'g');
        [n xout] = hist(spleenVoxVal,50); n = n/sum(n);
        bar(xout,n,'k');
%         [n xout] = hist(lungBV,50); n = n/sum(n);
%         bar(xout,n,'m');
         
        legend('Liver' , 'Right Kidney' , 'Left Kidney' , 'Spleen', 'lung BV' )
        title(num2str(sb));
         xlim([-400 600]); 
        print(fig,['organHist_subj_10000'  num2str(sb) '_v4' ],'-dpng')
       
    close(fig)
end

save('allStat.mat','allStat')
%%
% mean / std plot.
meanVal = zeros(5, length(allStat));
stdVal = zeros(5, length(allStat));

for ii=1:length(allStat)
    
    LiverVoxVal = allStat(ii).LiverVoxVal;
    rKidneyVoxVal = allStat(ii).rKidneyVoxVal ;
    lKidneyVoxVal = allStat(ii).lKidneyVoxVal;
    spleenVoxVal = allStat(ii).spleenVoxVal ;
    lungBV = allStat(ii).lungBV;
    
    meanVal(1, ii) = mean(LiverVoxVal);
    meanVal(2, ii) = mean(rKidneyVoxVal);
    meanVal(3, ii) = mean(lKidneyVoxVal);
    meanVal(4, ii) = mean(spleenVoxVal);
    meanVal(5, ii) = mean(lungBV);

    stdVal(1, ii) = std(LiverVoxVal);
    stdVal(2, ii) = std(rKidneyVoxVal);
    stdVal(3, ii) = std(lKidneyVoxVal);
    stdVal(4, ii) = std(spleenVoxVal);
    stdVal(5, ii) = std(lungBV);

end
%%
figure;
    hold on;
    grid on;
    c = {'bx','rx','gx','kx','mx'}
    for ii=1:4
        errorbar(meanVal(ii, :) ,stdVal(ii, :), c{ii})
    end
     errorbar(meanVal(5, :) ,stdVal(5, :), c{5})
    legend('Liver' , 'Right Kidney' , 'Left Kidney' , 'Spleen', 'lung BV' )

%% 
%
% plot Spleen
%
C = [];
grp = []
for jj=1:length(allStat)
    x =  allStat(jj).spleenVoxVal;
    C = [C;x]; 
    grp = [grp , jj*ones(1,length(x)) ];
end

fig= figure
    hold on 
    grid on
    boxplot(C,grp,'Symbol','','Whisker',2)
    ylim([-50 350]);
    xlabel('Subject num')
    ylabel('Gray Value')
    title('Spleen')

print(fig,'spleen_boxPlot_','-dpng')

%% - -  
C = [];
grp = []
for jj=1:length(allStat)
    x =  allStat(jj).LiverVoxVal;
    C = [C;x]; 
    grp = [grp , jj*ones(1,length(x)) ];
end

fig = figure
    hold on
    grid on
    boxplot(C,grp,'Symbol','','Whisker',2)
    ylim([-50 350]);
    xlabel('Subject num')
    ylabel('Gray Value')
    title('Liver')
  
    print(fig,'Liver_boxPlot_','-dpng')

%% - -  
C = [];
grp = []
for jj=1:length(allStat)
    x =  allStat(jj).rKidneyVoxVal;
    C = [C;x]; 
    grp = [grp , jj*ones(1,length(x)) ];
end

fig = figure
    hold on
    grid on
    boxplot(C,grp,'Symbol','','Whisker',2)
    ylim([-50 350]);
    xlabel('Subject num')
    ylabel('Gray Value')
    title('Right Kidney')
  
    print(fig,'Right_Kidney_boxPlot_','-dpng')
    
    
%% - -  
C = [];
grp = []
for jj=1:length(allStat)
    x =  allStat(jj).lKidneyVoxVal;
    C = [C;x]; 
    grp = [grp , jj*ones(1,length(x)) ];
end

fig = figure
    hold on
    grid on 
    boxplot(C,grp,'Symbol','','Whisker',2)
    ylim([-50 350]);
    xlabel('Subject num')
    ylabel('Gray Value')
    title('Left Kidney')
  
    print(fig,'Left_Kidney_boxPlot_','-dpng')    
    
%% - -  
C = [];
grp = []
for jj=1:length(allStat)
    x =  allStat(jj).lungBV;
    C = [C;x]; 
    grp = [grp , jj*ones(1,length(x)) ];
end

fig = figure
    hold on
    grid on 
    boxplot(C,grp,'Symbol','r','Whisker',2)
    ylim([-50 350]);
    xlabel('Subject num')
    ylabel('Gray Value')
    title('lung BV')
  
    print(fig,'lung_BV_boxPlot_','-dpng')    