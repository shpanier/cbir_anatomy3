addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\chummersone-boxPlot-cdcdfca')
clear all
close all
clc

% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% load pre-cacluate values. (
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

load('allStat.mat','allStat')

% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% For testing the CP values.
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
addpath('../')
CBIR_INIT 
fid = fopen(['../' CBIR_ip_params_file]);
buffer = fread(fid, [1 inf], '*uint8');
fclose(fid); 
CP = pb_read_CBIR_images_prms(buffer);



%% 
%
% plot Spleen
%

% - - - - - - - - - - - - - - -
% Plot 5 subjects on each plot
% - - - - - - - - - - - - - - -
for kk=1:5:20
    C = [];
    grp = []
    allData = []; 
    subjsLbl = [];
    organLbl = [];
   
    % - - - - - - - - - - - - - - -
    % 4 organs for each subject.
    % - - - - - - - - - - - - - - -
    
    fig = figure
    hold on
    pp=1
    for jj=kk:kk+4 % length(allStat)
        CP.ip(jj).ImageName
        Kmean = str2num(CP.ip(jj).prm104)
        bone = str2num(CP.ip(jj).prm102)    
 
        % rKidneyUp_  = round(round(str2num(CP.ip(jj).prm104)) - 0.1*round(str2num(CP.ip(jj).prm104)));
        
        up_  = round((CP.ip(jj).GrayLevel_thr1)) + 3*round((CP.ip(jj).GrayLevel_thr2)) ;
        % leftKidneyLow =  round(round((CP.ip(jj).prm104)) - 0.1*round((CP.ip(jj).prm104)));
       % Kmean = rKidneyUp_; 
         lo_ =  round(((CP.ip(jj).GrayLevel_thr1))) - 0.5*round((CP.ip(jj).GrayLevel_thr2));
       %  leftKidneyUP =  round(round(str2num(CP.ip(ix).prm102)))    ;

        LiverVoxVal = allStat(jj).LiverVoxVal;
        rKidneyVoxVal = allStat(jj).rKidneyVoxVal ;
        lKidneyVoxVal = allStat(jj).lKidneyVoxVal;
        spleenVoxVal = allStat(jj).spleenVoxVal ;
        lungBV = allStat(jj).lungBV;
 
        subjData = [LiverVoxVal; rKidneyVoxVal ; lKidneyVoxVal ; spleenVoxVal; lungBV];
        allData = [allData ; subjData ];
        subjsLbl = [subjsLbl , jj*ones(1,length(subjData)) ];
     
        plot([jj-0.5 jj+0.5],[Kmean Kmean],'r');
        plot([jj-0.5 jj+0.5],[bone bone],'r--');

        plot([jj-0.5 jj+0.5],[up_ up_],'k');
        
        plot([jj-0.5 jj+0.5],[lo_ lo_],'k--');

        pp=pp+1;
        organLbl = [organLbl , 1*ones(1,length(LiverVoxVal)) ...
                             , 2*ones(1,length(rKidneyVoxVal))...
                             , 3*ones(1,length(lKidneyVoxVal)) ...
                             , 4*ones(1,length(spleenVoxVal)) ...
                             , 5*ones(1,length(lungBV)) ...

                             ];

    end
    % 
    [y,x,g] = tab2box(subjsLbl',allData,organLbl');
    %
    
    h = boxPlot(x,y,...
      'boxColor','auto','medianColor','k',...
      'scalewidth',true,'xseparator',true,...
      'groupLabels',g,'showLegend',true,'showOutliers',false);
    %box on
    grid on
    hold on
     
    print(fig,['barPlot_'  num2str(kk) ],'-dpng')

end
