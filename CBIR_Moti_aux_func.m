classdef CBIR_Moti_aux_func

    methods(Static)
   
    %% Axial_Biggest_Slice 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Axial_Biggest_Slice
    %   Get:
    %      o orig_ct_im  - orig ct (as its name implies :) 
    %
    %   Set:  
    %     � Lung_Axial_Biggest_Slice
    % 
    %
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res] = Axial_Biggest_Slice( orig_ct_im)
        res = orig_ct_im; 
        
         global CP
         ix = PB_ip_shortcuts.get_image_idx( ) 
         CP.ip(ix) = pblib_set(CP.ip(ix), 'Lung_Axial_Biggest_Slice', size(orig_ct_im,3));
         CP.ip(ix) = pblib_set(CP.ip(ix), 'Liver_Axial_upperBound_Slice', size(orig_ct_im,3));

 
         
    end

	%% connect last Axial slice 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * connect last Axial slice
    %   
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [chest_ct] = connect_last_Axial_slice( LUNGS_n_TRACHEA, chest_ct)
      %   STOP_HERE();    
           global CP 
        ix = PB_ip_shortcuts.get_image_idx( )
        Lung_Axial_Biggest_Slice =  CP.ip(ix).Lung_Axial_Biggest_Slice;
        
                 last_sl  = LUNGS_n_TRACHEA(:,:,Lung_Axial_Biggest_Slice);  
                last_sl_conv = bwconvhull(last_sl);
                SE_disk8 =  strel('disk',8,0);  
                last_sl_conv_dilate = imdilate(last_sl_conv , SE_disk8);
                 res__ = (last_sl_conv_dilate-last_sl_conv); 
                chest_sl  = chest_ct(:,:,Lung_Axial_Biggest_Slice);
                chest_sl(logical(res__))=1;
				chest_ct(:,:,Lung_Axial_Biggest_Slice)=chest_sl; 
		
  
         
    end
	
    
    
    end
end

