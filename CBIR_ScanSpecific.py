import sys
sys.path.append(r'C:\build\VTK\bin\Release')
sys.path.append(r'C:\build\VTK\Wrapping\Python')
sys.path.append(r'C:\Dropbox\MY_CODE\lib\Python\PIMP')
sys.path.append(r'C:\Dropbox\MY_CODE\PHD\SpineSegmentation')
sys.path.append(r'C:\Dropbox\MY_CODE\lib\VTK')
sys.path.append(r'C:\Dropbox\MY_CODE\lib\Python\etc')
sys.path.append(r'C:\Dropbox\MY_CODE\lib1\opencv300\opencv\build\python\2.7\x64')

import imp_utils
import numpy as np
import morph_oper

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
#  B_seg -
#
#       Extract_body from the CT
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
def B_seg( image_data ):
    """
    """

    ret_ = np.where((image_data > -500) & (image_data < 2000), 1, 0)

    print("largest CC")
    ret_ = imp_utils.largest_conncomp(ret_)

    print("simple_3D_SEmorph - opening ")
    ret_ = morph_oper.simple_3D_SEmorph( ret_ )
    print(" opening done..")

    print("largest CC")
    ret_ = imp_utils.largest_conncomp(ret_)

    return ret_

    # Mathlab code
    # - - - - - - -
    #c_im_res2=threshold_image_BW(c_im,-505,2000);
    #c_im_res2=simple_3D_SEmorph.run(c_im_res2,'imopen',1,'disk',2,0);
    #c_im_res2=simple_3D_SEmorph.run(c_im_res2,'imopen',2,'disk',2,0);
    #c_im_res2=simple_2D_to_3D.run(c_im_res2,'largest_conncomp',3);


# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
#  A_seg
#    - Extract Air organs from body
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
def A_seg( image_data ):
    """

    """

    print("largest CC")
    ret_ = np.logical_not(image_data)

    ret_ = imp_utils.im_3d_clearborder(ret_)


    ret_ = imp_utils.largest_conncomp(ret_)

    ret_ = morph_oper.simple_3D_imfill( ret_ )

    return( ret_ )

    # Mathlab code
    # - - - - - - -
    #
    # c_im_res4=im_invert(c_im);
    # c_im_res4=simple_2D_to_3D.run(c_im_res4,'imclearborder',3);
    # c_im_res4=largest_conncomp(c_im_res4);


# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
#  SK_seg -
#
#            Spleen Kidney threshold diff
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
def SK_seg( image_data ):
    """

    """

    print("largest CC")
    ret_ = np.logical_not(image_data)

    ret_ = imp_utils.im_3d_clearborder(ret_)


    ret_ = imp_utils.largest_conncomp(ret_)

    ret_ = morph_oper.simple_3D_imfill( ret_ )

    return( ret_ )

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
#   analyse_subject:
#
def analyse_subject( subj_name ,use_pb= True ):
    """

    """
#
#
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    import  morph_oper
    print "= = "*20
    p_id =  subj_name

    if (use_pb):
        print "Load use_pb"
        sys.path.append(r'./protoBuf/')
        sys.path.append(r'../../../lib/Misc/protobuf-matlab/protobuflib')
        sys.path.append(r'C:\Dropbox\MY_CODE\lib\Misc\protobuf-matlab\protobuflib')

        import PB # need to install https://pypi.python.org/pypi/protobuf/2.4.1#downloads
                # or (pip install protobuf)

        p_ = PB.PB()
        p_.setImageName2idx(p_id)

    import socket
    hostname_ = socket.gethostname()
    print(hostname_)
    if (hostname_ =='LAPTOP-JCS65RGV'):
        file_name = "E:/VISERAL_OUT/INTERMEDIATE_RES/" + p_id + "/" + p_id +".nii.gz"
    else:
        file_name = "D:/MY_VISCERAL_TMP/" + p_id + ".nii.gz"




    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    #  load 3 copy of the files.
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    image_data ,img_affine,header_  = nii_utils.Nifti2Np( file_name  )
    image_data3 ,img_affine,header_ = nii_utils.Nifti2Np( file_name  )
    image_data4 ,img_affine,header_ = nii_utils.Nifti2Np( file_name  )
    image_data5 ,img_affine,header_ = nii_utils.Nifti2Np( file_name  )


    spaceing_ = (header_.get_sform()[0][0],  header_.get_sform()[1][1] , header_.get_sform()[2][2])

    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    # Get body
    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    b_ndarray = B_seg( image_data )

    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    # Get airways
    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    a_ndarray = A_seg( b_ndarray )

    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    # blood mean/std -->
    #      By isolating all voxels with values that are
    #      greater than zero inside the lungs
    #
    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    image_data3[a_ndarray!=1]=0

    # image_data3[image_data3<0]=0
    # image_data3[image_data3>15]=1
    aa = image_data3[image_data3>15]
    #
    # nii_utils.Np2Nifti(image_data3 , img_affine,header_,  to_file_name = "d:/MY_VISCERAL_TMP/" +p_id +"_bv_mean_.nii")
    bv_mean_ = np.mean(aa)
    bv_std_ = np.std(aa)
    print "blood HU est: , bv_mean_: ", bv_mean_ , "bv_std_: ", bv_std_

    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    # image_data3[image_data3!=0]=1
    # print """
    # # save lung seg to temp file ...just for sanity check
    # """
    # nii_utils.Np2Nifti(image_data3 , img_affine,header_)
    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    if (use_pb):
        p_.get_param().GrayLevel_thr1 = int(bv_mean_)
        p_.get_param().GrayLevel_thr2 = int(bv_std_)

    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    # bone_segmentation
    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    image_data[b_ndarray==0]=0
    import SpineSegmentaion

    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    # (1) SpineSegmentaion and (2) Bone threshold
    #
    #   o min_  -  bone threshold
    #   o idx_  -  Scan after threshing with the 'min_' value..
    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -

    #min_ = 0
    #if (use_pb):
    #    min_  = int(p_.get_param().prm102)
    #    ext_res_str = p_.get_param().prm101
    #    print "bone threshold min: " , min_

    #if (min_ == 0):
    # to_file_png_name_ = "D:/DATA/VISCERAL/volumes/" + p_id + ".png"
    idx_ , min_, ext_res_str = SpineSegmentaion.bone_segmentation( image_data , spaceing_)
    #else:
    #    idx_ = image_data > min_
    #    idx_ = idx_.astype(np.int8)

    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    #  Finalized the Spine Segmentation
    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    idx_ = morph_oper.simple_3D_SEmorph_dilate3d( idx_ )
    idx_ = morph_oper.simple_3D_SEmorph_close3d( idx_ )
    idx_ = morph_oper.simple_3D_imfill( idx_ )

    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    #  Calculate the Spleen/Kidney threshold diff
    # - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - -
    import morph_oper

    # - - - - - - - - - - - - - - - - - -
    # Isolate the Body
    # - - - - - - - - - - - - - - - - - -
    image_data4[b_ndarray==0]=0
    p_.get_param()
    ret_ = np.where((image_data4 > bv_mean_-10) & (image_data4 < min_+10), 1, 0)
    image_data4[ret_==0]=0

    # - - - - - - - - - - - - - - - - - -
    # Remove the bone skeleton
    # - - - - - - - - - - - - - - - - - -
    image_data4[idx_==1]=0

    # - - - - - - - - - - - - - - - - - -
    # Define the ROI
    # - - - - - - - - - - - - - - - - - -
    Lung_Left_Axial_lowerBound_Slice_ = p_.get_param().Lung_Left_Axial_lowerBound_Slice
    Sagittal_mid_Slice_ = p_.get_param().Sagittal_mid_Slice
    Cornal_mid_Slice_ = p_.get_param().Cornal_mid_Slice
    Bones_Axial_smalles_Slice = p_.get_param().Bones_Axial_smalles_Slice

    image_data4[:,:,Lung_Left_Axial_lowerBound_Slice_+35:]  = 0
    image_data4[:,:,1:Bones_Axial_smalles_Slice-40:]  = 0
    image_data4[Sagittal_mid_Slice_-40:,:,:]  = 0
    image_data4[: ,:Cornal_mid_Slice_-50,:]  = 0

    image_data5[image_data4>0] = 1
    image_data5[image_data4==0] = 0

    # - - - - - - - - - - - - - - - - - -
    # remove small objects
    # - - - - - - - - - - - - - - - - - -
    from scipy import ndimage
    label_im, nb_labels = ndimage.label(image_data5)
    sizes = ndimage.sum(image_data5, label_im, range(nb_labels + 1))
    mask_size = sizes < 5000
    remove_pixel = mask_size[label_im]

    image_data4[remove_pixel]=0

    # - - - - - - - - - - - - - - - - - -
    # Kmean - with K = 2
    #
    #  The average of those two cluster centers as mu-kmean . Those values will
    #  be used to define the threshold that differentiate between the kidneys,
    #  the spleen and the liver.
    # - - - - - - - - - - - - - - - - - -
    from sklearn.cluster import KMeans
    kk_model = KMeans(2)
    flat_data = image_data4.ravel()
    flat_data = flat_data[flat_data>0]
    flat_data = [[x] for x in flat_data]
    kk_model.fit(flat_data)
    kmean_centers_= [int(kk_model.cluster_centers_[0][0]),
                     int(kk_model.cluster_centers_[1][0])]
    print "kmean_centers_: " , kmean_centers_ , "--> mean: ", np.mean(kmean_centers_)

    from skimage.filters import threshold_otsu
    thresh = threshold_otsu(image_data4[image_data4>0])
    print "threshold_otsu: ", thresh

    image_data4[image_data4>0]=1
    #nii_utils.Np2Nifti(image_data4 , img_affine,header_ ,
    #                   to_file_name = "d:/MY_VISCERAL_TMP/" +p_id +"_idx_6.nii")

    eee=1

    # - - - - - - - - - - - - - - - - - -
    # save the values into PB
    # - - - - - - - - - - - - - - - - - -
    if (use_pb):
        print "ext_res_str: ", ext_res_str
        p_.get_param().prm101 = ext_res_str
        p_.get_param().prm102 = str(min_)
        p_.get_param().prm103 = str(kmean_centers_)
        p_.get_param().prm104 = str(np.mean(kmean_centers_))
        p_.get_param().isContrasted = True 
        print p_.get_param().isContrasted
        p_.get_param().modality = 'CTce'
        p_.save_config()


    eee = 1


# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# MAIN
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
if __name__ == "__main__":
    sys.path.append("C:/Dropbox/MY_CODE/lib/Python/PIMP/")
    import nii_utils
    # - - - - - - - - - - - - - - - - - -
    # save the values into PB
    # - - - - - - - - - - - - - - - - - -
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--subj-id' , action='store' , dest='p_id' ,
                        help='Subject ID to process' )
    results = parser.parse_args()
    print 'subj_id =', results.p_id

    import copy
    use_pb = True
    analyse_subject( results.p_id ,use_pb )


    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    # MAIN - old with loop
    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    # p_id = "10000100_1_CTce_ThAb"
    # p_id = "10000104_1_CTce_ThAb"
    # p_id = "10000106_1_CTce_ThAb"
    # p_id = "10000108_1_CTce_ThAb"
    # p_id = "10000110_1_CTce_ThAb"
    # p_id = "10000111_1_CTce_ThAb"
    # p_id = "10000112_1_CTce_ThAb"
    # p_id = "10000113_1_CTce_ThAb"
    # p_id = "10000127_1_CTce_ThAb"
    # p_id = "10000129_1_CTce_ThAb"
    # p_id = "10000128_1_CTce_ThAb"
    # p_id = "10000415_1_CTce_ThAb"
    # p_id = "10000416_1_CTce_ThAb"

    # p_id = "10000110_1_CTce_ThAb"
    # subjs = ["10000100_1_CTce_ThAb","10000104_1_CTce_ThAb","10000105_1_CTce_ThAb","10000106_1_CTce_ThAb","10000108_1_CTce_ThAb","10000109_1_CTce_ThAb","10000110_1_CTce_ThAb","10000111_1_CTce_ThAb","10000112_1_CTce_ThAb","10000113_1_CTce_ThAb","10000127_1_CTce_ThAb","10000128_1_CTce_ThAb","10000129_1_CTce_ThAb","10000130_1_CTce_ThAb","10000131_1_CTce_ThAb","10000132_1_CTce_ThAb","10000133_1_CTce_ThAb","10000134_1_CTce_ThAb","10000136_1_CTce_ThAb"]

    # ,"10000416_1_CTce_ThAb","10000417_1_CTce_ThAb","10000418_1_CTce_ThAb","10000419_1_CTce_ThAb","10000420_1_CTce_ThAb","10000422_1_CTce_ThAb","10000423_1_CTce_ThAb","10000424_1_CTce_ThAb","10000425_1_CTce_ThAb","10000427_1_CTce_ThAb","10000428_1_CTce_ThAb","10000429_1_CTce_ThAb","10000431_1_CTce_ThAb","10000135_1_CTce_ThAb","10000105_1_CTce_ThAb"]

    # subjs = ["10000015_1_CT_wb"]
    # subjs = ["10000113_1_CTce_ThAb"]

    #use_pb = True
    #for p_id in subjs:
    #    analyse_subject( p_id ,use_pb )


