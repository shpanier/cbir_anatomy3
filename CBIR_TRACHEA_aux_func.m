classdef CBIR_TRACHEA_aux_func
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
     
     methods(Static)
 
    %% TRACHEA_threshold
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * TRACHEA_threshold
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [input_img] = TRACHEA_threshold(input_img )
        global CP  

 
        ix = PB_ip_shortcuts.get_image_idx( );  
        Trachea_GrayLevel_mean = CP.ip(ix).Trachea_GrayLevel_mean
        Trachea_GrayLevel_std = CP.ip(ix).Trachea_GrayLevel_std; 

        %         PB.add_im_Process(  'threshold_image_BW', ...   
        %                          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
        %         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
        %         PB.add_im_Process_parm('prm_value', ...
        %               num2str(2*Trachea_GrayLevel_mean )   );   
        %         PB.add_im_Process_parm('prm_value',  ...      
        %               num2str(  Trachea_GrayLevel_mean )   );  
         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
         input_img=threshold_image_BW(input_img,2*Trachea_GrayLevel_mean,Trachea_GrayLevel_mean);                                                               


    end
    %% extract_trachea_from_air_organs
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * extract_trachea_from_air_organs
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [input_img] = extract_trachea_from_air_organs(input_img )
     global CP 
 
       ix = PB_ip_shortcuts.get_image_idx( );
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     %   Trachea_Axial_Start_Slice 
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     trachea_from = CP.ip(ix).Trachea_Axial_Start_Slice;
      
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     %   Trachea_Axial_Mid_Slice
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     trachea_to = CP.ip(ix).Trachea_Axial_Mid_Slice; 

     
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     %   Lung_Axial_Biggest_Slice 
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     Lung_Axial_Biggest_Slice = CP.ip(ix).Lung_Axial_Biggest_Slice; 
     
     orig_input_img = input_img;
     input_img(:,:,trachea_from:end)=0;
      input_img(:,:,1:trachea_to)=0;
     input_img(:,:,1:Lung_Axial_Biggest_Slice)=0; 
     %input_img(:,:,trachea_to:trachea_from)= input_img(:,:,trachea_to:trachea_from)*2;
     verbose_=1; 
 
      %  
      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
      %  The main loop to extect the teachea.... 
      %
      %
      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
      for sl=[trachea_from-1:-1:Lung_Axial_Biggest_Slice]
        if (verbose_)
           fprintf('.');
           if (mod(sl,10)==0)
                fprintf(num2str(sl));
           end

           if (mod(sl,80)==0)
               disp('.')
           end
        end  
         
        curr_sl = (squeeze(input_img(:,:,sl))); 
        next_sl = (squeeze(orig_input_img(:,:,sl-1)));
        CC_next_sl = bwconncomp(next_sl);
        num_of_obj =0;
         
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % Loop throw each Connected-Components in the "next slice" 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        for ii=[1:CC_next_sl.NumObjects]
            tmp_bw = zeros(size(input_img ,1), size(input_img ,2));
            tmp_bw(CC_next_sl.PixelIdxList{ii}) = 1;
            inter_curr_next = curr_sl & tmp_bw ;
            
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            % Check if the curent CC intersect with the previous slide.
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            if (any(inter_curr_next(:))) 
                 
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   %   save the bifurcation location                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   CP.ip(ix) = ... 
                       pblib_set(CP.ip(ix), 'Bronchi_Axial_End_Slice', sl);
     
   
                if (num_of_obj ==1)
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   % The bifurcation!
                   % if more the one object intersect!
                   %   --> more the one object interact == The bifurcation
                   %
                   %  In this case: return & set to zero all the following slices..  
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   input_img(:,:,1:sl )=0;
                   
                   
                   return 
                end 
                
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                % check if the "object" in the next slice is twice as big as 
                %  the "object" in the current slice
                % -> this is an indcation of LEAKAGE into another organ 
                %
                % In this case: keep only the intersect of the current &
                %               the next slice
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                if (sum(tmp_bw(:)) > 2*sum(curr_sl(:)))
                    input_img(:,:,sl-1) = inter_curr_next; 
                    num_of_obj =1;
                    continue 
                end
                  
                
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                % If we got here: it's means the the intersect between the
                % two consequent is "reasonable" 
                %
                % In this case: keep only the overlapping/coincident object
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
             %     imagesc(tmp_bw)
                input_img(:,:,sl-1) = tmp_bw;
                num_of_obj =1;
            end 
        end
        
      end   


eee=1;
    end
    
      
     end
end





 
        
%         
%         
%         one_cc_diff = (diff(one_cc));
%         one_cc_diff(one_cc_diff>1)=0;
%   
%         
%             CC = bwconncomp(one_cc_diff);
%             numPixels = cellfun(@numel,CC.PixelIdxList);
%             for ii=1:length(numPixels)
%                 ar =CC.PixelIdxList(ii);
%                 one_cc_diff(ar{:}) = numPixels(ii);
%                 num_of_pix_ = num_of_pix(one_cc);
%             end
% 
%             diff_TIMES_pix1 = num_of_pix_(2:end).*one_cc_diff;
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%             %  Extract the largest compneent of "ones"
%             %   --> this is saved in ""diff_TIMES_pix""
%             %
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% 
%             diff_TIMES_pix = one_cc_diff;
% 
%             x_sum =0;  
%             Trachea_from =0;
%             Trachea_to=0;
%             diff_TIMES_pix = [0 diff_TIMES_pix 0];
%             zeros_ind = find(diff_TIMES_pix==0);
% 
%    
%             % ~ ~ ~ ~ 
%             for ii=[1:length(zeros_ind)-1]
%         %         STOP_HERE();
%                 tx_sum =sum(diff_TIMES_pix(zeros_ind(ii):zeros_ind(ii+1)));
%                 if (tx_sum >  x_sum)
%                     x_sum = tx_sum;
%                     Trachea_to = one_cc(zeros_ind(ii));
%                     Trachea_from = one_cc(zeros_ind(ii+1)-1); 
%                 end
%             end 
%   
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%              %   Trachea_Axial_Start_Slice 
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% 
%            %  CP.ip().Trachea_Axial_Start_Slice = Trachea_from; 
%                CP.ip(ix) = pblib_set(CP.ip(ix), 'Trachea_Axial_Start_Slice', Trachea_from);
%           %  CP.ip(PB_ip_shortcuts.get_image_idx( )).Trachea_Axial_Mid_Slice = Trachea_to;


