  
% major update 9.06.2014

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];            nii_prefix =  [];    
process_description_str = ' * * * Skeleton * * * '; 
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load , process_description_str )
% PB_shortcuts.add_process_description(   )
%PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();   
   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load BODY_
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['BODY_'];   nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE(); 
 
%    continue  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Remove non-body region'); 
PB.add_im_Process(    'LogicalAssignment', ...   
    'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');    
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] ); 
    PB.add_im_Process_parm('B' ,              ['~logical(c_im_res' num2str(im_idx-1) ')']);
    PB.add_im_Process_parm('C' ,  '0') 

       
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ~~~~~~~~~~~~ -> extract_bones_tight
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('extract_bones_tight'); enabled_=1;  
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();   
 
CBIR_SKELETON_aux_func.extract_bones_tight(enabled_ , file2load   );
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it! 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = 'BONE_SKELETON_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   
%PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();  
  
   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  ->       gross_CT_area (** bwconvhull **)
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index();     % -1

PB.add_im_Process(    'simple_2D_to_3D.run', ... 
        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
    PB.add_im_Process_parm('oper_' ,'''bwconvhull'''  );
    PB.add_im_Process_parm('dim_' , '3' ); 
      
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% * *     CBIR_SKELETON_aux_func.gross_CT_landmarks (+ plot)
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('landmarks: Widest, narrowest skeleton resions..');
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();  
    PB.add_im_Process( 'CBIR_SKELETON_aux_func.gross_CT_landmarks', ...
                 'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , 'c_im')    % BONE_SKELETON_ 

 