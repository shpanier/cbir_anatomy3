classdef CBIR_LEAK_REMOVAL_v1
    methods(Static)
    %% ACC_Seg_w_given_sliceNumber_as_seed 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * ACC_Seg_w_given_sliceNumber_as_seed
    %   Given -
    %       o orig_ct
    %       o gross_mask
    %       o slice_as_seed  ( assumption: ~ middel slice )
    %       o to_upper 
    %       o to_lower  
    % 
    % 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
            
    function [fine_mask] = ACC_Seg_w_given_sliceNumber_as_seed( orig_ct     , ...
                                                        gross_mask             , ...
                                                        sliceNumber_as_seed    , ...
                                                        to_upper               , ... 
                                                        to_lower               , ...
                                                        larget_cc_in_seed     ) 
         
         if ~exist('larget_cc_in_seed','var'), larget_cc_in_seed = 0; end  
         
         if (larget_cc_in_seed)
             the_seed_slice = largest_conncomp(squeeze(gross_mask(:,:,sliceNumber_as_seed)));                                  
         else
             the_seed_slice = squeeze(gross_mask(:,:,sliceNumber_as_seed));                                  
         end
                                             
         [fine_mask] = CBIR_LEAK_REMOVAL_v1.ACC_Seg_w_given_slice_as_seed( ...
                                            orig_ct     , ...
                                            gross_mask             , ...
                                            the_seed_slice         , ... 
                                            sliceNumber_as_seed    , ...
                                            to_upper               , ... 
                                            to_lower                    ) ; 
                                        
                      
                                                     
    end
 
    %% ACC_Seg_w_given_slice_as_seed  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * ACC_Seg_w_given_slice_as_seed
    %   Given -
    %       o orig_ct
    %       o gross_mask
    %       o slice_as_seed  ( assumption: ~ middel slice )
    %       o to_upper 
    %       o to_lower  
    % 
    %  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
          
    function [fine_mask] = ACC_Seg_w_given_slice_as_seed( ...
                                            orig_ct                     , ...
                                            gross_mask                  , ...
                                            the_seed_slice              , ... 
                                            sliceNumber_as_seed         , ...
                                            to_upper                    , ... 
                                            to_lower                    , ...
                                            down_Active_Contour_max_its ,...
                                            up_Active_Contour_max_its   , ...
                                            allow_growing   )  
        %   
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  Inints....  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        dbg_  = 0  ;       
          
        if ~exist('verbose_','var'), verbose_ = 1; end  
        if ~exist('allow_growing','var'), allow_growing = 0; end  
        
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Segmentaion\ActiveContour\');
        alpha_p = .2; 
        dsp_p = false   ;       
        if (dbg_ ==1)
           dsp_p = true     ;       
        end 

        fine_mask = zeros(size( orig_ct ));
        from_ =     sliceNumber_as_seed;
        min_segmentation_size = 150;
     
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  Starting point: 
        %       o this is the seed ( seed slice)
        %       o  more or less a middel slice,
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        fine_mask(:,:,from_) =  the_seed_slice ;
   
        for delta_ = [-1 , 1]

            if (delta_ == -1)   
                to_ = to_lower; 
                if (~exist('down_Active_Contour_max_its','var'))
                       Active_Contour_max_its = 22 ;
                else
                       Active_Contour_max_its = down_Active_Contour_max_its ;
                end

            else 
                to_ = to_upper;
                if (~exist('up_Active_Contour_max_its','var')) 
                       Active_Contour_max_its = 12 ;
                else
                       Active_Contour_max_its = up_Active_Contour_max_its ;
                end

            end
            
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
            %  LOOP from the seed starting point up & down.... 
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            for jj = from_:delta_:to_
                 if (verbose_)
                    fprintf('.'); 
                    if (mod(jj,10)==0), fprintf(num2str(jj)); ,  end
                    if (mod(jj,80)==0),  disp('.'),    end
                 end  
%  
%     jj = Kidney_Axial_upperBound_Slice ...
%      -2           
                msk = int16(fine_mask(:,:,jj));
                orig_ct_sl = squeeze(orig_ct(:,:,jj+delta_));
                orig_ct_gross_mask = squeeze(gross_mask(:,:,jj+delta_));
                orig_ct_sl(~orig_ct_gross_mask)=0;
%     if (jj < 240 )       
%              dbg_  = 1;  
%     end  
   % figure      
               try
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   %  ATIVE CONTOUR
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                    seg =  region_seg( orig_ct_sl  , msk , Active_Contour_max_its  , alpha_p, dsp_p ); %-- Run segmentation

                    if (dbg_ ==1)
                       eee=1 ; 
                    end   
                catch  
%                       error('region_seg'); 
                    break  
                end             
%   pause   
%  seg3 =    CBIR_LEAK_REMOVAL_v1.Consecutive_slices ( seg , msk); 
 
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                %  Smooth_Curvature_in_Consecutive_slices
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                object_w_max_intersect_only =1;     
                seg3 = CBIR_LEAK_REMOVAL_v1.smoothit1_consecutive_slices(                 ...
                                        msk                    , ...
                                        seg                     , ... 
                                        object_w_max_intersect_only , ...
                                        dbg_                        , ...
                                        allow_growing                ); 
                                    
                                    
                                
  
                seg_size = sum(seg3(:)) ;
                if (seg_size < min_segmentation_size)  
                     disp('min reached 1');   
                     break  
                end  
                fine_mask(:,:,jj+delta_) = seg3;
    
            end 
        end
    end
     
       
     
% end
      
    %% Smooth_gross_Seg_WOct_Wslice_as_seed 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * Smooth_gross_Seg_WOct_Wslice_as_seed
    %   Given - 
    %       o gross_mask
    %       o slice_as_seed  ( assumption: ~ middel slice )
    %       o to_upper 
    %       o to_lower  
    % 
    % 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  This function is used by:
    % = = = = = = = = = = = = = 
    %   * CBIR_TRACHEA2_V2
    %   * SPLEEN_V5
    %
    %
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    function [fine_mask] = Smooth_gross_Seg_WOct_WsliceNumber_as_seed(     ...
                                        gross_seg                     , ...
                                        sliceNumber_as_seed           , ...
                                        to_upper                      , ... 
                                        to_lower                      , ...
                                        max_allow_thres               , ...
                                        object_w_max_intersect_only   , ...
                                        SCS_type                      , ...  %  the type of smoothness for consecutive slice
                                        epsilon_f_segmentation_size   , ... 
                                        dbg_gt_slice                  , ...
                                        dbg_lt_slice                      )  
                                                        
        the_seed_slice = squeeze(gross_seg(:,:,sliceNumber_as_seed)); 
        
        [fine_mask] = CBIR_LEAK_REMOVAL_v1.Smooth_gross_Seg_WOct_Wslice_as_seed(        ...
                                        gross_seg                     , ...
                                        the_seed_slice                , ... 
                                        sliceNumber_as_seed           , ...
                                        to_upper                      , ... 
                                        to_lower                      , ...
                                        max_allow_thres               , ...
                                        object_w_max_intersect_only   , ...
                                        SCS_type                      , ...  %  the type of smoothness for consecutive slice
                                        epsilon_f_segmentation_size   , ... 
                                        dbg_gt_slice                  , ... 
                                        dbg_lt_slice                       )  ; 
                                        
                    
                                                    
     end
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

   %%
%                     seg11 = squeeze(gross_seg(:,:,349));
% 
%         imagesc(seg11);

    %%   Smooth_gross_Seg_WOct_Wslice_as_seed
    function [fine_mask] = Smooth_gross_Seg_WOct_Wslice_as_seed(        ...
                                        gross_seg                     , ...
                                        the_seed_slice                , ... 
                                        sliceNumber_as_seed           , ...
                                        to_upper                      , ... 
                                        to_lower                      , ...
                                        max_allow_thres               , ...
                                        object_w_max_intersect_only   , ...
                                        SCS_type                      , ...  %  the type of smoothness for consecutive slice
                                        epsilon_f_segmentation_size   , ... 
                                        dbg_gt_slice                  , ...
                                        dbg_lt_slice                  ) 
  
        if ~exist('dbg_','var'),  dbg_ =0; end  
        if ~exist('verbose_','var'),  verbose_ =1; end  

        if ~exist('allow_growing','var'),  allow_growing = 0; end  
        if ~exist('object_w_max_intersect_only','var'),  object_w_max_intersect_only = 0; end 
           
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %   first debug level 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        if ((dbg_gt_slice~=0)  || (dbg_lt_slice~=0))
           %   dbg_ =1;   
        end 
        
   
%         if (dbg_ > 1)
%             STOP_HERE(); 
%         end   
        fine_mask = zeros(size( gross_seg ));
 
        from_ =     sliceNumber_as_seed;
        fine_mask(:,:,from_) =  the_seed_slice ;
         
        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
        %  LOOP from the seed starting point up & down.... 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        for delta_ = [-1 , 1]

            if (delta_ == -1) 
                to_ = to_lower;  
            else 
                 
                to_ = to_upper;
            end  
             
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
            %
            %  LOOP from the seed starting point up & down.... 
            %
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            for jj = from_:delta_:to_
                if (verbose_) 
                    fprintf('^');  
                    if (mod(jj,10)==0), fprintf(num2str(jj)); ,  end
                    if (mod(jj,80)==0),  disp('.'),    end
                end 
               
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                %   Second debug level 
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                if ((jj >= dbg_gt_slice)  || (jj <= dbg_lt_slice))
                    disp(['slice num: ' num2str(jj) ]);
                    dbg_ =2;  
                end  
                 
                msk = int16(fine_mask(:,:,jj)); 
                seg = squeeze(gross_seg(:,:,jj+delta_));
                %   
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                %  Smooth_Curvature_in_Consecutive_slices
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

                switch( SCS_type ) 
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

                    case 1
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    %  smoothit2 - don't all "reverse peramid"
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    allow_growing =1; 
                    seg3 = CBIR_LEAK_REMOVAL_v1.smoothit1_consecutive_slices( ...
                                        msk                         , ...
                                        seg                         , ...
                                        object_w_max_intersect_only , ...
                                        dbg_                        , ...
                                        allow_growing                );  
                                    
                    case 2  
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    %  smoothit2 - prevent "reverse pyramids"
                    %
                    %      - prevent leaks in the form of "reverse pyramids"
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    seg3 = CBIR_LEAK_REMOVAL_v1.smoothit2_consecutive_slices( ...
                                        msk                         , ...
                                        seg                         , ...
                                        object_w_max_intersect_only , ...
                                        max_allow_thres             , ...
                                        dbg_                         );  
                    case 3 
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    %  smoothit3 - find_bifurcation 
                    %      -     
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~     
                    seg3 = CBIR_LEAK_REMOVAL_v1.smoothit3_consecutive_slices_find_bifurcation( ...
                                        msk                         , ...
                                        seg                         , ...
                                        dbg_                         ); 
                                    
                                    
                    case 4 
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    %  smoothit4 - separate_lungs      
                    %      -     
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~     
                    seg3 = CBIR_LEAK_REMOVAL_v1.smoothit4_consecutive_slices_separate_lungs( ...
                                        msk                         , ...
                                        seg                         , ...
                                        dbg_                         );  
                                                            
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                    otherwise  
                          
                        
                        
                end 
% if (dbg_>1)      
%     subplot(1,3,1);
%         imagesc(seg);  
%         title(['seg']);
%     subplot(1,3,2);
%         imagesc(msk);   
%         title(['msk']);
%     subplot(1,3,3);
%         imagesc(seg3);  
%         title(['seg3, sl-number:' (num2str(jj))]);
%  
%     STOP_HERE();   
%  end 

           
                seg_size = sum(seg3(:)) ; 
                if (seg_size < epsilon_f_segmentation_size)  
                     disp('min reached 1');   
                     break  
                end   
                fine_mask(:,:,jj+delta_) = logical(seg3) ; 
           end 
        end
    end
    
     
    %% smoothit1_consecutive_slices 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * smoothit1_consecutive_slices
    %
    %       o curr_sl
    %       o next_sl 
    %       o object_w_max_intersect_only
    %       o dbg_ 
    %       o allow_growing
    %
    %
    %
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        
    function [res_] = smoothit1_consecutive_slices(                 ...
                                        curr_sl                    , ...
                                        next_sl                     , ... 
                                        object_w_max_intersect_only , ...
                                        dbg_                        , ...
                                        allow_growing                )
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % init  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        res_ = [];     
        if ~exist('allow_growing','var'),                    ...
                allow_growing = 0; end
        if ~exist('dbg_','var'),                    ...
                dbg_= 0; end
        if ~exist('check_for_max_intersect','var'), ...
                    object_w_max_intersect_only= 0; end

      
        SE_disk2 = strel('disk',3,0);

        BW = imerode(curr_sl,SE_disk2); 
        inner_ = curr_sl & ~BW;

        region_growed =   ~curr_sl  & next_sl; 
        region_growed = int8(region_growed);
        if (sum(region_growed(:))==0)
            res_ =  int8(curr_sl  & next_sl); 
          %    STOP_HERE();   
             disp('--> sum(region_growed(:))==0');
            return 
        end 
                %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                %         %   calcuate the Curvature on the given image. 
                %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                %   Curvature_threshold = -8;
                %         plot_it = 0;     
                %         [ crv ] = CBIR_LEAK_REMOVAL_v1.bwCurvature( next_sl ,Curvature_threshold, plot_it );  
                %         crv2 = crv;  

                
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
         % Search for the object with the maximum intersect.... 
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
         if (object_w_max_intersect_only)
             next_tmp_bw = CBIR_LEAK_REMOVAL_v1.object_with_max_intersect( curr_sl , next_sl);
             if (~allow_growing)
                crv2 =   int8(next_tmp_bw & curr_sl  );
             else
                crv2 =   int8(next_tmp_bw); 
             end
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
         % Else, takes everthing... 
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
         else 
             if (~allow_growing) 
                crv2 =   int8(next_sl & curr_sl  );
             else
                crv2 =   int8(next_sl); 
             end
         end
         res_ =   crv2; 
                
%          if (dbg_==1) 
%               if (0)
%                  %%
%                  figure,
%                     subplot(2,2,1)
%                         imagesc(next_tmp_bw);
%                     subplot(2,2,2)
%                         imagesc(next_sl);
%                     subplot(2,2,3)
%                         imagesc(res_);
%                     subplot(2,2,4)   
%                         imagesc(int8(~next_tmp_bw  & curr_sl2));
% 
%                  %% 
%               end 
%               STOP_HERE();          
%               eee=1; 
%          end 
 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %   Return in cases that growing isn't allowed.
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        if (~allow_growing)   
            return  
        end
       
          
           
           
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  Set a Windows and Trace the boundiers with this given window
        %      
        %  Two options:
        %     (1) When the curvature in the windows exceeds 
        %         the threshold value takes the intersects of the 
        %         curr_sl2 and next_sl (a.k.a curr_sl2 & next_sl)
        %
        %     (2) the curvature in the windows don't exceeds 
        %         the threshold takes the next_sl 
        %
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                
         % set the growd to -1
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
         crv2(region_growed>=1)=-1; 
         
         % set the "inner ring" to -2
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
         crv2(logical(inner_)) = -2; 

         % keep only the the "inner ring" and the "growd region (-1 & -2) 
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
         curr_sl3 = crv2;
         curr_sl3(curr_sl3>0)=0;
         if (dbg_>=2)  
            imagesc(curr_sl3);  
            set(gca,'XLim' , [168.242 424.242])
            set(gca,'YLim' , [0.5 256.5])
            ee=1; 
          %   ff= figure('Position', [1 31 1920 974]); 
         end 
         
     
        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % bounderies...
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
       
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\Geometry\bresenham_line');
        [B,L,N,A] = bwboundaries(curr_sl3);
 
         if (length(B)>1) 
%%  
            for k=1:2,  
                    boundary = B{k};
                if(k > 1)
                    in_brd =  boundary;
                else
                    out_brd =  boundary; 
                end
            end 
            map_in_out = round(linspace(1,length(in_brd),length(out_brd)));
            in_brd2  =  in_brd(map_in_out,:);
            outmat = zeros(size(curr_sl3));
   
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            % loop throut the bounderies (with delata of  dlt_)
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            dlt_ = 80;      
            strt_ =1;    
       
           for strt_=[1]
   %         for strt_=[1 ]
       
                for ii=strt_:dlt_:length(out_brd)-dlt_
                outmat = zeros(size(curr_sl3));

                [~,~,outmat,X1,Y1] = ...
                bresenham(outmat, [in_brd2(ii,2) in_brd2(ii,1) ; ...
                                 out_brd(ii,2) out_brd(ii,1)],0);


                [~,~,outmat,X2,Y2] = ... 
                 bresenham(outmat, [in_brd2(ii+dlt_,2) in_brd2(ii+dlt_,1) ; ...
                                    out_brd(ii+dlt_,2) out_brd(ii+dlt_,1)],0);
 
                all_X = [X1 , in_brd2(ii:ii+dlt_,1)' , fliplr(X2) ,fliplr(out_brd(ii:ii+dlt_,1)')  ];
                all_Y = [Y1 , in_brd2(ii:ii+dlt_,2)' , fliplr(Y2) ,fliplr(out_brd(ii:ii+dlt_,2)')  ];
                idd = sub2ind(size(outmat) , all_X, all_Y);
                outmat(idd)=1;
                outmat = bwconvhull(outmat);
                    curr_sl_inner_n_outer = curr_sl3; 
                    curr_sl_inner_n_outer(~outmat)=0; 
                    curr_sl_inner_n_outer = abs(curr_sl_inner_n_outer);
                curr_sl4 = curr_sl; %  squeeze( orig_ct(sl,:,:));
                 
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                %  The inner part
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                curr_sl_inner = curr_sl3; 
                    curr_sl_inner(~outmat)=0; 
                    curr_sl_inner = abs(curr_sl_inner);
                    curr_sl_inner(curr_sl_inner~=2)=0;   % (2 is the innder index) 
                    inner_STATS = regionprops(curr_sl_inner, 'Orientation' ...
                                   , 'MajorAxisLength', 'MinorAxisLength' );
                    try           
                        inner_STATS = inner_STATS(2)  ;         
                    end
                  %  inner_STATS
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                %  The outer part
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                curr_sl_outer = curr_sl3; 
                    curr_sl_outer(~outmat)=0;    
                    curr_sl_outer = abs(curr_sl_outer);
                    curr_sl_outer(curr_sl_outer~=1)=0;   % (1 is the innder index) 
                    outer_STATS = regionprops(curr_sl_outer, 'Orientation' ...
                                    , 'MajorAxisLength', 'MinorAxisLength' );
                  
                                 
                 if (~isempty(outer_STATS)) 
                    for ii=1:length(outer_STATS)   
                        out_mjor = outer_STATS.MajorAxisLength ;
                        out_minor = outer_STATS.MinorAxisLength;
                        in_mjor = 0.9*inner_STATS.MajorAxisLength;
                        in_minor = inner_STATS.MinorAxisLength;
                               
                        if ( (out_mjor > 0.9*in_mjor)) %   && ( in_mjor> 0.2*))
                              
                            if ((dbg_>=1))
                                disp(['remove object']);     
                            end
                            res_(logical(curr_sl_outer))=1;
%                         else   
%                             disp(['added object']);  
%                             res_  = res_ | curr_sl_outer ;
                        end
                    end
                 end  
%                 if (dbg_==1)
% %                    ff= figure('Position', [1 31 1920 974]);  
%   STOP_HERE();          
%             
%                      eee=1;  
%                    imagesc(curr_sl_inner_n_outer);  
%                    set(gcf, 'Position', [1 50 1920 900]); 
%                    set(gca,'XLim' , [168.242 424.242]);
%                    set(gca,'YLim' , [0.5 256.5]);
%                     STATS = regionprops(curr_sl_inner_n_outer, 'Orientation' , 'MajorAxisLength', ...
%                                                'MinorAxisLength' );
%  
%                     STATS(1).MajorAxisLength;
%                     STATS(2).MajorAxisLength;
%                     xlabel({[' STATS(1).MinorAxisLength:            ' ...
%                             num2str(STATS(1).MinorAxisLength)], ... 
%                             [' STATS(2).MinorAxisLength:            ' ... 
%                             num2str(STATS(2).MinorAxisLength)],    ...
%                             ['                  STATS(1).MajorAxisLength: ' ...
%                             num2str(STATS(1).MajorAxisLength)], ...
%                             ['                  STATS(2).MajorAxisLength: ' ...
%                             num2str(STATS(2).MajorAxisLength)]} );
%                     STOP_HERE();          
%            
%                     eee=1; 
%                end 
                end
            end 
         end 
                % res_(logical(curr_sl2)) =1;
                  
%                     
 % ff= figure('Position', [1 31 1920 974]); 
%      figure   
%      imagesc(res_);
%      title('res_'); 
%     set(gca,'XLim' , [168.242 424.242])
%     set(gca,'YLim' , [0.5 256.5])
%            if (dbg_==1)
%                figure  
%                 imagesc(res_); 
%                 STOP_HERE();           
%  
%                  eee=1; 
%            end   
         
    end 
     
    %% smoothit2_consecutive_slices
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * smoothit2_consecutive_slices   
    %            - prevent leaks in the form of "reverse pyramids"
    %
    % 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_] = smoothit2_consecutive_slices( curr_sl                    , ...
                                                   next_sl                     , ...
                                                   object_w_max_intersect_only , ...
                                                   max_growth_allowed                , ...
                                                   dbg_                       );  
    %% init  
        growth_factor = 1.5;  
        if ~exist('max_growth_allowed','var'), max_growth_allowed= 8; end
        if ~exist('dbg_','var'), dbg_ = 0; end

        SE_disk2 = strel('disk',2,0);     
        SE_disk_max_allow = strel('disk', growth_factor* max_growth_allowed ,0);
        curr_sl = int8(curr_sl); 
       
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % Search for the objects with the maximum intersect.... 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        if (object_w_max_intersect_only)
            next_tmp_bw = CBIR_LEAK_REMOVAL_v1.object_with_max_intersect( curr_sl , next_sl);
        else 
            next_tmp_bw = CBIR_LEAK_REMOVAL_v1.object_with_any_intersect( curr_sl , next_sl);
           %  next_sl; 
        end  
%         if (isempty(next_tmp_bw))
%             STOP_HERE(); 
%             res_= next_tmp_bw;
%             return
%         end
         
        %%  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  curr_sl2 is: -- 
        %          (curr_sl2 - include the contour level that overlap with the next-slice)
        %
        %  Dilate upto the end of the next-slide 
        %       o Build "contour" diffrences between the current and the next slice.
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        curr_sl2 = int8(curr_sl); 
        num_of_dilation = 0;
        inter_curr_next2 =1;  
        post_dilate = curr_sl2;
        post_dilate(post_dilate>0) =1; 
        curr_sl2(curr_sl2>0) =1;      
        while ((num_of_dilation~= growth_factor*max_growth_allowed) && any(inter_curr_next2(:)))
            num_of_dilation = num_of_dilation+1  ;
            pre_dilate = post_dilate;
            post_dilate = imdilate(pre_dilate,SE_disk2);
            idxx = ~pre_dilate & post_dilate;
            curr_sl2(idxx)=num_of_dilation+1;
            inter_curr_next2 = ~logical(curr_sl2) & next_tmp_bw;
        end
         
        if (dbg_>0) 
            any_inter_curr_next2 = any(inter_curr_next2(:))
            num_of_dilation 
        end   
        
        %% 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %    curr_sl_contour   - ring upto the  a 'contour level' bigger then 3.
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        curr_sl_contour=[];
  
        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %    if 'max_growth_allowed' ** IS ** exeided in curr_sl2 
        %          THEN take the intersect of the two slices as seed
        %       --> every *NOT-LEAK* will be added into the *SEED* 
        %
        %   if 'max_growth_allowed' ** IS-NOT ** exeided
        %          return "the objects with the maximum intersect"      
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        if (find(curr_sl2>max_growth_allowed))  
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                curr_sl3_dilate_disk8 = imdilate(curr_sl,SE_disk_max_allow);
                curr_sl3_erode_disk8  = imerode(curr_sl,SE_disk_max_allow);
                curr_sl_contour = curr_sl3_dilate_disk8 & ~curr_sl3_erode_disk8;
                 
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                res_seed_ = curr_sl & next_tmp_bw; 
        else
            res_ =  next_tmp_bw; 
            return;   
        end
           
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  curr_sl2_bnds - 
        %        a 'contour level' up-to 'max_growth_allowed'.
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        curr_sl2_bnds = curr_sl2;
        curr_sl2_bnds(~logical(curr_sl_contour))=0;
        curr_sl2_bnds(~logical(next_tmp_bw))=0;
         
        %%
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %   check bounderies...
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % curr_sl3 = seg3;
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\Geometry\bresenham_line');
        [B,~,~,~] = bwboundaries(curr_sl_contour);
 
        %%  return  
        if (length(B)>1)
%%
            for k=1:2,  
                    boundary = B{k};
                if(k > 1)
                    in_brd =  boundary;
                else
                    out_brd =  boundary; 
                end
            end 

            %in_brd2 = zeros(size(out_brd));
            map_in_out = round(linspace(1,length(in_brd),length(out_brd)));
            in_brd2  =  in_brd(map_in_out,:);
            outmat = zeros(size(curr_sl2_bnds));
          %  ct1_ = squeeze( orig_ct(sl,:,:));
%% 
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            %   loop throut the bounderies (with delata of dlt_) 
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            dlt_ = 20;   
            strt_ =1; 
            for strt_=[1,10]
                for ii=strt_:dlt_:length(out_brd)-dlt_
                outmat = zeros(size(curr_sl_contour));

                [~,~,outmat,X1,Y1] = ...
                bresenham(outmat, [in_brd2(ii,2) in_brd2(ii,1) ; ...
                                 out_brd(ii,2) out_brd(ii,1)],0);


                [~,~,outmat,X2,Y2] = ... 
                 bresenham(outmat, [in_brd2(ii+dlt_,2) in_brd2(ii+dlt_,1) ; ...
                                    out_brd(ii+dlt_,2) out_brd(ii+dlt_,1)],0);

                all_X = [X1 , in_brd2(ii:ii+dlt_,1)' , fliplr(X2) ,fliplr(out_brd(ii:ii+dlt_,1)')  ];
                all_Y = [Y1 , in_brd2(ii:ii+dlt_,2)' , fliplr(Y2) ,fliplr(out_brd(ii:ii+dlt_,2)')  ];
                idd = sub2ind(size(outmat) , all_X, all_Y);
                outmat(idd)=1;
                outmat = bwconvhull(outmat);
                
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                % * *    The patch that gonna be investgated of leaks..
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                curr_patch = curr_sl; 
                curr_patch(~outmat)=0; 
%
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                % * *     The patch 'contour level'
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                curr_sl2_bnds_sector = curr_sl2_bnds;  
                curr_sl2_bnds_sector(~outmat)=0;

                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                %  Make sure, that only a single CC is beeing investgated !!
                %       Make sure we don't check leaks to objects that we
                %       don't even leak into (but we "touched" only b/c of
                %       the dilation process)  
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                curr_sl2_bnds_sector_ez = CBIR_LEAK_REMOVAL_v1.object_with_any_intersect( curr_patch , curr_sl2_bnds_sector);
                curr_sl2_bnds_sector(~curr_sl2_bnds_sector_ez) = 0;
                
                IS_LEAK = false;    
               % max_thresh_allowed_in_slice = 5 ;

                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                %       if the slice has more then three...
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                 addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\freezeColors');
                if (find(curr_sl2_bnds_sector>=(growth_factor* (max_growth_allowed-1)))) 
                          
                     %       STOP_HERE(); 
                           IS_LEAK = true; 
    
 
                end
                %%   
               % if (find(curr_sl2_bnds_sector>=(max_growth_allowed-1)))
               if (IS_LEAK)  
 
%%
                         subplot(2,2,1) 
                        %       if (numel(find(curr_patch==max_thresh_allowed_in_slice)) ...
%                                     < numel(find(curr_patch==(max_thresh_allowed_in_slice+1))) ) 
%                                IS_LEAK = true; 
%                             end 
                            imagesc(curr_patch);
                            title('curr\_patch');
                            colormap(jet)
                            freezeColors

                         subplot(2,2,2)  
                            imagesc(curr_sl2_bnds_sector); 
                            title(['The patch contour level']);
 
                            xlabel([ 'max val:' num2str(max(curr_sl2_bnds_sector(:))) ]);

%                             ct_ = squeeze( orig_ct(sl,:,:));
%                             ct_(~outmat)=0;
%                             imagesc(ct_); 
%                             colormap(gray) 
%                             freezeColors

                         subplot(2,2,3)  
%                             curr_sl2_bnds_sector = curr_sl; %  squeeze( orig_ct(sl,:,:));
%                             curr_sl2_bnds_sector(~outmat)=0;
                            ccc= double(curr_sl2_bnds_sector); 
                            mm= ccc(ccc>1);
                            cc=histc(mm(:) ,1:max_growth_allowed);
                            bar(cc);    
                            ns=sort(mm); % only part of <unique>
                            c=histc(ns,ns); 
                            r=ns(c==max(c));   
                            if (length(r)) 
                                title(['Most freq: ' num2str(r(1))] );  
                            end

                                x= find(cc)';

                                   y=cc(x)';
                                p = polyfit(x,y,1);
                                yfit = polyval(p,x);
                                yresid = y - yfit; 
                                SSresid = sum(yresid.^2);
                                SStotal = (length(y)-1) * var(y);
                                 rsq = 1 - SSresid/SStotal ;
                                 if (rsq < 0.6) 
                                      IS_LEAK = true; 
                                 end  
                                    yfit = polyval(p,x(2:end));
                                    yresid = y(2:end) - yfit;
                                    SSresid = sum(yresid.^2);  
                                    SStotal = (length(y(2:end))-1) * var(y(2:end));
                                    rsq2 = 1 - SSresid/SStotal;
% rsq2=111;
                            title(['R^2(1): ' num2str(rsq) ', R^2(2): ' num2str(rsq2)      ]); 
                            colormap(jet);
                            freezeColors    ;
                        subplot(2,2,4)                              
%                             ct_ = squeeze( orig_ct(sl,:,:));
%                             ct_(idd)=2000; 
%                             imagesc(ct_); 
%                             colormap(gray) 
%                             freezeColors 
  
                       if (IS_LEAK)   
                           title('LEAK'); 
                           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                           %   Remove only the "leak" and keep the inside
                           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                           curr_sl2_bnds_sector_t = curr_sl2_bnds_sector;
                           curr_sl2_bnds_sector_t(curr_sl2_bnds_sector_t>1)=0;
                            res_seed_(logical(curr_sl2_bnds_sector_t)) =1;
%                             if (dbg_>1)
%                            STOP_HERE();   
% %                            curr_sl(curr_patch>1)=0;
% %                            curr_sl2_larg = largest_conncomp(curr_sl);
% %                            curr_sl(~curr_sl2_larg) = 0;    
%   
%                             eee=1;   
%                             end
                       else  
                           title('NOT A LEAK'); 
                           res_seed_(logical(curr_patch)) =1;

                         %  curr_sl2 = 
   
                       end 
%                        if (dbg_>1)
                %          STOP_HERE();    
%                         eee=1;  
%                        end     
%%
                else
                    res_seed_(logical(curr_sl2_bnds_sector)) =1;
                end

                end
                end 
            
        end   
%             subplot(2,2,4)  
%                 imagesc(res_seed_); 
%                  title([' res_seed ']); 
% % if (dbg_>1)
% %     STOP_HERE();   
% %     eee=1;  
% end 
         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');     
         res_  = largest_conncomp(  res_seed_ );  

         
        
    end
    %% smoothit3_consecutive_slices_find_bifurcation
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * smoothit3_consecutive_slices_find_bifurcation   
    %            
    % 
    %  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_] = smoothit3_consecutive_slices_find_bifurcation( ...
                                                    curr_sl                    , ...
                                                    next_sl                     , ...
                                                    dbg_                       );  
    %     STOP_HERE();
         if ~exist('dbg_','var'), dbg_ = 0; end
            
%          if (dbg_)
%              figure; subplot(1,2,1); imagesc(curr_sl); subplot(1,2,2); imagesc(next_sl)
%              STOP_HERE();
%          end 
        res_ = zeros(size(curr_sl));
     
        CC_next_sl = bwconncomp(next_sl);
        numOfPixels = cellfun(@numel,CC_next_sl.PixelIdxList);
        [B,IX] = sort(numOfPixels,'descend'); 
        
          num_of_obj =0; 
   
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % Loop throw each Connected-Components in the "next slice" 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        for ii=IX 
            res_ez = zeros(size(curr_sl));
            res_ez(CC_next_sl.PixelIdxList{ii}) = 1;
            inter_curr_next = curr_sl & res_ez  ;
               
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            % Check if the curent CC intersect with the previous slide.
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            if (any(inter_curr_next(:))) 
                     
                res_ = res_ez;  
        
                if (num_of_obj ==1)
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   % The bifurcation!
                   % if more the one object intersect!
                   %   --> more the one object interact == The bifurcation
                   %
                   %  In this case: return & set to zero all the following slices..  
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   % input_img(:,:,1:sl )=0;
                   res_ =  zeros(size(curr_sl)); 
                      %       STOP_HERE(); 
   
                   return 
                end 
                 
                num_of_obj = 1;  
            end
        end
%        if (dbg_)
%              figure; subplot(1,3,1); imagesc(curr_sl); subplot(1,3,2); imagesc(next_sl);
%                      subplot(1,3,3); imagesc(res_);
%               
%              
%              STOP_HERE();
%        end     
    end     
    

    %% smoothit4_consecutive_slices_separate_lungs
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * smoothit4_consecutive_slices_separate_lungs   
    %              
    %          
    %  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_] = smoothit4_consecutive_slices_separate_lungs(       ...
                                                    curr_sl              , ...
                                                    next_sl              , ...
                                                    dbg_                 ) ;  
    %     STOP_HERE();
         if ~exist('dbg_','var'), dbg_ = 0; end
            
%          if (dbg_)
%              figure; subplot(1,2,1); imagesc(curr_sl); subplot(1,2,2); imagesc(next_sl)
%              STOP_HERE();
%          end 
        res_ = zeros(size(curr_sl));
     
        CC_next_sl = bwconncomp(next_sl);
        numOfPixels = cellfun(@numel,CC_next_sl.PixelIdxList);
        [B,IX] = sort(numOfPixels,'descend'); 
        
          num_of_obj =0; 
   
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % Loop throw each Connected-Components in the "next slice" 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        for ii=IX  
            res_ez = zeros(size(next_sl));
            res_ez(CC_next_sl.PixelIdxList{ii}) = 1;
            inter_curr_next = curr_sl & res_ez  ;
               
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            % Check if the curent CC intersect with the previous slide.
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            if (any(inter_curr_next(:))) 
                     
               
                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                % check if the "object" in the next slice is twice as big as 
                %  the "object" in the current slice
                % -> this is an indcation of LEAKAGE into another organ 
                %
                % In this case: keep only the intersect of the current &
                %               the next slice
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                if (sum(res_ez(:)) > 1.5*sum(curr_sl(:)))
                    res_ = inter_curr_next; 
                else
                    res_ =  res_ez;    

                end
                
                 return ;    
            end
        end 
            
%         res_ =  res_ez;    
% 
%        if (dbg_)
%              figure; subplot(1,3,1); imagesc(curr_sl); subplot(1,3,2); imagesc(next_sl);
%                      subplot(1,3,3); imagesc(res_);
%               
%              
%              STOP_HERE();
%        end    
    end     
       
    

                                                
    %% object_with_max_intersect
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % Search for the object with the maximum intersect.... 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [max_inter_curr_n_next] = object_with_max_intersect( curr_sl , next_sl); 
        max_inter_curr_n_next = zeros(size(curr_sl)); 
        
        CC_next_sl = bwconncomp(next_sl);
        max_inter_CC = 0;  
        for ii=[1:CC_next_sl.NumObjects]  
            next_tmp_bw = zeros(size(curr_sl));

            next_tmp_bw(CC_next_sl.PixelIdxList{ii}) = 1;
            inter_curr_next = curr_sl & next_tmp_bw ;
            if (sum( inter_curr_next(:)) > max_inter_CC )  
                 max_inter_curr_n_next = next_tmp_bw;
                 max_inter_CC = sum( inter_curr_next(:))  ;
            end 
        end 
    end  
    
    %% object_with_any_intersect 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % Search for the object with the any intersect.... 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [any_inter_curr_n_next] = object_with_any_intersect( curr_sl , next_sl); 
    
        CC_next_sl = bwconncomp(next_sl);
        any_inter_curr_n_next = zeros(size(curr_sl)); 
        for ii=[1:CC_next_sl.NumObjects]  
            next_tmp_bw = zeros(size(curr_sl));

            next_tmp_bw(CC_next_sl.PixelIdxList{ii}) = 1;
            inter_curr_next = curr_sl & next_tmp_bw ;
            if ( sum( inter_curr_next(:)) )     
                 any_inter_curr_n_next(logical(next_tmp_bw)) = 1;
            end 
        end 
    end
        
      %% bwCurvature
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * bwCurvature
    %       calcuate the Curvature on the given image. 
    % 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_] = bwCurvature( D2_slice ,Curvature_threshold, plot_it ) 
        res_ = int8(D2_slice); 
         
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        Vertices_dilate = 3; 
        if ~exist('plot_it','var'),        plot_it= 0; end
        if ~exist('Curvature_threshold','var'), Curvature_threshold = -8; end
  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  extract the curve of the given segmentaion.  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        [B,L,N,A] = bwboundaries(D2_slice);
        Vertices1 = B{:}; 
        Vertices = Vertices1(1:Vertices_dilate:end,:); 
   
        Lines=[(1:size(Vertices,1))' (2:size(Vertices,1)+1)']; Lines(end,2)=1;
        k=LineCurvature2D(Vertices,Lines);
        N=LineNormals2D(Vertices,Lines);
         
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  takes the Curvature bigger then given threshold
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        k=k*10; 
        k1 = zeros(size(k)); 
        k1(k <  Curvature_threshold ) =1;
%           

        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  Takes only the 'growes Curvature' 
        %       (toward outside the object)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        vs1 = round(abs(k.*N(:,1))); 
        vs2 = round(abs(k.*N(:,2)));

        vs1(vs1==0)=1;           
        vs2(vs2==0)=1;           
        vs3 = vs1 | vs2;
        
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %  Embed the Curvature into the res_ image... 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        sub_Vertices = sub2ind(size(D2_slice), Vertices(:,1),Vertices(:,2));
        res_(sub_Vertices) = vs3;
             
%         if (plot_it) 
%                
%             figure,
%                 hold on; 
%                 imagesc(res_); 
% % %             figure; 
% % %                  imagesc(res_);   
% % %                 plot([Vertices(:,2) Vertices(:,2)+k1.*k.*N(:,1)]',[Vertices(:,1) Vertices(:,1)+k1.*k.*N(:,2)]','g');
% % %                 plot([Vertices(Lines(:,1),2) Vertices(Lines(:,2),2)]',[Vertices(Lines(:,1),1) Vertices(Lines(:,2),1)]','b');
% % %                 %plot(sin(0:0.01:2*pi)*10,cos(0:0.01:2*pi)*10,'r.');
% % %                 % axis equal; 
% % % %                 vs1 = round(abs(k1.*k.*N(:,1)));
% % % %                 vs1(vs1==0)=1; 
% % %                 
% % %                 %scatter(Vertices(:,2),Vertices(:,1),, [1 0 0], 'fill')
% % %                 scatter(Vertices(:,2),Vertices(:,1),vs1, [1 0 0], 'fill');
% % % 
% % %                 set(gca,'YDir','reverse'); 
%         end
%         

 
    end
   
  
    
    
    
    %% smoothing_it5
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * smoothing_it5
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res_] = smoothing_it5(orig_ct, segmentation_to_smooth , dim_ , smooth_from, smooth_to )
        verbose_=1;
        if ~exist('dim_','var'), dim_= 3; end
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');  
        segmentation_to_smooth = CTpermute(segmentation_to_smooth, 'forward', dim_);
        orig_ct = CTpermute(orig_ct, 'forward', dim_);
        res_ = zeros(size(segmentation_to_smooth)); 
        res_all = zeros([size(segmentation_to_smooth) 3],'uint8'); 

        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %%
        % init...
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        MAX_DILATION_ALLOWED = 8;
        next_tmp_bw = zeros(size(segmentation_to_smooth ,1), size(segmentation_to_smooth ,2));
        curr_sl2 = zeros(size(segmentation_to_smooth ,1), size(segmentation_to_smooth ,2));
        pre_dilate = zeros(size(segmentation_to_smooth ,1), size(segmentation_to_smooth ,2));
        SE = strel('disk',2,0);
        SE_disk8 = strel('disk',10,0);

        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % set the direction (up or done)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

        delta_ = -1;  
        if (delta_ == -1)
            rng_ = [size(segmentation_to_smooth,1):delta_:2] ;
        end
        if (delta_ == 1)
            rng_ = [1:size(segmentation_to_smooth,1)-1] ;
        end
  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        for sl=  rng_
             %for sl=  [1:size(input_img,1)-1] 
            if (verbose_)  
                fprintf('^');
                if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
                if (mod(sl,80)==0),  disp('.'),    end
            end
             
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            % 2. save (A) the current & (B) the next slice.
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            curr_sl = (squeeze(segmentation_to_smooth(sl,:,:))); 
            if (~any(curr_sl(:))) 
                continue
            end 
            curr_sl2 = curr_sl; 
            next_sl = (squeeze(segmentation_to_smooth(sl+delta_,:,:)));

            cuvature_ratio    = 3;
            cuvature_threshod = -8;

            after_leak_removal = ... 
                CBIR_LEAK_REMOVAL_v1.Consecutive_slices( curr_sl2 , next_sl);
           
            segmentation_to_smooth(sl+delta_,:,:) = after_leak_removal;
        end



        %%  
         segmentation_to_smooth = logical(segmentation_to_smooth); 
        %   STOP_HERE();
        res_ = CTpermute(segmentation_to_smooth, 'backward', dim_);
    end

   
    end
    
   
end

 % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%  
% delta_ = 1  
% 
% for jj = Kidney_Axial_upperBound_Slice:(Kidney_Axial_upperBound_Slice+100) 
%      msk = int16(fine_mask(:,:,jj));
%     orig_ct_sl = squeeze(orig_ct(:,:,jj+delta_));
%     orig_ct_gross_mask = squeeze(gross_mask(:,:,jj+delta_));
%     orig_ct_sl(~orig_ct_gross_mask)=0; 
%      
%     try 
%         seg =  region_seg( orig_ct_sl  , msk , Active_Contour_max_its  , alpha_p, dsp_p ); %-- Run segmentation
%          
%         if (dbg_ ==1)
%             
%               eee=1 ;  
%             eee=1 ; 
%         end   
%     catch   
%         error('region_seg'); 
%     end
%     
%     check_for_max_intersect =1;
%     seg3 = CBIR_LEAK_REMOVAL_v1.Smooth_Curvature_in_Consecutive_slices( ...
%                                 msk, seg, check_for_max_intersect, dbg_  );
%   
%     seg_size = sum(seg3(:))
%     if (seg_size < 50) 
%          disp('min reached 2'); 
%         break  
%     end   
%     fine_mask(:,:,jj+delta_) = seg3;
%          
    
  


%  
%     [fine_mask] = CBIR_LEAK_REMOVAL_v1.Segment_with_given_slice_as_seed(...
%                                   orig_ct                                  , ...
%                                   gross_mask                               , ...
%                                   squeeze(gross_mask(:,:,sliceNumber_as_seed)) ) ;                                 
%     end
%     %% Segment_with_given_slice_as_seed 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * Segment_with_given_slice_as_seed
%     %   Given -
%     %       o orig_ct
%     %       o gross_mask
%     %       o slice_as_seed 
%     %
%     %
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%           
%     function [fine_mask] = Segment_with_given_slice_as_seed( orig_ct     , ...
%                                                         gross_mask       , ...
%                                                          slice_as_seed     ) 
% %     ix = PB_ip_shortcuts.get_image_idx( )
% %         Kidney_Axial_upperBound_Slice = CP.ip(ix).Kidney_Axial_upperBound_Slice
% %         Bones_Axial_smalles_Slice = CP.ip(ix).Heart_GrayLevel_std; 

