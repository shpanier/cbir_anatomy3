classdef CBIR_TRACHEA_n_BRONCHI_aux_func
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
     
     methods(Static)
 
    %% extract_TRACHEA_n_BRONCHI
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * TRACHEA_threshold
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [TRACHEA_n_BRONCHI] = extract_TRACHEA_n_BRONCHI(  noisy_TRACHEA_n_BRONCHI  )
        global CP  
        ix = PB_ip_shortcuts.get_image_idx( );    
        Trachea_Axial_in_Slice = double(CP.ip(ix).Trachea_Axial_in_Slice );                                                          
              
       % STOP_HERE();
          
        seed_slice = noisy_TRACHEA_n_BRONCHI(:,:,Trachea_Axial_in_Slice);
        [seed_row, seed_col]  = find(seed_slice);  
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Segmentaion\regionGrowing');
         
         
          
        [~, TRACHEA_n_BRONCHI] = regionGrowing((noisy_TRACHEA_n_BRONCHI), [seed_row(1), seed_col(1),Trachea_Axial_in_Slice ]);
        
    end
   
      
     end
end





 
        
%         
%         
%         one_cc_diff = (diff(one_cc));
%         one_cc_diff(one_cc_diff>1)=0;
%   
%         
%             CC = bwconncomp(one_cc_diff);
%             numPixels = cellfun(@numel,CC.PixelIdxList);
%             for ii=1:length(numPixels)
%                 ar =CC.PixelIdxList(ii);
%                 one_cc_diff(ar{:}) = numPixels(ii);
%                 num_of_pix_ = num_of_pix(one_cc);
%             end
% 
%             diff_TIMES_pix1 = num_of_pix_(2:end).*one_cc_diff;
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%             %  Extract the largest compneent of "ones"
%             %   --> this is saved in ""diff_TIMES_pix""
%             %
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% 
%             diff_TIMES_pix = one_cc_diff;
% 
%             x_sum =0;  
%             Trachea_from =0;
%             Trachea_to=0;
%             diff_TIMES_pix = [0 diff_TIMES_pix 0];
%             zeros_ind = find(diff_TIMES_pix==0);
% 
%    
%             % ~ ~ ~ ~ 
%             for ii=[1:length(zeros_ind)-1]
%         %         STOP_HERE();
%                 tx_sum =sum(diff_TIMES_pix(zeros_ind(ii):zeros_ind(ii+1)));
%                 if (tx_sum >  x_sum)
%                     x_sum = tx_sum;
%                     Trachea_to = one_cc(zeros_ind(ii));
%                     Trachea_from = one_cc(zeros_ind(ii+1)-1); 
%                 end
%             end 
%   
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%              %   Trachea_Axial_Start_Slice 
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% 
%            %  CP.ip().Trachea_Axial_Start_Slice = Trachea_from; 
%                CP.ip(ix) = pblib_set(CP.ip(ix), 'Trachea_Axial_Start_Slice', Trachea_from);
%           %  CP.ip(PB_ip_shortcuts.get_image_idx( )).Trachea_Axial_Mid_Slice = Trachea_to;


