classdef PB_shortcuts
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
 methods(Static)
     
    

     
    %% my_mat_load 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * my_mat_load
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [loaded_mat] = my_mat_load( fl_mat )
         XX= load(fl_mat);
         eval( [loaded_mat '=XX.mat2save;']);
    end
    
    
     %% load2_nii_image 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * load2_nii_image
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function load2_nii_image( file_path, nii_prefix, nii_image , process_description_str )
       %          change_curr_image_name ) - remove at 27.5@13:13:00
       %          change_curr_image_name ) - added  at 09.06@13:13:00
        
        
       
        file2load  = ['''' fullfile([file_path, '\\' nii_prefix, nii_image]) ''''];
        mat_file2load = [file2load(1:end-1) '.mat''']; 


        PB.add_im_p( 'NEW', 'NEW')  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % check, If .mat version exists -- then load it. 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        if (exist( mat_file2load, 'file'))
            PB.add_im_p( 'imageName', mat_file2load );  
            PB.add_im_p( 'imageLoadFunc', 'PB_shortcuts.my_mat_load')
            PB.add_im_p( 'imageLoadFuncPath', ' '); 
            
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % load .nii version (when .mat don't exists)
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        else 
            PB.add_im_p( 'imageName', file2load );
            PB.add_im_p( 'imageLoadFunc', 'load_untouch2_nii')
            PB.add_im_p( 'imageLoadFuncPath', ...  
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Nii\')
        end
        
        if (exist('process_description_str','var'))
             PB.add_im_p( 'documantaion', process_description_str );   
        end

        PB.add_im_p( 'DisplayResult', 0)
        PB.add_im_p( 'isSegmentation',0 );
        PB.add_im_p( 'Enabled', 1)   
        
        % Processing   
        % ~~~~~~~~~~ 
        PB.add_im_P_p( 'NEW', 'NEW')
        PB.add_im_P_p( 'id', 1);
        PB.add_im_P_p( 'Enabled', 1);  
        PB.add_im_P_p( 'MethodName', ' ');  % AD-hoc, must be empty 
                                            % for name convenstion 
            % paramter indexes
            % ~~~~~~~~~~~~~~~~ 
            PB.add_im_P_Prm_p( 'NEW', 'NEW' );
            PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
            PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
             
%          else
%              
%             PB.add_im_p( 'NEW', 'NEW')     
%             PB.add_im_p( 'imageName', file2load );
%             PB.add_im_p( 'imageLoadFunc', 'load_untouch2_nii')
%             PB.add_im_p( 'imageLoadFuncPath', ...  
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Nii\')
%             PB.add_im_p( 'documantaion', process_description_str ); 
%             PB.add_im_p( 'DisplayResult', 0)
%             PB.add_im_p( 'isSegmentation',0 );
%             PB.add_im_p( 'Enabled', 1)    
% 
%                 % Processing   
%                 % ~~~~~~~~~~ 
%                 PB.add_im_P_p( 'NEW', 'NEW')
%                 PB.add_im_P_p( 'id', 1);
%                 PB.add_im_P_p( 'Enabled', 1);   
%                 PB.add_im_P_p( 'MethodName', ' ');  % AD-hoc, must be empty 
%                                                     % for name convenstion 
%                     % paramter indexes
%                     % ~~~~~~~~~~~~~~~~ 
%                     PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                     PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                     PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
%         end
  
    end
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

 
    
    %% add_process_description
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * add_process_description
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function add_process_description( process_description_str )
        PB.add_im_p( 'documantaion', process_description_str ); 
        % PB.add_im_p( 'processDescription', process_description_str);
    end
    
    
    %% FORCE_RECALCULATION_FROM_HERE
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * FORCE_RECALCULATION_FROM_HERE
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function FORCE_RECALCULATION_FROM_HERE(  )
        PB.add_im_p( 'forceRecalculation', 1);
    end 
     
    
    %% DISPLAY_RESULT_AT_THIS_POINT
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * DISPLAY_RESULT_AT_THIS_POINT
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function DISPLAY_RESULT_AT_THIS_POINT(  )
        PB.add_im_p( 'DisplayResult', 1);
    end
    
    %% KEEP_RESULT_AT_THIS_POINT
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * KEEP_RESULT_AT_THIS_POINT 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function KEEP_RESULT_AT_THIS_POINT(  )
        PB.add_im_p( 'DisplayResult', 2);  
    end
    
    %% KEEP_RESULT_AT_THIS_POINT
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * KEEP_RESULT_AT_THIS_POINT 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function KEEP3_RESULT_AT_THIS_POINT(  ) 
        PB.add_im_p( 'DisplayResult', 3);  
    end  
    %% DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT(  )
        PB.add_im_p( 'DisplayResult', 1);
        PB.add_im_p( 'isSegmentation', 1 ); 
    end
    
    
    
    %% save_untouch4_nii 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * save_untouch4_nii
    %         
    % * *
    % * *  Save the file both into .nii and .mat format.
    % * *
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function save_untouch4_nii( based_file, file2save  )       
        global last_saved_file_name  
        last_saved_file_name = file2save ;   
        if (~exist('out_prefix_' ,'var')),  out_prefix_ =''; , end
        %   STOP_HERE(); 
          
      PB_shortcuts.set_new_image_index(); 
     % PB.add_im_p( 'forceRecalculation', 1)  
       
      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
      % 1.  save into .nii format
      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
      PB.add_im_P_p( 'NEW', 'NEW') 
      PB.add_im_P_p( 'id', 1); 
        PB.add_im_P_p( 'Enabled', 1);
        PB.add_im_P_p( 'MethodName', 'save_untouch2_nii');
        PB.add_im_P_p( 'MethodFilesLocation', ...
                       'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Nii\')
          
 
         % paramter indexes
            % ~~~~~~~~~~~~~~~~
            PB.add_im_P_Prm_p( 'NEW', 'NEW' );
            PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
            PB.add_im_P_Prm_p( 'prm_value', based_file );

            % paramter indexes
            % ~~~~~~~~~~~~~~~~
            PB.add_im_P_Prm_p( 'NEW', 'NEW' );
            PB.add_im_P_Prm_p( 'prm_name', 'segment' );
            PB.add_im_P_Prm_p( 'prm_value', ['c_im'] );
 
            % paramter indexes
            % ~~~~~~~~~~~~~~~~ 
            PB.add_im_P_Prm_p( 'NEW', 'NEW' );
            PB.add_im_P_Prm_p( 'prm_name', 'out_file' );  
            PB.add_im_P_Prm_p( 'prm_value', file2save );    
  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % 2.  save into mat format
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    PB.add_im_P_p( 'NEW', 'NEW') 
      PB.add_im_P_p( 'id', 1); 
        PB.add_im_P_p( 'Enabled', 1); 
        PB.add_im_P_p( 'MethodName', 'PB_shortcuts.my_mat_save'); 
        PB.add_im_P_p( 'MethodFilesLocation', ' ') 
           
            % paramter indexes
            % ~~~~~~~~~~~~~~~~
            PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
            PB.add_im_P_Prm_p( 'prm_name', '3d image' );
            PB.add_im_P_Prm_p( 'prm_value', ['c_im'] );  
 
            % paramter indexes
            % ~~~~~~~~~~~~~~~~ 
            PB.add_im_P_Prm_p( 'NEW', 'NEW' );
            PB.add_im_P_Prm_p( 'prm_name', 'out_file' );    
                                                    % (1:end-1) remove the
                                                    % last ' and add onther
                                                    % one after the .mat
                                                    % extension 
            PB.add_im_P_Prm_p( 'prm_value', [file2save(1:end-1) '.mat' '''']  );          

            
    end
     
    %% my_mat_save
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  since my CBIR-process can't run
    %  'c_im_res3=save('c_im','D:/MY_VISCERAL_TMP/BODY_10000110_1_CTce_ThAb.nii.mat');'
    %   the process assume every function had a return value... 
    %
    %   --> this helper function call save with not return value....
    %            .... that all .....     
    function [mat2save] = my_mat_save( mat2save, filename)
        eval(['save(''' filename ''', ''mat2save'' )' ])
    end  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %% prepare_filename2save 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * prepare_filename2save
    %         ( based_file , out_dir , out_file_prefix_, out_file_suffix  )  
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [ filename2save ] = prepare_filename2save( based_file, out_dir , out_file_prefix_, out_file_suffix  )
        [path_, name_ , ext_]=   fileparts(based_file);  
        filename2save =  fullfile(out_dir, [out_file_prefix_ name_ out_file_suffix '.nii.gz']);
        filename2save = [ '''' filename2save '''']; 
        filename2save = regexprep(filename2save ,'(\\)','/');   % convert '\' --> into '/'
%             STOP_HERE();      
%             eee=1;  
          
    end
      
    
    %% set_new_image_index (documantaion_)
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * set_new_image_index
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     function set_new_image_index( documantaion_ )
        global im_idx 
        PB.add_im_p( 'NEW', 'NEW')
        PB.add_im_p( 'imageName', ...
                ['c_im_res' num2str(im_idx-1)]);
        PB.add_im_p( 'imageLoadFunc', ' ')
        PB.add_im_p( 'imageLoadFuncPath', ' ')
        PB.add_im_p( 'DisplayResult', 0)  
        % PB.add_im_p( 'forceRecalculation', 0)  
         
        % documantion of the current process  ...     
        % - - -           
        if exist('documantaion_','var')  
            PB.add_im_p( 'documantaion', documantaion_ ); 
        end
        

    end
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 
    %% extract_body
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * 
    % * *  extract_body
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function extract_body(enabled_)  
       if (~enabled_), return , end
 
        global im_idx  
 
            % Processing , threshold_image_BW( -400 , 3000 )
            % ~~~~~~~~~~
            PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 1);
            PB.add_im_P_p( 'Enabled', 1); 
            PB.add_im_P_p( 'MethodName', 'threshold_image_BW');

            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');

                % paramter indexes 
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', 'c_im' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                PB.add_im_P_Prm_p( 'prm_value', '-505' );
    
                PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                PB.add_im_P_Prm_p( 'prm_value', '2000' );
                
            % Processing
            % ~~~~~~~~~~ 
            PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 2);
            PB.add_im_P_p( 'Enabled', 1);
            PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
                % paramter indexes 
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                PB.add_im_P_Prm_p( 'prm_value', '1' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
                PB.add_im_P_Prm_p( 'prm_value', '''disk''' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                PB.add_im_P_Prm_p( 'prm_value', '2' );
                
                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                PB.add_im_P_Prm_p( 'prm_value', '0' );
             % Processing
            % ~~~~~~~~~~ 
            PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 2);
            PB.add_im_P_p( 'Enabled', 1);
            PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
                % paramter indexes 
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                PB.add_im_P_Prm_p( 'prm_value', '2' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
                PB.add_im_P_Prm_p( 'prm_value', '''disk''' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                PB.add_im_P_Prm_p( 'prm_value', '2' );
                
                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                PB.add_im_P_Prm_p( 'prm_value', '0' );
                
            % Processing
            % ~~~~~~~~~~ 
            PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 3);
            PB.add_im_P_p( 'Enabled', 1);
            PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
                % paramter indexes  
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'prm' );
                PB.add_im_P_Prm_p( 'prm_value', '''largest_conncomp'''  );
                 
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                PB.add_im_P_Prm_p( 'prm_value', '3' );
           
    end    
    
   %% extract_body
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * 
    % * *  extract_body
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [clusters] = SpectralClustering(orig_image)  

        
    end
    spectralcluster
%% find_smoothest_point
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % * * find_smoothest_point
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% function [smoothest_point] = find_smoothest_point(input_img  , dim_ ,smooth_from, smooth_to )
%     verbose_=1;
%     if ~exist('dim_','var'), dim_= 3; end
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     %     
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% 
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     % * *  Assume input segmentaion...   
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     if ((max(input_img(:)) >1) || (max(input_img(:)) <0))
%        error(' Assume input segmentaion...   ');  
%     end  
%     input_img = CTpermute(input_img, 'forward', dim_);
%     
%     if ~exist('smooth_from','var'), smooth_from= 1; end
%     if ~exist('smooth_to','var'), smooth_to= size(input_img,1); end
%     input_img = input_img(smooth_from:smooth_to,:,:);
%     s1 = size(input_img,1); 
%     img__ = reshape(input_img, s1, []);
% 
% 
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     % * *  Normlize aera in each slice.   
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     SM_area = sum(abs(img__),2);
%     SM_area = SM_area/max(SM_area(:));
% 
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     % * * remove outliers and zero area....  
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     SM_area_outliers = [SM_area < 0.9 & SM_area > 0.1 & SM_area ~= 0];
%     CC = bwconncomp(SM_area_outliers);
% 
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     % * * After outliers removed:
%     % * *        Looking for the largest continuous region ( * without
%     % * *           outliers * )
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     numPixels = cellfun(@numel,CC.PixelIdxList);
%     [biggest,idx] = max(numPixels);
%     smoothest_point = round(mean(CC.PixelIdxList{idx}));
%     
% % %     input_img = cat(3, zeros(size(input_img,1),size(input_img,2),'int16') , ...
% % %                            input_img);      
% % %         input_img = diff(input_img);
% % %         s1 = size(input_img,1); 
% % %         img__ = reshape(input_img, s1, []);
% end
%      
%  %% smoothing_it
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % * * smoothing_it
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% function [input_img] = smoothing_it(input_img, dim_ , smooth_from, smooth_to )
%     verbose_=1;
%     if ~exist('dim_','var'), dim_= 3; end
%     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
%     input_img = logical(input_img);
%     
%     [smoothest_point] = PB_shortcuts.find_smoothest_point(input_img , dim_ );
%     
%     
%     input_img = CTpermute(input_img, 'forward', dim_);
%     
%     if ~exist('smooth_from','var'), smooth_from= 1; end
%     if ~exist('smooth_to','var'), smooth_to= size(input_img,1); end
%     
%     
%     %                  figure,
%     direction = '';
%     step_ = 0;
%     
%     overlap_ratio =1.3;
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     %  DOWN
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     %
%     for sl=[smoothest_point:-1:smooth_from+1  ]
%         
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % no more objects DOWN the road.....
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         if (numel(find(input_img(sl-1:-1:smooth_from,:,:)))<=10)
%             break
%         end
%         
%         
%         if (verbose_)
%             fprintf('.');
%             if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
%             if (mod(sl,80)==0),  disp('.'),    end
%         end
%         curr_sl = (squeeze(input_img(sl,:,:)));
%         next_sl = (squeeze(input_img(sl-1,:,:)));
%         
%         inter_curr_next = curr_sl & next_sl ;
%         
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % Check intersect with the previous slide.
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         if (any(inter_curr_next(:)))
%             
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             % check if the "object" in the next slice is twice as big as
%             %  the "object" in the current slice
%             % -> this is an indcation of LEAKAGE into another organ
%             %
%             % In this case: keep only the intersect of the current &
%             %               the next slice
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             if (sum(next_sl(:)) > overlap_ratio*sum(curr_sl(:)))
%                 input_img(sl-1,:,:) = inter_curr_next;
%                 continue
%             end
%             
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             % If we got here: it's means the the intersect between the
%             % two consequent is "reasonable"
%             %
%             %
%             % In this case: keep only the overlapping/coincident object
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             %     imagesc(tmp_bw)
%             input_img(sl-1,:,:) = next_sl;
%         else
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             % intersect do not exists....!!!!!!
%             %           keep the current!
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             input_img(sl-1,:,:) = curr_sl;
%         end
%         
%     end
%     
%     
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     %  UP
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     %%
%     for sl=[smoothest_point:smooth_to-1  ]
%         
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % no more objects UP the road.....
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         if (numel(find(input_img(sl+1:smooth_to-1,:,:)))<=10)
%             %                  addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
%             %                     STOP_HERE();
%             break
%         end
%         
%         
%         if (verbose_)
%             fprintf('.');
%             if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
%             if (mod(sl,80)==0),  disp('.'),    end
%         end
%         curr_sl = (squeeze(input_img(sl,:,:)));
%         next_sl = (squeeze(input_img(sl+1,:,:)));
%         
%         inter_curr_next = curr_sl & next_sl ;
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % Check if the curent CC intersect with the previous slide.
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         
%         if (any(inter_curr_next(:)))
%             
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             % check if the "object" in the next slice is twice as big as
%             %  the "object" in the current slice
%             % -> this is an indcation of LEAKAGE into another organ
%             %
%             % In this case: keep only the intersect of the current &
%             %               the next slice
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             if (sum(next_sl(:)) > overlap_ratio*sum(curr_sl(:)))
%                 input_img(sl+1,:,:) = inter_curr_next;
%                 continue
%             end
%             
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             % If we got here: it's means the the intersect between the
%             % two consequent is "reasonable"
%             %
%             %
%             % In this case: keep only the overlapping/coincident object
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             %     imagesc(tmp_bw)
%             input_img(sl+1,:,:) = next_sl;
%         else
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             % intersect do not exists....!!!!!!
%             %           keep the current!
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             input_img(sl+1,:,:) = curr_sl;
%         end
%         
%     end
%     
%     
%     input_img = CTpermute(input_img, 'backward', dim_);
% end
% 
% 
% %% smoothing_it2
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % * * smoothing_it2
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% function [res_] = smoothing_it2(input_img, input_img2,  dim_ , smooth_from, smooth_to )
%     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Segmentaion\LevelSets\DRLSE_v0\'); 
%     verbose_=1;
%     if ~exist('dim_','var'), dim_= 3; end
%     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
%   %   input_img = logical(input_img);
%      
% %     [smoothest_point] = PB_shortcuts.find_smoothest_point(input_img , dim_ );
%     
%    input_img = CTpermute(input_img, 'forward', dim_);
%    input_img2 = CTpermute(input_img2, 'forward', dim_);
% 
%    res_ = zeros(size(input_img));
%    
%     if ~exist('smooth_from','var'), smooth_from= 1; end
%     if ~exist('smooth_to','var'), smooth_to= size(input_img,1); end
%      
%     
%     %                  figure,
%     direction = '';
%     step_ = 0;
%     
%     overlap_ratio =1.3; 
%    % STOP_HERE(); 
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     %  DOWN
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%   %   sl=250
%     %%
%     for sl=  [1:size(input_img,1)-1]
%         
% %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% %         % no more objects DOWN the road.....
% %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% %         if (numel(find(input_img(sl-1:-1:smooth_from,:,:)))<=10)
% %             break
% %         end
%         
%      %%    
%         if (verbose_)
%             fprintf('.');
%             if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
%             if (mod(sl,80)==0),  disp('.'),    end
%         end 
%         curr_sl = double(squeeze(input_img(sl,:,:)));
%         next_sl = double(squeeze(input_img2(sl,:,:)));
%           
%         
%         %parameter setting
%         timestep=5;  % time step
%         mu=0.2/timestep;  % coefficient of the distance regularization term R(phi)
%         iter_inner=5;
%         iter_outer=2;
%         lambda=5; % coefficient of the weighted length term L(phi)
%         alfa=1.5;  % coefficient of the weighted area term A(phi)
%         epsilon=1.5; % papramater that specifies the width of the DiracDelta function
%         %  
%         sigma=1.5;     % scale parameter in Gaussian kernel
%         G=fspecial('gaussian',15,sigma);
%         Img_smooth=conv2((curr_sl),G,'same');  % smooth image by Gaussiin convolution
%         [Ix,Iy]=gradient(Img_smooth);
%         f=Ix.^2+Iy.^2; 
%         g=1./(1+f);  % edge indicator function.
%  
%         potentialFunction = 'single-well';  % use single well potential p1(s)=0.5*(s-1)^2, which is good for region-based model 
%    
%         next_sl = drlse_edge(next_sl, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);
% 
%         alfa=0;
%         iter_refine = 10;
%         next_sl = drlse_edge(next_sl, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);
%         res_(sl,:,:) = next_sl;   
%         
%         %  figure;
%        %   imagesc(next_sl);
% % 
% %         disp('Done!!')
%  
% % %         if (0)
% % %            %% 
% % %            sl = 202;
% % %            t = squeeze(c_im_res2(sl,:,:) );
% % %             imagesc(t); 
% % % 
% % % 
% % %         %%    
% % %         end 
% 
% %% 
%     end 
%   %   STOP_HERE(); 
%     res_ = CTpermute(res_, 'backward', dim_);
% end
% 
% 
%  
% %% smoothing_it4
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % * * smoothing_it4
% % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% function [res_] = smoothing_it4(orig_ct, segmentation_to_smooth , dim_ , smooth_from, smooth_to )
%     verbose_=1;
%     if ~exist('dim_','var'), dim_= 3; end
%     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
%     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp');  
%     segmentation_to_smooth = CTpermute(segmentation_to_smooth, 'forward', dim_);
%     orig_ct = CTpermute(orig_ct, 'forward', dim_);
%     res_ = zeros(size(segmentation_to_smooth)); 
%  res_all = zeros([size(segmentation_to_smooth) 3],'uint8'); 
%  
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     %%
%     % init...
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     MAX_DILATION_ALLOWED = 8;
%     next_tmp_bw = zeros(size(segmentation_to_smooth ,1), size(segmentation_to_smooth ,2));
%     curr_sl2 = zeros(size(segmentation_to_smooth ,1), size(segmentation_to_smooth ,2));
%     pre_dilate = zeros(size(segmentation_to_smooth ,1), size(segmentation_to_smooth ,2));
%     SE = strel('disk',2,0);
%     SE_disk8 = strel('disk',10,0);
%  
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     % set the direction (up or done)
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% 
%     delta_ = -1;  
%     if (delta_ == -1)
%         rng_ = [size(segmentation_to_smooth,1):delta_:2] ;
%     end
%     if (delta_ == 1)
%         rng_ = [1:size(segmentation_to_smooth,1)-1] ;
%     end
%        
%     %%
%     for sl=  rng_
%          %for sl=  [1:size(input_img,1)-1] 
%         if (verbose_) 
%             fprintf('.');
%             if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
%             if (mod(sl,80)==0),  disp('.'),    end
%         end
%         
%         %% 
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % 1. Search for the object with the maximum intersect.... 
%         % 2. save (A) the current & (B) the next slice.
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         curr_sl = (squeeze(segmentation_to_smooth(sl,:,:))); 
%         if (~any(curr_sl(:))) 
%             continue
%         end 
%         curr_sl2 = curr_sl; 
%         next_sl = (squeeze(segmentation_to_smooth(sl+delta_,:,:)));
%  
%         %%    
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % Search for the object with the maximum intersect.... 
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         CC_next_sl = bwconncomp(next_sl);
%         max_inter_CC = 0;
%         for ii=[1:CC_next_sl.NumObjects] 
%             next_tmp_bw = zeros(size(curr_sl));
% 
%             next_tmp_bw(CC_next_sl.PixelIdxList{ii}) = 1;
%             inter_curr_next = curr_sl & next_tmp_bw ;
%             if (sum( inter_curr_next(:)) > max_inter_CC )  
%                  max_inter_curr_n_next = next_tmp_bw;
%                  max_inter_CC = sum( inter_curr_next(:))  ;
%             end
%         end
%         next_tmp_bw = max_inter_curr_n_next; 
%                 %     STOP_HERE()   
% 
%         %% 
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % Dilate upto the next slide 
%         %   o Build "contour" difrences between the current and the next slice.
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% 
%         num_of_dilation = 0;
%         inter_curr_next2 =1;  
%         post_dilate = curr_sl;
%         post_dilate(post_dilate>0) =1; 
%         curr_sl2(curr_sl2>0) =1;
% 
%         while ((num_of_dilation~=MAX_DILATION_ALLOWED) && any(inter_curr_next2(:)))
%             num_of_dilation = num_of_dilation+1 ;  
%             pre_dilate = post_dilate;
%             post_dilate = imdilate(pre_dilate,SE);
%             idxx = ~pre_dilate & post_dilate;
%             curr_sl2(idxx)=num_of_dilation+1;
%             inter_curr_next2 = ~logical(curr_sl2) & next_tmp_bw;
%         end 
%         curr_sl2(~next_tmp_bw)=0;
%           
% 
%         %% 
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % Remove new componets which has a 'contour level' bigger then 3.
%         %   
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         max_thresh_allowed = 3;
%         curr_sl3=[];
%         if (find(curr_sl2>max_thresh_allowed))
%             % STOP_HERE() ;    
% %             curr_sl3 = curr_sl2; 
% %             curr_sl3(curr_sl3==1)=0; 
%             curr_sl3_dilate_disk8 = imdilate(curr_sl,SE_disk8);
%             curr_sl3_erode_disk8  = imerode(curr_sl,SE_disk8);
%             curr_sl3 = curr_sl3_dilate_disk8 & ~curr_sl3_erode_disk8; 
%               
%              
%         end
%         
%         % bounderies...
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\Geometry\bresenham_line');
%         [B,L,N,A] = bwboundaries(curr_sl3);
%        %   curr_sl4 = curr_sl2; %  squeeze( orig_ct(sl,:,:));
%  
%         if (length(B)>1)
%            
%             for k=1:2, 
%                     boundary = B{k};
%                 if(k > 1)
%                     in_brd =  boundary;
%                 else
%                     out_brd =  boundary;
%                 end
%             end 
% 
%             in_brd2 = zeros(size(out_brd));
%             map_in_out = round(linspace(1,length(in_brd),length(out_brd)));
%             in_brd2  =  in_brd(map_in_out,:);
%             outmat = zeros(size(curr_sl3));
%             ct1_ = squeeze( orig_ct(sl,:,:));
% 
%             % loop throut the bounderies (with delata of  dlt_)
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%             dlt_ = 40; 
%             for strt_=[1,20]
%                 for ii=strt_:dlt_:length(out_brd)-dlt_
%                 outmat = zeros(size(curr_sl3));
% 
%                 [~,~,outmat,X1,Y1] = ...
%                 bresenham(outmat, [in_brd2(ii,2) in_brd2(ii,1) ; ...
%                                  out_brd(ii,2) out_brd(ii,1)],0);
% 
% 
%                 [~,~,outmat,X2,Y2] = ... 
%                  bresenham(outmat, [in_brd2(ii+dlt_,2) in_brd2(ii+dlt_,1) ; ...
%                                     out_brd(ii+dlt_,2) out_brd(ii+dlt_,1)],0);
% 
%                 all_X = [X1 , in_brd2(ii:ii+dlt_,1)' , fliplr(X2) ,fliplr(out_brd(ii:ii+dlt_,1)')  ];
%                 all_Y = [Y1 , in_brd2(ii:ii+dlt_,2)' , fliplr(Y2) ,fliplr(out_brd(ii:ii+dlt_,2)')  ];
%                 idd = sub2ind(size(outmat) , all_X, all_Y);
%                 outmat(idd)=1;
%                 outmat = bwconvhull(outmat);
% 
%                 curr_sl4 = curr_sl2; %  squeeze( orig_ct(sl,:,:));
%                 ct_ = curr_sl2; 
%                 ct_(~outmat)=0;
%                 IS_LEAK = false; 
%                 max_thresh_allowed_in_slice = 5 ;
%                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%                 % if the slice has more then three...
%                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%                         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\freezeColors');
% 
%                 if (find(ct_>max_thresh_allowed))
%                     
%                      
%                          subplot(2,2,1) 
%                             curr_patch = curr_sl2; %  squeeze( orig_ct(sl,:,:));
%                             curr_patch(~outmat)=0; 
% %                             if (numel(find(curr_patch==max_thresh_allowed_in_slice)) ...
% %                                     < numel(find(curr_patch==(max_thresh_allowed_in_slice+1))) ) 
% %                                IS_LEAK = true; 
% %                             end
%                             imagesc(curr_patch);
%                             title([ 'max val:' num2str(max(ct_(:))) ]);
%                             colormap(jet)
%                             freezeColors
%  
%                          subplot(2,2,2)
%                             ct_ = squeeze( orig_ct(sl,:,:));
%                             ct_(~outmat)=0;
%                             imagesc(ct_); 
%                             colormap(gray) 
%                             freezeColors
% 
%                          subplot(2,2,3)
%                             ct_ = curr_sl2; %  squeeze( orig_ct(sl,:,:));
%                             ct_(~outmat)=0;
%                             ccc= double(ct_); 
%                             mm= ccc(ccc>1);
%                             cc=histc(mm(:) ,1:MAX_DILATION_ALLOWED);
%                             bar(cc);    
%                             ns=sort(mm); % only part of <unique>
%                             c=histc(ns,ns); 
%                             r=ns(c==max(c));   
%                             if (length(r)) 
%                                 title(['Most freq: ' num2str(r(1))] );  
%                             end
%                               
%                                 x= find(cc)';
%                                   
%                                    y=cc(x)';
%                                 p = polyfit(x,y,1);
%                                 yfit = polyval(p,x);
%                                 yresid = y - yfit; 
%                                 SSresid = sum(yresid.^2);
%                                 SStotal = (length(y)-1) * var(y);
%                                  rsq = 1 - SSresid/SStotal ;
%                                  if (rsq < 0.6) 
%                                       IS_LEAK = true; 
%                                  end
% %                                     yfit = polyval(p,x(2:end))
% %                                     yresid = y(2:end) - yfit;
% %                                     SSresid = sum(yresid.^2);  
% %                                     SStotal = (length(y(2:end))-1) * var(y(2:end));
% %                                     rsq2 = 1 - SSresid/SStotal
% rsq2=111;
%                             title(['R^2(1): ' num2str(rsq) ', R^2(2): ' num2str(rsq2)      ]); 
%                             colormap(jet)
%                             freezeColors 
%                         subplot(2,2,4)                            
%                             ct_ = squeeze( orig_ct(sl,:,:));
%                             ct_(idd)=2000; 
%                             imagesc(ct_); 
%                             colormap(gray) 
%                             freezeColors 
%                                      
%                        if (IS_LEAK)  
%                            title('LEAK');
%                            %  STOP_HERE();  
%                            curr_sl2(curr_patch>1)=0;
%                           curr_sl2_larg = largest_conncomp(curr_sl2);
%                           curr_sl2(~curr_sl2_larg) = 0;    
%                             
%                             eee=1;  
%                        else 
%                            title('NOT A LEAK');
%                           
%                          %  curr_sl2 = 
%                              
%                        end  
%                       
%                 end 
% 
%                 end
%             end 
%             if (IS_LEAK)    
%                        
%                          
%             end  
%         end 
%  %  imshow(outmat); 
%  % ct1_ = squeeze( orig_ct(sl,:,:));     
%         
% %               %   STOP_HERE()
% %                 eee=1;     
%                 %   curr_sl2(curr_sl2>3)=0;  
%                 segmentation_to_smooth(sl+delta_,:,:) = curr_sl2; 
%    
%          %    end 
%               
%         %end  
%         %%  
% %        if ((sl >= 100) && (sl <= 335))  %  (1) % ((sl >= 100) && (sl <= 376))  % (0) % 
% %         %f1 = figure('visible','off');  
% %              hold on;
% %              grid on; 
% %         subplot(2,6,[1,2]) 
% %             imagesc(next_sl);  
% %             title('next\_sl');
% %             ylabel(['Slice: ' num2str(sl)]);
% %             colormap(jet)
% %             freezeColors
% %         %subplot(2,6,[3,4]) 
% %         %                imagesc(curr_sl);
% %         %                 title('curr\_sl'); 
% %         subplot(2,6,[3,4])  
% %             imagesc(curr_sl2); 
% %             title('curr\_sl2');
% %             colormap(jet)
% %             freezeColors
% %         subplot(2,6,[5,6])   
% %             imagesc(curr_sl3);  
% %             title('curr\_sl3'); 
% %             colormap(jet)
% %             freezeColors
% %         subplot(2,6,[9,10])   
% %             t_inside = orig_ct(curr_sl2==1);
% %             
% %             hold on;  
% %            %   STOP_HERE()   
% %            dbl1 = double(t_inside(t_inside>0));
% %             [nelements1,centers1] =hist(dbl1,40);
% %             nelements1 =nelements1/sum(nelements1);
% %             bar(centers1,nelements1,'b');
% %             k1 = kurtosis(dbl1);
% %             xlabel(['kurtosis-in: ' num2str(k1)]); 
% % 
% %             if ~isempty(curr_sl3)  
% %                 t_outside =  orig_ct(curr_sl3>1);
% %                 dbl = double(t_outside(t_outside>2));
% %                 [nelements2,centers2] =hist(dbl,40);
% %                 nelements2 =nelements2/sum(nelements2);
% %                 bar(centers2,nelements2,'r');   
% %                 k = kurtosis(dbl);
% %                 [h,p] =ttest2(dbl1,dbl); 
% %                 title(['T-TEST2( h: ' num2str(h) ', p: ' num2str(p) ')']);
% %                 xlabel({['kurtosis-in: ' num2str(k1)],['kurtosis-out: ' num2str(k)]});
% %           %          STOP_HERE()  
% %                 legend('inside graylevel' , 'outside graylevel'); 
% %             end
% %              
% %            %  legend('inside graylevel' , 'outside graylevel'); 
% %            %  title('orig\_ct');  
% %            %  imagesc(squeeze(orig_ct(sl,:,:))); 
% %            colormap(jet)
% %            freezeColors  
% %         subplot(2,6,[11,12])  ;
% %             ccc= double(curr_sl3); 
% %             mm= ccc(ccc>1);
% %             cc=histc(mm(:) ,1:MAX_DILATION_ALLOWED);
% %             bar(cc);    
% %             ns=sort(mm); % only part of <unique>
% %             c=histc(ns,ns); 
% %             r=ns(c==max(c));  
% %             if (length(r)) 
% %                 title(['Most freq: ' num2str(r(1))] );  
% %             end 
% %             colormap(jet)
% %             freezeColors    
% %         subplot(2,6,[7,8])   
% %             title('orig\_ct');  
% %             imagesc(squeeze(orig_ct(sl,:,:))); 
% %             colormap(gray)
% %             freezeColors
% %        %%   
% %        %  res = hardcopy(f1, '-dzbuffer', '-r0');  
% %       %    res_all(sl,:,:,:) =  imresize(res , [size(res_all,2) size(res_all,3)]);   
% %       %    close(f1) 
% % %      if (r >2) 
% %                STOP_HERE() 
% % %              eee=1;  
% % %      end   
% %        end
% %        
%     end
% %    figure
% %   %    size(res_all)
% %    STOP_HERE()  
% %    
% %    eee=1;  
% %    %%
% %     iii=320; 
% %     imagesc(squeeze( res_all(sl,:,:,:))); 
%    %%
% %      
% %     if (0)
% %         %% 
% %          
% %         %%
% %         sl = 202;
% %         t = squeeze(c_im_res2(sl,:,:) );
% %         imagesc(t);
% %         
% %         
% %         %%
% %     end
%     
%     %%  
%      segmentation_to_smooth = logical(segmentation_to_smooth); 
%     %   STOP_HERE();
%     res_ = CTpermute(segmentation_to_smooth, 'backward', dim_);
% end
% 
% 
% % % % 
% % % % %% smoothing_it3
% % % % % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % % % * * smoothing_it3
% % % % % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% % % % function [res_] = smoothing_it3(orig_ct, input_img, dim_ , smooth_from, smooth_to )
% % % %     verbose_=1;
% % % %     if ~exist('dim_','var'), dim_= 3; end
% % % %     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
% % % %     input_img = CTpermute(input_img, 'forward', dim_);
% % % %     orig_ct = CTpermute(orig_ct, 'forward', dim_);
% % % %     res_ = zeros(size(input_img));
% % % %  res_all = zeros([size(input_img) 3],'uint8'); 
% % % %  
% % % %     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %     %%
% % % %     % init...
% % % %     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %     MAX_DILATION_ALLOWED = 8;
% % % %     next_tmp_bw = zeros(size(input_img ,1), size(input_img ,2));
% % % %     curr_sl2 = zeros(size(input_img ,1), size(input_img ,2));
% % % %     pre_dilate = zeros(size(input_img ,1), size(input_img ,2));
% % % %     SE = strel('disk',2,0);
% % % % 
% % % %     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %     % set the direction (up or done)
% % % %     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % % 
% % % %     delta_ = -1;  
% % % %     if (delta_ == -1)
% % % %         rng_ = [size(input_img,1):delta_:2] ;
% % % %     end
% % % %     if (delta_ == 1)
% % % %         rng_ = [1:size(input_img,1)-1] ;
% % % %     end
% % % %     
% % % %     %%
% % % %     for sl=  rng_
% % % %          %for sl=  [1:size(input_img,1)-1] 
% % % %         if (verbose_) 
% % % %             fprintf('.');
% % % %             if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
% % % %             if (mod(sl,80)==0),  disp('.'),    end
% % % %         end
% % % %         
% % % %         %% 
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % 1. Search for the object with the maximum intersect.... 
% % % %         % 2. save (A) the current & (B) the next slice.
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         curr_sl = (squeeze(input_img(sl,:,:))); 
% % % %         if (~any(curr_sl(:))) 
% % % %             continue
% % % %         end 
% % % %         curr_sl2 = curr_sl; 
% % % %         next_sl = (squeeze(input_img(sl+delta_,:,:)));
% % % %  
% % % %         %%    
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % Search for the object with the maximum intersect.... 
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         CC_next_sl = bwconncomp(next_sl);
% % % %         max_inter_CC = 0;
% % % %         for ii=[1:CC_next_sl.NumObjects] 
% % % %             next_tmp_bw = zeros(size(curr_sl));
% % % % 
% % % %             next_tmp_bw(CC_next_sl.PixelIdxList{ii}) = 1;
% % % %             inter_curr_next = curr_sl & next_tmp_bw ;
% % % %             if (sum( inter_curr_next(:)) > max_inter_CC )  
% % % %                  max_inter_curr_n_next = next_tmp_bw;
% % % %                  max_inter_CC = sum( inter_curr_next(:))  ;
% % % %             end
% % % %         end
% % % %         next_tmp_bw = max_inter_curr_n_next; 
% % % %                 %     STOP_HERE()   
% % % % 
% % % %         %% 
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % Dilate upto the next slide 
% % % %         %   o Build "contour" difrences between the current and the next slice.
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % % 
% % % %         num_of_dilation = 0;
% % % %         inter_curr_next2 =1;  
% % % %         post_dilate = curr_sl;
% % % %         post_dilate(post_dilate>0) =1; 
% % % %         curr_sl2(curr_sl2>0) =1;
% % % % 
% % % %         while ((num_of_dilation~=MAX_DILATION_ALLOWED) && any(inter_curr_next2(:)))
% % % %             num_of_dilation = num_of_dilation+1 
% % % %             pre_dilate = post_dilate;
% % % %             post_dilate = imdilate(pre_dilate,SE);
% % % %             idxx = ~pre_dilate & post_dilate;
% % % %             curr_sl2(idxx)=num_of_dilation+1;
% % % %             inter_curr_next2 = ~logical(curr_sl2) & next_tmp_bw;
% % % %         end 
% % % %         curr_sl2(~next_tmp_bw)=0;
% % % %         
% % % % 
% % % %         %% 
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % Remove new componets which has a 'contour level' bigger then 3.
% % % %         %   
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % %         max_thresh_allowed = 3;
% % % %         curr_sl3=[];
% % % %         if (find(curr_sl2>max_thresh_allowed))
% % % %             % STOP_HERE() ;  
% % % %             curr_sl3 = curr_sl2; 
% % % %             curr_sl3(curr_sl3==1)=0; 
% % % % %             CC_curr_sl3 = bwconncomp(curr_sl3);
% % % % %             max_inter_CC = 0;
% % % % %             ii=2;   
% % % % %             for ii=[1:CC_curr_sl3.NumObjects] 
% % % % %                 curr_sl3_tmp_bw = zeros(size(curr_sl));
% % % % % 
% % % % %                 curr_sl3_tmp_bw(CC_curr_sl3.PixelIdxList{ii}) = 1;
% % % % %                 inter_curr_next = curr_sl & next_tmp_bw ;
% % % % %                 if (sum( inter_curr_next(:)) > max_inter_CC )  
% % % % %                      max_inter_curr_n_next = next_tmp_bw;
% % % % %                      max_inter_CC = sum( inter_curr_next(:))  ;
% % % % %                 end
% % % % %             end
% % % %         end
% % % %         
% % % %               %   STOP_HERE()
% % % %                 eee=1;    
% % % %                 %   curr_sl2(curr_sl2>3)=0;  
% % % %                 input_img(sl+delta_,:,:) = curr_sl2; 
% % % %    
% % % %          %    end 
% % % %               
% % % %         %end  
% % % %         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\freezeColors');
% % % %           
% % % %         if ((sl >= 100) && (sl <= 335))  %  (1) % ((sl >= 100) && (sl <= 376))  % (0) % 
% % % %                 %%
% % % %         %f1 = figure('visible','off');  
% % % %              hold on;
% % % %              grid on; 
% % % %         subplot(2,6,[1,2]) 
% % % %             imagesc(next_sl);  
% % % %             title('next\_sl');
% % % %             ylabel(['Slice: ' num2str(sl)]);
% % % %             colormap(jet)
% % % %             freezeColors
% % % %         %subplot(2,6,[3,4]) 
% % % %         %                imagesc(curr_sl);
% % % %         %                 title('curr\_sl'); 
% % % %         subplot(2,6,[3,4])  
% % % %             imagesc(curr_sl2); 
% % % %             title('curr\_sl2');
% % % %             colormap(jet)
% % % %             freezeColors
% % % %         subplot(2,6,[5,6])   
% % % %             imagesc(curr_sl3);  
% % % %             title('curr\_sl3'); 
% % % %             colormap(jet)
% % % %             freezeColors
% % % %         subplot(2,6,[9,10])   
% % % %             t_inside = orig_ct(curr_sl2==1);
% % % %             
% % % %             hold on; 
% % % %            %   STOP_HERE()   
% % % %            dbl1 = double(t_inside(t_inside>0));
% % % %             [nelements1,centers1] =hist(dbl1,40);
% % % %             nelements1 =nelements1/sum(nelements1);
% % % %             bar(centers1,nelements1,'b');
% % % %             k1 = kurtosis(dbl1);
% % % %             xlabel(['kurtosis-in: ' num2str(k1)]); 
% % % % 
% % % %             if ~isempty(curr_sl3)  
% % % %                 t_outside =  orig_ct(curr_sl3>1);
% % % %                 dbl = double(t_outside(t_outside>2));
% % % %                 [nelements2,centers2] =hist(dbl,40);
% % % %                 nelements2 =nelements2/sum(nelements2);
% % % %                 bar(centers2,nelements2,'r');   
% % % %                 k = kurtosis(dbl);
% % % %                 [h,p] =ttest2(dbl1,dbl); 
% % % %                 title(['T-TEST2( h: ' num2str(h) ', p: ' num2str(p) ')']);
% % % %                 xlabel({['kurtosis-in: ' num2str(k1)],['kurtosis-out: ' num2str(k)]});
% % % %           %          STOP_HERE()  
% % % %                 legend('inside graylevel' , 'outside graylevel'); 
% % % %             end
% % % %              
% % % %            %  legend('inside graylevel' , 'outside graylevel'); 
% % % %            %  title('orig\_ct');  
% % % %            %  imagesc(squeeze(orig_ct(sl,:,:))); 
% % % %            colormap(jet)
% % % %            freezeColors  
% % % %         subplot(2,6,[11,12])  ;
% % % %             ccc= double(curr_sl3); 
% % % %             mm= ccc(ccc>1);
% % % %             cc=histc(mm(:) ,1:MAX_DILATION_ALLOWED);
% % % %             bar(cc);    
% % % %             ns=sort(mm); % only part of <unique>
% % % %             c=histc(ns,ns); 
% % % %             r=ns(c==max(c));  
% % % %             if (length(r)) 
% % % %                 title(['Most freq: ' num2str(r(1))] );  
% % % %             end
% % % %             colormap(jet)
% % % %             freezeColors    
% % % %         subplot(2,6,[7,8])   
% % % %             title('orig\_ct');  
% % % %             imagesc(squeeze(orig_ct(sl,:,:))); 
% % % %             colormap(gray)
% % % %             freezeColors
% % % %        %%   
% % % %        %  res = hardcopy(f1, '-dzbuffer', '-r0');  
% % % %       %    res_all(sl,:,:,:) =  imresize(res , [size(res_all,2) size(res_all,3)]);   
% % % %       %    close(f1) 
% % % % %      if (r >2) 
% % % %                STOP_HERE() 
% % % % %              eee=1;  
% % % % %      end   
% % % %         end 
% % % %     end 
% % % %    figure
% % % %   %    size(res_all)
% % % %    STOP_HERE()  
% % % %    
% % % %    eee=1;  
% % % %    %%
% % % %     iii=320; 
% % % %     imagesc(squeeze( res_all(sl,:,:,:))); 
% % % %    %%
% % % % %      
% % % % %     if (0)
% % % % %         %% 
% % % % %          
% % % % %         %%
% % % % %         sl = 202;
% % % % %         t = squeeze(c_im_res2(sl,:,:) );
% % % % %         imagesc(t);
% % % % %         
% % % % %         
% % % % %         %%
% % % % %     end
% % % %     
% % % %     %%
% % % %      input_img = logical(input_img); 
% % % %     %   STOP_HERE();
% % % %     res_ = CTpermute(input_img, 'backward', dim_);
% % % % end
% % % %  
% 
% 
%    %%  
% % % % %                 
% % % % %                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % % %                 % check if the "object" in the next slice is twice as big as 
% % % % %                 %  the "object" in the current slice
% % % % %                 % -> this is an indcation of LEAKAGE into another organ 
% % % % %                 %
% % % % %                 % In this case: keep only the intersect of the current &
% % % % %                 %               the next slice
% % % % %                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % % %                 if (sum(next_tmp_bw(:)) > 2*sum(curr_sl(:)))
% % % % %                     input_img(:,:,sl-1) = inter_curr_next; 
% % % % %                     num_of_obj =1;
% % % % %                     continue 
% % % % %                 end
%                   
%                   
%                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%                 % If we got here: it's means the the intersect between the
%                 % two consequent is "reasonable" 
%                 %
%                 % In this case: keep only the overlapping/coincident object
%                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%              %     imagesc(tmp_bw)
% % % %                 input_img(:,:,sl-1) = next_tmp_bw;
% % % %                 num_of_obj =1;
%              
%       
%  
% %% soft_tissue 1
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * 
%     % * *  soft_tissue
%     % * * 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function soft_tissue(enabled_)
%        if (~enabled_), return , end
%    
%        global im_idx 
%         PB.add_im_p( 'NEW', 'NEW');
%         PB.add_im_p( 'imageName', ...
%                 ['c_im_res' num2str(im_idx-1)]); 
%         PB.add_im_p( 'imageLoadFunc', ' ');
%         PB.add_im_p( 'imageLoadFuncPath', ' ');
%         PB.add_im_p( 'DisplayResult', 0) ;
%         
%             % Processing 
%             % ~~~~~~~~~~
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 1);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'threshold_image_BW');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '50' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );  
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '160' );
%                 
%                 
%                 
%              % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 2);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''disk''' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%                 PB.add_im_P_Prm_p( 'prm_value', '1' );   
%                 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm2' );
%                 PB.add_im_P_Prm_p( 'prm_value', '0' );   
%                 
%                  
%             % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 3);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
%                 % paramter indexes  
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imfill'''  );
%                  
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
%                 
%           % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 2);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''disk''' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%                 PB.add_im_P_Prm_p( 'prm_value', '2' );  
%                 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm2' );
%                 PB.add_im_P_Prm_p( 'prm_value', '0' );   
%     end
% %% soft_tissue 2
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * 
%     % * *  soft_tissue
%     % * * 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function soft_tissue1(enabled_)
%        if (~enabled_), return , end
%    
%        global im_idx 
%         PB.add_im_p( 'NEW', 'NEW');
%         PB.add_im_p( 'imageName', ...
%                 ['c_im_res' num2str(im_idx-1)]); 
%         PB.add_im_p( 'imageLoadFunc', ' ');
%         PB.add_im_p( 'imageLoadFuncPath', ' ');
%         PB.add_im_p( 'DisplayResult', 0) ;
%         
%             % Processing 
%             % ~~~~~~~~~~
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 1);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'threshold_image_BW');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
%  
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '150' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '300' );
%           
%                 
%           % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 2);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''disk''' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%                 PB.add_im_P_Prm_p( 'prm_value', '2' );   
%                 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm2' );
%                 PB.add_im_P_Prm_p( 'prm_value', '0' ); 
%                 
%                 
%                 
%                 
%                   % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 3);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
%                 % paramter indexes  
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imfill'''  );
%                  
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
%                 
%                 
%                 
%            % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 2);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''disk''' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%                 PB.add_im_P_Prm_p( 'prm_value', '2' );   
%                 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm2' );
%                 PB.add_im_P_Prm_p( 'prm_value', '0' );       
%                 
%                 
%                 
%     end
% %% gross_CT_area
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * 
%     % * *  gross_CT_area
%     % * * 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function gross_CT_area(enabled_)
%        if (~enabled_), return , end
% 
%       
%        global im_idx 
%         PB.add_im_p( 'NEW', 'NEW')
%         PB.add_im_p( 'imageName', ...
%                 ['c_im_res' num2str(im_idx-1)]); 
%         PB.add_im_p( 'imageLoadFunc', ' ')
%         PB.add_im_p( 'imageLoadFuncPath', ' ')
%         PB.add_im_p( 'DisplayResult', 0)
%         id_ =0;
%   
%         % Processing
%         % ~~~~~~~~~~ 
%         id_ = id_ +1;
%         PB.add_im_P_p( 'NEW', 'NEW')
%         PB.add_im_P_p( 'id', id_);
%         PB.add_im_P_p( 'Enabled', 1);
%         PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
%         PB.add_im_P_p( 'MethodFilesLocation', ...
%                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
%             % paramter indexes  
%             % ~~~~~~~~~~~~~~~~
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%             PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
%  
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%             PB.add_im_P_Prm_p( 'prm_value', '''bwconvhull'''  );
% 
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%             PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
% 
%     end
% 
%    
% %% gross_CT_landmarks
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * 
%     % * *  gross_CT_landmarks
%     % * * 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% %     function gross_CT_landmarks(enabled_)
% %        if (~enabled_), return , end
% % 
% %      
% %         global im_idx 
% %         PB.add_im_p( 'NEW', 'NEW')
% %         PB.add_im_p( 'imageName', ...
% %                 ['c_im_res' num2str(im_idx-1)]); 
% %         PB.add_im_p( 'imageLoadFunc', ' ')
% %         PB.add_im_p( 'imageLoadFuncPath', ' ')
% %         PB.add_im_p( 'DisplayResult', 0) 
% %         id_ =0; 
% %   
% %         % Processing
% %         % ~~~~~~~~~~ 
% %         id_ = id_ +1;
% %         PB.add_im_P_p( 'NEW', 'NEW')
% %         PB.add_im_P_p( 'id', id_);
% %         PB.add_im_P_p( 'Enabled', 1);
% %         PB.add_im_P_p( 'MethodName', 'CBIR_SKELETON_aux_func.gross_CT_landmarks');
% %         PB.add_im_P_p( 'MethodFilesLocation', ...
% %                        ' ');
% %             % paramter indexes  
% %             % ~~~~~~~~~~~~~~~~
% %             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
% %             PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
% %             PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
% % 
% %  
% % 
% %     end
% 
% %% boold_pool
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * 
%     % * *  boold_pool
%     % * * 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function boold_pool(enabled_)
%        if (~enabled_), return , end
% 
%       
%         global im_idx 
%         PB.add_im_p( 'NEW', 'NEW');
%         PB.add_im_p( 'imageName', ...
%                 ['c_im_res' num2str(im_idx-1)]); 
%         PB.add_im_p( 'imageLoadFunc', ' ')
%         PB.add_im_p( 'imageLoadFuncPath', ' ');
%         PB.add_im_p( 'DisplayResult', 0);
%         PB.add_im_p( 'isSegmentation', 0 );
% 
%         id_ =0; 
%   
%          % Processing
%         % ~~~~~~~~~~ 
%         id_ = id_ +1;
%         PB.add_im_P_p( 'NEW', 'NEW')
%         PB.add_im_P_p( 'id', id_);
%         PB.add_im_P_p( 'Enabled', 1);
%         PB.add_im_P_p( 'MethodName', 'CT_arterial_abdominal_masks');
%         PB.add_im_P_p( 'MethodFilesLocation', ...
%                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
%             % paramter indexes  
%             % ~~~~~~~~~~~~~~~~
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%             PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
%     end
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% 
% 
%     
% %% PCA_CT1 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * PCA_CT1
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function PCA_CT1( enabled_ )
%         if (~enabled_), return , end
%         global im_idx 
%         PB.add_im_p( 'NEW', 'NEW')
%         PB.add_im_p( 'imageName', ...
%                 ['c_im_res' num2str(im_idx-1)]);
%         PB.add_im_p( 'imageLoadFunc', ' ')
%         PB.add_im_p( 'imageLoadFuncPath', ' ')
%         PB.add_im_p( 'DisplayResult', 0)
%             % Processing
%             % ~~~~~~~~~~
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 1);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'CT_PCA');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%             'C:\Dropbox\MY_CODE\lib\MATLAB\dim_reduction\EIGENFACES\EIGENFACES\');
%                 % paramter indexes
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx-1)] );
%     end
%     
%     
% %% extract_bones1
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * 
%     % * *  extract_bones1
%     % * * 
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function extract_bones1(enabled_) 
%        if (~enabled_), return , end
%     error(' please use extract_bones_tight !!!!');
%         global im_idx  
% %         PB.add_im_p( 'NEW', 'NEW')
% %         PB.add_im_p( 'imageName', ...
% %                 ['c_im_res' num2str(im_idx-1)]); 
% %         PB.add_im_p( 'imageLoadFunc', ' ')
% %         PB.add_im_p( 'imageLoadFuncPath', ' ')
% %         PB.add_im_p( 'DisplayResult', 0) 
%             % Processing 
%             % ~~~~~~~~~~
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 1);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'threshold_image_BW');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', 'c_im' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '200' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '1000' );
% 
%             % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 2);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imclose'''  );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''square''' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
%             
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 2);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%                 % paramter indexes 
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''disk''' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%                 PB.add_im_P_Prm_p( 'prm_value', '4' );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%                 PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%                 PB.add_im_P_Prm_p( 'prm_value', '0' );
%             % Processing
%             % ~~~~~~~~~~ 
%             PB.add_im_P_p( 'NEW', 'NEW')
%             PB.add_im_P_p( 'id', 3);
%             PB.add_im_P_p( 'Enabled', 1);
%             PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
%             PB.add_im_P_p( 'MethodFilesLocation', ...
%                            'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
%                 % paramter indexes  
%                 % ~~~~~~~~~~~~~~~~
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%                 PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%                 PB.add_im_P_Prm_p( 'prm_value', '''imfill'''  );
%                  
%                 PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%                 PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%                 PB.add_im_P_Prm_p( 'prm_value', '3' );
%   
% 
%     end
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%  
  end 
   
 
end


