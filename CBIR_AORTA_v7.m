
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load SPINAL_COLUMN_ segmentation  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['SPINAL_COLUMN_'];    nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();
   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];            nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
% PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% spinal column conex-hull   
%            FROM Bones_Axial_smalles_Slice TO Lung_Axial_Biggest_Slice
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('spinal column conex-hull'); 
PB.add_im_Process(    'simple_3D_SEmorph.run', ...
                   'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\'); 

    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-2)] );
    PB.add_im_Process_parm( 'edge_detection_type' , '''imclose'''   );
    PB.add_im_Process_parm('dim_' , '1'  );
    PB.add_im_Process_parm('strl_shape' , '''disk''' );
    PB.add_im_Process_parm('strl_prm1', '18' );
    PB.add_im_Process_parm('strl_prm1', '0' );

PB.add_im_Process(    'simple_2D_to_3D.run', ...
        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
    PB.add_im_Process_parm('oper_' ,'''bwconvhull'''  );
    PB.add_im_Process_parm('dim_' , '3' );



% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> spinal     Aorta_n_Spinal_BoundingBox  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index(' Aorta_n_Spinal_BoundingBox');     
PB.add_im_Process(    'CBIR_SPINAL_COLUMN_aux_func.Aorta_n_Spinal_BoundingBox', ...
                  'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\'); 
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-3)] );




% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% gross_ROI (A band around the spinal column) 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('band around the spinal column'); 
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();
PB.add_im_Process(    'CBIR_AORTA_2_aux_func_ThAb.gross_ROI', ...
                   'C:\Dropbox\MY_CODE\PHD\CBIR');
  PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-1)] ); % Aorta_n_Spinal_BoundingBox
  PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-2)] ); % Spinal_seg
   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it! 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = 'BAND_AROUND_THE_SPINAL_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   


% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% spinal strip -- use insteed of the Orta !! 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('A Spinal Strip');   
PB.add_im_Process(    'CBIR_AORTA_2_aux_func_ThAb.spinal_strip_from_spinal_band', ...
                                      'C:\Dropbox\MY_CODE\PHD\CBIR');
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'])
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it! 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = 'SPINAL_STRIP_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   
 

% % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% %   Aorat - cont.
% % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%  
% PB_shortcuts.set_new_image_index('A Spinal Strip');   
% PB.add_im_Process(    'CBIR_AORTA_2_aux_func_ThAb.segment_AORTA_stage2', ...
%                    'C:\Dropbox\MY_CODE\PHD\CBIR');
%       PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-7)] ); % Aorta_n_Spinal_BoundingBox
%       PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-3)] ); % Spinal_seg
%    
%  
%           

%PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT();       
 
% % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% % % Aorta thresholding.  
% % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% % ix = PB_ip_shortcuts.get_image_idx();  
% % 
% % PB_shortcuts.set_new_image_index(); 
% % PB.add_im_Process(    'LogicalAssignment', ...
% %      'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');  
% %      PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-4)] );
% %      PB.add_im_Process_parm('B' ,              ['~logical(c_im)']);
% %      PB.add_im_Process_parm('C' ,  '0')  
% %             prm101 = str2num(CP.ip(ix).prm101);     
% %   
% % Heart_GrayLevel_mean = CP.ip(ix).Heart_GrayLevel_mean;
% % Heart_GrayLevel_std = CP.ip(ix).Heart_GrayLevel_std; 
% % HH_ = round(Heart_GrayLevel_mean - 0.4*Heart_GrayLevel_std);
% % HH1_ = round(Heart_GrayLevel_mean + 1.2*Heart_GrayLevel_std); 
% % PB.add_im_Process(  'threshold_image_BW', ...  
% %                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
% %     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );
% %     PB.add_im_Process_parm('prm_value', ...
% %           num2str(HH_ )   ); 
% %     PB.add_im_Process_parm('prm_value',  ...   
% %          num2str(HH1_ )   ); 
% %          % num2str(Heart_GrayLevel_mean + prm101*Heart_GrayLevel_std )   );
% % 
% % 
% % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % Last cleaning & final tache 
% % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % PB_shortcuts.set_new_image_index('Last cleaning & final tache '); 
% % 
% % PB.add_im_Process(    'simple_3D_SEmorph.run', ...  
% %                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\'); 
% % 
% %     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] ); 
% %     PB.add_im_Process_parm( 'edge_detection_type' , '''imclose'''   );
% %     PB.add_im_Process_parm('dim_' , '3'  ); 
% %     PB.add_im_Process_parm('strl_shape' , '''disk''' );
% %     PB.add_im_Process_parm('strl_prm1', '1' );
% %     PB.add_im_Process_parm('strl_prm1', '0' ); 
% % 
% % PB.add_im_Process(  'simple_2D_to_3D.run', ... 
% %                   'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
% %     PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
% %     PB.add_im_Process_parm('oper_type' , '''imfill''' );
% %     PB.add_im_Process_parm('dim_' , '3' ); 
% % 
% % PB.add_im_Process(    'simple_3D_SEmorph.run', ...  
% %                    'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\'); 
% % 
% %     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
% %     PB.add_im_Process_parm( 'edge_detection_type' , '''imopen'''   );
% %     PB.add_im_Process_parm('dim_' , '3'  ); 
% %     PB.add_im_Process_parm('strl_shape' , '''disk''' );
% %     PB.add_im_Process_parm('strl_prm1', '3' );
% %     PB.add_im_Process_parm('strl_prm1', '0' );
% % 
% % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % PB_shortcuts.set_new_image_index(); 
% % PB.add_im_Process(    'largest_conncomp', ... 
% %      'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
% %       PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );  
% % 
% %  
% % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% % %    save it! 
% % % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% % out_file_prefix_ = 'AORTA_GROSS_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
% % file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
% % PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   

