
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% %  for liver + Heart + left kidney == 1
% %  for spleen + right kidney == 0 
% %  
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
  
     
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% ===> load the CT volume  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];            nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load SPINAL_STRIP_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%                    file_prefix = ['AORTA_GROSS_'];    nii_prefix =  [];    
file_prefix = ['SPINAL_STRIP_'];    nii_prefix =  [];    


PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();  

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> The Aorta's triu mask 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index('Aorta''s triu mask');  
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();  
    PB.add_im_Process(    'CBIR_LIVER_aux_func.segment_LIVER_triu', '');
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );
%     PB_shortcuts.KEEP3_RESULT_AT_THIS_POINT( );   
    
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load BONE_SKELETON_ segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['BONE_SKELETON_'];  change_curr_image_name =0;  nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% close the chest 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
PB_shortcuts.set_new_image_index(); 
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE();  

    CBIR_LIVER_aux_func.segment_LIVER_close_chest(); % -3
    PB_shortcuts.KEEP_RESULT_AT_THIS_POINT(); 


% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% ~~~~~~~~~~~~ -> make sure 2 keep the area above the chest
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
PB_shortcuts.set_new_image_index();     % -2 

PB.add_im_Process(    'simple_2D_to_3D.run', ...
      'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
   PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );
   PB.add_im_Process_parm('oper_' ,'''mark_upto_max_dim2'''  );
   PB.add_im_Process_parm('dim_' , '3' );

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it! 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = 'CHEST_POSTERIOR_P_V2_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   
   
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];            nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load ) 

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB_shortcuts.set_new_image_index();     % -1
PB.add_im_Process(    'simple_2D_to_3D.run', ... 
          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx-4)] );
    PB.add_im_Process_parm('oper_' ,'''bwconvhull'''  );
    PB.add_im_Process_parm('dim_' , '3' );
    
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
switch (str2num(RadID))   
    case {29662, 29663} ,  % Kidneys!    
        PB.add_im_Process(    'simple_3D_SEmorph.run', ... 
          'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');   
            PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
            PB.add_im_Process_parm('oper_' ,'''imdilate'''  );
            PB.add_im_Process_parm('dim_' , '3' );  
            PB.add_im_Process_parm('SE1' , '''disk''' ); 
            PB.add_im_Process_parm('SE2' , '15' );    
            PB.add_im_Process_parm('SE3' , '0' ); 
        
end
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%           Build a mask2remove 
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
PB_shortcuts.set_new_image_index();  % -3

% ones matrix (Ad-hoc)  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB.add_im_Process(    'LogicalOperators', ...
     'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   %  A(A 'oper_' B) = C
     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
     PB.add_im_Process_parm('oper_' ,  '''>''')  
     PB.add_im_Process_parm('B' ,     '-10000' );
     PB.add_im_Process_parm('C' ,  '1')  


% Remove the outside of the chest-convex 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB.add_im_Process(    'LogicalAssignment', ...   
    'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');  
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] );
    PB.add_im_Process_parm('B' ,              ['~logical(c_im_res' num2str(im_idx-1) ')']);
    PB.add_im_Process_parm('C' ,  '0') 

% keep the chest posterior  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
PB.add_im_Process(    'LogicalAssignment', ...   
    'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
    PB.add_im_Process_parm('B' ,              ['logical(c_im_res' num2str(im_idx-4) ')']);
    PB.add_im_Process_parm('C' ,  '1') 

% Remove the chest (the clsoe chest...) 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
PB.add_im_Process(    'LogicalAssignment', ... 
    'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
    PB.add_im_Process_parm('B' ,              ['logical(c_im_res' num2str(im_idx-5) ')']);
    PB.add_im_Process_parm('C' ,  '0') ;

% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
% Clean + largest-componnet 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
PB.add_im_Process(  'simple_3D_SEmorph.run', ...
                  'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
    PB.add_im_Process_parm('oper_type' , '''imopen''' );
    PB.add_im_Process_parm('dim_' , '3' );   
    PB.add_im_Process_parm('SE1' , '''disk''' ); 
    PB.add_im_Process_parm('SE2' , '4' );  
    PB.add_im_Process_parm('SE3' , '0' );

 PB.add_im_Process(    'largest_conncomp', ...
     'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );  


% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%           LIVER FINAL MUSK 
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

%%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
PB_shortcuts.set_new_image_index();

% Remove The-MASK
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB.add_im_Process(    'LogicalAssignment', ...    
    'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');  
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-3)] );
    PB.add_im_Process_parm('B' ,              ['~logical(c_im_res' num2str(im_idx-1) ')']);
    PB.add_im_Process_parm('C' ,  '0')    

  
% Remove The-TRIU    
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB.add_im_Process(    'LogicalAssignment', ...    
    'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx)] ); 
    
switch (str2num(RadID))   
    case {29663, 86} , 
        PB.add_im_Process_parm('B' ,              ['~logical(c_im_res' num2str(im_idx-8) ')']);    
      
    case {29662, 58} ,   
        PB.add_im_Process_parm('B' ,              ['logical(c_im_res' num2str(im_idx-8) ')']);
end
    PB.add_im_Process_parm('C' ,  '0')  
% 
% if (MUSK_LEFT_ORGAN)    
%     PB.add_im_Process_parm('B' ,              ['logical(c_im_res' num2str(im_idx-8) ')']);
% else 
%     PB.add_im_Process_parm('B' ,              ['~logical(c_im_res' num2str(im_idx-8) ')']);
% end
%     PB.add_im_Process_parm('C' ,  '0')  
 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> 
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 

PB_shortcuts.set_new_image_index();
PB.add_im_Process(    'largest_conncomp', ...
     'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');  
         PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );  

PB.add_im_Process(    'LogicalAssignment', ... 
    'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');   
    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
    PB.add_im_Process_parm('B' ,               ['c_im_res' num2str(im_idx)] );
    PB.add_im_Process_parm('C' ,  '1') ;
  
%STOP_HERE()   
%     %      RadID = '58'  % liver
%   %      RadID = '86'  % Spleen  
%       RadID = '29662' % right kidney 
%  %   RadID =   '29663'    % left  kidney 
switch (str2num(RadID))   
    case {29662} , 
      out_file_prefix_ = 'KIDNEY_RIGHT_COARSE_SEG_';     
    case {29663} , 
       out_file_prefix_ = 'KIDNEY_LEFT_COARSE_SEG_';    
    case {86} , 
       out_file_prefix_ = 'SPLEEN_COARSE_SEG_';     
    case {58} , 
       out_file_prefix_ = 'LIVER_COARSE_SEG_';
end
  
 
  
switch (str2num(RadID))   
    case {86} ,  
     
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % Some more refinement of the SPLEEN Coarse ROI  !! 
    %    Set to zero all pixels to the rigth of the!! 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    PB_shortcuts.set_new_image_index('Somre more refinement of the SPLEEN Coarse ROI');   
        PB.add_im_Process(    'CBIR_SPLEEN_aux_func.Sagittal_mid_Slice_Coarse_ROI', ...
                                      'C:\Dropbox\MY_CODE\PHD\CBIR');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'])
   

        PB.add_im_Process(  'simple_2D_to_3D.run', ...    
                         'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' ,  ['c_im_res' num2str(im_idx)] ); 
            PB.add_im_Process_parm('oper_type' , '''largest_conncomp''' );
            PB.add_im_Process_parm('dim_' , '3' ); 
         PB.add_im_Process(    'largest_conncomp', ... 
             'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');    
              PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 

    
    
   %  out_file_prefix_ = 'SPLEEN_n_RIGHT_KIDNEY_GROSS_SEG_';     
end
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it! 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_suffix = [ ];  out_dir = TMP_DIR;  
   
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   


