classdef CBIR_SKELETON_aux_func

    methods(Static)
         
    %% extract_bones_tight
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *  extract_bones_tight
    % * *   used by:  
    % * *         o CBIR_SKELETON_w_PLOT.m
    % * *  
    % * *   Assume: (are known) 
    % * *      Heart_GrayLevel_mean
    % * *      Bones_GrayLevel_std
    % * * 
 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function extract_bones_tight(enabled_  , file2load  )         
        global im_idx   CP   ;  
        ix = PB_ip_shortcuts.get_image_idx( );  
        Heart_GrayLevel_mean = CP.ip(ix).Heart_GrayLevel_mean; 
        Heart_GrayLevel_std = CP.ip(ix).Heart_GrayLevel_std; 
        HU_bone_low =  round(Heart_GrayLevel_mean + 1.9*Heart_GrayLevel_std); 
       %    STOP_HERE();  
  
        PB.add_im_Process(  'threshold_image_BW', ...
                              'C:\Dropbox\MY_CODE\lib\MATLAB\imp\Thresholding\');
                PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im' ] );
                PB.add_im_Process_parm('prm_value', num2str(HU_bone_low)   ); 
                PB.add_im_Process_parm('prm_value', '1200' );
 
%  
%         PB.add_im_Process(  'simple_2D_to_3D.run', ...
%                               'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%                 PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
%                 PB.add_im_Process_parm('oper_type' , '''imclearborder''' );
%                 PB.add_im_Process_parm('dim_' , '3' );  

        PB.add_im_Process(  'simple_2D_to_3D.run', ...
                              'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
                PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
                PB.add_im_Process_parm('oper_type' , '''imfill''' );
                PB.add_im_Process_parm('dim_' , '3' ); 
                   
        PB.add_im_Process(  'simple_3D_SEmorph.run', ...
             'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
            PB.add_im_Process_parm('oper_type' , '''imclose''' );
            PB.add_im_Process_parm('dim_' , '3' );   
            PB.add_im_Process_parm('SE1' , '''disk''' ); 
            PB.add_im_Process_parm('SE2' , '1' );     
            PB.add_im_Process_parm('SE3' , '0' );            
       
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * IF MOTI -- connect last Axial slice
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
      %  if (~isempty(strfind(curr_image_name, 'Moti')))
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                % this is just temporay loading (of the LUNGS_n_TRACHEA_) and 
                % we don't want to change the "curr_image_name" variable name
                %    
                %  Note: Changeing the "curr_image_name" variable efect the "ix"
                %  varable & where param are store in the CP struct... 
                %
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                change_curr_image_name = 0;       
    
                PB_shortcuts.KEEP_RESULT_AT_THIS_POINT();   

                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                % ~~~~~~~~~~~~ -> load_nii_image
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                   
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ===> load LUNGS_n_TRACHEA_ segmentation 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    global TMP_DIR  
    file_prefix = ['LUNGS_n_TRACHEA_'];   nii_prefix =  [];    
    PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
   
%                 im_str = [' ''C:/Users/owner/DATA/VISCERAL/visceral-dataset/trainingset/volumes/' ...
%                      ['LUNGS_n_TRACHEA_'] curr_image_name '.nii'' '];
%                 PB_shortcuts.load_nii_image( im_str , change_curr_image_name ); 

                PB.add_im_Process('CBIR_Moti_aux_func.connect_last_Axial_slice', '' ); 
                    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ...
                          ['c_im' ] );                    %  LUNGS_n_TRACHEA
                    PB.add_im_Process_parm('IMAGE_TAMPLATE' , ...
                        ['c_im_res' num2str(im_idx-1)] );  % chest  CT 

     %  end     
        

          
        PB.add_im_Process(    'largest_conncomp', ...
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
                     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
 
        PB.add_im_Process(  'simple_3D_SEmorph.run', ...
             'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
            PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] );
            PB.add_im_Process_parm('oper_type' , '''imclose''' );
            PB.add_im_Process_parm('dim_' , '3' );   
            PB.add_im_Process_parm('SE1' , '''disk''' ); 
            PB.add_im_Process_parm('SE2' , '2' );     
            PB.add_im_Process_parm('SE3' , '0' );            
   
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
     % ~~~~~~~~~~~~ -> Tight it...()
     %                  --> clean it (open)
     %                   --> outcome: bones! not all of them.. but what we
     %                                leaft with is "Guarante" to be a bone.
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
     %          
%     % ~~~~~~~~~~ 
%     PB.add_im_P_p( 'NEW', 'NEW')
%     PB.add_im_P_p( 'id', 2);
%     PB.add_im_P_p( 'Enabled', 1);
%     PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
%     PB.add_im_P_p( 'MethodFilesLocation', ...
%                    'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
%         % paramter indexes 
%         % ~~~~~~~~~~~~~~~~
%         PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%         PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%         PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
% 
%         PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%         PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
%         PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );
% 
%         PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%         PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
%         PB.add_im_P_Prm_p( 'prm_value', '3' );
% 
%         PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%         PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
%         PB.add_im_P_Prm_p( 'prm_value', '''square''' );
% 
%         PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
%         PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
%         PB.add_im_P_Prm_p( 'prm_value', '4' ); 
    
      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
     % Processing -- largest_conncomp  
        % ~~~~~~~~~~ 
%         PB.add_im_P_p( 'NEW', 'NEW')
%         PB.add_im_P_p( 'id', 1);
%         PB.add_im_P_p( 'Enabled', 1);
%         PB.add_im_P_p( 'MethodName', 'largest_conncomp');
%         PB.add_im_P_p( 'MethodFilesLocation', ...
%                        'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');
% 
%             % paramter indexes 
%             % ~~~~~~~~~~~~~~~~
%             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
%             PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
%             PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );
                
                
    end 
 
    %% gross_CT_landmarks 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * gross_CT_landmarks
    %   Get:
    %      o img_        - bone convhull
    %
    %   Set: 
    %       o Bones_Axial_smalles_Slice
    %       o Bones_Axial_smalles_Slice_Ytop
    %       o Bones_Axial_smalles_Slice_Ybottom 
    %
    % 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [res] = gross_CT_landmarks(img_)
   %  STOP_HERE() ; 
    % pre axial-plane-sum processing....
    % --- 
    s1 = size(img_,1); 
    s2 = size(img_ ,2);
    img__ = reshape(img_, s1*s2, []);
 
    % sum over the axial plane
    % ---  
    SM = sum(img__,1);
    SM = smooth(SM); 
     
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    global curr_image_name 
    if (isempty(strfind(curr_image_name, 'Moti')))

        SM_t = SM;   
        SM_t(1:round(length(SM)/2))=0;
        [max_p ,I_max] = max( SM_t );  
        SM(I_max(1):end) = max_p;
        SM(1:100) = max_p;
    end  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
      
    
    [min_p ,I_min] = min(SM);

    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
    % ~~~~~~~~~~~~~ save markers.
    c_im = img_(:,:,I_min);
%     orig_ct_im_sl = orig_ct_im(:,:,I_min);
%     orig_ct_im_sl(~c_im) = 0;
%      
    %  STOP_HERE(); 
    prp = regionprops(c_im, 'Extrema' )
    global CP  
    ix = PB_ip_shortcuts.get_image_idx( )
    I_min = round(I_min); 
    CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_Axial_smalles_Slice', ...
            I_min);  
     
     Kidney_Left_Axial_lowerBound_Slice_ = I_min-40;   
     CP.ip(ix) = pblib_set(CP.ip(ix), 'Kidney_Left_Axial_lowerBound_Slice', ...
            Kidney_Left_Axial_lowerBound_Slice_); 
           
    try     
        CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_Axial_smalles_Slice_Ytop',...
              round(prp.Extrema(1,2)));
        CP.ip(ix) = pblib_set(CP.ip(ix), 'Bones_Axial_smalles_Slice_Ybottom', ...
            round(prp.Extrema(6,2)));
           
%         if (0) 
%            sss = orig_ct_im_sl(orig_ct_im_sl>0);
%            hist(double(sss),50)  
%            STOP_HERE();
%            eee=1;   
%         end 
    catch
         error('@@@@@@ Bones_Axial_smalles_Slice_Ytop    can''t be set ');
         error('@@@@@@ Bones_Axial_smalles_Slice_Ybottom can''t be set ');
%          [ymax,imax,ymin,imin] = extrema(SM);
%          global mn mx
%          mx = sort(imax); mn = sort(imin);
%          addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
        %   STOP_HERE(); 
    end
%     
%     addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
%     STOP_HERE() ; 
       
    f1 = figure('visible','off'); 
        hold on; 
        plot(SM,'k');
        plot(I_min(1), min_p ,'k*','MarkerSize',10);
        text(I_min(1), round((7)*min_p) ,num2str(I_min(1)));
        %xlim([100 300])
 
        grid on;
        
   res = hardcopy(f1, '-dzbuffer', '-r0');        
end
    
    
    end
end

