classdef CBIR_AORTA_2_aux_func_ThAb
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
      
     methods(Static)
 
     
    %% spinal_strip_from_sprinal_band
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * spinal_strip_from_sprinal_band
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    function [spinal_strip] = spinal_strip_from_spinal_band(pinal_band)
            global CP

            spinal_strip = pinal_band;
            ix = PB_ip_shortcuts.get_image_idx( );
            Sagittal_mid_Slice = CP.ip(ix).Sagittal_mid_Slice; 
             
            spinal_strip([1:Sagittal_mid_Slice-1,Sagittal_mid_Slice+1:end],:,:)=0;
         
         
    end


%     %% gross_ROI
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     % * * gross_ROI
%     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%     function [Aorta_BoundingBox] = gross_ROI(Aorta_n_Spinal_BoundingBox , Spinal_seg )
%          global CP    curr_image_name 
%          %   STOP_HERE(); 
%                
%          Aorta_BoundingBox = Aorta_n_Spinal_BoundingBox;
%          clear Aorta_n_Spinal_BoundingBox 
% %                  Aorta_BoundingBox = Spinal_seg;
% % return  
%          verbose_ =1; 
%          ix = PB_ip_shortcuts.get_image_idx( )
%          Trachea_Axial_lowerBound_Slice         = CP.ip(ix).Trachea_Axial_lowerBound_Slice;
%           
%          if ((Trachea_Axial_lowerBound_Slice==-1) && (~isempty(strfind(curr_image_name, 'Moti'))))
%             Trachea_Axial_lowerBound_Slice  = CP.ip(ix).Lung_Axial_Biggest_Slice      
%          end
%          Bones_Axial_smalles_Slice         = CP.ip(ix).Bones_Axial_smalles_Slice; 
%  
%                   Kidney_Left_Axial_lowerBound_Slice_      ...
%                       = CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice; 
% 
%                   Kidney_Left_Axial_lowerBound_Slice_
%           
%          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%          for ii = Bones_Axial_smalles_Slice:Trachea_Axial_lowerBound_Slice
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
%              if (verbose_)
%                fprintf('*.');
%                if (mod(ii,10)==0),  fprintf(num2str(ii));,  end
%                if (mod(ii,80)==0) , disp('*.'), end
%              end 
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%              
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%              % * * locate & remove the erea "behind" the Spinal Column
%              % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%              curr_slice = Spinal_seg(:, :, ii );
%              STATS = regionprops(curr_slice, 'Centroid');
%              if (~isempty(STATS))
%                 cen = round(STATS.Centroid(1));
%                 Aorta_BoundingBox(:, cen:end ,ii)=0;
%              end
%              
%          end
%           
%          % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%    
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         % * * use opening to determin the Aorate ROI
%         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%         Aorta_BoundingBox(logical(Spinal_seg))=0;
%         SE = strel('disk',35,0);
%         Spinal_seg2 = (   logical(Spinal_seg) );
%         for ii = Bones_Axial_smalles_Slice:Trachea_Axial_lowerBound_Slice
%               if (verbose_)
%                fprintf('-.');
%                if (mod(ii,10)==0),  fprintf(num2str(ii));,  end
%                if (mod(ii,80)==0) , disp('-.'), end
%              end 
%              Spinal_seg2(:, :, ii ) = imdilate(Spinal_seg2(:, :, ii ) , SE);
%         end 
%         Aorta_BoundingBox(logical(~Spinal_seg2))=0;
%          
%          
%     end

     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
        function [Aorta_BoundingBox] = gross_ROI(Aorta_n_Spinal_BoundingBox , Spinal_seg )
         global CP    curr_image_name 
         %   STOP_HERE(); 
               
         Aorta_BoundingBox = Aorta_n_Spinal_BoundingBox;
         clear Aorta_n_Spinal_BoundingBox 
%                  Aorta_BoundingBox = Spinal_seg;
% return  
         verbose_ =1; 
         ix = PB_ip_shortcuts.get_image_idx( )
         Trachea_Axial_lowerBound_Slice         = CP.ip(ix).Trachea_Axial_lowerBound_Slice;
          
         if ((Trachea_Axial_lowerBound_Slice==-1) && (~isempty(strfind(curr_image_name, 'Moti'))))
            Trachea_Axial_lowerBound_Slice  = CP.ip(ix).Lung_Axial_Biggest_Slice      
         end
         Kidney_Left_Axial_lowerBound_Slice_         = CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice; 
  
%                   Kidney_Left_Axial_lowerBound_Slice_      ...
%                       = CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice; 
% 
%                   Kidney_Left_Axial_lowerBound_Slice_
%           
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
         for ii = Kidney_Left_Axial_lowerBound_Slice_:Trachea_Axial_lowerBound_Slice
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   
             if (verbose_)
               fprintf('*.');
               if (mod(ii,10)==0),  fprintf(num2str(ii));,  end
               if (mod(ii,80)==0) , disp('*.'), end
             end 
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
             
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
             % * * locate & remove the erea "behind" the Spinal Column
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
             curr_slice = Spinal_seg(:, :, ii );
             STATS = regionprops(curr_slice, 'Centroid');
             if (~isempty(STATS))
                cen = round(STATS.Centroid(1));
                Aorta_BoundingBox(:, cen:end ,ii)=0;
             end
             
         end
          
         % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
   
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % * * use opening to determin the Aorate ROI
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        Aorta_BoundingBox(logical(Spinal_seg))=0;
        SE = strel('disk',35,0);
        Spinal_seg2 = (   logical(Spinal_seg) );
        for ii = Kidney_Left_Axial_lowerBound_Slice_:Trachea_Axial_lowerBound_Slice
              if (verbose_)
               fprintf('-.');
               if (mod(ii,10)==0),  fprintf(num2str(ii));,  end
               if (mod(ii,80)==0) , disp('-.'), end
             end 
             Spinal_seg2(:, :, ii ) = imdilate(Spinal_seg2(:, :, ii ) , SE);
        end 
        % addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2')
        Aorta_BoundingBox(logical(~Spinal_seg2))=0;
          
       
    end
 
    
     %% segment_AORTA_stage2
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * 
    %       o Aorta_n_Spinal_BoundingBox
    %       o Aorta_ROI                     - Aorta_BoundingBox (as binary segmentaion )
    %        
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [AORTA_gross_seg] = segment_AORTA_stage2( orig_CT , Aorta_ROI )
         global CP  
         verbose_ = 1; 
        % STOP_HERE(); 
         orig_CT1 = orig_CT;
         orig_CT((~Aorta_ROI))=0;
         AORTA_gross_seg =  orig_CT; 
         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp')
         c_im_res2=simple_2D_to_3D.run(AORTA_gross_seg,'findCircleS',3); 
         c_im_res2_lr = largest_conncomp(c_im_res2);
         
          
         [a,b,c] =ind2sub(size(c_im_res2), find(c_im_res2_lr,1))
         iSeed = [a+6,b+6,c];
         dMaxDif = 20;  
         dImg = 20;
         
          img__ = ones(size(orig_CT)); 
          [img__ ] = CBIR_SPINAL_COLUMN_aux_func.extract_spinal_from_BonesSeg_and_Limits1( orig_CT1  ); 
       
          addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Segmentaion\FastRegionGrowing')
         lMask = RegionGrowing(img__, 12, iSeed);
         lMask1 = simple_3D_SEmorph.run(lMask,'imclose',3,'disk',5,0);
        im_230 = lMask1(:,:,230);
        im_230= logical(largest_conncomp(im_230));

        im_226 = lMask1(:,:,226);
        im_226= logical(largest_conncomp(im_226));

        % imagine(lMask1);
        in_im_226 = ~im_226;
        in_im_230 = ~im_230; 
        D_in_im_230 = bwdist(in_im_230,'euclidean');
        D_in_im_226 = bwdist(in_im_226,'euclidean');

       figure
           subplot(1,2,1), imagesc(D_in_im_230), 
           %title(' slice 226')
           subplot(1,2,2), imagesc(D_in_im_226),
           %title('slice 230')
           % hold on, imcontour(D_in_im_230)
          
           % hold on, imcontour(D_in_im_226)


        
        
         addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2c')
         c_im_res3=simple_2D_to_3D.run(lMask1,'findCircleS',3);
          imagine(lMask1);
         imagine(lMask, img__);
         %  imagine_v2(orig_CT , Aorta_ROI, 'aa');
         % c_im_res2=simple_2D_to_3D.run(lMask,'findCircleS',3); 
         % c_im_res2=simple_2D_to_3D.run(c_im_res2,'largest_conncomp',3); 
         orig_CT((~Aorta_ROI))=0;
         AORTA_gross_seg =  orig_CT;
         clear Aorta_n_Spinal_BoundingBox;
                             
         ix = PB_ip_shortcuts.get_image_idx( )
         Trachea_Axial_lowerBound_Slice         = CP.ip(ix).Trachea_Axial_lowerBound_Slice;
         Kidney_Left_Axial_lowerBound_Slice         = CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice;
         
         
        
         SE_13 = strel('disk',13,0);
         SE_10 = strel('disk',10,0); 
         SE_7 = strel('disk',7,0);
         SE_5 = strel('disk',5,0);
         
          for ii = Kidney_Left_Axial_lowerBound_Slice:Trachea_Axial_lowerBound_Slice
             if (verbose_)
               fprintf('**.');
               if (mod(ii,10)==0),  fprintf(num2str(ii));,  end
               if (mod(ii,80)==0) , disp('**.'), end
             end 
                
              cr = AORTA_gross_seg(:, :, ii );
              cr_open = imopen(cr, SE_13);
              AORTA_gross_seg(:, :, ii ) = cr_open;
                
              if ( (isempty(find(cr_open(:)>0))) )  
                   cr_open = imopen(cr, SE_10);
                    AORTA_gross_seg(:, :, ii ) = cr_open; 
                  addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
                %   STOP_HERE(); 
                  eee=1;
              end
%               % SE_13
%               % ~~~~~~~
%               if (any(cr_open))
%                   AORTA_gross_seg(:, :, ii ) = cr_open;
%               else 
%                   % SE_10
%                   % ~~~~~~~ 
%                   cr_open = imopen(cr, SE_10);
%                   if (any(cr_open(:)))
%                        AORTA_gross_seg(:, :, ii ) = cr_open;
%                        
%                   % SE_8
%                   % ~~~~~~~     
%                   else  
%                        cr_open = imopen(cr, SE_7); 
%                        AORTA_gross_seg(:, :, ii ) = cr_open;
% %                        if (any(cr_open(:)))
% %                             AORTA_gross_seg(:, :, ii ) = cr_open;
% %                        
% %                        % SE_6
% %                        % ~~~~~~~     
% %                        else 
% %                           cr_open = imopen(cr, SE_5);
% %                           AORTA_gross_seg(:, :, ii ) = cr_open;
% %                        end 
%                       %  AORTA_gross_seg(:, :, ii ) = cr_open;
%                   end
%               end
%           
         end  
       
       AORTA_gross_seg(AORTA_gross_seg<0)=0;
       AORTA_gross_seg = logical( AORTA_gross_seg);
        
        
    end
    
     end
end

