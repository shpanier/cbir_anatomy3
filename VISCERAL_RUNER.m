% function [] = VISCERAL_RUNER( file_as_URL , output_PATH , ConfigerID, RadID , to_exit , dry_run ) 
function [] = VISCERAL_RUNER( file_as_URL , output_PATH , ConfigerID, RadID , ...
    to_exit , dry_run ) 
% the image-name is needed as an index to save the configuration, for
% each image (in the PB mechanism....) this is the reason we need it as a
% global variabl..  
%     Changeing the "curr_image_name" variable efect the "ix"
%     varable & where param are store in the CP struct... 
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -     
[status,~] = system('python -h');
assert( status == 0  , 'assert ERROR -- please install anaconda2 + openCV and add it to your path varialbe')



global curr_image_name    
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -     
 
global forceRecalculation    
 
% defulat not forceing... 
forceRecalculation = 0;
  
disp(['VISCERAL_RUNER(' file_as_URL ',' output_PATH ',' ConfigerID ',' RadID ')']  )   ; 
addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE\');  
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%  Default values
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
if (~exist('modality_','var'))
       modality_ = 'ct';       
end  
 
if (~exist('isContrasted_','var'))
       isContrasted_ = 1;  
end      
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% to exit matlab at the end of this function ?
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
if (~exist('dry_run','var'))
        dry_run = 0;      
end  

if (~exist('to_exit','var'))
        to_exit = 1;      
end 
%STOP_HERE(); 
%%  
% % ConfigerID =1; 
% % RadID = 58;
% % output_PATH = 'd:/asdf/'; 
% % file_as_URL = 'http://visceralstorage1.blob.core.windows.net/testset/CT1.nii.gz?sr=c&si=readonly&sig=Z69O9Vz8TU0RxawtASpmpWZnT%2FhF2OgJOI7iEt60mis%3Dhttp://visceralstorage1.blob.core.windows.net/testset/MR1.nii.gz?sr=c&si=readonly&sig=Z69O9Vz8TU0RxawtASpmpWZnT%2FhF2OgJOI7iEt60mis%3D';
% % file_as_URL = 'http://visceralstorage1.blob.core.windows.net/trainingset/volumes/10000104_1_CTce_ThAb.nii.gz?sr=c&si=readonly&sig=M%2F9JXtDEmqtPDaEteJ3naYXQLCzeWUjLCwX4MHEsSko%3D'
    
      RadID 
      if (~exist('RadID','var'))
        exit      
      end
      
     isnumeric_ = is_str_numeric(RadID); 
      
     if (~isnumeric_)
       exit
     end  
      
    
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % make sure a there is a single filename 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    VISCERAL_SUBMIT_MODE = 1; 
    dbstop if error 
    %STOP_HERE()  
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%     % make sure a there is a single filename 
%     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     ptrn_ ='http';
     fs = strfind( file_as_URL, ptrn_ );  
     remote_file = 0;  
     if (length(fs)>=1)  
         remote_file =1; 
     end  
   
    file_as_URL_w_sak = file_as_URL; 
    
   
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %  remove the 'data key' (sak)
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    if (remote_file)
        ptrn2_= 'nii.gz';
        f_nii_gz = strfind( file_as_URL_w_sak, ptrn2_);  
        file_as_URL = file_as_URL_w_sak(1:f_nii_gz(1)+length(ptrn2_)-1);
    end  
  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %   PatientID_ModalityCounter_ModalityName_RegionName
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    f_slash = findstr('/', file_as_URL); 
    curr_image_name = ... 
        file_as_URL(f_slash(end)+1:end-7)

    %  STOP_HERE();    
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %   return for LMAnnotations  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    f3 = findstr('LMAnnotations', curr_image_name); 
    if ((f3)>1)  
        exit  
    end 
      
     
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %   return for _MRT  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    f3 = findstr('_MRT', curr_image_name);
    if ((f3)>1)  
        exit  
    end
     
        
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %   return for _wb  
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    f3 = findstr('_wb', curr_image_name);
    if ((f3)>1)  
        exit  
    end       
    
 %     STOP_HERE()  
    
    [path,name,ext]=fileparts(file_as_URL);
    if (str2num(  ConfigerID )~= 1) 
        exit 
    end 
    
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    ParticipantID = 'DSsO3u'; 
 
   
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %   My temp & RES  dir...
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    global VISCERAL_TMP
    if (isempty(VISCERAL_TMP))
         
        VISCERAL_TMP = [ 'd:\\VISCERAL_TMP\\' curr_image_name ]
    end 
    if (~exist(VISCERAL_TMP, 'dir')) 
        mkdir(VISCERAL_TMP);
    end
    RES_DIR = 'd:/Results';
    if (~exist(RES_DIR, 'dir')) 
        mkdir(RES_DIR);
    end 
   
    
    
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %   MY_VISCERAL_INPUT_FILENAME
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    MY_VISCERAL_INPUT_FILENAME = ... 
            [ VISCERAL_TMP , '/' ...
              curr_image_name , ...
              '.nii.gz'];

          
    %% return for cased
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *      
    % * *   return for cased
    % * *   
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    switch (str2num(RadID)) 
                      
        case {237, 480, 40358, 40357, 29193, 7578, 170, 33249, 33248, 332, 187 , 2473, 30325, 30324 },  
           exit  
      
    end
            
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %   FIT to MY (somewhat ridiculous) executename convention
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        [path_, file2load_ , ext_]=   fileparts(MY_VISCERAL_INPUT_FILENAME	);
        f__ = findstr('_', file2load_);  
        f_d = findstr('.', file2load_);  
        subjects_ids =    file2load_(1:f__(1)-1)   ;
        subjects_ids =   str2num(  subjects_ids) ;   
        image_sufix =  file2load_(f__(1):f_d(1)-1)    ;
        [path_, file2load , ext_]=   fileparts(MY_VISCERAL_INPUT_FILENAME	);
        based_on_file =  ['''' VISCERAL_TMP, '/', file2load ''''];
              
    %%  download the file....   
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %       download the file.... 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    if ~exist(MY_VISCERAL_INPUT_FILENAME(1:end-3),'file') 
        if (remote_file)
          disp(['===> Downloading ' file_as_URL_w_sak] );
          if ~dry_run, urlwrite(file_as_URL_w_sak, MY_VISCERAL_INPUT_FILENAME);, end
        else 
              disp(['===> copying ' file_as_URL_w_sak] );

              copyfile(file_as_URL_w_sak, MY_VISCERAL_INPUT_FILENAME);
        end 
 
        % unzip it 
        % unzip(zipfilename,outputdir)  
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        disp(['===> Gunzip ' MY_VISCERAL_INPUT_FILENAME ]);
        if ~dry_run, gunzip(MY_VISCERAL_INPUT_FILENAME,VISCERAL_TMP); end
    else
       disp(['===> Already exists: ' MY_VISCERAL_INPUT_FILENAME] ); 
    end
   
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    %    VISCERAL_SEG_OUTPUT_FILENAME
    %        (my script will zip the result)
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    VISCERAL_SEG_OUTPUT_FILENAME = ... 
            [ output_PATH    '\\' ...
              curr_image_name ...
              '_' num2str( RadID ) ...
              '_' ParticipantID ...    
               '_' num2str( ConfigerID ) ...
              '.nii.gz' ];
 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    CBIR_INIT     
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     
    
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * load the CBIR_ip_params_file PB  
    %     CP This Protocol buffer holds information about the image
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     
    global CBIR_ip_params_file CP  

    try % tries 2 load the PB file
        if (~exist(CBIR_ip_params_file,'file'))
            err = MException('CBIR_images_prms not exists'); 
            throw(err);
        end        
        fid = fopen(CBIR_ip_params_file);
        buffer = fread(fid, [1 inf], '*uint8');
        fclose(fid); 
        CP = pb_read_CBIR_images_prms(buffer);
        cprintf([1,0.5,0],['pb_read_CBIR_images_prms: loading file: ' CBIR_ip_params_file '\n']); 
    catch % init in the first time
        msg_ = ['  * * *  Delete: TMP_DATA_DIR/*.txt * * *  '];
         
        % If no paramter file exists, a recalculation is needed! 
        %    so, we delete all .mat .txt (hash paramter files...) 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
        cprintf([1,0,0],msg_ );  
        result = input('Are you sure ?') 
         
        delete([TMP_DATA_DIR '/*.mat']) 
        delete([TMP_DATA_DIR '/*.txt']) 
  

        CP = pb_read_CBIRImagesPrms();   
        cprintf([1,0.5,0],'pb_read_CBIR_images_prms: init a new file\n'); 
    end 

    ix = PB_ip_shortcuts.get_image_idx( );
    CP.ip(ix) = pblib_set(CP.ip(ix), 'modality',  modality_);  
    CP.ip(ix) = pblib_set(CP.ip(ix), 'isContrasted',  isContrasted_);  
 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *  1. Lungs & Teachea 2. HU_estimation 
    % * *           must run for every segmentaion.   
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % if (0)
            CBIR_INIT, 
                CBIR_LUNGS_n_TEACHEA_v2       
                CBIR_HU_estimation  
            CBIR_main_v8,
   %   end               
       
      
  
  
   
    %% THE MAIN SWITCH (RadID)
    %= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * *    
    % * *   the main switch (RadID)
    % * *   
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    switch (str2num(RadID))  
  
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        % 1. Trachea
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        case 1247,  
            disp('+++++++  Trachea ++++++++++'); 
            CBIR_INIT,      
                CBIR_TRACHEA2_V2
            CBIR_main_v8
            
            
            
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        % 2. left Lung
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        case 1326,    
            disp('+++++++  LEFT Lung 1326 ++++++++++');
            % ===================================
            % * * MY SWAP
            % ===================================
            CBIR_INIT,   
                CBIR_TRACHEA_n_BRONCHI_V1
                RIGHT_LUNG = 1;   
                CBIR_LUNGS2_V1    
            CBIR_main_v8
          
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        % 3. Right Lung
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        case 1302,    
            disp('+++++++  RIGHT Lung 1302 ++++++++++'); 
            % ===================================
            % * * MY SWAP
            % ===================================
            CBIR_INIT,  
                CBIR_TRACHEA_n_BRONCHI_V1 
                RIGHT_LUNG = 0;   
                CBIR_LUNGS2_V1 
            CBIR_main_v8
            
       
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        % 5. Spleen 
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        
        case 86,  
          
        %if (0)
              eee=1 
                CBIR_INIT,    
                    CBIR_SKELETON_V1      % extract bons skelation  
                    disp( curr_image_name )
                CBIR_main_v8  
                 
                CBIR_ScanSpecific  
                  
                CBIR_INIT,  disp('@@@@@@@ CBIR_SPINAL_COLUMN_V4');
                    CBIR_SPINAL_COLUMN_V4  
                CBIR_main_v8

                CBIR_INIT,   disp('@@@@@@@ CBIR_AORTA_v7');
                    CBIR_AORTA_v7    % Extract the ""aorta""  (-->now it is spinal strip).
                CBIR_main_v8   
           %end                         
                disp('@@@@@@@ CBIR_KIDNEYS_V5, MUSK_LEFT_ORGAN =0 ');
                CBIR_INIT,          
                    CBIR_KIDNEY_LEFT_V6  
                CBIR_main_v8    
     
       %  end       
                CBIR_INIT, disp('@@@@@@@ CBIR_SPLEEN_V3');
                    CBIR_SPLEEN_V5  
                CBIR_main_v8       

        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        % [ VISERAL right ]   6. my "left" kidneyi (is 29663, )  
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        case 29662,
            disp('+++++++  right kidney ++++++++++'); 
          
                
                CBIR_INIT,  
                    CBIR_SKELETON_V1      % extract bons skelation  
                CBIR_main_v8 
                CBIR_ScanSpecific
                
                
                CBIR_INIT,  disp('@@@@@@@ CBIR_SPINAL_COLUMN_V4');
                    CBIR_SPINAL_COLUMN_V4  
                CBIR_main_v8

                CBIR_INIT,   disp('@@@@@@@ CBIR_AORTA_v7');
                    CBIR_AORTA_v7    % Extract the ""aorta""  (-->now it is spinal strip).
                CBIR_main_v8   
%            end                           
                disp('@@@@@@@ CBIR_KIDNEYS_V5, MUSK_LEFT_ORGAN =0 ');
                CBIR_INIT,          
                    CBIR_KIDNEY_LEFT_V6   
                CBIR_main_v8 
                
                 CBIR_INIT, disp('@@@@@@@ CBIR_SPLEEN_V3');
                    CBIR_SPLEEN_V5  
                CBIR_main_v8      
  
%             end             
            %end  
                disp('@@@@@@@ CBIR_KIDNEY_RIGHT_V6, MUSK_LEFT_ORGAN =0 ');
                CBIR_INIT,  
                    CBIR_KIDNEY_RIGHT_V6     
                CBIR_main_v8

        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        % 7. left kidney 
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        case 29663,     
            disp('+++++++  left kidney ++++++++++'); 
              %  if (0)  
                CBIR_INIT, disp('@@@@@@@ CBIR_SKELETON_V1 ');
                    CBIR_INIT,  
                        CBIR_SKELETON_V1      % extract bons skelation  
                    CBIR_main_v8 
                    CBIR_ScanSpecific
                    
              %  end  
                CBIR_INIT,  disp('@@@@@@@ CBIR_SPINAL_COLUMN_V4');
                    CBIR_SPINAL_COLUMN_V4  
                CBIR_main_v8

                CBIR_INIT,   disp('@@@@@@@ CBIR_AORTA_v7');
                    CBIR_AORTA_v7    % Extract the ""aorta""  (-->now it is spinal strip).
                CBIR_main_v8  
                    
                
                disp('@@@@@@@ CBIR_KIDNEY_LEFT_V6, MUSK_LEFT_ORGAN =0 ');
                CBIR_INIT,     
                   CBIR_KIDNEY_LEFT_V6   
                CBIR_main_v8      

               
      
          
    
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        % 4. Liver  (58)
        % @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
        case 58, 
            disp('+++++++  Liver ++++++++++'); 
             
                 
             % if (0)     
                    CBIR_INIT,  
                        CBIR_SKELETON_V1      % extract bons skelation  
                    CBIR_main_v8 
                    CBIR_ScanSpecific
               %if (0)     
                    CBIR_INIT,  disp('@@@@@@@ CBIR_SPINAL_COLUMN_V4');
                        CBIR_SPINAL_COLUMN_V4  
                    CBIR_main_v8
               % if (0)
                    CBIR_INIT,   disp('@@@@@@@ CBIR_AORTA_v7');
                        CBIR_AORTA_v7    % Extract the ""aorta"" (-->now it is spinal strip).
                    CBIR_main_v8   
              %  end      
                    % KIDNEY_LEFT
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~      
                    disp('@@@@@@@ CBIR_KIDNEYS_V4, MUSK_LEFT_ORGAN =0 ');
                    CBIR_INIT,     
                        CBIR_KIDNEY_LEFT_V6  
                    CBIR_main_v8
                     
                    % KIDNEY_RIGHT_
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~      
                    disp('@@@@@@@ CBIR_KIDNEY_RIGHT_V4,  ');
                    CBIR_INIT, 
                        CBIR_KIDNEY_RIGHT_V6  
                    CBIR_main_v8
                   
                    % SPLEEN_ 
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~      
                    CBIR_INIT, disp('@@@@@@@ CBIR_SPLEEN_V3');
                        CBIR_SPLEEN_V5               % for gray level of the kidney 
                    CBIR_main_v8       
            %  end           
            
               % left lung, for hart spertor...
               % ~ ~ ~ ~ ~ ~ ~
               CBIR_INIT,  
                    CBIR_TRACHEA_n_BRONCHI_V1 
                    RIGHT_LUNG = 0;   
                    CBIR_LUNGS2_V1 
               CBIR_main_v8
                
                   
             CBIR_INIT, disp('@@@@@@@ CBIR_LIVER_V6');
                CBIR_LIVER_V6 
             CBIR_main_v8                 
    
             CBIR_INIT, disp('@@@@@@@ CBIR_LIVER_V6 PRE !!');
                CBIR_LIVER_V6_post   
             CBIR_main_v8   
      otherwise
            exit   
        
    end
       
    if (~dry_run), 
     try
        buffer = pblib_generic_serialize_to_string(CP);
        fid1 = fopen(CBIR_ip_params_file , 'w+');
        fwrite(fid1, buffer, 'uint8');
        fclose(fid1); 
     end
    end
    
    
    %%  zip it!
      
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % * * load the last file & SAVE THE RESUT
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    
     global last_saved_file_name 
     eval(['A = exist(''' output_PATH ''',''dir'');'])
       
    if   ( ~A )  
        eval(['mkdir(''' output_PATH ''')']);
    end
      
    %% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % Last sanity check.  
    % ~ ~ ~ ~ ~
    if (1) 
        addpath('C:\Dropbox\MY_CODE\lib\MATLAB\plots_n_figs\imagine_v2c')
       CK_ = load( [last_saved_file_name(2:end-1) '.mat'] ); 
        im_2_ck = CK_.mat2save;
       write_zeros = 0; 
       switch (str2num(RadID))  
  
        % 1. Trachea
        % @ @ @ @ @ 
        case 1247,    
            MIN_TRACHEA = 10000;     
            if(sum(im_2_ck(:)) < MIN_TRACHEA)
                write_zeros = 1; 
            end  
         % 2. left Lung
        % @ @ @ @ @ @ 
        case 1326,
            if(~any(im_2_ck(:)))
                write_zeros = 1; 
            end 
        % 3. Right Lung
        % @ @ @ @ @ @ @ 
        case 1302,
            if(~any(im_2_ck(:)))
                write_zeros = 1; 
            end
        % 4. Spleen 
        % @ @ @ @ @
        case 86,  
           MIN_SPLLEN = 100000;   
           if (sum(im_2_ck(:)) < MIN_SPLLEN)
                write_zeros = 1; 
           end  
        % 5. "left" kidneyi
        % @ @ @ @ @ @ @
        case 29662,
           MIN_KIDNEY = 100000;   
           if (sum(im_2_ck(:)) < MIN_KIDNEY)
                write_zeros = 1; 
           end   
           
        % 6. "right" kidney 
        % @ @ @ @ @ @ @ @
        case 29663,   
            MIN_KIDNEY = 100000;   
           if (sum(im_2_ck(:)) < MIN_KIDNEY)
                write_zeros = 1; 
           end  
             
        % 7. Liver  (58)
        % @ @ @ @ @ @ @ @
        case 58, 
           MIN_LIVER = 1000000;  
           if (sum(im_2_ck(:)) < MIN_LIVER)
                write_zeros = 1; 
           end  
            
       end
         
       if (write_zeros==1)
           im_2_ck = zeros(size(im_2_ck));
           addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\Nii')   
          
           save_untouch2_nii(based_on_file  , ...
                             im_2_ck                        , ...
                             last_saved_file_name(2:end-1)    );                                    
       end
    end 
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    %% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    % zip it !!    
    % ~ ~ ~ ~ ~
    %  ziped the 'last_saved_file_name'
    %       last_saved_file_name - is being set in 'save_untouch4_nii'.
    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~    disp(['===> copy it! ' VISCERAL_SEG_OUTPUT_FILENAME ]);
    ex_ = ['copyfile(' last_saved_file_name ',''' VISCERAL_SEG_OUTPUT_FILENAME ''')']
    disp(ex_)
    eval(ex_) 
     
    %disp(['===> zip it! ' VISCERAL_SEG_OUTPUT_FILENAME ]);
    %gzip(VISCERAL_SEG_OUTPUT_FILENAME);
    
    %eval(['delete (''' VISCERAL_SEG_OUTPUT_FILENAME ''')']) 

      
    if (to_exit==1) 
        try
            exit
        catch
            exit
        end
    end
end

  