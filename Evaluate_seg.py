import sys
sys.path.append(r'C:\build\VTK\bin\Release')
sys.path.append(r'C:\build\VTK\Wrapping\Python')
sys.path.append(r'C:\Dropbox\MY_CODE\lib\Python\PIMP')
sys.path.append(r'C:\Dropbox\MY_CODE\PHD\SpineSegmentation')
sys.path.append(r'C:\Dropbox\MY_CODE\lib\VTK')
sys.path.append(r'C:\Dropbox\MY_CODE\lib\Python\etc')
import imp_utils
import numpy as np
import morph_oper

def evaluate_seg( p_id ,RadID , measurement2use = "DICE"):

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # EvaluateSegmentation command
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    evaluate_seg_exe = "C:/Dropbox/MY_CODE/lib1/EvaluateSegmentation-master/EvaluateSegmentation-master/builds/Windows/EvaluateSegmentation.exe"
    post_evaluate_seg_exe = " -use " + measurement2use + " "
    # "D:\DATA\VISCERAL\manualsegmentations\10000110_1_CTce_ThAb_1247_.nii.gz"
    gt_ = "D:/DATA/VISCERAL/manualsegmentations/" + p_id + "_" +  RadID + "_.nii.gz"
    # "D:\seg_out\10000413_1_CTce_ThAb_1247_DSsO3u_1.nii.gz"
    my_seg_ = "D:/seg_out/"+ p_id + "_" +  RadID + "_DSsO3u_1.nii.gz"
    exe_ = evaluate_seg_exe + " " + gt_ + " " + my_seg_ + post_evaluate_seg_exe

    # print ( exe_ )
    import os
    out_res = os.popen(exe_).read()
    # print(out_res)


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Extract the measurement2use from the output
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    m_idx = out_res.find(measurement2use)
    idx_ = m_idx+len(measurement2use)+3

    str_res = p_id + ", " +RadID +" , "+ out_res[idx_:idx_+4]
    return str_res


# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
# MAIN
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
if __name__ == "__main__":
    sys.path.append("C:/Dropbox/MY_CODE/lib/Python/PIMP/")
    import nii_utils
    import copy
    # p_id = "10000100_1_CTce_ThAb"
    # p_id = "10000104_1_CTce_ThAb"
    # p_id = "10000106_1_CTce_ThAb"
    # p_id = "10000108_1_CTce_ThAb"
    # p_id = "10000110_1_CTce_ThAb"
    # p_id = "10000111_1_CTce_ThAb"
    # p_id = "10000112_1_CTce_ThAb"
    # p_id = "10000113_1_CTce_ThAb"
    p_id = "10000127_1_CTce_ThAb"
    # p_id = "10000129_1_CTce_ThAb"
    # p_id = "10000128_1_CTce_ThAb"
    #p_id = "10000415_1_CTce_ThAb"
    #p_id = "10000416_1_CTce_ThAb"

    # p_id = "10000110_1_CTce_ThAb"
    subjs = ["10000100_1_CTce_ThAb","10000104_1_CTce_ThAb","10000105_1_CTce_ThAb","10000106_1_CTce_ThAb","10000108_1_CTce_ThAb","10000109_1_CTce_ThAb","10000110_1_CTce_ThAb","10000111_1_CTce_ThAb","10000112_1_CTce_ThAb","10000113_1_CTce_ThAb","10000127_1_CTce_ThAb","10000128_1_CTce_ThAb","10000129_1_CTce_ThAb","10000130_1_CTce_ThAb","10000131_1_CTce_ThAb","10000132_1_CTce_ThAb","10000133_1_CTce_ThAb","10000134_1_CTce_ThAb","10000136_1_CTce_ThAb"]

    # ,"10000416_1_CTce_ThAb","10000417_1_CTce_ThAb","10000418_1_CTce_ThAb","10000419_1_CTce_ThAb","10000420_1_CTce_ThAb","10000422_1_CTce_ThAb","10000423_1_CTce_ThAb","10000424_1_CTce_ThAb","10000425_1_CTce_ThAb","10000427_1_CTce_ThAb","10000428_1_CTce_ThAb","10000429_1_CTce_ThAb","10000431_1_CTce_ThAb","10000135_1_CTce_ThAb","10000105_1_CTce_ThAb"]

    # subjs = ["10000015_1_CT_wb"]
    subjs = ["10000100_1_CTce_ThAb","10000104_1_CTce_ThAb","10000105_1_CTce_ThAb","10000106_1_CTce_ThAb","10000108_1_CTce_ThAb","10000109_1_CTce_ThAb","10000110_1_CTce_ThAb","10000111_1_CTce_ThAb","10000112_1_CTce_ThAb","10000113_1_CTce_ThAb","10000127_1_CTce_ThAb","10000128_1_CTce_ThAb","10000129_1_CTce_ThAb","10000130_1_CTce_ThAb","10000131_1_CTce_ThAb","10000132_1_CTce_ThAb","10000133_1_CTce_ThAb","10000134_1_CTce_ThAb","10000135_1_CTce_ThAb","10000136_1_CTce_ThAb"]
    # subjs = ["10000109_1_CTce_ThAb", "10000113_1_CTce_ThAb"]
    # RadID = '1247'   #  Trachea
    # RadID = '1326'  # left Lung
    # RadID = '1302'  # right  Lung
    #
    # RadID = '58'  # liver
    # RadID = '86'  # Spleen
    # RadID = '29662'  # right kidney
    RadID = '29663'  # left  kidney

    measurement2use = "DICE"
    use_pb = True

    str_res= ""
    for p_id in subjs:
        str_res1 = evaluate_seg( p_id ,RadID , measurement2use )
        print(str_res1)
        str_res = str_res + str_res1 + "\n"

    write_res2file = True
    if (write_res2file):
        res_file = "C:/Dropbox/MY_CODE/PHD/CBIR_anatomy3/VISCERAL_book_res.csv"
        f = open(res_file,'a')
        f.write(str_res)
        f.close()


