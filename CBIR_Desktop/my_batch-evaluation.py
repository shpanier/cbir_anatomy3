'''
Created on 21 nov. 2013
Last Update : 10 jan. 2014
@author: Roger Schaer & Oscar Jimenez
'''
import sys, argparse, string, os, platform

#
#    -i C:/Dropbox/MY_CODE/PHD/CBIR_anatomy3/CBIR_Desktop/my_isbi_ONE_FILE_NEW_train_URLs.txt -s d:\seg_out -r d:\resout -thd 1 -use all -p DSsO3u
# 
#
#

# Print program usage
def usage(args):
    print(args.inputlist , args.segmentationdir , args.results , args.threshold , args.metriclist , args.participant)
    print 'Usage: batch-evaluation.py -i <inputfile> -s <segmentationdir> -r <resultsdir> \
    -thd <threshold> -use <metriclist> -p <participantID>'

# Main function
def main(argv):

    # Parse arguments
    parser = argparse.ArgumentParser(description='Batch evaluation of files.')
    parser.add_argument('-i', '--inputlist', help='list of image URLs')
    parser.add_argument('-s', '--segmentationdir', help='output directory for segmentations')
    parser.add_argument('-r', '--results', help="directory with evaluation results")
    parser.add_argument('-thd', '--threshold', help="threshold to evaluate the segmentations")
    parser.add_argument('-use', '--metriclist', help="list of metrics to evaluate")
    parser.add_argument('-p', '--participant', help="unique ID of the participant (CASE-SENSITIVE)")
    parser.add_argument('-b', '--benchmark', help="ID of the benchmark")

    args = parser.parse_args()

    # Error in the arguments
    if not (args.inputlist and args.segmentationdir and args.results and args.threshold and args.metriclist and args.participant):
        usage(args);
        sys.exit(2) 


    # Shared access key
    sak = '?sr=c&si=readonly&sig=M%2F9JXtDEmqtPDaEteJ3naYXQLCzeWUjLCwX4MHEsSko%3D'
     
    # Anatomy2 key
    # ---------
    sak = '?sr=c&si=readonly&sig=m9Mcn6m1DuMXHhVYnDkzUS66ga3LlYAOA2%2FU5flycJo%3D'
    sak = r'?sr=c&si=readonly&sig=m9Mcn6m1DuMXHhVYnDkzUS66ga3LlYAOA2%2FU5flycJo%3D' 
    # ISBI key
    # -------------
    #sak = '?sr=c&si=readonly&sig=bS7kYI771u42afvGzyW2m9Wh0Q4POMkiHDmz3hbMvMY%3D'

   # Anatomy3 key
    # ---------
    sak = r'?sr=c&si=readonly&sig=ioBUV3Wx%2FEH8HVRg3GLXvNOCGr%2FZ%2BPWEETny33CSVWA%3D'
 
    #Container names  
    #segmentations = "manualsegmentations"
    segmentations = "segmentations"
    #segmentations = "Volumes"
    volumes = "volumes"
    #volumes = "segmentations" 
  
    #Landmark filename component
    landmarkInput = "LMAnnotations"
    landmarkOutput = "Landmarks"

    #Find participants' executable
    exename = "execute_standard" 
    evalexe = "EvaluateSegmentation"
    user = "azureuser"
    if os.path.exists("C:\\"):
        executablepath = "C:\\Users\\" + user + "\\Desktop\\" + exename
        evaluationToolPath = "C:\\EvaluateSegmentation\\" + evalexe
    else:
        executablepath = "/home/" + user + "/" + exename
        evaluationToolPath = "/home/" + user + "/metrics/build/" + evalexe

    # Find out all the Radlex IDs for each patient
    # Find out the volume path for each patient
    inputfile = open(args.inputlist, 'r')

    patientsRadlex = {}
    patientsVolume = {}

    # Go through the input list file and prepare the dicts
    for segTruth in inputfile:
        #Ignore landmarks (for now)
        if segTruth.find("LMAnnotations") == -1:
            # Figure out volume path
            index = 0
            occurrence = 1
            while index < len( segTruth ):
                index = segTruth.find( '_', index )
                if index == -1:
                    break
                if occurrence == 4: 
                    break
                index += 1 # +1 because len('_') == 1
                occurrence += 1
 
            # Fill the list of radlex terms for the patient
            radlex = segTruth.split("_")[-2]
            path, patient = os.path.split(segTruth.split("_")[0])
            if not `patient` in patientsRadlex.keys():
                patientsRadlex[`patient`] = []

            patientsRadlex[`patient`].append(radlex)

            # Assign volume path to patient
            volume = segTruth[:index].replace(segmentations, volumes) + ".nii.gz" + sak
            patientsVolume[`patient`] = volume

    # Go through all the configurations
    for configuration in range(1,2):

        # Go through the patients and perform the segmentation with the participant's executable
        for patient, radlexids in patientsRadlex.iteritems():
            # Create the space-separated radlex term list
            radlexstring = " ".join(radlexids)

            # Figure out volume path
            volumePath = patientsVolume[patient]

            # Here the segmentation should be done, based on a volume
            #print 'Calling : ' + executablepath + " -i \"" + volumePath + "\" -o " + args.segmentationdir + " -c " + `configuration` + " -r " + radlexstring
            os.system(executablepath + " -i \"" + volumePath + "\" -o " + args.segmentationdir + " -c " + `configuration` + " -r " + radlexstring)


        # Go through the patients and peform the landmark annotation with the participant's executable

        # File declaration (read the image list, prepare the output)
        inputfile = open(args.inputlist, 'r')

        # Perform the annotation
        for segTruth in inputfile:
            if segTruth.find("LMAnnotations") != -1:
                patient = segTruth.split("_")[0]
                patient = patient[patient.rfind("/") + 1:]
                volumePath = patientsVolume[`patient`]

                #print 'Calling : ' + executablepath + " -i \"" + volumePath + "\" -o " + args.segmentationdir + " -c " + `configuration` + " -m"
                os.system(executablepath + " -i \"" + volumePath + "\" -o " + args.segmentationdir + " -c " + `configuration` + " -m")

        inputfile.close()

        # File declaration (read the image list, prepare the output)
        inputfile = open(args.inputlist, 'r')

        # Go through all the images for evaluation
        for segTruth in inputfile:
            segTruth = segTruth.rstrip()
            segTruth = segTruth + sak

            # Figure out image name (without path)
            head, tail = os.path.split(segTruth)
            imagename = tail

            # Manage difference between landmarks and segmentations
            if segTruth.find("LMAnnotations") == -1 :
                extension = ".nii.gz"
                locparameter = ""
            else :
                extension = ".fcsv"
                imagename = imagename.replace(landmarkInput, landmarkOutput)
                locparameter = " -loc "

            # Compute segmentation output path
            configimagename = imagename[:imagename.rfind("_")] + "_" + args.participant + "_" + `configuration` + extension
            segOut = args.segmentationdir + configimagename
            xmlPath = args.results + configimagename[:-len(extension)] + ".xml"

            # Here the evaluation is performed with the output segmentation & ground truth (if the config exists)
            if os.path.exists(segOut):
                print "Evaluating : " + evaluationToolPath + locparameter + " \"" + segTruth + "\" " + segOut + " -xml " + args.results + configimagename[:-len(extension)] + ".xml" + " -use " + args.metriclist
                os.system(evaluationToolPath + locparameter + " \"" + segTruth + "\" " + segOut
                          + " -thd " + args.threshold
                          + " -xml " + xmlPath
                          + " -use " + args.metriclist
                          )
                print "Finished Evaluating " + segTruth

                #After the evaluation, upload the segmentation output & evaluation  results (XML)

                #payload = {'benchmark': args.benchmark}
                #SegOut
                #r = requests.post('http://faster.hevs.ch/VisceralDataUploadService/resources/data/upload', files={'file':(configimagename, open(segOut, 'rb'))}, params=payload)

                #EvalResult
                #r = requests.post('http://faster.hevs.ch/VisceralDataUploadService/resources/data/upload', files={'file':(configimagename[:-len(extension)] + ".xml", open(xmlPath, 'rb'))}, params=payload)

                #print "Finished Uploading " + segOut + " and " + xmlPath

    # Close input file
    inputfile.close()

# Run main function
if __name__ == '__main__':
    main(sys.argv[1:])

