classdef PB
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    methods(Static)
        
        % =================================================================   
        % Driver functions, (.... for installing in the gui .... )
        % =================================================================   

         function [  ] = new_Process(varargin)
            if (nargin < 4)
                error(' number if paramter should be at least 4 (path , name, Enabled, true/false)');
            end
            
            if (rem(nargin,2)) 
                error(' number if paramter should be multiple of 2');
            end
             
            
            % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
            %  minmum variables:
            %        varargin{1}: 'FUNC_DIR'          , varargin{2}: 'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\'  , ...
            %        varargin{3}: 'FUNC_NAME'         , varargin{4}: 'CBIR_LUNG_n_TRACHEA_aux_func.lung_markers', ...
            %        varargin{5}: 'Enabled'           , varargin{6}: 1 , ...
            %  -- function path
            % varargin{2} -- function name
            % varargin{4} -- 
            % varargin{4} --  true/false 
            % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
         
            % args check
            assert( strcmp(varargin{1} ,'FUNC_PATH')  ,  'new_Process: First argument should be function FUNC_PATH')
            assert( strcmp(varargin{3} ,'FUNC_NAME')  ,  'new_Process: Second argument should be function FUNC_NAME')
            assert( strcmp(varargin{5} ,'Enabled')    ,  'new_Process: Third argument should be function Enabled')
            assert((varargin{6}==1 || varargin{6}==0)  , 'new_Process: forth argument should be function 1 or 0')
            
            PB.add_im_Process(    varargin{4} , ... 
                                  varargin{2}  , ...
                                  varargin{6} );
                              
%   'INPUT_IMAGE_STEP' , ['c_im_res'  num2str(im_idx-6)] ,... % ['c_im'] ,  from where to take the input image... ( -1 == c_im)

            % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
            % from '7' and above are function paraamters...
            % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
            
            global im_idx              
            if (nargin>7) 
                for prm_idx=[7:2:nargin] 
                    key_ = varargin{prm_idx};
                    val_ = varargin{prm_idx+1};
                    % STOP_HERE() 
                    if (strcmp(key_ ,'INPUT_IMAGE_STEP'))
                        assert( ~isstr(val_) ,  'INPUT_IMAGE_STEP value should be a number.')
                        if (val_ == -1)
                           val_ = 'c_im'; % work on current image.
                        else  
                            val_ = ['c_im_res'  num2str(im_idx+val_)];
                        end
                    end
                  
                    
                    PB.add_im_Process_parm(key_ ,val_ );
                end
            end
         end


        
        
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * add_im_p
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        function [  ] = add_im_p( key_, val_)
                global C im_idx p_idx sv_idx;
             
                if (strcmp(key_,'NEW') && strcmp(val_,'NEW'))
                     im_idx = im_idx+1;
                     p_idx =0;
                     sv_idx =0;

                     C.im(im_idx) = pb_read_image_processing();

                     return
                 end

                 C.im(im_idx) = pblib_set(C.im(im_idx), key_, val_);
         
         end
        % -----------------------------------------------------------------
         
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * add_im_SV
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        function [  ] = add_im_SV(key_, val_)
             global C   im_idx sv_idx ;
             
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
             % Adding a new supportvolume record
             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
             if (strcmp(key_,'NEW') && strcmp(val_,'NEW'))
                  sv_idx = sv_idx +1;
                 
                  % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                  % the first record
                  if (sv_idx ==1)
                     C.im(im_idx) = ...
                        pblib_set(C.im(im_idx), 'SV', pb_read_volumeinfo());
                    
                  % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                  % from the second....
                  else
                     C.im(im_idx).SV(sv_idx) = ...
                                          pb_read_volumeinfo();
                  end
                  
                  return
             end
             
             
             C.im(im_idx).SV(sv_idx) = pblib_set(C.im(im_idx).SV(sv_idx), ...
                 key_, val_);
        end
        % -----------------------------------------------------------------

         
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * add_im_P_p
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        function [  ] = add_im_P_p(key_, val_)
             global C   im_idx p_idx prm_idx;
             
             if (strcmp(key_,'NEW') && strcmp(val_,'NEW'))
                  p_idx = p_idx +1;
                  prm_idx =0; 
                  
                  if (p_idx ==1)
                     C.im(im_idx) = ...
                        pblib_set(C.im(im_idx), 'P', pb_read_Processing());
                  else
                     C.im(im_idx).P(p_idx) = ...
                                          pb_read_Processing();
                  end
                  
                  return 
             end
             C.im(im_idx).P(p_idx) = pblib_set(C.im(im_idx).P(p_idx), ...
                 key_, val_);
        end
        % -----------------------------------------------------------------

        
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        function [  ] =add_im_Process( MethodName_ , MethodFilesLocation_ , Enabled_)
            if ~exist('Enabled_','var'), Enabled_ = 1; end 
            
            PB.add_im_P_p( 'NEW', 'NEW');
            PB.add_im_P_p( 'Enabled', Enabled_);
            PB.add_im_P_p( 'MethodName', MethodName_);
            PB.add_im_P_p( 'MethodFilesLocation', MethodFilesLocation_);
        end
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * add_im_P_Prm_p
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        function [  ] = add_im_P_Prm_p(key_, val_)
             global C im_idx p_idx prm_idx;
             if (strcmp(key_,'NEW') && strcmp(val_,'NEW'))
               %  prm_idx =0;
                 prm_idx = prm_idx+1;
                 if (prm_idx==1)
                    C.im(im_idx).P(p_idx) = ...
                        pblib_set(C.im(im_idx).P(p_idx), 'prms', ...
                        pb_read_Processing__Parameters());
                 else 
                     C.im(im_idx).P(p_idx).prms(prm_idx) = ...
                         pb_read_Processing__Parameters();
                 end
                    return
             end
             
             C.im(im_idx).P(p_idx).prms(prm_idx) = ...
                 pblib_set(C.im(im_idx).P(p_idx).prms(prm_idx), ...
                    key_, val_ );
           end
        % -----------------------------------------------------------------
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        function [  ] = add_im_Process_parm(prm_name_, prm_value_)
             PB.add_im_P_Prm_p( 'NEW', 'NEW' );
             PB.add_im_P_Prm_p( 'prm_name', prm_name_ );
             PB.add_im_P_Prm_p( 'prm_value', prm_value_ );
        end
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        
        %% get_hashCode_by_node_idx2
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * get_hashCode_by_node_idx2  
        %  Get:
        %   im_idx_ -  given index process.  
        %
        %  Return:
        %    1. hash_signature - hash signature of the given index process
        %    2. newest_dependent_time -
        %            Holds the date of the newest file in process. 
        %            (this will be used in case one of the files has changed. 
        %               Then we will recalucate the process all over again)        
        %
        %    3. sting_signature  -  
        %           String that holds the names of the functions and parameters
        %            (this will be used to make sure that the file is indeed 
        %                        contains the process that we seek to load)
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
         function [ hash_signature , newest_dependent_time , sting_signature] ...
                = get_hashCode_by_node_idx2( im_idx_)
           sting_signature_ = {};   
           newest_dependent_time = [0 0 0 0 0 0] ;
              
           global C  curr_image_name ; 
           debug_val = 0;             
 
           for ii=im_idx_:-1:1
                imageLoadFunc_ = C.im(ii).imageLoadFunc;   
                imageLoadFunc_ = strtrim(imageLoadFunc_); 

                if (length(imageLoadFunc_)>0)
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ~~~~~~~~~~~~~ retrive the original image name in the pipe-line
   
                            im_name = C.im(ii).imageName;
                            mat_file2load = [im_name(1:end-1) '.mat'''];
% % % % % % % % %  
% % % % % % % % %                     % im_name  
% % % % % % % % %                     C.im(im_idx_).imageName = im_name; 
% % % % % % % % %    

% identify the newest .nii file inside the 'hash'
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
 
                    addpath('C:\Dropbox\MY_CODE\lib\MATLAB\sys\FileTime');
                    if (exist( mat_file2load, 'file'))
                        eval(['curr_f_time = GetFileTime(' mat_file2load ' , [], ''Write'')'])                 %   break
                        if (any(curr_f_time > newest_dependent_time))
                             newest_dependent_time = curr_f_time; 
                        end 
                    end 
                end
           end  
           %KK{ind}K = {} ;  
            
           % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
           ind = 0;
           for jj=ii:im_idx_
               ind = ind +1;
                KK{ind} = C.im(jj);
       
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                % ~~~~~~~~~~~~~ remove unnecessary (not unique) fields.  
                KK{ind} = rmfield(KK{ind},{'documantaion', 'isSegmentation','forceRecalculation', 'unknown_fields' ,'imageLoadFuncPath' ...
                             'processDescription', 'has_field', 'DisplayResult', 'Enabled' ,'descriptor_function' } );
              %  KK{ind}.SV
              
              
                % save the process image name into sting_signature
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                im_ = strtrim(KK{ind}.imageName);
                if (~isempty(im_))
                    sting_signature_{end+1} = im_; 
                end 
                   
              if (debug_val)
                disp(['> ===================  IMAGE ================== : ' num2str(ind) ' out of ' num2str(im_idx_-ii+1) ]); 
                KK{ind}
                
                
                % sting_signature = [sting_signature , KK{ind}.imageName]
              end
              
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                % ~~~~~~~~~~~~~ NOT USED! 
                for sv_idx=[1:length( KK{ind}.SV )]
                 % remove unnecessary (not unique) fields.
                     KK{ind}.SV(sv_idx).unknown_fields ='';
                     KK{ind}.SV(sv_idx).descriptor_function ='';
                     KK{ind}.SV(sv_idx).has_field ='';

                end 
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
                % ~~~~~~~~~~~~~ clear not-unique P fields
                for p_idx=[1:length( KK{ind}.P )]
                 % remove unnecessary (not unique) fields.
                     KK{ind}.P(p_idx).MethodFilesLocation ='';
                     KK{ind}.P(p_idx).id =''; 
                     KK{ind}.P(p_idx).unknown_fields ='';
                     KK{ind}.P(p_idx).descriptor_function ='';
                     KK{ind}.P(p_idx).has_field ='';
                     
                     % save the process methos name into sting_signature
                     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                     mt_ = strtrim(KK{ind}.P(p_idx).MethodName);
                     if (~isempty(mt_))  
                        sting_signature_{end+1} = mt_;
                     end
                     
                     if (debug_val)
                        disp(['> ============================================ process: ' num2str(p_idx) ]); 
                        KK{ind}.P(p_idx)  
                         
                        
                        
                     end
                     
                    % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                    % ~~~~~~~~~~~~~ clear not-unique P.prms fields
                    for prm_idx=[1:length( KK{ind}.P(p_idx).prms )] 
                        KK{ind}.P(p_idx).prms(prm_idx).has_field ='';
                        KK{ind}.P(p_idx).prms(prm_idx).descriptor_function ='';
                        KK{ind}.P(p_idx).prms(prm_idx).unknown_fields ='';
                        if (strcmp(KK{ind}.P(p_idx).prms(prm_idx).prm_name, 'IMAGE_TAMPLATE') || ...
                            strcmp(KK{ind}.P(p_idx).prms(prm_idx).prm_name, 'INPUT_IMAGE_STEP') )
                            KK{ind}.P(p_idx).prms(prm_idx).prm_value =''; 
                        end
                         
                        % save the function paramters into sting_signature
                        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
                        pv_ = strtrim(KK{ind}.P(p_idx).prms(prm_idx).prm_value);
                        if (length(pv_)>0)   
%                             isnumeric_ = isnumeric(pv_)  
%                             if isnumeric_  
%                                 STOP_HERE() 
%                                 sting_signature_{end+1} = num2str(round(pv_)); 
%                             else 
                                sting_signature_{end+1} = pv_; 
%                             end
                            
                        end
                        
                        if (debug_val) 
                            disp(['         > ============== prm_idx: ' num2str(prm_idx) ]);
                            KK{ind}.P(p_idx).prms(prm_idx) 
                        end
                    end

                end
                
                
           end  
           KK{ind+1} = curr_image_name;
               
           if (debug_val) 
               disp('sting_signature') 
               sting_signature_   ;
               addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
                STOP_HERE();  
           end  
           addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\')  
           % hash_signature = DataHash(sting_signature); 
           sting_signature = strjoin(sting_signature_,'') ;     % from cell-2-string
           sting_signature = strrep(sting_signature,'''','') ; % remove '  
           %sting_signature = strrep(sting_signature,'~','')   % remove ~  
   
           sting_signature = strtrim(sting_signature);       % remove spaces  
           
             
           % 128 char long   
           Opt.Method = 'SHA-512';     
   
           addpath('C:\Dropbox\MY_CODE\lib\MATLAB\misc'); 
           hash_signature = DataHash_20120627(sting_signature, Opt);  
           
           %hash_signature = DataHash_20120627(sting_signature, Opt);
   
         end
         
           
          
          
%         %% 
%         % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%         % * * add_im_P_Prm_p 
%         % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%          function [ hk ] = get_load_files_by_node_idx2( im_idx_)
%              
%            global C  curr_image_name ; 
%            debug_val = 0 ;  
%            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%            % ~~~~~~~~~~~~~ retrive the original image name in the pipe-line
%            for ii=im_idx_:-1:1
%                 imageLoadFunc_ = C.im(ii).imageLoadFunc;   
%                 imageLoadFunc_ = strtrim(imageLoadFunc_); 
%                 if (length(imageLoadFunc_)>0)
%                     im_name = C.im(ii).imageName;
%                     im_name 
%                     C.im(im_idx_).imageName = im_name;
%                      break
%                 end
%            end 
%            %KK{ind}K = {} ;  
%             
%            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%            ind = 0;
%            for jj=ii:im_idx_
%                ind = ind +1;
%                 KK{ind} = C.im(jj);
%        
%                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%                 % ~~~~~~~~~~~~~ remove unnecessary (not unique) fields.  
%                 KK{ind} = rmfield(KK{ind},{'documantaion', 'isSegmentation','forceRecalculation', 'unknown_fields' ,'imageLoadFuncPath' ...
%                              'processDescription', 'has_field', 'DisplayResult', 'Enabled' ,'descriptor_function' } );
%               %  KK{ind}.SV
%               if (debug_val)
%                 disp(['> ===================  IMAGE ================== : ' num2str(ind) ' out of ' num2str(im_idx_-ii+1) ]); 
%                 KK{ind}
%               end
%                  % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%                 % ~~~~~~~~~~~~~ clear not-unique SV fields
%                 for sv_idx=[1:length( KK{ind}.SV )]
%                  % remove unnecessary (not unique) fields.
%                      KK{ind}.SV(sv_idx).unknown_fields ='';
%                      KK{ind}.SV(sv_idx).descriptor_function ='';
%                      KK{ind}.SV(sv_idx).has_field ='';
% 
%                 end
%                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%                 % ~~~~~~~~~~~~~ clear not-unique P fields
%                 for p_idx=[1:length( KK{ind}.P )]
%                  % remove unnecessary (not unique) fields.
%                      KK{ind}.P(p_idx).MethodFilesLocation ='';
%                      KK{ind}.P(p_idx).id ='';
%                      KK{ind}.P(p_idx).unknown_fields ='';
%                      KK{ind}.P(p_idx).descriptor_function ='';
%                      KK{ind}.P(p_idx).has_field ='';
% 
%                      if (debug_val)
%                         disp(['> ============================================ process: ' num2str(p_idx) ]); 
%                         KK{ind}.P(p_idx)
%                      end
%                      
%                     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%                     % ~~~~~~~~~~~~~ clear not-unique P.prms fields
%                     for prm_idx=[1:length( KK{ind}.P(p_idx).prms )]
%                         KK{ind}.P(p_idx).prms(prm_idx).has_field ='';
%                         KK{ind}.P(p_idx).prms(prm_idx).descriptor_function ='';
%                         KK{ind}.P(p_idx).prms(prm_idx).unknown_fields ='';
%                         if (strcmp(KK{ind}.P(p_idx).prms(prm_idx).prm_name, 'IMAGE_TAMPLATE'))
%                             KK{ind}.P(p_idx).prms(prm_idx).prm_value =''; 
%                         end
%                         
%                         if (debug_val)
%                             disp(['         > ============== prm_idx: ' num2str(prm_idx) ]); 
%                             KK{ind}.P(p_idx).prms(prm_idx)
%                         end
%                     end
% 
%                 end
%                 
%                 
%            end 
%            KK{ind+1} = curr_image_name;
%                
% % %            if (debug_val)
% % %                addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\STOP_HERE');
% % %                STOP_HERE();
% % %            end
%            addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\')
%            hk = DataHash(KK);
% 
%          end    
%          
    end
    
end


    %% get_hashCode_by_node_idx 
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        % * * add_im_P_Prm_p
        % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
%          function [ hk ] = get_hashCode_by_node_idx( im_idx_)
%            global C  ;
%              
%            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%            % ~~~~~~~~~~~~~ retrive the original image name in the pipe-line
%            for ii=im_idx_:-1:1
%                 imageLoadFunc_ = C.im(ii).imageLoadFunc;   
%                 imageLoadFunc_ = strtrim(imageLoadFunc_); 
%                 if (length(imageLoadFunc_)>0)
%                     im_name = C.im(ii).imageName;
%                     im_name
%                     C.im(im_idx_).imageName = im_name;
%                     break
%                 end
%            end   
%              
%             KK{ind} = C.im(im_idx_);
%                
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%             % ~~~~~~~~~~~~~ remove unnecessary (not unique) fields.
%             KK{ind} = rmfield(KK{ind},{ 'documantaion', 'isSegmentation','forceRecalculation', 'unknown_fields' ,'imageLoadFuncPath' ...
%                           'has_field', 'DisplayResult', 'Enabled' ,'descriptor_function' } );
%           
%             % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%             % ~~~~~~~~~~~~~ clear not-unique P fields
%             for p_idx=[1:length( KK{ind}.P )]
%              % remove unnecessary (not unique) fields.
%                  KK{ind}.P(p_idx).MethodFilesLocation ='';
%                  KK{ind}.P(p_idx).id ='';
%                  KK{ind}.P(p_idx).unknown_fields ='';
%                  KK{ind}.P(p_idx).descriptor_function ='';
%                  KK{ind}.P(p_idx).has_field ='';
% 
%                 % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
%                 % ~~~~~~~~~~~~~ clear not-unique P.prms fields
%                 for prm_idx=[1:length( KK{ind}.P(p_idx).prms )]
%                     KK{ind}.P(p_idx).prms(prm_idx).has_field ='';
%                     KK{ind}.P(p_idx).prms(prm_idx).descriptor_function ='';
%                     KK{ind}.P(p_idx).prms(prm_idx).unknown_fields ='';
%                     if (strcmp(KK{ind}.P(p_idx).prms(prm_idx).prm_name, 'IMAGE_TAMPLATE'))
%                         KK{ind}.P(p_idx).prms(prm_idx).prm_value =''; 
%                     end
%                %     KK{ind}.P(p_idx).prms(prm_idx)
%                 end
%                     
%             end
%           % KK{ind}
%             addpath('C:\Dropbox\MY_CODE\lib\MATLAB\debug\'); 
%             hk = DataHash(KK{ind});
% 
%          end
%           
    
