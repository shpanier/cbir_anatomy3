

%     %%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load volume: LUNGS_LEFT_
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
file_prefix = ['LIVER_PRE_SMOOTH_V5_'];  change_curr_image_name =0;  nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load , change_curr_image_name ) 
 
  
       
global CP  

ix = PB_ip_shortcuts.get_image_idx( );
% largest CC of liver (from CBIR_LIVER_aux_func.FIND_LIVER_SLICE_AS_SEED(
% - - - -  - - - - - - - - - - - - - - - - 
sliceNumber_as_seed_  = CP.ip(ix).Liver_Axial_in_Slice;
% - - - -  - - - - - - - - - - - - - - - -


upperBound_Slice_ =  CP.ip(ix).Liver_Axial_upperBound_Slice;  
lowerBound_Slice_ =  CP.ip(ix).Kidney_Left_Axial_lowerBound_Slice;

object_w_max_intersect_only =  1;  % <-- this is exatctly what we search for!
epsilon_f_segmentation_size = -1;  % not relevant in this case...
max_allow_thres =             -1;  % not relevant in this case...
dbg_ =0;     
SCS_type =                    4 ;  % <-- Consecutive slices with the largerest
                                 %     Intersect objects  

dbg_lt_slice = -1; dbg_gt_slice = -1;
% % % %  
% % % % %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
% % % % % smooth it...    
% % % % %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
PB.add_im_Process(    'CBIR_LEAK_REMOVAL_v1.Smooth_gross_Seg_WOct_Wslice_as_seed', ...
         'C:\Dropbox\MY_CODE\lib\MATLAB\imp\');      

     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ...     
                       ['c_im_res' num2str(im_idx)] );    % gross segment
     PB.add_im_Process_parm('slice_seed' , ...       
                      ['largest_conncomp(squeeze(c_im_res' num2str(im_idx) '(:,:,' num2str(sliceNumber_as_seed_) ' )))'] );                % slice_seed 
     PB.add_im_Process_parm('sliceNumber_as_seed' , ...
                      num2str(sliceNumber_as_seed_) );    % sliceNumber_as_seed  

     PB.add_im_Process_parm('to_upper' , ...       
                      num2str(upperBound_Slice_) );   % to_upper   
     PB.add_im_Process_parm('to_lower' , ...   
                      num2str(lowerBound_Slice_));    % to_lower 
     PB.add_im_Process_parm('max_allow_thres' , ...     
                      num2str(max_allow_thres));   % max_allow_thres             
     PB.add_im_Process_parm('object_w_max_intersect_only' , ...      
                        num2str(object_w_max_intersect_only));            % max_allow_thres             
     PB.add_im_Process_parm('SCS_type' , ...       
                            num2str(SCS_type));                            % SCS_type    
     PB.add_im_Process_parm('epsilon_f_segmentation_size' , ...       
                         num2str(epsilon_f_segmentation_size));     % stop cratria                
     PB.add_im_Process_parm('dbg_gt_slice' , ...      
                        num2str(dbg_gt_slice));                             % dbg_gt_slice             
     PB.add_im_Process_parm('dbg_lt_slice' , ...      
                        num2str(dbg_lt_slice));                             % dbg_lt_slice    

                             
  

 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it!! --  LIVER_V5_
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
out_file_prefix_ = ['LIVER_V5_'] ;     out_file_suffix = [];   out_dir = TMP_DIR;    
file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   ;
 
