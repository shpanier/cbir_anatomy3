
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load the CT volume
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = [];            nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, nii_prefix, file2load  ) 
%  PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ; 
PB_shortcuts.DISPLAY_RESULT_AT_THIS_POINT();  

% ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> load TRACHEA segmentation 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['LUNGS_n_TRACHEA_'];    nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
  
% ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
% ===> remove the  TRACHEA segmentation  
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
file_prefix = ['TRACHEA_n_BRONCHI_V1_'];    nii_prefix =  [];    
PB_shortcuts.load2_nii_image( TMP_DIR, file_prefix, file2load  ) 
PB.add_im_Process(  'simple_3D_SEmorph.run', ...
					  'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
		PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
		PB.add_im_Process_parm('oper_type' , '''imdilate''' ); 
		PB.add_im_Process_parm('dim_' , '3' );  
		PB.add_im_Process_parm('SE1' , '''disk''' ); 
		PB.add_im_Process_parm('SE2' , '4' );  
		PB.add_im_Process_parm('SE3' , '0' ); 
	 				 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
PB_shortcuts.set_new_image_index();  
PB.add_im_Process(    'LogicalAssignment', ...      
	 'C:\Dropbox\MY_CODE\lib\MATLAB\misc\');      
	 PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res'  num2str(im_idx-2)] );  
	 PB.add_im_Process_parm('B' ,              ['logical(c_im_res'  num2str(im_idx-1) ,')']);
	 PB.add_im_Process_parm('C' ,  '0') 
	 	 
       
%%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
%  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  DEBUG ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
PB_shortcuts.set_new_image_index();    
%PB_shortcuts.FORCE_RECALCULATION_FROM_HERE() ;  
%  function [lung_img] = lung_smooth_region_growing( lungs_BI_ROI ,  ...
%                                                       lung_LACSS    , ... 
%                                                       LACSS_number   ...
%                                                       )
% 
if (RIGHT_LUNG) 
	PB.add_im_Process(    'CBIR_LUNG_aux_func.right_lung_seed_slice', ' ');  
else 
	PB.add_im_Process(    'CBIR_LUNG_aux_func.left_lung_seed_slice',  ' ');       
end    
        PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im'] );    % LUNGS_n_TRACHEA_ segmentation   
 
PB.add_im_Process( 'CBIR_LUNG_aux_func.lung_smooth_region_growing', ' ' );
    PB.add_im_Process_parm('lungs_BI_ROI' , ['c_im_res'  num2str(im_idx-1)] );  % lungs_BI_ROI --
    PB.add_im_Process_parm('lung_LUCSS' , ['c_im_res'  num2str(im_idx)] );  % lung Start Slice 

      
       
PB_shortcuts.DISPLAY_RESULT_AS_SEGMENTATION_AT_THIS_POINT(); 
 
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
%    save it!      
% ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  
if (RIGHT_LUNG)
	out_file_prefix_ = 'LUNG_RIGHT2_V1_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
else
	out_file_prefix_ = 'LUNG_LEFT2_V1_';   out_file_suffix = [ ];  out_dir = TMP_DIR;      
end

file2save_t = PB_shortcuts.prepare_filename2save( file2load, out_dir , out_file_prefix_, out_file_suffix  ) ;
PB_shortcuts.save_untouch4_nii( based_on_file, file2save_t  )   
	
  