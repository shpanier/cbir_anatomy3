classdef CBIR_LUNG_n_TRACHEA_aux_func
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
     
     methods(Static)
 
     
       
%% extract_air_organs_from_body
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * 
    % * *  extract_air_organs_from_body
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  threshold_image_BW , imclose, imopen
    function extract_air_organs_from_body(enabled_)  
       if (~enabled_), return , end
 
        global im_idx  
            
            % C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\im_invert.m   
            % Processing , threshold_image_BW( -400 , 3000 )
            % ~~~~~~~~~~
            PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 1);
            PB.add_im_P_p( 'Enabled', 1);
            PB.add_im_P_p( 'MethodName', 'im_invert');
            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill\');
 
                % paramter indexes 
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value',  ['c_im'] );   % ['c_im_res' num2str(im_idx-1)] );
             
               
                  
                 
            % Processing
            % ~~~~~~~~~~ 
            PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 3);
            PB.add_im_P_p( 'Enabled', 1);
            PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
            PB.add_im_P_p( 'MethodFilesLocation', ...
                           'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
                       
                % paramter indexes  
                % ~~~~~~~~~~~~~~~~ 
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'prm1' );
                PB.add_im_P_Prm_p( 'prm_value', '''imclearborder''' );

                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                PB.add_im_P_Prm_p( 'prm_value', '3' );
 
       
             % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
             %  IF NOT MOTI's files
             %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
             %  In Moti's files the lung is't connested - so we don't want
             %  this largest_conncomp
             % ( Moti's files are abdomina CT that ended in the hart) 
             % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
            global curr_image_name ;  
            if (isempty(strfind(curr_image_name, 'Moti')))

                             
                 PB.add_im_Process(    'largest_conncomp', ...
                      'C:\Dropbox\MY_CODE\lib\MATLAB\imp\'); 
                     PB.add_im_Process_parm('IMAGE_TAMPLATE' , ['c_im_res' num2str(im_idx)] ); 
            end  
            
    
             
    end
%% extract_lung_from_air_n_trachea 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * 
    % * *  extract_lung_from_air_n_trachea
    % * * 
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    %  
        function extract_lung_from_air_n_trachea(enabled_) 
            if (~enabled_), return , end
            global im_idx 
         %      PB.add_im_p( 'forceRecalculation', 1)  
            PB.add_im_P_p( 'NEW', 'NEW')
            PB.add_im_P_p( 'id', 1);
            PB.add_im_P_p( 'Enabled', 1);

            PB.add_im_P_p( 'MethodName', 'CBIR_LUNG_n_TRACHEA_aux_func.extract_lung_from_air_n_trachea');
            PB.add_im_P_p( 'MethodFilesLocation', ... 
                           'C:\Dropbox\MY_CODE\PHD\CBIR_anatomy3\');
                % the trachea mask 
                % ~~~~~~~~~~~~~~~~
            PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx-2)] );

                % the "air" mask 
                % ~~~~~~~~~~~~~~~~
                PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx-1)] );


           
            PB.add_im_P_p( 'NEW', 'NEW')
                PB.add_im_P_p( 'id', 3);
                PB.add_im_P_p( 'Enabled', 1);
                PB.add_im_P_p( 'MethodName', 'simple_2D_to_3D.run');
                PB.add_im_P_p( 'MethodFilesLocation', ...
                               'C:\Dropbox\MY_CODE\lib\MATLAB\imp');
                    % paramter indexes  
                    % ~~~~~~~~~~~~~~~~
                    PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                    PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                    PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );

                    PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                    PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                    PB.add_im_P_Prm_p( 'prm_value', '''imfill'''  );

                    PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                    PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                    PB.add_im_P_Prm_p( 'prm_value', '3' );     

             PB.add_im_P_p( 'NEW', 'NEW')
                PB.add_im_P_p( 'id', 2);
                PB.add_im_P_p( 'Enabled', 1);
                PB.add_im_P_p( 'MethodName', 'simple_3D_SEmorph.run');
                PB.add_im_P_p( 'MethodFilesLocation', ...
                               'C:\Dropbox\MY_CODE\lib\MATLAB\imp\morph\');
                    % paramter indexes 
                    % ~~~~~~~~~~~~~~~~
                    PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                    PB.add_im_P_Prm_p( 'prm_name', 'IMAGE_TAMPLATE' );
                    PB.add_im_P_Prm_p( 'prm_value', ['c_im_res' num2str(im_idx)] );

                    PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                    PB.add_im_P_Prm_p( 'prm_name', 'edge_detection_type' );
                    PB.add_im_P_Prm_p( 'prm_value', '''imopen'''  );

                    PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                    PB.add_im_P_Prm_p( 'prm_name', 'dim_' );
                    PB.add_im_P_Prm_p( 'prm_value', '3' );

                    PB.add_im_P_Prm_p( 'NEW', 'NEW' );
                    PB.add_im_P_Prm_p( 'prm_name', 'strl_shape' );
                    PB.add_im_P_Prm_p( 'prm_value', '''disk''' );

                    PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                    PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                    PB.add_im_P_Prm_p( 'prm_value', '3' );

                     PB.add_im_P_Prm_p( 'NEW', 'NEW' ); 
                    PB.add_im_P_Prm_p( 'prm_name', 'strl_prm1' );
                    PB.add_im_P_Prm_p( 'prm_value', '0' );         
     
         
        end

        
   %% extract_trachea_from_air_organs
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    % * * extract_trachea_from_air_organs
    % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
    function [input_img] = extract_trachea_from_air_organs(input_img )
     global CP 
  
     ix = PB_ip_shortcuts.get_image_idx( );
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     %   Trachea_Axial_Start_Slice 
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     trachea_from = CP.ip(ix).Trachea_Axial_Start_Slice;
      
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     %   Trachea_Axial_Mid_Slice
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     trachea_to = CP.ip(ix).Trachea_Axial_Mid_Slice; 

     
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     %   Lung_Axial_Biggest_Slice 
     % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
     Lung_Axial_Biggest_Slice = CP.ip(ix).Lung_Axial_Biggest_Slice; 
      
     orig_input_img = input_img;
     input_img(:,:,trachea_from:end)=0;
     input_img(:,:,1:trachea_to)=0;
     input_img(:,:,1:Lung_Axial_Biggest_Slice)=0; 
     %input_img(:,:,trachea_to:trachea_from)= input_img(:,:,trachea_to:trachea_from)*2;
     verbose_=1; 
 
      %  
      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
      %  The main loop to extect the teachea.... 
      %
      %
      % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
      for sl=[trachea_from-1:-1:Lung_Axial_Biggest_Slice]
        if (verbose_)
           fprintf('.');
           if (mod(sl,10)==0)
                fprintf(num2str(sl));
           end
  
           if (mod(sl,80)==0)
               disp('.')
           end
        end  
         
        curr_sl = (squeeze(input_img(:,:,sl))); 
        next_sl = (squeeze(orig_input_img(:,:,sl-1)));
        CC_next_sl = bwconncomp(next_sl);
        num_of_obj =0;
         
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        % Loop throw each Connected-Components in the "next slice" 
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        for ii=[1:CC_next_sl.NumObjects]
            tmp_bw = zeros(size(input_img ,1), size(input_img ,2));
            tmp_bw(CC_next_sl.PixelIdxList{ii}) = 1;
            inter_curr_next = curr_sl & tmp_bw ;
            
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            % Check if the curent CC intersect with the previous slide.
            % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
            if (any(inter_curr_next(:))) 
                 
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   %   save the bifurcation location                
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   CP.ip(ix) = ... 
                       pblib_set(CP.ip(ix), 'Bronchi_Axial_End_Slice', sl);
      
   
                if (num_of_obj ==1)
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   % The bifurcation!
                   % if more the one object intersect!
                   %   --> more the one object interact == The bifurcation
                   %
                   %  In this case: return & set to zero all the following slices..  
                   % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                   input_img(:,:,1:sl )=0;
                   
                   
                   return 
                end 
                
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                % check if the "object" in the next slice is twice as big as 
                %  the "object" in the current slice
                % -> this is an indcation of LEAKAGE into another organ 
                %
                % In this case: keep only the intersect of the current &
                %               the next slice
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                if (sum(tmp_bw(:)) > 2*sum(curr_sl(:)))
                    input_img(:,:,sl-1) = inter_curr_next; 
                    num_of_obj =1;
                    continue 
                end
                  
                
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
                % If we got here: it's means the the intersect between the
                % two consequent is "reasonable" 
                %
                % In this case: keep only the overlapping/coincident object
                % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
             %     imagesc(tmp_bw)
                input_img(:,:,sl-1) = tmp_bw;
                num_of_obj =1;
            end 
        end
        
      end   

 
    eee=1;
    end
      

     %% lung_markers
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % * * lung_markers
     %          The markers that are being set  during this function are: 

     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     function [res] = lung_markers(lungs_n_trachea, dim_) 
        res = []; 
        if ~exist('dim_','var'), dim_= 3; end                           
        if ~exist('verbose_','var'), verbose_= 1; end
        %  STOP_HERE();
		addpath('C:\Dropbox\MY_CODE\lib\MATLAB\imp\CT_utill'); 
        lungs_n_trachea = CTpermute(lungs_n_trachea, 'forward', dim_);

        num_of_cc = zeros(1, size(lungs_n_trachea,1));
        num_of_pix = zeros(1, size(lungs_n_trachea,1));   
   
        %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
        %  Foreach axial slice: 
        %      (1) Count the number of connected components 
        %      (2) The size of each components 
        %  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
        for sl = 1:size(lungs_n_trachea,1)  
             if (verbose_) 
                fprintf('.'); 
                if (mod(sl,10)==0), fprintf(num2str(sl)); ,  end
                if (mod(sl,80)==0),  disp('.'),    end
             end   

             CC = bwconncomp(squeeze(lungs_n_trachea(sl,:,:)));
             numel_ans = cellfun(@numel,CC.PixelIdxList);
             mn  = length(numel_ans);
             if (mn>0)
                 num_of_cc(sl) = mn ;
                 num_of_pix(sl) =  sum(numel_ans(:));
             end

        end
          

        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        %   Lung_Axial_Biggest_Slice
        % ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
        global CP    
        ix = PB_ip_shortcuts.get_image_idx( ); 
         CP.ip(ix) = pblib_set(CP.ip(ix), 'Axial_upperBound_Slice', ...
                    size(lungs_n_trachea,1));  
        
  
        
        Lung_Axial_Biggest_Slice = find((num_of_pix==max(num_of_pix(:))),1, 'first')
        CP.ip(ix) = pblib_set(CP.ip(ix), 'Lung_Axial_Biggest_Slice', Lung_Axial_Biggest_Slice);
      
     end
  
     
  
     %% trachea_markers 2
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     % * * trachea_markers2 
     % The markers that are being set  during this function are:
     %     o Trachea_Axial_in_Slice
     %     o Trachea_Axial_in_Slice
     % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
     
     function [orig_ct] = trachea_markers2(orig_ct         , ...
                                                    lungs_n_trachea  ) 
            global CP 
            %  STOP_HERE();    
 
            ix = PB_ip_shortcuts.get_image_idx( );
 
            Trachea_Axial_in_Slice = -1;   
            Lung_Axial_Biggest_Slice =  CP.ip(ix).Lung_Axial_Biggest_Slice;

            for jj=[Lung_Axial_Biggest_Slice:size(orig_ct,3)]
                 jj  ;  
                 sl1 = lungs_n_trachea(:,:,jj);
                 % sl2 = input_img_lung2(:,:,jj);
                bwc = bwconncomp(sl1);
                 
                
                 %  The end of the Trache / 
                 %
                 %  Two condition must be met
                 %     1. less then 1000 pixels.
                 %     2. single connected comment
                 % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
                if ((sum(sl1(:)) < 1000) && (bwc.NumObjects==1)) 
                     Trachea_Axial_in_Slice = jj ;
                     break  ;    
                end    
            end         
   
            %  Trachea_Axial_in_Slice = Trachea_Axial_in_Slice+1;
 
            Trachea_Axial_in_sl_seg =    lungs_n_trachea(: , :,      Trachea_Axial_in_Slice  ); 
            Trachea_Axial_in_sl_ct =    orig_ct(: , :,      Trachea_Axial_in_Slice  ); 
               
        %   figure;imagesc(Trachea_Axial_in_sl_ct);
         %   STOP_HERE();
              
              
            Trachea_Axial_in_sl_ct(~Trachea_Axial_in_sl_seg)=0;
            to_hist_ = double(Trachea_Axial_in_sl_ct(Trachea_Axial_in_sl_ct<0));

            %   figure; imagesc(Trachea_Axial_in_sl_ct) 
            mean_ =         round(mean(to_hist_));
            std_ =         round(std(to_hist_));

            %         figure; hist(to_hist_ ,50);  
            %                 title(['mean: ' num2str(mean_) ', std: ' num2str(std_)]); 
            %         STOP_HERE();    

            CP.ip(ix) = ...    
                    pblib_set(CP.ip(ix), 'Trachea_GrayLevel_mean', mean_);  

            CP.ip(ix) = ...  
                    pblib_set(CP.ip(ix), 'Trachea_GrayLevel_std', std_ );  
 
            CP.ip(ix) = ...    
                    pblib_set(CP.ip(ix), 'Trachea_Axial_in_Slice', Trachea_Axial_in_Slice);  

             CP.ip(ix) = ...  
                    pblib_set(CP.ip(ix), 'Trachea_Axial_upperBound_Slice', size(orig_ct,3));  

            %       Bronchi_Axial_lowerBound_Slice =  CP.ip(ix).Lung_Axial_Biggest_Slice;

            CP.ip(ix) = ...
                    pblib_set(CP.ip(ix), 'Trachea_Axial_lowerBound_Slice', Lung_Axial_Biggest_Slice);  
    
              %     STOP_HERE() 

     end  
    

     end
end
    