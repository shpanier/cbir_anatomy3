﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NDesk.Options;
using System.Diagnostics;

namespace execute_standard
{
    class Program
    {
        static void Main(string[] args)
        {
            string volumePath = null;
            string segmentationdir = null;
            string configuration = null;
            string radlexstring = null;
            string currentParameter = "";
            Dictionary<string, List<string>> parameters = new Dictionary<string, List<string>>();
             
            string Exec_string ="" ;
                //  #print 'Calling : ' + executablepath + " -i \"" + volumePath + "\" -o " + args.segmentationdir + " -c " + `configuration` + " -r " + radlexstring
                var p = new OptionSet() {
                    { "i=", "volumePath.", 
                      (string v) => volumePath=v }, 
                    { "o=", "segmentation output dir",
                      (string v) => segmentationdir = v },
                    { "c=", "configuration id",  
                      (string v) => configuration = v },
                //     { "r=", "radlex string",
                  //     (v) => radlexstring = v },
                      { "r", "radlex string", v => currentParameter = "j" },
                      { "<>", v => {
                            
                        List<string> values;
                        if (parameters.TryGetValue(currentParameter, out values))
                        {
                            values.Add(v);
                        }
                        else
                        {
                            values = new List<string> { v };
                            parameters.Add(currentParameter, values);
                        }
                        }
                     }
                   
                };  
                List<string> extra;
                try
                {
                    extra = p.Parse(args);
                }
                catch (OptionException e)
                {
                    Console.Write("greet: ");
                    Console.WriteLine(e.Message);
                    return; 
                }  
                
                
               
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.FileName = "matlab.exe"; 
                // startInfo.WorkingDirectory = "C:/Dropbox/MY_CODE/PHD/CBIR_anatomy3/";
                startInfo.WorkingDirectory = "C:/BITBUCKET/cbir_anatomy3/";

           
                var currentPathVariable = Environment.GetEnvironmentVariable("path");
               // var newPathVariable = currentPathVariable + ";C:/Dropbox/MY_CODE/PHD/CBIR_anatomy3/";
                var newPathVariable = currentPathVariable + ";" + startInfo.WorkingDirectory;
             
            Environment.SetEnvironmentVariable("path", newPathVariable);



                //  Console.WriteLine("CMD: : " + volumePath);
                foreach (var parameter in parameters)
                {
                    // Console.WriteLine("Parameter: {0}", parameter.Key);
                    foreach (var value in parameter.Value)
                    {  
                         Exec_string = " -nodesktop  -nosplash -r \"VISCERAL_RUNER('"
                        //Exec_string = " -nodesktop -noFigureWindows -nosplash -r \"VISCERAL_RUNER('"
                            + volumePath + "' , '"
                            + segmentationdir + "' , '"
                            + configuration + "' , '"
                            + value + "' )\" -wait   ";  
                         
                        startInfo.Arguments = Exec_string;
                        Console.WriteLine("volumePath: " + Exec_string);
                        // Start the process with the info we specified.
                        // Call WaitForExit and then the using statement will close.
                         using (Process exeProcess = Process.Start(startInfo))
                         {    
                               exeProcess.WaitForExit();
                         }       
                    }
                }


            //  chdir C:\Dropbox\MY_CODE\PHD\CBIR  
            // Exec_string = " -nodesktop -nosplash -r \"VISCERAL_RUNER('" 
             

            

        }
    }
}
