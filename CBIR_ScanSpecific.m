% CBIR_ScanSpecific


ix = PB_ip_shortcuts.get_image_idx( )  
   
if ( (CP.ip(ix).GrayLevel_thr2 == -1) && strcmp(CP.ip(ix).prm104,'0'))
     
    % - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    % save the PB with the values we have so far....
    % - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    buffer = pblib_generic_serialize_to_string(CP);
    fid1 = fopen(CBIR_ip_params_file , 'w+');
    fwrite(fid1, buffer, 'uint8');
    fclose(fid1); 
   % STOP_HERE()  
    % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    % run the CBIR_ScanSpecific python script (this scripts also save the PB)
    % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    %%
    curr_image_name = CP.ip(ix).ImageName    
    % py = '"C:/Program Files/WinPython-64bit-2.7.9.5/python-2.7.9.amd64/python.exe"'
     
    [status,~] = system('python -h');
    assert( status == 0  , 'assert ERROR -- please install anaconda2 + openCV and addit to your path varialbe')
 
    global CODE_DIR
  
    prog = [ CODE_DIR '/CBIR_ScanSpecific.py'];
    assert( exist(prog,'file')==2  , 'Can not find CBIR_ScanSpecific.py');

    %cmd_ = [PY_EXEC ' ' prog ' --subj-id=1234'] 
        
    cmd_ = ['python ' prog ' --subj-id=' curr_image_name] 
   % 
    system(cmd_) 
    % STOP_HERE();
    %%
    %     
    % STOP_HERE()     
    % eee=1      
  
    % - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    % load back the PB with the updated ScanSpecific values
    % - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    fid = fopen(CBIR_ip_params_file);
    buffer = fread(fid, [1 inf], '*uint8');
    fclose(fid); 
    CP = pb_read_CBIR_images_prms(buffer);
     
    
end 
  
% ----- check values.
curr_image_name = CP.ip(ix).ImageName    
assert(~strcmp(CP.ip(ix).prm104,'0'),'assert, CBIR_ScanSpecific: (CP.ip(ix).prm104==0)')
    
    
