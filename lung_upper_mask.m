% lung_upper_mask
clear all
close all
 
%%
subj = 128

data2load = ['D:\MY_VISCERAL_TMP\LUNGS_n_TRACHEA_10000' num2str(subj) '_1_CTce_ThAb.nii.mat']
load(data2load);
num_of_slices_= size(mat2save,3);

reshape_data_ = reshape(permute(mat2save,[3 1 2]),num_of_slices_,[]);
reshape_data_sum_ = sum(reshape_data_,2);
figure;area(reshape_data_sum_)
title(num2str(subj))
 